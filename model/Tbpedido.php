<?php

/**
 * Description of Tbpedido
 *
 * @author eric
 */
class Tbpedido extends BaseDB {

    protected $tabela = "tbpedido";
    public $arrStatusPedido = array(
        "A" => "Ativo",
        "P" => "Pendente de entrega",
        "C" => "Cancelado",
        "F" => "Finalizado / Entregue"
    );

    public function save($data) {
        if (empty($data['dt_pedido'])) {
            $data['dt_pedido'] = date('Y-m-d');
        }

        /*
         * 1 dia para fechamento, ou seja para os produtores enviarem suas quantidades de entrega
         * depois é só alterar aqui
         */
        $data['dt_fechamento'] = date('Y-m-d', strtotime($data['dt_pedido'] . ' + 1 days'));
        return parent::save($data);
    }

    public function getPedidoByIdUsuario($id_usuario) {

        $stringSql = "SELECT date_format(p.dt_pedido,'%d/%m/%Y') as data_pedido,
                     p.st_pedido,p.id_pedido
                     FROM {$this->tabela} p 
                     WHERE p.id_usuario=? 
                     order by p.dt_pedido ASC
                     ";
        $dataUsuario = $this->getConection()->prepare($stringSql);
        $dataUsuario->bindParam(1, $id_usuario, PDO::PARAM_INT);
        $dataUsuario->execute();
        return $dataUsuario->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getStatusPedido($st_pedido) {
        if (isset($this->arrStatusPedido[$st_pedido])) {
            return $this->arrStatusPedido[$st_pedido];
        }
        return "";
    }

}
