<?php

/**
 * Description of Tbpedidoitem
 *
 * @author eric
 */
class Tbpedidoitem extends BaseDB {

    protected $tabela = "tbpedidoitem";

    public function getPedidoItemPorIdPedido($id_pedido) {

        $stringSql = "SELECT pi.*,p.tx_produto
                     FROM {$this->tabela} pi
                     INNER JOIN tbproduto p on p.id_produto = pi.id_produto
                     WHERE pi.id_pedido=? 
                   
                     ";
        $dataUsuario = $this->getConection()->prepare($stringSql);
        $dataUsuario->bindParam(1, $id_pedido, PDO::PARAM_INT);
        $dataUsuario->execute();
        return $dataUsuario->fetchAll(PDO::FETCH_ASSOC);
    }

}
