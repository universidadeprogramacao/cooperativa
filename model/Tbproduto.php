<?php

/**
 * Description of Tbproduto
 *
 * @author eric
 */
class Tbproduto extends BaseDB {
  
    protected $tabela = "tbproduto";
    
    public function getProduto($limit=null){
        $stringSql = "SELECT * FROM {$this->tabela} ";
        if(!empty($limit)){
            $stringSql.=" LIMIT {$limit}";
        }
        return $this->getConection()->query($stringSql)->fetchAll(PDO::FETCH_ASSOC);
    }
}
