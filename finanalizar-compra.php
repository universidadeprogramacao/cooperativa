<?php
session_start();
require_once './model/BaseDb.php';
require_once './model/Tbproduto.php';
require_once './model/Tbusuario.php';
require_once './model/Tbpedido.php';
require_once './model/Tbpedidoitem.php';
$_SESSION['usuario'] = array();


if (!empty($_POST['login'])) {
    $usuario = new Tbusuario();
    /**
      echo '<pre>';
      print_r($_POST);
      echo '</pre>';
     * 
     */
    $usuario->logar($_POST['login']);
    if (!empty($_SESSION['usuario'])) {
        header("Location: painel.php");
    }
}
?>

<!DOCTYPE html>
<!--[if lte IE 8]> <html class="oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="pt-br"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="format-detection" content="telephone=no">
        <title>Carrinho</title>
        <link rel="stylesheet" href="css/fancySelect.css" />
        <link rel="stylesheet" href="css/uniform.css" />
        <link rel="stylesheet" href="css/all.css" />
        <link media="screen" rel="stylesheet" type="text/css" href="css/screen.css" />
        <!--[if lt IE 9]>
                <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="js/fancySelect.js"></script>
        <script type="text/javascript" src="js/main.js"></script>

    </head>
    <body>
        <div id="wrapper">
            <div class="wrapper-holder">
                <?php
                include 'includes/cabecalhoTodasPaginas.php';
                ?>
                <section class="bar">
                    <div class="bar-frame">
                        <ul class="breadcrumbs">
                            <li><a href="index.php">Home</a></li>
                            <li>Finalizar compra</li>
                        </ul>
                    </div>
                </section>
                <section id="main">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="login-panel panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Login</h3>
                                </div>
                                <div class="panel-body">
                                    <form role="form" method="post" class="populate form-horizontal" id="validate" action="finanalizar-compra.php">
                                        <fieldset>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-xs-5 control-label">
                                                    Email</label>
                                                <div class="col-xs-10 col-sm-8">
                                                    <div class="input-group">
                                                        <input required="required" class="form-control validate[required,custom]]" id="login-email" name="login[email]" type="text" maxlength="60" autofocus>
                                                        <span class="input-group-addon">
                                                            <i class="fa fa-user"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-2 col-xs-5 control-label">Senha</label>
                                                <div class="col-xs-10 col-sm-8">
                                                    <div class="input-group">
                                                        <input required="required" class="form-control validate[required]" name="login[senha]" id="login-senha" type="password" maxlength="80">
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-lock"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <div  class="col-sm-4 col-xs-7 col-sm-offset-2">

                                                    <button style="width: 100%;" type="submit" id="logar" class="btn btn-success">
                                                        <span class="glyphicon glyphicon-send"></span>
                                                        Logar
                                                    </button>

                                                </div>


                                                <div class="col-sm-4 col-xs-7 col-sm-offset-0">

                                                    <a href="cadastrar.php" id="recuperar" class="btn btn-info">
                                                        <span class="glyphicon glyphicon-info-sign"></span>
                                                        Efetuar cadastro
                                                    </a>
                                                </div>


                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-9 col-xs-9 col-sm-offset-2">

                                                    <button style="width: 94%;" type="button" id="recuperarSenha" class="btn btn-adn">
                                                        <span class="glyphicon glyphicon-lock"></span>
                                                        Esqueci minha senha
                                                    </button>
                                                </div>
                                            </div>



                                        </fieldset>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php
            include 'includes/RodapesTodasAsPaginas.html';
            ?>
        </div>       
    </body>
</html>

<script>

    $(function () {
        /**
         $("#logar").click(function () {
         logarPainel($("#validate"));
         });
         **/

        $("#recuperarSenha").click(function () {
            var email = $("#login-email").val();
            if (email === '') {
                alert('Informa seu e-mail');
            } else {
                recuperarSenha(email);
            }
        });
    });

    function logarPainel(formulario) {

        $.ajax({
            url: formulario.attr('action'),
            type: 'POST',
            dataType: 'json',
            data: formulario.serialize(),
            success: function (data) {
                if (data.success !== undefined && data.success == 'S') {
                    window.location = "painel.php";
                } else {
                    alert('falha ao logar');
                }

            },
            error: function () {
                alert('falha ao logar');
            }
        });
    }

    function recuperarSenha(email) {

        $.ajax({
            url: 'admin/login_usuario/recuperarSenha',
            type: 'POST',
            dataType: 'json',
            data: {email: email},
            success: function (data) {
                if (data.success !== undefined && data.success !== '') {
                    alert(data.success);
                } else if (data.error !== undefined && data.error !== '') {
                    alert(data.error);
                } else {
                    alert('falha ao recuperar e-mail');
                }

            },
            error: function () {
                alert('falha ao recuperar e-mail');
            }
        });
    }
</script>