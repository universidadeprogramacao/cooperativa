<?php
session_start();
require_once './model/BaseDb.php';
require_once './model/Tbproduto.php';

$tbproduto = new Tbproduto();
$where = null;
if (!empty($_GET['id'])) {
    $id = intval($_GET['id']);
    $where = "id_produto = {$id}";
}
$listadeprodutos = $tbproduto->fetchAll($where);
/**
  echo '<pre>';
  print_r($listadeprodutos);
  echo '</pre>';
 * 
 */
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="format-detection" content="telephone=no">
    <title>Produtos</title>
    <link rel="stylesheet" href="css/fancySelect.css" />
    <link rel="stylesheet" href="css/uniform.css" />
    <link rel="stylesheet" href="css/all.css" />
    <link media="screen" rel="stylesheet" type="text/css" href="css/screen.css" />
    <!--[if lt IE 9]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
</head>
<body>
    <div id="wrapper">
        <div class="wrapper-holder">
            <?php
            include 'includes/cabecalhoTodasPaginas.php';
            ?>
            <section class="bar">
                <div class="bar-frame">
                    <ul class="breadcrumbs">
                        <li><a href="index.php">Home</a></li>
                        <li>Produtos</li>
                    </ul>
                </div>
            </section>
            <section id="main">

                <ul class="item-list">
                    <?php foreach ($listadeprodutos as $listaprodutos) { ?>
                        <li>
                            <div class = "item">
                                <div class = "image">
                                    <img style="width: 100px;height: 80px;" src = "uploads/produto/<?php echo $listaprodutos['tx_foto']; ?>" alt = "" />
                                    <div class = "hover">
                                        <div class = "item-content">
                                            <a href = "carrinho.php?id=<?php echo $listaprodutos['id_produto'] ?>&acao=add&qtd=1" class = "btn white normal">Adicionar ao Carrinho</a>
                                            <a href = "cadaProduto.php?id=<?php echo $listaprodutos['id_produto'] ?>" class = "btn white normal">Detalhes</a>
                                        </div>
                                        <span class = "bg"></span>
                                    </div>
                                </div>
                                <span class = "name"><?php echo $listaprodutos['tx_produto'] ?></span>
                                <span>R$<?php echo $listaprodutos['vl_unitario'] ?> /Unitário</span>
                            </div>
                        </li>   
                    <?php } ?> 
                </ul>
            </section>
        </div>
        <?php
        include 'includes/RodapesTodasAsPaginas.html';
        ?>
    </div>

    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="js/jquery.placeholder.js"></script>
    <script type="text/javascript" src="js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="js/fancySelect.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
</body>
</html>