<?php
session_start();
/**
  echo '<pre>';
  print_r($_SESSION);
  echo '</pre>';
  #die;
 * 
 */
if (empty($_SESSION['usuario'])) {
    header("Location: finanalizar-compra.php");
    exit;
}

require_once './model/BaseDb.php';
require_once './model/Tbproduto.php';
require_once './model/Tbpedido.php';
require_once './model/Tbpedidoitem.php';
$tbproduto = new Tbproduto();
$tbpedido = new Tbpedido();
$tbpedidoitem = new Tbpedidoitem();

if (empty($_SESSION['carrinho'])) {
    $_SESSION['total']['qtd'] = 0;
    $_SESSION['total']['valor'] = 0;
    $_SESSION['carrinho'] = array();
} else {
    $valorTotal = 0;
    $totalItens = 0;
    foreach ($_SESSION['carrinho'] as $produtos) {
        $valorTotal+=(float) $produtos['subtotal'];
        $totalItens++;
    }
    $_SESSION['total']['qtd'] = $totalItens;
    $_SESSION['total']['valor'] = $valorTotal;
}

if (!empty($_GET['acao'])) {
    if ($_GET['acao'] == 'finalizar' && !empty($_SESSION['carrinho'])) {

        $id_pedidoitem = null;

        $dataPedido = array();
        $dataPedido['id_usuario'] = $_SESSION['usuario']['id_usuario'];
        $dataPedido['dt_pedido'] = date('Y-m-d');
        $id_pedido = $tbpedido->save($dataPedido);
        foreach ($_SESSION['carrinho'] as $produtos) {
            $pedidoItem = array();
            $pedidoItem['id_pedido'] = $id_pedido;
            $pedidoItem['id_produto'] = $produtos['id_produto'];
            $pedidoItem['vl_unitario'] = $produtos['vl_unitario'];
            $pedidoItem['nr_quantidade'] = $produtos['qtd'];
            $pedidoItem['vl_subtotal'] = $pedidoItem['vl_unitario'] * $pedidoItem['nr_quantidade'];
            $id_pedidoitem = $tbpedidoitem->save($pedidoItem);
        }

        if (!empty($id_pedidoitem)) {
            $_SESSION['carrinho'] = array();
            $_SESSION['total']['qtd'] = 0;
            $_SESSION['total']['valor'] = 0;
        }
    }
}


/**
  echo '<pre>';
  print_r($_SESSION);
  echo '</pre>';
 * 
 */
?>
<!DOCTYPE html>
<!--[if lte IE 8]> <html class="oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="pt-br"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="format-detection" content="telephone=no">
        <title>Carrinho</title>
        <link rel="stylesheet" href="css/fancySelect.css" />
        <link rel="stylesheet" href="css/uniform.css" />
        <link rel="stylesheet" href="css/all.css" />
        <link media="screen" rel="stylesheet" type="text/css" href="css/screen.css" />
        <!--[if lt IE 9]>
                <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="js/fancySelect.js"></script>
        <script type="text/javascript" src="js/main.js"></script>

    </head>
    <body>
        <div id="wrapper">
            <div class="wrapper-holder">
                <?php
                include 'includes/cabecalhoTodasPaginas.php';
                ?>
                <section class="bar">
                    <div class="bar-frame">
                        <ul class="breadcrumbs">
                            <li><a href="index.php">Home</a></li>
                            <li>Painel - <?php echo $_SESSION['usuario']['tx_nome']; ?></li>
                        </ul>
                    </div>
                </section>
                <section id="main">

                    <?php
                    if (!empty($_GET['acao']) && $_GET['acao'] == 'finalizar') {
                        if (!empty($id_pedidoitem)) {
                            #$_SESSION['carrinho'] = array();
                            ?>

                            <div class="alert alert-success">
                                Pedido salvo com sucesso.
                            </div>
                            <?php
                        } else {
                            ?>

                            <div class="alert alert-danger">
                                Falha ao salvar pedido
                            </div>

                            <?php
                        }
                        ?>


                        <?php
                    }
                    ?>


                    <?php
                    if (!empty($_SESSION['carrinho'])) {
                        ?>

                        <div>
                            <p>

                                <a title="Confirmar Pedido" style="width: 250px;" class="btn btn-success" href="painel.php?acao=finalizar">
                                    <span class="glyphicon glyphicon-check"></span>
                                    Confirmar Pedido
                                </a>

                            </p>

                        </div>

                        <table class="table table-bordered">
                            <tr style="background-color: #CCC;">
                                <td style="width:10%;">
                                    Imagem
                                </td>
                                <td style="width: 40%;">
                                    Produto
                                </td>
                                <td style="width: 10%;">
                                    Vl. Unitário
                                </td>
                                <td style="width: 5%;">
                                    Qtd
                                </td>
                                <td style="width: 10%;">
                                    Sub Total
                                </td>
                            </tr>
                            <?php
                        }
                        ?>

                        <?php
                        $cor = "#CCC";
                        $total = 0;

                        foreach ($_SESSION['carrinho'] as $produtos) {
                            $total+=$produtos['subtotal'];


                            if ($cor == "#CCC") {
                                $cor = "#FFF";
                            } else {
                                $cor = "#CCC";
                            }
                            ?>

                            <tr style="background-color:<?php echo $cor; ?>;padding: 4px;margin: 5px;">
                                <td>
                                    <img style="width: 95%;height:80px;text-align: center;" src="uploads/produto/<?php echo $produtos['tx_foto'] ?>"  alt="" />
                                </td>
                                <td>
                                    <?php echo $produtos['tx_produto'] ?>
                                </td>
                                <td>
                                    <?php echo BaseDB::floatToMoneyStatic($produtos['vl_unitario']); ?>
                                </td>
                                <td>
                                    <?php echo $produtos['qtd']; ?>
                                </td>
                                <td>
                                    <?php echo BaseDB::floatToMoneyStatic($produtos['subtotal']); ?>
                                </td>

                            </tr>

                            <?php
                        }

                        if (!empty($_SESSION['carrinho'])) {
                            ?>

                            <tr style="background-color: #d7dbf2;">

                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    R$:
                                </td>
                                <td>
                                    <?php echo BaseDB::floatToMoneyStatic($total); ?>
                                </td>

                                <td>
                                    &nbsp;
                                </td>
                            </tr>

                            <?php
                        }
                        ?>

                    </table>

                    <div class="row">
                        <div class="col-sm-12">
                            <h2>Meus pedidos</h2>
                            <table class="table table-bordered" id="datatabless">
                                <thead>
                                    <tr style="background-color: #CCC;">
                                        <th style="width:5%;">
                                            Código
                                        </th>
                                        <th style="width: 10%;">
                                            Data pedido
                                        </th>
                                        <th style="width: 8%;">
                                            Status
                                        </th>
                                        <th style="width: 70%;">
                                            Produtos
                                        </th>
                                        <th style="width: 12%;">
                                            Total
                                        </th>

                                    </tr>
                                </thead>

                                <tbody>
                                    <?php
                                    $dataPedidos = $tbpedido->getPedidoByIdUsuario($_SESSION['usuario']['id_usuario']);

                                    if (!empty($dataPedidos)) {
                                        foreach ($dataPedidos as $pedido) {
                                            $valorTotalPedido = 0.00;
                                            $itensPedido = $tbpedidoitem->getPedidoItemPorIdPedido($pedido['id_pedido']);
                                            ?>

                                            <tr>
                                                <td><?php echo $pedido['id_pedido']; ?></td>
                                                <td><?php echo $pedido['data_pedido']; ?></td>
                                                <td><?php echo $tbpedido->getStatusPedido($pedido['st_pedido']); ?></td>
                                                <td>
                                                    <table>
                                                        <?php
                                                        foreach ($itensPedido as $item) {
                                                            $valorTotalPedido+=$item['vl_subtotal'];
                                                            ?>
                                                            <tr>
                                                                <td>
                                                                    <p><b>Prod: </b><?php echo $item['tx_produto']; ?></p>
                                                                    <p><b>Unitário: </b><?php echo $item['vl_unitario']; ?></p>
                                                                    <p><b>Qtd: </b><?php echo $item['nr_quantidade']; ?></p>
                                                                    <p><b>Sub Total: </b><?php echo BaseDB::floatToMoneyStatic($item['vl_subtotal']); ?></p>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                        ?>

                                                    </table>
                                                </td>
                                                <td>
                                                    <?php echo BaseDB::floatToMoneyStatic($valorTotalPedido); ?>
                                                </td>
                                            </tr>

                                            <?php
                                        }
                                    }
                                    ?>

                                </tbody>
                            </table>
                        </div>
                    </div>

                </section>
            </div>

            <?php
            include 'includes/RodapesTodasAsPaginas.html';
            ?>
        </div>       
    </body>
</html>

<script>
    $(function () {
        $("#datatabless").DataTable({
            
           "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.12/i18n/Portuguese-Brasil.json"
            }
            
        });
    });</script>

