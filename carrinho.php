<?php
#unset($_SESSION['carrinho']);
session_start();
require_once './model/BaseDb.php';
require_once './model/Tbproduto.php';


$tbproduto = new Tbproduto();

if (empty($_SESSION['carrinho'])) {
    $_SESSION['carrinho'] = array();
}

if (!empty($_GET['acao'])) {
    $acao = $_GET['acao'];
    $id = null;
    $qtd = null;

    if (!empty($_GET['id'])) {
        $id = intval($_GET['id']);
    }
    if(!empty($_GET['qtd'])){
        $qtd = intval($_GET['qtd']);
    }

    //adicionar
    if ($acao == 'add' && !empty($qtd)) {
        if (!empty($id)) {
            if (empty($_SESSION['carrinho'][$id])) {
                $produto = $tbproduto->findById($id);
                if (!empty($produto)) {
                    $_SESSION['carrinho'][$id] = $produto;
                    if (empty($_SESSION['carrinho'][$id]['qtd'])) {
                        $_SESSION['carrinho'][$id]['qtd'] = $qtd;
                    }

                    $_SESSION['carrinho'][$id]['subtotal'] = $_SESSION['carrinho'][$id]['qtd'] * $_SESSION['carrinho'][$id]['vl_unitario'];
                }
            } else {
                $_SESSION['carrinho'][$id]['qtd'] = $qtd;
                $_SESSION['carrinho'][$id]['subtotal'] = $_SESSION['carrinho'][$id]['qtd'] * $_SESSION['carrinho'][$id]['vl_unitario'];
            }
        }
    }

    //remover
    if ($acao == 'remove' && !empty($id)) {
        unset($_SESSION['carrinho'][$id]);
    }
}

if (empty($_SESSION['carrinho'])) {
    $_SESSION['total']['qtd'] = 0;
    $_SESSION['total']['valor'] = 0;
} else {
    $valorTotal = 0;
    $totalItens = 0;
    foreach ($_SESSION['carrinho'] as $produtos) {
        $valorTotal+=(float) $produtos['subtotal'];
        $totalItens++;
    }
     $_SESSION['total']['qtd']   = $totalItens;
     $_SESSION['total']['valor'] = $valorTotal;
}


/**
  echo '<pre>';
  print_r($_SESSION);
  echo '</pre>';
 * 
 */
?>
<!DOCTYPE html>
<!--[if lte IE 8]> <html class="oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="pt-br"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="format-detection" content="telephone=no">
        <title>Carrinho</title>
        <link rel="stylesheet" href="css/fancySelect.css" />
        <link rel="stylesheet" href="css/uniform.css" />
        <link rel="stylesheet" href="css/all.css" />
        <link media="screen" rel="stylesheet" type="text/css" href="css/screen.css" />
        <!--[if lt IE 9]>
                <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="js/fancySelect.js"></script>
        <script type="text/javascript" src="js/main.js"></script>

    </head>
    <body>
        <div id="wrapper">
            <div class="wrapper-holder">
                <?php
                include 'includes/cabecalhoTodasPaginas.php';
                ?>
                <section class="bar">
                    <div class="bar-frame">
                        <ul class="breadcrumbs">
                            <li><a href="index.php">Home</a></li>
                            <li>Carrinho</li>
                        </ul>
                    </div>
                </section>
                <section id="main">
                    <?php
                    if (empty($_SESSION['carrinho'])) {
                        ?>
                        <div>
                            <h2>Carrinho vazio!</h2>
                        </div>
                        <?php
                    } else {
                        ?>

                    <div>
                        <p>
                            <div class="btn-group">
                                <a title="Continuar comprando" style="width: 250px;margin-right: 5px;" class="btn btn-info" href="produtos.php">
                                    <span class="glyphicon glyphicon-shopping-cart"></span>
                                    Continuar comprando
                                </a>
                                
                                <a title=" Finalizar compra" style="width: 250px;" class="btn btn-success" href="finanalizar-compra.php">
                                    <span class="glyphicon glyphicon-check"></span>
                                    Finalizar compra
                                </a>
                            </div>
                    </p>
                            
                        </div>

                        <?php
                    }
                    ?>

                    <table class="table table-bordered">
                        <tr style="background-color: #CCC;">
                            <td style="width:10%;">
                                Imagem
                            </td>
                            <td style="width: 40%;">
                                Produto
                            </td>
                            <td style="width: 10%;">
                                Vl. Unitário
                            </td>
                            <td style="width: 5%;">
                                Qtd
                            </td>
                            <td style="width: 10%;">
                                Sub Total
                            </td>

                            <td style="width: 10%;">
                                Acao
                            </td>
                        </tr>

                        <?php
                        $cor = "#CCC";
                        $total = 0;

                        foreach ($_SESSION['carrinho'] as $produtos) {
                            $total+=$produtos['subtotal'];


                            if ($cor == "#CCC") {
                                $cor = "#FFF";
                            } else {
                                $cor = "#CCC";
                            }
                            ?>

                            <tr style="background-color:<?php echo $cor; ?>;padding: 4px;margin: 5px;">
                                <td>
                                    <img style="width: 95%;height:80px;text-align: center;" src="uploads/produto/<?php echo $produtos['tx_foto'] ?>"  alt="" />
                                </td>
                                <td>
                                    <?php echo $produtos['tx_produto'] ?>
                                </td>
                                <td>
                                    <?php echo BaseDB::floatToMoneyStatic($produtos['vl_unitario']); ?>
                                </td>
                                <td>
                                    <?php echo $produtos['qtd']; ?>
                                </td>
                                <td>
                                    <?php echo BaseDB::floatToMoneyStatic($produtos['subtotal']); ?>
                                </td>

                                <td>
                                    <p>
                                       
                                        <input title="Quantidade" onchange="recalcular('<?php echo $produtos['id_produto']; ?>',$(this).val());" type="number" name="qtd[<?php echo $produtos['id_produto']; ?>]" value="<?php echo $produtos['qtd']; ?>">
                                    </p>

                                    <p>
                                        <a title="remover" href="carrinho.php?id=<?php echo $produtos['id_produto']; ?>&acao=remove">
                                            <button class="btn btn-danger" type="button">
                                                <span class="glyphicon glyphicon-remove"></span>
                                            </button>
                                        </a>
                                    </p>
                                </td>
                            </tr>

                            <?php
                        }

                        if (!empty($_SESSION['carrinho'])) {
                            ?>

                            <tr style="background-color: #d7dbf2;">
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    &nbsp;
                                </td>
                                <td>
                                    R$:
                                </td>
                                <td>
                                    <?php echo BaseDB::floatToMoneyStatic($total); ?>
                                </td>

                                <td>
                                        <button class="btn btn-success" type="button" name="recalcular">
                                            <span class="fa fa-money"></span>
                                            Recalcular
                                        </button>
                                </td>
                            </tr>

                            <?php
                        }

                        ?>

                    </table>

                    <!--
                    <form action="#" class="form-payment">
                        <fieldset>
                            <div class="column">
                                <h2>Delivery address:</h2>
                                <div class="row">
                                    <label for="name">Name:</label>
                                    <input type="text" id="name" placeholder="Patrick Biggins" />
                                </div>
                                <div class="row">
                                    <label for="street">Street:</label>
                                    <input type="text" id="street" placeholder="Winkle" />
                                </div>
                                <div class="row">
                                    <label for="city">City:</label>
                                    <input type="text" id="city" placeholder="Detroit" />
                                </div>
                                <div class="row">
                                    <label for="phone">Phone:</label>
                                    <input type="text" id="phone" placeholder="(46) 527 526 763" />
                                </div>
                                <div class="row">
                                    <label for="email">Email:</label>
                                    <input type="text" id="email" placeholder="Patrick_biggie@hotmail.com" />
                                </div>
                            </div>
                            <div class="column column-add">
                                <h2>Payment method:</h2>
                                <ul class="pay-list">
                                    <li class="paypal">
                                        <div class="pay-holder">
                                            <p>Paypal</p>
                                        </div>
                                        <input type="radio" name="pay" value="paypal" />
                                    </li>
                                    <li class="mastercard">
                                        <div class="pay-holder">
                                            <p>MasterCard</p>
                                        </div>
                                        <input type="radio" name="pay" value="mastercard" />
                                    </li>
                                    <li class="visa">
                                        <div class="pay-holder">
                                            <p>MasterCard</p>
                                        </div>
                                        <input type="radio" name="pay" value="visa" />
                                    </li>
                                    <li class="express">
                                        <div class="pay-holder">
                                            <p>American Express</p>
                                        </div>
                                        <input type="radio" name="pay" value="american express" />
                                    </li>
                                    <li class="discover">
                                        <div class="pay-holder">
                                            <p>Discover</p>
                                        </div>
                                        <input type="radio" name="pay" value="discover" />
                                    </li>
                                </ul>
                                <div class="row row-subtotal">
                                    <h4 class="subtotal">Total to pay: <strong>$599.00</strong></h4>
                                    <input type="submit" class="btn black normal"  value="Make a payment" />
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    -->
                </section>
            </div>
            <?php
            include 'includes/RodapesTodasAsPaginas.html';
            ?>
        </div>       
    </body>
</html>

<script>
    function recalcular(id_produto,quantidade){
      window.location = "carrinho.php?acao=add&id="+id_produto+"&qtd="+quantidade;
    }
</script>