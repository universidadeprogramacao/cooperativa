<?php
session_start();
require_once 'model/BaseDb.php';
require_once 'model/Tbproduto.php';
?>
<!DOCTYPE html>
 <html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, 
              minimum-scale=1, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="format-detection" content="telephone=no">
        <title>SGCR</title>
        <link rel="stylesheet" href="css/fancySelect.css" />
        <link rel="stylesheet" href="css/uniform.css" />
        <link rel="stylesheet" href="css/all.css" />
        <link media="screen" rel="stylesheet" type="text/css" href="css/screen.css" />
        
        
        
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="js/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="js/fancySelect.js"></script>
        <script type="text/javascript" src="js/main.js"></script>

        
    </head>
    <body>
        <div id="wrapper">
            <div class="wrapper-holder">
                <?php
                include 'includes/cabecalhoTodasPaginas.php';
                ?>
               
               
                <section class="promo">
                    <ul class="slider">
                       
                        <li style="background: url(images/Alteradas/Slide1.jpg) no-repeat 50% 50%;">
                            <?php
                            include 'includes/ComplementoLiPaginaPrincipal.html';
                            ?>
                        </li>
                        <li style="background: url(images/Alteradas/Slide2.jpg) no-repeat 50% 50%;">
                            <?php
                            include 'includes/ComplementoLiPaginaPrincipal.html';
                            ?>                                            
                        </li>
                        <li style="background: url(images/Alteradas/Slide3.jpg) no-repeat 50% 50%;">
                            <?php
                            include 'includes/ComplementoLiPaginaPrincipal.html';
                            ?>
                        </li>
                    </ul>
                </section>
                
                
                
                <section id="main">
                    
                    
                    <div class="boxes">
                        
                    <?php
                    $tbproduto = new Tbproduto();
                    $dataProduto = $tbproduto->getProduto(3);
                    foreach ($dataProduto as $produto){
                    ?>
                        <div class="box">
                            <a href="produtos.php?id=<?php echo $produto['id_produto']; ?>">
                                <span class="bg"></span>                                                        
                                <img src="uploads/produto/<?php echo $produto['tx_foto']; ?>" alt="" style="width: 300px;height: 200px;" />
                                <div class="box-info">
                                    <div class="box-info-holder">
                                        <span class="title"><span><?php echo $produto['tx_produto']; ?></span></span>
                                      
                                    </div>
                                </div>
                            </a>
                        </div>
                        <?php
                    }
                        ?>
                        
                        <!--
                        <div class="box">
                            <a href="#">
                                <span class="bg"></span>
                                <img src="images/img-02.jpg" alt="" />
                                <div class="box-info">
                                    <div class="box-info-holder">
                                        <span class="title"><span>Complementos</span></span>
                                        <h2>Complete seu arranjo</h2>
                                        <span class="btn white normal">Deixe-o mais bonito</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        
                        <div class="box">
                            <a href="#">
                                <span class="bg"></span>
                                <img src="images/img-03.jpg" alt="" />
                                <div class="box-info">
                                    <div class="box-info-holder">
                                        <span class="title"><span>Outros</span></span>
                                        <h2>Veja mais produtos</h2>
                                        <span class="btn white normal">Sempre a sua escolha</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        -->
                        
                    </div>
                </section>
            </div>
            <?php
                include 'includes/RodapesTodasAsPaginas.html';
            ?>
        </div>
    </body>
</html>
