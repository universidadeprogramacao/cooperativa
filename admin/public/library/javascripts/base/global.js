/** altere o código depois acesse http://jscompress.com/ para comprimir o código e colocar no arquivo */
$(function () {
    /* inicia as configuracao do js */
    //_initLayout();
    _populateFormPhp();
    _returnSubmitPhp();
    _initForm();
    _initMask();
    //_initTinymce();
    //_iniTabs();
    //_initAjaxErrorDefault();
    //_initChangeColor();
    //_initFixedActionBar();
    //_initPermissoesCadastrarModal();
    _initPhpJs();
    _initDataTable();
    if (_Module == 'admin') {
        _initTooltip();
    }

});

function _initDataTable() {
    $("#dataTable").dataTable({
        "oLanguage": {
            "sProcessing": "Processando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "Não foram encontrados resultados",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando de 0 até 0 de 0 registros",
            "sInfoFiltered": "",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "Primeiro",
                "sPrevious": "Anterior",
                "sNext": "Seguinte",
                "sLast": "Último"
            }

        }
    });
}

function _initLayout() {

    if (_Module == 'admin') {
        _initEscondeBotaoSalvarTopo();
        _initEscondeBotaoNovoTopo();
        _initShowAllBotoesTopo();
    }
}

function _initEscondeBotaoSalvarTopo() {
    /**
     $('.escondeBotaoSalvarTopo').click(function () {
     $('#btnSalvar').hide();
     });
     
     $('.escondeBotaoSalvarTopo').keyup(function (event) {
     if (event.which == 13) {
     $('#btnSalvar').hide();
     }
     });
     **/
}


function _initEscondeBotaoNovoTopo() {
    /**
     $('.escondeBotaoNovoTopo').click(function () {
     $('#btnNovo').hide();
     });
     
     $('.escondeBotaoNovoTopo').keyup(function (event) {
     if (event.which == 13) {
     $('#btnNovo').hide();
     }
     });
     **/
}

function _initShowAllBotoesTopo() {
    /**
     $('.showAllBotoesTopo').click(function () {
     $('#btnNovo').show();
     $('#btnSalvar').show();
     });
     **/
}

function _initTooltip() {
    $('[data-toggle="tooltip"]').tooltip();
    $('a, input, button, label, span').each(function () {
        if ($.trim($(this).attr('title')) != '') {

            direction = $.trim($(this).attr('placement'));
            if (direction == '') {
                direction = 'bottom';
            }

            $(this).tooltip({
                placement: direction
            });
        }
    });
}

function _initPhpJs() {
//STRING


}

function _configHelp() {

    $('.btnHelp').each(function () {
        if ($.trim($(this).attr('title')) == '') {
            $(this).attr('title', 'Sobre esse item...');
        }
    });


    if (_Module == 'admin') {
        $('.btnHelp').cluetip({
            hoverClass: 'highlight',
            sticky: true,
            closePosition: 'title',
            arrows: true,
            closeText: '<img src="' + _baseUrl + '/public/admin/images/close_icon.png" title="Fechar" />'
        });
    }
}

function limpaSessaoIsModal() {
    //Ao fechar a janela limpar a sessão que guarda se está na modal
    $.ajax({
        url: _baseUrl + '/default/ajax/clear-session-is-modal'
    });
}

/** Faz com que a actionbar de botoes fique sempre no top da tela */
distancia_topo = null;
heightActionBar = null;
function _initFixedActionBar() {
    /*
     if ($("#localizacao").length > 0) {
     if ($("#message-action-bar").length > 0) {
     retornaStaticMenuAction();
     
     distancia_topo = $("#message-action-bar").offset().top;
     heightActionBar = $("#message-action-bar").height();
     
     $(window).scroll(function() {
     calculaPosicaoScrollMenuAction();
     });
     
     //Outra coisa importante, é imaginar que o usuário pode redimensionar a janela a 
     //qualquer tempo, e quando isso acontecer, você deve recalcular a altura da janela:
     $(window).unbind("resize");
     $(window).resize(function() {
     retornaStaticMenuAction();
     _initFixedActionBar();//chama a funcao para reiniar os calculos
     });
     
     calculaPosicaoScrollMenuAction();
     }
     }
     */
}

function retornaStaticMenuAction() {
    $("#message-action-bar").css('position', 'static');
    $("#acesso-rapido").css('position', 'absolute');
    $("#acesso-recente").css('position', 'absolute');
    $("#busca-menu").css('position', 'absolute');
    $("#message-action-bar").css('top', '');
    $("#message-action-bar").css('left', '');
    $('#div-form').css('margin-top', '');
    $("#message-action-bar").css('border-bottom', '2px');
    $("#message-action-bar").css('margin-top', '8px');
    $("#message-action-bar").css('margin-left', '12px');
    $("#message-action-bar").css('margin-right', '12px');
    $("#message-action-bar").css('width', '');

}

function calculaPosicaoScrollMenuAction() {
    //Para saber a que distância do topo está determinado elemento, use o seguinte comando:		
    posicao_scroll = $(document).scrollTop();
    //$('#action-bar').append('<label style="color:transparent">' + distancia_topo + ':' + $(document).scrollTop() + '</label>');
    if (posicao_scroll >= distancia_topo) {
        $("#message-action-bar").css('position', 'fixed');
        $("#acesso-rapido").css('position', 'fixed');
        $("#acesso-recente").css('position', 'fixed');
        $("#busca-menu").css('position', 'fixed');
        $("#message-action-bar").css('width', '98%');
        $("#message-action-bar").css('top', '0px');
        $("#message-action-bar").css('left', '0px');
        $('#div-form').css('margin-top', heightActionBar + 'px');
        $("#message-action-bar").css('border-bottom', '1px solid #CCC');
        $("#message-action-bar").css('z-index', '99');
        $("#acesso-rapido").css('z-index', '99');
        $("#acesso-recente").css('z-index', '99');
        $("#busca-menu").css('z-index', '99');
    } else {
        retornaStaticMenuAction();
    }
}


function _initChangeColor() {
    $('.changeColor').mousemove(function () {
        $(this).css('background-color', 'rgb(210,226,188)');
    });

    $('.changeColor').mouseout(function () {
        $(this).css('background-color', '');
    });
}

function addButtonModal(objAfter, strClass, title, strClick) {

    id = 'group-add-on-' + $(objAfter).attr('id');

    if ($('#' + id).length == 0) {
        html = '<span class="input-group-addon btnCadastrarModal ' + strClass + '" id="' + id + '">'
                + '<a title="' + title + '" onclick="' + strClick + '">'
                + '<span class="glyphicon glyphicon-new-window"></span>'
                + '</a>'
                + '</span>';

        //alert(html)

        //$(this).after('<input type="button" class="btnCadastrarModal btnModalTipoMidiaMarketing" title="Cadastrar Novo Tipo de Mídia de Marketing" onclick="openModalTipoMidiaMarketing()">');

        $(objAfter).parent().addClass('input-group').css('padding-left', '15px');
        $(objAfter).after(html);
        _initTooltip();
    }
}

function _initPermissoesCadastrarModal() {
    //desabilita os botoes de cadastro de modal caso o usuario logado não possua permissao
    try {
        if (_Module == 'admin') {

            //se possui permissao adiciona o botão de cadastrar modal
            if (_permiteCadastroTipoProduto == 'S') {
                //remove todos os botoes para inserir novamente	
                $('.btnModalTipoProduto').remove();
                $('.addBtnCadastrarModalTipoProduto').each(function () {
                    objAfter = $(this);
                    strClass = 'btnModalTipoProduto';
                    title = 'Cadastrar Novo Tipo de Produto (Categoria)';
                    strClick = 'openModalTipoProduto()';
                    addButtonModal(objAfter, strClass, title, strClick);
                });
            }


            //se possui permissao adiciona o botão de cadastrar modal
            if (_permiteCadastroUnidadeMedidaProduto == 'S') {
                //remove todos os botoes para inserir novamente	
                $('.btnModalUnidadeMedidaProduto').remove();
                $('.addBtnCadastrarModalUnidadeMedida').each(function () {
                    objAfter = $(this);
                    strClass = 'btnModalUnidadeMedidaProduto';
                    title = 'Cadastrar Nova Unidade de Medida';
                    strClick = 'openModalUnidadeMedida()';
                    addButtonModal(objAfter, strClass, title, strClick);
                });
            }

            //se possui permissao adiciona o botão de cadastrar modal
            if (_permiteCadastroMarca == 'S') {
                //remove todos os botoes para inserir novamente	
                $('.btnModalMarca').remove();
                $('.addBtnCadastrarModalMarca').each(function () {
                    objAfter = $(this);
                    strClass = 'btnModalMarca';
                    title = 'Cadastrar Nova Marca';
                    strClick = 'openModalMarcaProduto()';
                    addButtonModal(objAfter, strClass, title, strClick);
                });
            }

            if (_permiteCadastroModelo == 'S') {
                //remove todos os botoes para inserir novamente	
                $('.btnModalModelo').remove();
                $('.addBtnCadastrarModalModelo').each(function () {
                    objAfter = $(this);
                    strClass = 'btnModalModelo';
                    title = 'Cadastrar Novo Modelo';
                    strClick = 'openModalModeloProduto()';
                    addButtonModal(objAfter, strClass, title, strClick);
                });
            }

            if (_permiteCadastroFamilia == 'S') {
                //remove todos os botoes para inserir novamente	
                $('.btnModalFamilia').remove();
                $('.addBtnCadastrarModalFamilia').each(function () {
                    objAfter = $(this);
                    strClass = 'btnModalFamilia';
                    title = 'Cadastrar Nova Família';
                    strClick = 'openModalFamiliaProduto()';
                    addButtonModal(objAfter, strClass, title, strClick);
                });
            }


            if (_permiteCadastroTipoMidiaMarketing == 'S') {
                //remove todos os botoes para inserir novamente	
                $('.btnModalTipoMidiaMarketing').remove();
                $('.addBtnCadastrarModalTipoMidiaMarketing').each(function () {
                    objAfter = $(this);
                    strClass = 'btnModalTipoMidiaMarketing';
                    title = 'Cadastrar Novo Tipo de Mídia';
                    strClick = 'openModalTipoMidiaMarketing()';
                    addButtonModal(objAfter, strClass, title, strClick);
                });
            }

            if (_permiteCadastroCartao == 'S') {
                //remove todos os botoes para inserir novamente	
                $('.btnModalCartao').remove();
                $('.addBtnCadastrarModalCartao').each(function () {
                    objAfter = $(this);
                    strClass = 'btnModalCartao';
                    title = 'Cadastrar Novo Cartão';
                    strClick = 'openModalCartao()';
                    addButtonModal(objAfter, strClass, title, strClick);
                });
            }

            if (_permiteCadastroFornecedor != 'S') {
                $('.btnCadastrarModalFornecedor').remove();
            } else {
                $('.btnCadastrarModalFornecedor').unbind('click');
                $('.btnCadastrarModalFornecedor').click(function () {
                    openModalFornecedor();
                    if ($(this).hasClass("populateFieldsFornecedor")) {
                        /** busca os dados do fornecedor e popula o form */
                        ShowMsgAguarde();
                        $.getJSON(_baseUrl + '/default/ajax/json-ultimo-fornecedor-cadastrado', function (data) {
                            if (data.status_data == 'success') {
                                $('#lbl-selecionar-fornecedor-tx_nomfantasia').html(data.tx_nomfantasia.toUpperCase());
                                $('#lbl-selecionar-fornecedor-tx_razaosocial').html(data.tx_razaosocial.toUpperCase());
                                $('#lbl-selecionar-fornecedor-tx_municipio').html(data.tx_municipio.toUpperCase());
                                $('#lbl-selecionar-fornecedor-tx_cpf').html(data.tx_cpf);
                                $('#lbl-selecionar-fornecedor-tx_cnpj').html(data.tx_cnpj);
                                $('#lbl-selecionar-fornecedor-tx_email').html(data.tx_email.toLowerCase());
                                $('#lbl-selecionar-fornecedor-tx_telefone').html(data.tx_telefone);
                                $('#selecionar-fornecedor-id_fornecedor').val(data.id_fornecedor);
                            }
                            CloseMsgAguarde();
                        });
                    }
                });
            }

            if (_permiteCadastroCooperado != 'S') {
                $('.btnCadastrarModalCooperado').remove();
            } else {
                $('.btnCadastrarModalCooperado').unbind('click');
                $('.btnCadastrarModalCooperado').click(function () {
                    openModalCooperado();
                    if ($(this).hasClass("populateFieldsCooperado")) {
                        /** busca os dados do fornecedor e popula o form */
                        ShowMsgAguarde();
                        $.getJSON(_baseUrl + '/default/ajax/json-ultimo-cooperado-cadastrado', function (data) {
                            if (data.status_data == 'success') {
                                $('#selecionar-cooperado-id_cooperado').val(data.id_cooperado);
                                $('#lbl-selecionar-cooperado-tx_nomcooperado').html(data.tx_nomcooperado);
                                $('#lbl-selecionar-cooperado-tx_nromatricula').html(data.tx_nromatricula);
                                $('#lbl-selecionar-cooperado-tx_cpf').html(data.tx_cpf);
                                $('#lbl-selecionar-cooperado-tx_rg').html(data.tx_rg);

                            }
                            CloseMsgAguarde();
                        });
                    }
                });
            }

            if (_permiteCadastroFuncionario != 'S') {
                $('.btnCadastrarModalFuncionario').remove();
            } else {
                $('.btnCadastrarModalFuncionario').unbind('click');
                $('.btnCadastrarModalFuncionario').click(function () {
                    openModalFuncionario($(this).attr('tipo'));
                });
            }

            if (_permiteCadastroMunicipio != 'S') {
                $('.btnCadastrarModalMunicipio').remove();
            } else {
                $('.btnCadastrarModalMunicipio').unbind('click');
                $('.btnCadastrarModalMunicipio').click(function () {
                    openModalMunicipio();
                });
            }

            if (_permiteCadastroUF != 'S') {
                $('.btnCadastrarModalUF').remove();
            } else {
                $('.btnCadastrarModalUF').unbind('click');
                $('.btnCadastrarModalUF').click(function () {
                    openModalUF();
                });
            }

            if (_permiteCadastroPais != 'S') {
                $('.btnCadastrarModalPais').remove();
            } else {
                $('.btnCadastrarModalPais').unbind('click');
                $('.btnCadastrarModalPais').click(function () {
                    openModalPais();
                });
            }

            if (_permiteCadastroGrupoConta == 'S') {
                //remove todos os botoes para inserir novamente	
                $('.btnModalGrupoConta').remove();
                $('.addBtnCadastrarModalGrupoConta').each(function () {
                    objAfter = $(this);
                    strClass = 'btnModalGrupoConta';
                    title = 'Cadastrar Novo Grupo de Conta';
                    strClick = 'openModalGrupoConta()';
                    addButtonModal(objAfter, strClass, title, strClick);
                });
            }


            if (_permiteCadastroGrupoContaReceber == 'S') {
                //remove todos os botoes para inserir novamente	
                $('.btnModalGrupoContaReceber').remove();
                $('.addBtnCadastrarModalGrupoContaReceber').each(function () {
                    objAfter = $(this);
                    strClass = 'btnModalGrupoContaReceber';
                    title = 'Cadastrar Novo Grupo de Conta a Receber';
                    strClick = 'openModalGrupoContaReceber()';
                    addButtonModal(objAfter, strClass, title, strClick);
                });
            }


            if (_permiteCadastroModeloEmail == 'S') {
                //remove todos os botoes para inserir novamente	
                $('.btnModalModeloEmail').remove();
                $('.addBtnCadastrarModalModeloEmail').each(function () {
                    objAfter = $(this);
                    strClass = 'btnModalModeloEmail';
                    title = 'Cadastrar Novo Modelo de Email';
                    strClick = 'openModalModeloEmail()';
                    addButtonModal(objAfter, strClass, title, strClick);
                });
            }

            if (_permiteCadastroSubGrupoConta == 'S') {
                //remove todos os botoes para inserir novamente	
                $('.btnModalSubGrupoConta').remove();
                $('.addBtnCadastrarModalSubGrupoConta').each(function () {
                    objAfter = $(this);
                    strClass = 'btnModalSubGrupoConta';
                    title = 'Cadastrar Novo Subgrupo de Conta';
                    strClick = 'openModalSubGrupoConta()';
                    addButtonModal(objAfter, strClass, title, strClick);
                });
            }

            if (_permiteCadastroNaturezaContabil == 'S') {
                //remove todos os botoes para inserir novamente	
                $('.btnModalNaturezaContabil').remove();
                $('.addBtnCadastrarModalNaturezaContabil').each(function () {
                    objAfter = $(this);
                    strClass = 'btnModalNaturezaContabil';
                    title = 'Cadastrar Nova Natureza Contábil';
                    strClick = 'openModalNaturezaContabil()';
                    addButtonModal(objAfter, strClass, title, strClick);
                });
            }

            if (_permiteCadastroSubGrupoContaReceber == 'S') {
                //remove todos os botoes para inserir novamente	
                $('.btnModalSubGrupoContaReceber').remove();
                $('.addBtnCadastrarModalSubGrupoContaReceber').each(function () {
                    objAfter = $(this);
                    strClass = 'btnModalSubGrupoContaReceber';
                    title = 'Cadastrar Novo Subgrupo de Conta a Receber';
                    strClick = 'openModalSubGrupoContaReceber()';
                    addButtonModal(objAfter, strClass, title, strClick);
                });
            }

            if (_permiteCadastroBanco == 'S') {
                //remove todos os botoes para inserir novamente	
                $('.btnModalBanco').remove();
                $('.addBtnCadastrarModalBanco').each(function () {
                    objAfter = $(this);
                    strClass = 'btnModalBanco';
                    title = 'Cadastrar Novo Banco';
                    strClick = 'openModalBanco()';
                    addButtonModal(objAfter, strClass, title, strClick);
                });
            }


            if (_permiteCadastroContaCorrente == 'S') {
                //remove todos os botoes para inserir novamente	
                $('.btnModalContaCorrente').remove();
                $('.addBtnCadastrarModalContaCorrente').each(function () {
                    objAfter = $(this);
                    strClass = 'btnModalContaCorrente';
                    title = 'Cadastrar Nova Conta Corrente';
                    strClick = 'openModalContaCorrente()';
                    addButtonModal(objAfter, strClass, title, strClick);
                });
            }

            if (_permiteCadastroTipoFornecedor == 'S') {
                //remove todos os botoes para inserir novamente	
                $('.btnModalTipoFornecedor').remove();
                $('.addBtnCadastrarModalTipoFornecedor').each(function () {
                    objAfter = $(this);
                    strClass = 'btnModalTipoFornecedor';
                    title = 'Cadastrar Novo Tipo de Fornecedor';
                    strClick = 'openModalTipoFornecedor()';
                    addButtonModal(objAfter, strClass, title, strClick);
                });
            }
        }
    } catch (Exception) {
    }
}

function openModalTipoProduto() {
    /** url do método que gera o recibo */
    url = _baseUrl + '/admin/tipo-produto/formulario/ismodal/S';
    name = 'CADASTRO DE TIPO DE PRODUTO (Categoria)';
    height = 550;
    width = 850;

    ShowMsgAguarde();

    modalWin(url, name, height, width, function () {
        limpaSessaoIsModal();



        ShowMsgAguarde();
        $.getJSON(_baseUrl + '/ajax/json-combo-tipo-produto', null, function (data) {


            $('.addBtnCadastrarModalTipoProduto').each(function () {
                valueSelected = $(this).val();
                $(this).populateSelectJson(data);
                $(this).val(valueSelected);
            });

            CloseMsgAguarde();
        });
        $('#modal_win').remove();
    });
}


function openModalMarcaProduto() {
    /** url do método que gera o recibo */
    url = _baseUrl + '/admin/marca/formulario/ismodal/S';
    name = 'CADASTRO DE MARCA';
    height = 550;
    width = 850;

    ShowMsgAguarde();

    modalWin(url, name, height, width, function () {
        limpaSessaoIsModal();



        ShowMsgAguarde();
        $.getJSON(_baseUrl + '/ajax/json-combo-marca', null, function (data) {


            $('.addBtnCadastrarModalMarca').each(function () {
                valueSelected = $(this).val();
                $(this).populateSelectJson(data);
                $(this).val(valueSelected);
            });

            CloseMsgAguarde();
        });
        $('#modal_win').remove();
    });
}


function openModalModeloProduto() {
    /** url do método que gera o recibo */
    url = _baseUrl + '/admin/modelo/formulario/ismodal/S';
    name = 'CADASTRO DE MODELO';
    height = 550;
    width = 850;

    ShowMsgAguarde();

    modalWin(url, name, height, width, function () {
        limpaSessaoIsModal();



        ShowMsgAguarde();
        $.getJSON(_baseUrl + '/ajax/json-combo-modelo', null, function (data) {


            $('.addBtnCadastrarModalModelo').each(function () {
                valueSelected = $(this).val();
                $(this).populateSelectJson(data);
                $(this).val(valueSelected);
            });

            CloseMsgAguarde();
        });
        $('#modal_win').remove();
    });
}

function openModalFamiliaProduto() {
    /** url do método que gera o recibo */
    url = _baseUrl + '/admin/familia/formulario/ismodal/S';
    name = 'CADASTRO DE FAMÍLIA DE PRODUTO';
    height = 550;
    width = 850;

    ShowMsgAguarde();

    modalWin(url, name, height, width, function () {
        limpaSessaoIsModal();



        ShowMsgAguarde();
        $.getJSON(_baseUrl + '/ajax/json-combo-familia', null, function (data) {


            $('.addBtnCadastrarModalFamilia').each(function () {
                valueSelected = $(this).val();
                $(this).populateSelectJson(data);
                $(this).val(valueSelected);
            });

            CloseMsgAguarde();
        });
        $('#modal_win').remove();
    });
}

function openModalUnidadeMedida() {
    /** url do método que gera o recibo */
    url = _baseUrl + '/admin/unidade-medida-produto/formulario/ismodal/S';
    name = 'CADASTRO DE UNIDADE DE MEDIDA';
    height = 550;
    width = 850;

    ShowMsgAguarde();

    modalWin(url, name, height, width, function () {
        limpaSessaoIsModal();



        ShowMsgAguarde();
        $.getJSON(_baseUrl + '/ajax/json-combo-unidade-medida-produto', null, function (data) {


            $('.addBtnCadastrarModalUnidadeMedida').each(function () {
                valueSelected = $(this).val();
                $(this).populateSelectJson(data);
                $(this).val(valueSelected);
            });

            CloseMsgAguarde();
        });
        $('#modal_win').remove();
    });
}


function openModalTipoMidiaMarketing() {
    /** url do método que gera o recibo */
    url = _baseUrl + '/admin/tipo-midia-marketing/formulario/ismodal/S';
    name = 'CADASTRO DE TIPO DE MÍDIA DE MARKETING';
    height = 550;
    width = 850;

    //chama a modal e fica aguardando o fechamento da janela nessa linha para somente depois prosseguir com o script
    //modalWin(url, name, height, width);

    ShowMsgAguarde();

    modalWin(url, name, height, width, function () {
        limpaSessaoIsModal();



        ShowMsgAguarde();
        $.getJSON(_baseUrl + '/ajax/json-combo-tipo-midia-marketing', null, function (data) {


            $('.addBtnCadastrarModalTipoMidiaMarketing').each(function () {
                valueSelected = $(this).val();
                $(this).populateSelectJson(data);
                $(this).val(valueSelected);
            });

            CloseMsgAguarde();
        });
        $('#modal_win').remove();
    });
}

function openModalCartao() {
    /** url do método que gera o recibo */
    url = _baseUrl + '/admin/cartao/formulario/ismodal/S';
    name = 'CADASTRO DE CARTÃO';
    height = 550;
    width = 850;

    //chama a modal e fica aguardando o fechamento da janela nessa linha para somente depois prosseguir com o script
    //modalWin(url, name, height, width);
    ShowMsgAguarde();
    modalWin(url, name, height, width, function () {
        limpaSessaoIsModal();



        ShowMsgAguarde();
        $.getJSON(_baseUrl + '/ajax/json-combo-cartao', null, function (data) {


            $('.addBtnCadastrarModalCartao').each(function () {
                valueSelected = $(this).val();
                $(this).populateSelectJson(data);
                $(this).val(valueSelected);
            });

            CloseMsgAguarde();
        });
    });
}



function openModalGrupoConta() {
    /** url do método que gera o recibo */
    url = _baseUrl + '/admin/grupo-conta-pagar/formulario/ismodal/S';
    name = 'CADASTRO DE GRUPO DE CONTA A PAGAR';
    height = 550;
    width = 850;

    //chama a modal e fica aguardando o fechamento da janela nessa linha para somente depois prosseguir com o script
    //modalWin(url, name, height, width);
    ShowMsgAguarde();
    modalWin(url, name, height, width, function () {
        limpaSessaoIsModal();



        ShowMsgAguarde();
        $.getJSON(_baseUrl + '/ajax/json-combo-grupo-conta-pagar', null, function (data) {


            $('.addBtnCadastrarModalGrupoConta').each(function () {
                valueSelected = $(this).val();
                $(this).populateSelectJson(data);
                $(this).val(valueSelected);
            });

            CloseMsgAguarde();
        });
    });
}


function openModalGrupoContaReceber() {
    /** url do método que gera o recibo */
    url = _baseUrl + '/admin/grupo-conta-receber/formulario/ismodal/S';
    name = 'CADASTRO DE GRUPO DE CONTA A RECEBER';
    height = 550;
    width = 850;

    //chama a modal e fica aguardando o fechamento da janela nessa linha para somente depois prosseguir com o script
    //modalWin(url, name, height, width);
    ShowMsgAguarde();
    modalWin(url, name, height, width, function () {
        limpaSessaoIsModal();



        ShowMsgAguarde();
        $.getJSON(_baseUrl + '/ajax/json-combo-grupo-conta-receber', null, function (data) {


            $('.addBtnCadastrarModalGrupoContaReceber').each(function () {
                valueSelected = $(this).val();
                $(this).populateSelectJson(data);
                $(this).val(valueSelected);
            });

            CloseMsgAguarde();
        });
    });
}


function openModalSubGrupoConta() {
    /** url do método que gera o recibo */
    url = _baseUrl + '/admin/sub-grupo-conta-pagar/formulario/ismodal/S';
    name = 'CADASTRO DE SUBGRUPO DE CONTA A PAGAR';
    height = 550;
    width = 850;

    //chama a modal e fica aguardando o fechamento da janela nessa linha para somente depois prosseguir com o script
    //modalWin(url, name, height, width);
    ShowMsgAguarde();
    modalWin(url, name, height, width, function () {
        limpaSessaoIsModal();



        ShowMsgAguarde();
        id_grupocontapagar = '';

        //tenta capturar o primeiro id_grupocontapagar 
        id_grupocontapagar = $('#id_grupocontapagar').val();

        $.getJSON(_baseUrl + '/ajax/json-combo-sub-grupo-conta/id_grupocontapagar/' + id_grupocontapagar, null, function (data) {

            $('.addBtnCadastrarModalSubGrupoConta').each(function () {
                valueSelected = $(this).val();
                $(this).populateSelectJson(data);
                $(this).val(valueSelected);
            });

            CloseMsgAguarde();
        });
    });
}

function openModalNaturezaContabil() {
    /** url do método que gera o recibo */
    url = _baseUrl + '/admin/natureza-contabil/formulario/ismodal/S';
    name = 'CADASTRO DE NATUREZA CONTÁBIL';
    height = 550;
    width = 850;

    //chama a modal e fica aguardando o fechamento da janela nessa linha para somente depois prosseguir com o script
    //modalWin(url, name, height, width);
    ShowMsgAguarde();
    modalWin(url, name, height, width, function () {
        limpaSessaoIsModal();



        ShowMsgAguarde();
        $.getJSON(_baseUrl + '/ajax/json-combo-natureza-contabil', null, function (data) {


            $('.addBtnCadastrarModalNaturezaContabil').each(function () {
                valueSelected = $(this).val();
                $(this).populateSelectJson(data);
                $(this).val(valueSelected);
            });

            CloseMsgAguarde();
        });
    });
}

function openModalSubGrupoContaReceber() {
    /** url do método que gera o recibo */
    url = _baseUrl + '/admin/sub-grupo-conta-receber/formulario/ismodal/S';
    name = 'CADASTRO DE SUBGRUPO DE CONTA A RECEBER';
    height = 550;
    width = 850;

    //chama a modal e fica aguardando o fechamento da janela nessa linha para somente depois prosseguir com o script
    //modalWin(url, name, height, width);
    ShowMsgAguarde();
    modalWin(url, name, height, width, function () {
        limpaSessaoIsModal();



        ShowMsgAguarde();
        id_grupocontareceber = '';

        //tenta capturar o primeiro id_grupocontapagar 
        id_grupocontareceber = $('#id_grupocontareceber').val();

        $.getJSON(_baseUrl + '/ajax/json-combo-sub-grupo-conta-receber/id_grupocontareceber/' + id_grupocontareceber, null, function (data) {

            $('.addBtnCadastrarModalSubGrupoContaReceber').each(function () {
                valueSelected = $(this).val();
                $(this).populateSelectJson(data);
                $(this).val(valueSelected);
            });

            CloseMsgAguarde();
        });
    });
}



function openModalBanco() {
    /** url do método que gera o recibo */
    url = _baseUrl + '/admin/banco/formulario/ismodal/S';
    name = 'CADASTRO DE BANCO';
    height = 550;
    width = 850;

    //chama a modal e fica aguardando o fechamento da janela nessa linha para somente depois prosseguir com o script
    //modalWin(url, name, height, width);
    ShowMsgAguarde();
    modalWin(url, name, height, width, function () {
        limpaSessaoIsModal();



        ShowMsgAguarde();
        $.getJSON(_baseUrl + '/ajax/json-combo-banco', null, function (data) {


            $('.addBtnCadastrarModalBanco').each(function () {
                valueSelected = $(this).val();
                $(this).populateSelectJson(data);
                $(this).val(valueSelected);
            });

            CloseMsgAguarde();
        });
    });
}

function openModalContaCorrente() {
    /** url do método que gera o recibo */
    url = _baseUrl + '/admin/banco-conta-corrente/formulario/ismodal/S';
    name = 'CADASTRO DE CONTA CORRENTE';
    height = 550;
    width = 850;

    //chama a modal e fica aguardando o fechamento da janela nessa linha para somente depois prosseguir com o script
    //modalWin(url, name, height, width);
    ShowMsgAguarde();
    modalWin(url, name, height, width, function () {
        limpaSessaoIsModal();



        ShowMsgAguarde();
        $.getJSON(_baseUrl + '/ajax/json-combo-banco-conta-corrente', null, function (data) {


            $('.addBtnCadastrarModalContaCorrente').each(function () {
                valueSelected = $(this).val();
                $(this).populateSelectJson(data);
                $(this).val(valueSelected);
            });

            CloseMsgAguarde();
        });
    });
}

function openModalFornecedor() {
    /** url do método que gera o recibo */
    url = _baseUrl + '/admin/fornecedor/formulario/ismodal/S'; //checkunidadeexterna -> faz com que o cadastro venha por default com o tipo Unidade Externa marcado
    name = 'CADASTRO DE FORNECEDOR';
    height = 550;
    width = $('body').width() - 50;

    //chama a modal e fica aguardando o fechamento da janela nessa linha para somente depois prosseguir com o script
    //modalWin(url, name, height, width);
    ShowMsgAguarde();
    modalWin(url, name, height, width, function () {
        limpaSessaoIsModal();
    });
}

function openModalCooperado() {
    /** url do método que gera o recibo */
    url = _baseUrl + '/admin/cooperado/formulario/ismodal/S'; //checkunidadeexterna -> faz com que o cadastro venha por default com o tipo Unidade Externa marcado
    name = 'CADASTRO DE COOPERADO / CLIENTE';
    height = 800;
    width = $('body').width() - 50;

    //chama a modal e fica aguardando o fechamento da janela nessa linha para somente depois prosseguir com o script
    //modalWin(url, name, height, width);
    ShowMsgAguarde();
    modalWin(url, name, height, width, function () {
        limpaSessaoIsModal();
    }, 'S');
}

function openModalTipoFornecedor() {
    /** url do método que gera o recibo */
    url = _baseUrl + '/admin/tipo-fornecedor/formulario/ismodal/S';
    name = 'CADASTRO DE TIPO DE FORNECEDOR';
    height = 550;
    width = 850;

    //chama a modal e fica aguardando o fechamento da janela nessa linha para somente depois prosseguir com o script
    //modalWin(url, name, height, width);
    ShowMsgAguarde();
    modalWin(url, name, height, width, function () {
        limpaSessaoIsModal();



        ShowMsgAguarde();
        $.getJSON(_baseUrl + '/ajax/json-combo-tipo-fornecedor', null, function (data) {


            $('.addBtnCadastrarModalTipoFornecedor').each(function () {
                valueSelected = $(this).val();
                $(this).populateSelectJson(data);
                $(this).val(valueSelected);
            });

            CloseMsgAguarde();
        });
    });
}

function openModalFuncionario(tipo) {
    url = _baseUrl + '/admin/funcionario/formulario/ismodal/S';

    name = 'CADASTRO DE FUNCIONÁRIO';

    height = 300;
    width = 700;

    //chama a modal e fica aguardando o fechamento da janela nessa linha para somente depois prosseguir com o script
    //modalWin(url, name, height, width);
    ShowMsgAguarde();
    modalWin(url, name, height, width);
}

function openModalMunicipio() {
    url = _baseUrl + '/admin/municipio-cliente/formulario/ismodal/S';
    name = 'CADASTRO DE MUNICÍPIO';
    height = 300;
    width = 700;

    //chama a modal e fica aguardando o fechamento da janela nessa linha para somente depois prosseguir com o script
    //modalWin(url, name, height, width);
    ShowMsgAguarde();
    modalWin(url, name, height, width, function () {
        limpaSessaoIsModal();

        //atualiza os autocomplete de municipio 
        ShowMsgAguarde();
        $.getJSON(_baseUrl + '/ajax/atualizar-autocomple-municipios', null, function (data) {
            //alert('teste');        
            //dataMunicipio = new Array();

            $('.dataMunicipioAutoComplete').each(function () {
                //dataRefreshAutoCompleteData = data;
                $(this).setOptions({
                    dataRefreshAutoComplete: data
                });

            });

            CloseMsgAguarde();
        });
    });
}

function openModalUF() {
    url = _baseUrl + '/admin/estado-cliente/formulario/ismodal/S';
    name = 'CADASTRO DE UF';
    height = 300;
    width = 700;

    //chama a modal e fica aguardando o fechamento da janela nessa linha para somente depois prosseguir com o script
    //modalWin(url, name, height, width);
    ShowMsgAguarde();
    modalWin(url, name, height, width, function () {
        limpaSessaoIsModal();
    });
}

function openModalPais() {
    url = _baseUrl + '/admin/pais-cliente/formulario/ismodal/S';
    name = 'CADASTRO DE PAÍS';
    height = 300;
    width = 900;

    //chama a modal e fica aguardando o fechamento da janela nessa linha para somente depois prosseguir com o script
    //modalWin(url, name, height, width);
    ShowMsgAguarde();
    modalWin(url, name, height, width, function () {
        limpaSessaoIsModal();

        //atualiza os autocomplete de municipio 
        ShowMsgAguarde();
        $.getJSON(_baseUrl + '/ajax/atualizar-autocomple-pais', null, function (data) {
            jsonPais = data;

        });
        $.getJSON(_baseUrl + '/ajax/atualizar-autocomple-nacionalidade', null, function (data) {
            jsonNacionalidade = data;
        });
        $.getJSON(_baseUrl + '/ajax/atualizar-autocomple-nacionalidades', null, function (data) {
            $('.dataNacionalidadeAutoComplete').each(function () {
                $(this).setOptions({
                    dataRefreshAutoComplete: data
                });

            });
        });
        CloseMsgAguarde();
    });
}

function openModalContaReceber(id_cooperado, id_, id_contareceber, acao) {
    url = _baseUrl + '/admin/conta-receber/formulario/ismodal/S';

    if (id_cooperado != null) {
        url += '/id_cooperado/' + id_cooperado;
    }


    if (id_contareceber != null) {
        url += '/id_contareceber/' + id_contareceber;
    }

    if (acao != null) {
        url += '/acao/' + acao;
    }

    name = 'CADASTRO DE CONTAS A RECEBER';
    height = 300;
    width = 700;

    //chama a modal e fica aguardando o fechamento da janela nessa linha para somente depois prosseguir com o script
    //modalWin(url, name, height, width);
    ShowMsgAguarde();
    modalWin(url, name, height, width, function () {
        if (arrUltimasContas.length > 0) {
            if (acao == 'excluir') {
                arrUltimasContas = [];
                Dialog.show('Conta excluída com sucesso!', 'SUCESSO!', function () {
                    $('#get-contareceber').load(urlGetConta, lastDataConta);
                });
            } else {
                Dialog.show('Conta registrada com sucesso!', 'SUCESSO!', function () {
                    $('#get-contareceber').load(urlGetConta, lastDataConta);
                });
            }
        }
        limpaSessaoIsModal();
    }, 'N');
}



function openHelpField() {
//Dialog.help();
}


function modalWin(url, name, height, width, close, use_height_param) {
    $('#modal_win').remove();

    if (use_height_param != 'S') {
        height = ($(window).height() * 0.9);
    }

    width = ($(window).width() - 40);


    modal_win = '<div id="modal_win" class="modal fade" style="width:100%;">'
            + '<div class="modal-dialog" style="width:90%;">'
            + '<div class="modal-content" style="width:100%; height:' + height + 'px;">'
            + '<div class="modal-header">'
            + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'
            + '<h4 class="modal-title">' + name + '</h4>'
            + '</div>'
            + '<div class="modal-body" id="modal-body-modal-win" style="width:100%; height:90%;">'
            + '<iframe id="iframe_modal" name="iframe_modal" onload="CloseMsgAguarde();" style="width:100%;height:100%;overflow:auto;" '
            + ' width="100%" '
            + ' heigth="100%" '
            + ' src="' + url + '" '
            + ' frameborder="0">'
            + '</iframe>'
            + '</div>'
            //+ '<div class="modal-footer">'
            //  + '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'
            //  + '<button type="button" class="btn btn-primary">Save changes</button>'
            //+ '</div>'
            + '</div>'
            + '</div>'
            + '</div>'


    $('body').append(modal_win);
    $('#modal_win').on('hidden.bs.modal', close);
    $('#modal_win').modal({
        backdrop: 'static'
    });

}

function iframeLoaded() {
//    var iFrameID = document.getElementById('iframe_modal');
//    if(iFrameID) {
//        // here you can meke the height, I delete it first, then I make it again
//        iFrameID.height = "";
//        iFrameID.height = iFrameID.contentWindow.document.body.scrollHeight + "px";
//    }   
}

function fecharModal() {
    //window.parent.$("#modal_win").dialog("close");
    //window.parent.$(window).find('.ui-dialog-titlebar-close').click();
    window.parent.$("#modal_win").modal('hide');
}


/** define uma mensagem padrao para a ocorrencia de erro em uma requisição ajax */
function _initAjaxErrorDefault() {
    $(document).ajaxError(function () {
        /** informa o erro para o usuario em msg modal */
        msg = 'Desculpe! Ocorreu um erro ao tentar executar a operação. Isso pode ter acontecido devido a velocidade baixa da conexão ou perca da conexão. Tente novamente, caso persistir contato o suporte do sistema.'
        Dialog.error(msg, 'ATENÇÃO');
    });
}

/* popula o formulario apartir da variavel enviada da view  */
function _populateFormPhp()
{
    /* se a variavel setada no layout "_populateForm" nao estiver vazia
     * popula o formularios com a classe populate  */
    if (_populateForm != null && _populateForm != '[]') {
        $(".populate").populate(_populateForm);
    }
}

/* trata o retorno do sumit */
function _returnSubmitPhp()
{
    if (_returnSubmit != null) {
        /* exibe a mensagem vindo do sumit */
        if (_returnSubmit.message != '') {
            Message.show(_returnSubmit.message, _returnSubmit.type);
        }
        /* popula dos dados novamente */
        if (_returnSubmit.data) {
            $(".populate").populate(_returnSubmit.data);
        }
    }
}

function selectOptionUnique() {
    /** todas as combos do sistema que tiverem apenas um option, trazer selecionado */
    $('select[class*="required"]').each(function () {
        /** testa o número de itens considerando apenas os options onde o value seja diferente de vazio */
        if ($(this).find('option[value!=""]').length == 1) {
            $(this).find('option[value!=""]:first').attr('SELECTED', 'SELECTED');
            $(this).val($(this).find('option[value!=""]:first').attr('value'));

            //valueItem = $(this).find('option[value!=""]:first').attr('value');
            //$(this).find('option[value="' + valueItem + '"]').attr('SELECTED','SELECTED');
        }
    });
}

/* inicia as funcoes default dos forms */
function _initForm()
{
    /* seta o foco no primeiro campo de todos os formulario do sistema */
    $('form').focusFirstForm();

    /*adiciona a validação no form*/
    $('#validate').validationEngine();
    /* adiciona maxlength nos textareas do ssitema */
    $('textarea').addMaxlength();

    /** todas as combos do sistema que tiverem apenas um option, trazer selecionado */
    selectOptionUnique();


    //para todos os inputs de forms de filtro ao pressioner enter ele ativa o botão de pesquisa que estiver dentro dele
    $('.formSearch input').keypress(function (event) {
        if (event.which == 13) {
            $('.search:first').click();
        }
    });



    //Desabilito o backspace em todo componente Select para evitar que o navegador volte páginas por erro de digitação do usuário
    $('select').keypress(function (event) {
        if (event.which == 8) {
            return false;
        } else {
            return true;
        }
    }).keyup(function (event) {
        if (event.which == 8) {
            return false;
        } else {
            return true;
        }
    }).keydown(function (event) {
        if (event.which == 8) {
            return false;
        } else {
            return true;
        }
    });

    _initFormSearch();
}

function _initFormSearch() {
    //$('.fieldsetSearch legend').find('.lupa-pesquisar').remove();
    //$('.fieldsetSearch legend').prepend('<span class="glyphicon glyphicon-search lupa-pesquisar"></span>');
    $('.fieldsetSearch').find('legend').remove();
}

function soNm(nm, extraNegative) {
    extraNegative = $.trim(extraNegative);
    nm += ""; // Converte qualquer valor de "var nm" em String
    var r = ""; // Cria var para retorno
    var l; // Cria var temporaria
    for (var i = 0; i < nm.length; i++) { // Laço que percorre todos os caractere de "var nm"
        l = nm.charAt(i); // Caractere atual
        if ((l >= "0") && (l <= "9") || (l == extraNegative && i == 0))
            r += l; // Checa cada caractere; se for nº adiciona em "var r"
    }
    return r; // Retorno so os nº de "var nm"
}


/* seta as mascaras no sitema apartir da classe do input */
function _initMask()
{

    $('.onlyNumbers, .numeric').keypress(function (event) {
        var tecla = (window.event) ? event.keyCode : event.which;
        if ((tecla > 47 && tecla < 58))
            return true;
        else {
            if (tecla == 8 || tecla == 0)
                return true;
            else
                return false;
        }
    });

    $('.onlyNumbers, .numeric').blur(function () {
        texto = soNm($(this).val());
        $(this).val(texto);
    });


    $('.numericNegative').keypress(function (event) {
        var tecla = (window.event) ? event.keyCode : event.which;
        if ((tecla > 47 && tecla < 58) || tecla == 45)
            return true;
        else {
            if (tecla == 8 || tecla == 0)
                return true;
            else
                return false;
        }
    });

    $('.numericNegative').blur(function () {
        texto = soNm($(this).val(), '-');
        $(this).val(texto);
    });

    //    
    //    $('.date').setMask('date');
    //    $('.cep').setMask('cep');
    //    $('.maskCNPJ').setMask({
    //        mask:'99.999.999/9999-99'
    //    });
    //    $('.maskCPF').setMask({
    //        mask:'999.999.999-99'
    //    });
    //    $('.time').setMask('time');
    //    $('.year').setMask({
    //        mask:'9999'
    //    });
    //    
    //    $('.fone').each(function(){
    //        id = $(this).attr('id');
    //        
    //        $(this).keyup(function(){
    //            val = str_replace([' ','(',')','-'],'',$(this).val());
    //            if(val.length == 11){
    //                $(this).setMask({
    //                    mask:'(99)99999-9999'
    //                });
    //            } else {
    //                $(this).setMask({
    //                    mask:'(99)9999-99999'
    //                });
    //            }
    //        });
    //    });
    //    
    //    $('.url').each(function(){
    //        $(this).focus(function(){
    //            if($(this).val() == ''){
    //                $(this).val('http://');
    //            }
    //        }).blur(function(){
    //            if($(this).val() == 'http://'){
    //                $(this).val('');
    //            }
    //        })
    //    });
    //    
    //    $('.numeric_plus').each(function(){
    //        id = $(this).attr('id');
    //        
    //        var int_size = parseInt($(this).attr('int_size'));
    //        var dec_size =  parseInt($(this).attr('dec_size'));
    //        
    //        mask = ''
    //        def_mask = '0';
    //        if(dec_size != '' && dec_size != undefined){
    //            size_count = 0;
    //            while(size_count < dec_size){
    //                mask += '9';
    //                def_mask += '0'
    //                size_count++;
    //            }
    //        }
    //        if(int_size != '' && int_size != undefined){
    //            size_count = 0; 
    //            mask+=',';
    //            while(size_count < int_size){
    //                mask += '9';
    //                size_count++;
    //            }
    //        }
    //        
    //        console.log(mask);
    //        
    //        $('#'+id).focus(function(){
    //            $(this).setMask({
    //                mask:mask,
    //                type:'reverse',
    //                defaultValue:def_mask
    //            });
    //        }).blur(function(){
    //            $(this).unsetMask();
    //        });
    //    });
    //    
    //    $('.numeric').setMask({
    //        mask:'9',
    //        type:'repeat',
    //        autoTab: false
    //    });
    //     
    //    $('.decimal').setMask({
    //        mask:'99,999.999.999',
    //        type:'reverse',
    //        defaultValue:'000',
    //        autoTab: false
    //    });
    //    
    //    $('.decimal3').setMask({
    //        mask:'999,999999999',
    //        type:'reverse',
    //        defaultValue:'0000',
    //        autoTab: false
    //    });
    //    
    //    $('.money').setMask({
    //        mask:'99,999.999.999',
    //        type:'reverse',
    //        defaultValue:'000',
    //        autoTab: false
    //    });




    /** remove as mascaras primeiro pois pode acontecer de chamar o initMask num ajax e estragar as mascaras que já estão colocadas */
    $('.time').unmask();
    $('.year').unmask();
    $('.date').unmask();
    $('.maskCNPJ').unmask();
    $('.maskCPF').unmask();
    $('.cep').unmask();
    $('.fone').unmask();
    $('.mes-ano').unmask();


    $('.time').mask("99:99");
    $('.mes-ano').mask("99/9999");
    $('.year').mask("9999");
    $('.date').mask("99/99/9999");
    $('.maskCNPJ').mask("99.999.999/9999-99");
    $('.maskCPF').mask("999.999.999-99");
    $('.cep').mask("99.999-999");
    $('.fone').mask("(99)9999-9999");


    //jQuery Masked Input para Fone com o dígito 9 de SP
    $('.fone').mask("(99)9999-9999?9").ready(function (event) {
        var target, phone, element;
        target = (event.currentTarget) ? event.currentTarget : event.srcElement;
        if (target) {
            phone = target.value.replace(/\D/g, '');
            element = $(target);
            element.unmask();
            if (phone.length > 10) {
                element.mask("(99)99999-999?9");
            } else {
                element.mask("(99)9999-9999?9");
            }
        }
    });

    $('.fone').focusout(function () {
        var phone, element;
        element = $(this);
        element.unmask();
        phone = element.val().replace(/\D/g, '');
        if (phone.length > 10) {
            element.mask("(99)99999-999?9");
        } else {
            element.mask("(99)9999-9999?9");
        }
    }).trigger('focusout');

    $('.decimal').unmaskMoney();
    $('.decimal1').unmaskMoney();
    $('.decimal3').unmaskMoney();
    $('.decimal4').unmaskMoney();
    $('.money').unmaskMoney();

    $('.decimal').maskMoney({
        allowZero: true,
        decimal: ",",
        thousands: ".",
        defaultZero: false
    });
    $('.decimal').numeric(",");
    $('.decimal').floatnumber(",", 2);


    $('.decimal1').numeric(",");
    $('.decimal1').floatnumber(",", 1);

    $('.decimal3').maskMoney({
        allowZero: true,
        decimal: ",",
        thousands: ".",
        precision: 3,
        defaultZero: false
    });


    $('.decimal4').maskMoney({
        allowZero: true,
        decimal: ",",
        thousands: ".",
        precision: 4,
        defaultZero: false
    });

    $('.decimal1').maskMoney({
        allowZero: true,
        decimal: ",",
        thousands: ".",
        precision: 1,
        defaultZero: false
    });
    $('.money').maskMoney({
        allowZero: true,
        decimal: ",",
        thousands: ".",
        defaultZero: false
    });



    /** valida se a data fornecida é menor que a data atual*/
    $(".datePicker").datepicker({
        buttonImage: _baseUrl + "/public/default/images/icones/calendario.gif",
        buttonImageOnly: true,
        showAnim: 'clip',
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
        dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        nextText: 'Próximo',
        prevText: 'Anterior'
    }).keypress(function () {
        return false;
    });

    $(".date").datepicker({
     
        buttonImageOnly: true,
        showAnim: 'clip',
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
        dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        nextText: 'Próximo',
        prevText: 'Anterior'
    }).keypress(function () {
        return false;
    });
    
    /** valida se a data fornecida é menor que a data atual*/
    $(".datePickerMinDate").datepicker({
        buttonImage: _baseUrl + "/public/default/images/icones/calendario.gif",
        buttonImageOnly: true,
        minDate: _dataAtual,
        showAnim: 'clip',
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
        dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        nextText: 'Próximo',
        prevText: 'Anterior'
    }).keypress(function () {
        return false;
    });
}

/* inicia as abas em query */
function _iniTabs()
{
    $("div.contaba").hide();
    $("div.contaba:first").show();
    if ($("#abas li.active").length == 0) {
        $("#abas li:first").addClass("active");
    }
    $("#abas a").click(function () {
        val = $(this).attr("href").replace('/ismodal/S', '').replace('/ismodal/N', '');

        if (val.indexOf('/') < 0) {
            if ($(val).length > 0) {
                $("div.contaba").hide();
                $("#abas li").removeClass("active");
                $(this).parent().addClass("active");
                $(val).show();
            }
        }
        return false;
    });
}

function _iniTabs2()
{
    /*
     $("div.contaba2").hide();
     $("div.contaba2:first").show();
     $("#abas2 li:first").addClass("active");
     $("#abas2 a").click(function() {
     $("div.contaba2").hide();
     
     $("#abas2 li").removeClass("active");
     $(this).parent().addClass("active");
     
     
     val = $(this).attr("href").replace('/ismodal/S', '').replace('/ismodal/N', '');
     
     $(val).show();
     return false;
     });*/


    $('#abas2 a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })

}

/* inicia o editor de texto */
function _initTinymce()
{
    if (_Module == 'admin' || _Module == 'painel') {
        //        $('textarea.tinymce').setTinymce();
        //        $('textarea.tinymceSimple').setTinymceSimple();
        //        $('textarea.tinymceReadOnly').setTinymceReadOnly();

        /*tinyMCE_GZ.init({
         mode: "textareas",
         editor_selector: "tinymce",
         // Location of TinyMCE script
         script_url: _baseUrl + '/public/library/javascripts/tiny_mce/tiny_mce.js',
         // General options
         language: "pt",
         theme: "advanced",
         plugins: "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
         // Theme options
         theme_advanced_buttons1: "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect",
         theme_advanced_buttons2: "bullist,numlist,|,outdent,indent,|,forecolor,backcolor,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code",
         //		theme_advanced_buttons3 : "hr,removeformat,visualaid,|,sub,sup,|,charmap",
         theme_advanced_buttons3: "",
         theme_advanced_toolbar_location: "top",
         theme_advanced_toolbar_align: "left",
         // Example content CSS (should be your site CSS)
         content_css: _baseUrl + "/public/library/javascripts/tiny_mce/themes/advanced/skins/default/content.css",
         // Drop lists for link/image/media/template dialogs
         template_external_list_url: "lists/template_list.js",
         external_link_list_url: "lists/link_list.js",
         external_image_list_url: "lists/image_list.js",
         media_external_list_url: "lists/media_list.js",
         file_browser_callback: "tinyBrowser",
         // Replace values for the template plugin
         template_replace_values: {
         username: "Some User",
         staffid: "991234"
         }
         
         });*/

        tinyMCE.init({
            mode: "textareas",
            editor_selector: "tinymce",
            // Location of TinyMCE script
            script_url: _baseUrl + '/public/library/javascripts/tiny_mce/tiny_mce.js',
            // General options
            language: "en",
            theme: "advanced",
            plugins: "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
            // Theme options
            theme_advanced_buttons1: "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect",
            theme_advanced_buttons2: "bullist,numlist,|,outdent,indent,|,forecolor,backcolor,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code",
            //		theme_advanced_buttons3 : "hr,removeformat,visualaid,|,sub,sup,|,charmap",
            theme_advanced_buttons3: "",
            theme_advanced_toolbar_location: "top",
            theme_advanced_toolbar_align: "left",
            // Example content CSS (should be your site CSS)
            content_css: _baseUrl + "/public/library/javascripts/tiny_mce/themes/advanced/skins/default/content.css",
            // Drop lists for link/image/media/template dialogs
            template_external_list_url: "lists/template_list.js",
            external_link_list_url: "lists/link_list.js",
            external_image_list_url: "lists/image_list.js",
            media_external_list_url: "lists/media_list.js",
            file_browser_callback: "tinyBrowser",
            // Replace values for the template plugin
            template_replace_values: {
                username: "Some User",
                staffid: "991234"
            }
        });

        //SIMPLE
        /*tinyMCE_GZ.init({
         mode: "textareas",
         editor_selector: "tinymceSimple",
         // Location of TinyMCE script
         script_url: _baseUrl + '/public/library/javascripts/tiny_mce/tiny_mce.js',
         // General options
         language: "pt",
         theme: "advanced",
         plugins: "safari,pagebreak,style,layer,table,save,advhr,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,gsynuhimgupload",
         //plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
         // Theme options
         theme_advanced_buttons1: "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,|,gsynuhimgupload",
         theme_advanced_buttons2: "bullist,numlist,|,outdent,indent,|,forecolor,backcolor,|,undo,redo,|,link,unlink,anchor,cleanup,help,code",
         //		theme_advanced_buttons3 : "hr,removeformat,visualaid,|,sub,sup,|,charmap",
         theme_advanced_buttons3: "",
         theme_advanced_toolbar_location: "top",
         theme_advanced_toolbar_align: "left",
         relative_urls: false,
         //lina adicionada dia 04/04/2013 pra submeter acentos no ajax submit, nao sei se causara erros no submit normal ou qq programação relacionada ao tiny
         //entity_encoding: "raw",
         
         // Example content CSS (should be your site CSS)
         content_css: _baseUrl + "/public/library/javascripts/tiny_mce/themes/advanced/skins/default/content.css",
         // Drop lists for link/image/media/template dialogs
         template_external_list_url: "lists/template_list.js",
         external_link_list_url: "lists/link_list.js",
         external_image_list_url: "lists/image_list.js",
         media_external_list_url: "lists/media_list.js",
         //file_browser_callback : "tinyBrowser",
         // Replace values for the template plugin
         template_replace_values: {
         username: "Some User",
         staffid: "991234"
         }
         });*/

        tinyMCE.init({
            mode: "textareas",
            editor_selector: "tinymceSimple",
            // Location of TinyMCE script
            script_url: _baseUrl + '/public/library/javascripts/tiny_mce/tiny_mce.js',
            // General options
            language: "en",
            theme: "advanced",
            plugins: "safari,pagebreak,style,layer,table,save,advhr,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,gsynuhimgupload",
            //plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
            // Theme options
            theme_advanced_buttons1: "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,|,gsynuhimgupload",
            theme_advanced_buttons2: "bullist,numlist,|,outdent,indent,|,forecolor,backcolor,|,undo,redo,|,link,unlink,anchor,cleanup,help,code",
            //		theme_advanced_buttons3 : "hr,removeformat,visualaid,|,sub,sup,|,charmap",
            theme_advanced_buttons3: "",
            theme_advanced_toolbar_location: "top",
            theme_advanced_toolbar_align: "left",
            relative_urls: false,
            //lina adicionada dia 04/04/2013 pra submeter acentos no ajax submit, nao sei se causara erros no submit normal ou qq programação relacionada ao tiny
            //entity_encoding: "raw",

            // Example content CSS (should be your site CSS)
            content_css: _baseUrl + "/public/library/javascripts/tiny_mce/themes/advanced/skins/default/content.css",
            // Drop lists for link/image/media/template dialogs
            template_external_list_url: "lists/template_list.js",
            external_link_list_url: "lists/link_list.js",
            external_image_list_url: "lists/image_list.js",
            media_external_list_url: "lists/media_list.js",
            //file_browser_callback : "tinyBrowser",
            // Replace values for the template plugin
            template_replace_values: {
                username: "Some User",
                staffid: "991234"
            }
        });


        //READ ONLY
        /*tinyMCE_GZ.init({
         mode: "textareas",
         editor_selector: "tinymceReadOnly",
         // Location of TinyMCE script
         script_url: _baseUrl + '/public/library/javascripts/tiny_mce/tiny_mce.js',
         // General options
         language: "pt",
         theme: "advanced",
         plugins: "safari,pagebreak,style,layer,table,save,advhr,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,gsynuhimgupload",
         // Theme options
         theme_advanced_buttons1: "",
         theme_advanced_buttons2: "",
         //		theme_advanced_buttons3 : "hr,removeformat,visualaid,|,sub,sup,|,charmap",
         theme_advanced_buttons3: "",
         theme_advanced_toolbar_location: "top",
         theme_advanced_toolbar_align: "left",
         relative_urls: false,
         readonly: true,
         // Example content CSS (should be your site CSS)
         content_css: _baseUrl + "/public/library/javascripts/tiny_mce/themes/advanced/skins/default/content.css",
         // Drop lists for link/image/media/template dialogs
         template_external_list_url: "lists/template_list.js",
         external_link_list_url: "lists/link_list.js",
         external_image_list_url: "lists/image_list.js",
         media_external_list_url: "lists/media_list.js"
         });*/

        tinyMCE.init({
            mode: "textareas",
            editor_selector: "tinymceReadOnly",
            // Location of TinyMCE script
            script_url: _baseUrl + '/public/library/javascripts/tiny_mce/tiny_mce.js',
            // General options
            language: "en",
            theme: "advanced",
            plugins: "safari,pagebreak,style,layer,table,save,advhr,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,gsynuhimgupload",
            // Theme options
            theme_advanced_buttons1: "",
            theme_advanced_buttons2: "",
            //		theme_advanced_buttons3 : "hr,removeformat,visualaid,|,sub,sup,|,charmap",
            theme_advanced_buttons3: "",
            theme_advanced_toolbar_location: "top",
            theme_advanced_toolbar_align: "left",
            relative_urls: false,
            readonly: true,
            // Example content CSS (should be your site CSS)
            content_css: _baseUrl + "/public/library/javascripts/tiny_mce/themes/advanced/skins/default/content.css",
            // Drop lists for link/image/media/template dialogs
            template_external_list_url: "lists/template_list.js",
            external_link_list_url: "lists/link_list.js",
            external_image_list_url: "lists/image_list.js",
            media_external_list_url: "lists/media_list.js"
        });



    }
}



function initBtnPageLocalizar() {
    if (_permissaoCadastrar != 'N') {
        enabledButton('#btnNovo');
    }
//enabledButton('#btnImprimir');			
}

function initBtnPageFormulario() {
    //alert('ok'); return false;
    var buttons = ' <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> ';
    buttons += '  <div class="btn-group">';
    buttons += ' <button class="btn btn-default" id="btnNovo" style="display: none;">';
    buttons += '<span class="glyphicon glyphicon-plus"></span>Novo </button>';

    buttons += '<button class="btn btn-success" id="btnSalvar" style="display: none;">';
    buttons += ' <span class="glyphicon glyphicon-saved"></span>Salvar</button>';

    buttons += '<button class="btn btn-info" id="btnListar" style="display: none;">';
    buttons += '<span class="glyphicon glyphicon-list-alt"></span>Listar</button>';


    buttons += ' <button class="btn btn-danger" id="btnExcluir" style="display: none;">';
    buttons += '<span class="glyphicon glyphicon-remove"></span>Excluir</button>';
    buttons += ' </div>';
    buttons += '</div>';
    $('#divBotoes').html(buttons);


    enabledButton('#btnCancelar');
    $('#divButtons').show();

    if (_acao == 'salvar' && _permissaoCadastrar != 'N') {
        enabledButton('#btnSalvar');
    }

    if (_acao == 'excluir' && _permissaoExcluir != 'N') {
        enabledButton('#btnExcluir');
        /** esconde os botoes e links que não podem ser acionados no excluir*/
        _closeButtonsExcluir();
    }

    if (_exibeBtnNovo == 'S' && _acao != 'excluir' && _permissaoCadastrar != 'N') {
        enabledButton('#btnNovo');
    }
    if (_action == 'index') {
        enabledButton('#btnNovo');
    } else if (_action == 'formulario') {
        enabledButton('#btnSalvar');
        enabledButton('#btnListar');
    }
    enabledButton('#btnNovo');

    $('#btnListar').click(function () {
        window.location = _urlPadrao;
    });

}

/** esconde os botoes e links que não podem ser acionados no excluir*/
function _closeButtonsExcluir() {
    /** input file readonly*/
    $('[type="file"]').css('visibility', 'hidden');

    /** não deixa entrar no campo pois se ele tiver mascara o readonly não funciona */
    $(':input:not(:button)').addClass('readonly').attr('disabled', true);

    /** pode haver botoes de salvar e novo no form, então, esconde eles*/
    $('.save').removeClass('displayButton').css('display', 'none');
    $('.new').removeClass('displayButton').css('display', 'none');

    $('.btnRemover').remove();

    /** pode haver grid no form, então retira as ações do grid */
    $('.delGrid').css('display', 'none');
    $('.editGrid').css('display', 'none');
    $('.search').css('display', 'none');

    $('.removerSmall').hide();
    $('.btnCadastrarModal').hide();

    //habilita o input de busca de menu que pode ter sido desabilitado indevidamente
    $('#inputBuscaMenu').attr('disabled', false);
    $('#inputBuscaMenu').removeClass('readonly');

    $('.removeInExcluir').remove();
}

function disabledButton(id) {
    $(id).css('display', 'none');
}

function enabledButton(id) {
    $(id).css('display', 'block');
}

//var qtdShowAguarde = 0;
//var qtdHideAguarde = 0;

function ShowMsgAguarde() {

    if (_Module == 'admin') {

        html = '<div class="modal bs-modal-sm" id="div-aguarde-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="z-index:1050;">'
                + '<div class="modal-dialog modal-sm" style="max-width:200px!important">'
                + '  <div class="modal-content">'
                + '<div class="modal-header">'
                + '     <h4 class="modal-title" id="myModalLabel">Aguarde...</h4>'
                + '   </div>'
                + '    <div class="modal-body" style="text-align:center">'
                + '      <img src=' + _baseUrl + '/public/admin/images/loading_azul.gif></h1>'
                + '    </div>'
                + '  </div>'
                + ' </div>'
                + '</div>';

        if ($('#div-aguarde-modal').length > 0) {
            $('#div-aguarde-modal').modal('hide');
            //$('#div-aguarde-modal').remove();
        } else {
            $('body').append(html);
        }

        $('#div-aguarde-modal').modal({
            backdrop: 'static'
        });
    }
//qtdShowAguarde++;

//$('body').append('<br>qtdShowAguarde:' + qtdShowAguarde);
}

function CloseMsgAguarde() {
    if (_Module == 'admin') {
        $('#div-aguarde-modal').modal('hide');
        $('.modal-backdrop').hide();
    }
//qtdHideAguarde++;

//$('body').append('<br>qtdHideAguarde:' + qtdHideAguarde);
}

/** Ao excluir remove a função de validação do form */
function excluirGlobal() {
    formobj = $('#validate');
    $(formobj).unbind("submit");
    $(formobj).die("submit");
    $(formobj).bind("submit", function () {
        return true;
    });
    $(formobj).live("submit", function () {
        return true;
    });
}



function moneyToFloat($vl) {

    if ($.trim($vl) == '') {
        return 0;
    } else {
        $vl = str_replace('.', '', $vl);
        $vl = str_replace(',', '.', $vl);
        return parseFloat($vl);
    }
}

function floatToMoney($vl) {
    return number_format($vl, 2, ',', '.');
}

function number_format(number, decimals, dec_point, thousands_sep) {
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
            prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
            sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
            dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
            s = '',
            toFixedFix = function (n, prec) {
                var k = Math.pow(10, prec);
                valor = (n * k).toFixed(prec);
                return '' + (Math.round(valor) / k)
                        .toFixed(prec);
            };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n))
            .split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '')
            .length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1)
                .join('0');
    }
    return s.join(dec);
}

function strtotime(str, now) {
    var i, match, s, strTmp = '',
            parse = '';

    strTmp = str;
    strTmp = strTmp.replace(/\s{2,}|^\s|\s$/g, ' '); // unecessary spaces
    strTmp = strTmp.replace(/[\t\r\n]/g, ''); // unecessary chars
    if (strTmp == 'now') {
        return (new Date()).getTime() / 1000; // Return seconds, not milli-seconds
    } else if (!isNaN(parse = Date.parse(strTmp))) {
        return (parse / 1000);
    } else if (now) {
        now = new Date(now * 1000); // Accept PHP-style seconds
    } else {
        now = new Date();
    }

    strTmp = strTmp.toLowerCase();

    var __is = {
        day: {
            'sun': 0,
            'mon': 1,
            'tue': 2,
            'wed': 3,
            'thu': 4,
            'fri': 5,
            'sat': 6
        },
        mon: {
            'jan': 0,
            'feb': 1,
            'mar': 2,
            'apr': 3,
            'may': 4,
            'jun': 5,
            'jul': 6,
            'aug': 7,
            'sep': 8,
            'oct': 9,
            'nov': 10,
            'dec': 11
        }
    };

    var process = function (m) {
        var ago = (m[2] && m[2] == 'ago');
        var num = (num = m[0] == 'last' ? -1 : 1) * (ago ? -1 : 1);

        switch (m[0]) {
            case 'last':
            case 'next':
                switch (m[1].substring(0, 3)) {
                    case 'yea':
                        now.setFullYear(now.getFullYear() + num);
                        break;
                    case 'mon':
                        now.setMonth(now.getMonth() + num);
                        break;
                    case 'wee':
                        now.setDate(now.getDate() + (num * 7));
                        break;
                    case 'day':
                        now.setDate(now.getDate() + num);
                        break;
                    case 'hou':
                        now.setHours(now.getHours() + num);
                        break;
                    case 'min':
                        now.setMinutes(now.getMinutes() + num);
                        break;
                    case 'sec':
                        now.setSeconds(now.getSeconds() + num);
                        break;
                    default:
                        var day;
                        if (typeof (day = __is.day[m[1].substring(0, 3)]) != 'undefined') {
                            var diff = day - now.getDay();
                            if (diff == 0) {
                                diff = 7 * num;
                            } else if (diff > 0) {
                                if (m[0] == 'last') {
                                    diff -= 7;
                                }
                            } else {
                                if (m[0] == 'next') {
                                    diff += 7;
                                }
                            }
                            now.setDate(now.getDate() + diff);
                        }
                }
                break;

            default:
                if (/\d+/.test(m[0])) {
                    num *= parseInt(m[0], 10);

                    switch (m[1].substring(0, 3)) {
                        case 'yea':
                            now.setFullYear(now.getFullYear() + num);
                            break;
                        case 'mon':
                            now.setMonth(now.getMonth() + num);
                            break;
                        case 'wee':
                            now.setDate(now.getDate() + (num * 7));
                            break;
                        case 'day':
                            now.setDate(now.getDate() + num);
                            break;
                        case 'hou':
                            now.setHours(now.getHours() + num);
                            break;
                        case 'min':
                            now.setMinutes(now.getMinutes() + num);
                            break;
                        case 'sec':
                            now.setSeconds(now.getSeconds() + num);
                            break;
                    }
                } else {
                    return false;
                }
                break;
        }
        return true;
    };

    match = strTmp.match(/^(\d{2,4}-\d{2}-\d{2})(?:\s(\d{1,2}:\d{2}(:\d{2})?)?(?:\.(\d+))?)?$/);
    if (match != null) {
        if (!match[2]) {
            match[2] = '00:00:00';
        } else if (!match[3]) {
            match[2] += ':00';
        }

        s = match[1].split(/-/g);

        for (i in __is.mon) {
            if (__is.mon[i] == s[1] - 1) {
                s[1] = i;
            }
        }
        s[0] = parseInt(s[0], 10);

        s[0] = (s[0] >= 0 && s[0] <= 69) ? '20' + (s[0] < 10 ? '0' + s[0] : s[0] + '') : (s[0] >= 70 && s[0] <= 99) ? '19' + s[0] : s[0] + '';
        return parseInt(this.strtotime(s[2] + ' ' + s[1] + ' ' + s[0] + ' ' + match[2]) + (match[4] ? match[4] / 1000 : ''), 10);
    }

    var regex = '([+-]?\\d+\\s' + '(years?|months?|weeks?|days?|hours?|min|minutes?|sec|seconds?' + '|sun\\.?|sunday|mon\\.?|monday|tue\\.?|tuesday|wed\\.?|wednesday' + '|thu\\.?|thursday|fri\\.?|friday|sat\\.?|saturday)' + '|(last|next)\\s' + '(years?|months?|weeks?|days?|hours?|min|minutes?|sec|seconds?' + '|sun\\.?|sunday|mon\\.?|monday|tue\\.?|tuesday|wed\\.?|wednesday' + '|thu\\.?|thursday|fri\\.?|friday|sat\\.?|saturday))' + '(\\sago)?';

    match = strTmp.match(new RegExp(regex, 'gi')); // Brett: seems should be case insensitive per docs, so added 'i'
    if (match == null) {
        return false;
    }

    for (i = 0; i < match.length; i++) {
        if (!process(match[i].split(' '))) {
            return false;
        }
    }

    return (now.getTime() / 1000);
}

function floor(value) {
    return Math.floor(value);
}

function str_replace(search, replace, subject, count) {
    var i = 0,
            j = 0,
            temp = '',
            repl = '',
            sl = 0,
            fl = 0,
            f = [].concat(search),
            r = [].concat(replace),
            s = subject,
            ra = Object.prototype.toString.call(r) === '[object Array]',
            sa = Object.prototype.toString.call(s) === '[object Array]';
    s = [].concat(s);
    if (count) {
        this.window[count] = 0;
    }

    for (i = 0, sl = s.length; i < sl; i++) {
        if (s[i] === '') {
            continue;
        }
        for (j = 0, fl = f.length; j < fl; j++) {
            temp = s[i] + '';
            repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
            s[i] = (temp).split(f[j]).join(repl);
            if (count && s[i] !== temp) {
                this.window[count] += (temp.length - s[i].length) / f[j].length;
            }
        }
    }
    return sa ? s : s[0];
}

function reverseDate(data) {
    data = $.trim(data);
    data = data.replace('-', '/');
    data = data.replace('-', '/');
    data = data.split('/');
    data = data.reverse();
    data = $.trim(data.join('/'));
    return data;
}

/** recebe a data em formato americano e transforma em um numerico */
function dateToNumber(dt) {
    dt = dt.replace('-', '');
    dt = dt.replace('-', '');
    dt = dt.replace('/', '');
    dt = dt.replace('/', '');
    return parseInt(dt);
}

function removeAcento(text) {
    text = text.replace(new RegExp('[ÁÀÂÃ]', 'gi'), 'A');
    text = text.replace(new RegExp('[ÉÈÊ]', 'gi'), 'E');
    text = text.replace(new RegExp('[ÍÌÎ]', 'gi'), 'I');
    text = text.replace(new RegExp('[ÓÒÔÕ]', 'gi'), 'O');
    text = text.replace(new RegExp('[ÚÙÛ]', 'gi'), 'U');
    text = text.replace(new RegExp('[Ç]', 'gi'), 'C');

    text = text.replace(new RegExp('[áàâã]', 'gi'), 'a');
    text = text.replace(new RegExp('[éèê]', 'gi'), 'e');
    text = text.replace(new RegExp('[íìî]', 'gi'), 'i');
    text = text.replace(new RegExp('[óòôõ]', 'gi'), 'o');
    text = text.replace(new RegExp('[úùû]', 'gi'), 'u');
    text = text.replace(new RegExp('[ç]', 'gi'), 'c');

    text = text.toLowerCase();
    return text;
}

function replaceAll(str, de, para) {
    var pos = str.indexOf(de);
    while (pos > -1) {
        str = str.replace(de, para);
        pos = str.indexOf(de);
    }
    return (str);
}

/** data no formato americano */
function isDate(value) {
    /*var dateRegEx = new RegExp(/^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$|^(?:(?:(?:0?[13578]|1[02])(\/|-)31)|(?:(?:0?[1,3-9]|1[0-2])(\/|-)(?:29|30)))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^(?:(?:0?[1-9]|1[0-2])(\/|-)(?:0?[1-9]|1\d|2[0-8]))(\/|-)(?:[1-9]\d\d\d|\d[1-9]\d\d|\d\d[1-9]\d|\d\d\d[1-9])$|^(0?2(\/|-)29)(\/|-)(?:(?:0[48]00|[13579][26]00|[2468][048]00)|(?:\d\d)?(?:0[48]|[2468][048]|[13579][26]))$/);
     if (dateRegEx.test(value)) {
     if(value.substring(0,4) == '0000'){
     return false;
     }
     return true;
     }
     return false;*/


    var date = reverseDate(value);
    var ardt = new Array;
    var ExpReg = new RegExp("(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/[12][0-9]{3}");
    ardt = date.split("/");
    erro = false;
    if (date.search(ExpReg) == -1) {
        erro = true;
    }
    else if (((ardt[1] == 4) || (ardt[1] == 6) || (ardt[1] == 9) || (ardt[1] == 11)) && (ardt[0] > 30))
        erro = true;
    else if (ardt[1] == 2) {
        if ((ardt[0] > 28) && ((ardt[2] % 4) != 0))
            erro = true;
        if ((ardt[0] > 29) && ((ardt[2] % 4) == 0))
            erro = true;
    }
    if (erro) {
        return false;
    }
    return true;
}

/** formata com caracter v informado a esquerda ate que a string atinja o tamanho tam*/
function lpad(tam, n, v) {
    while ((n + '').length <= tam) {
        n = v + n;
    }
    return n;
}

$.fn.selectSorting = function () {
    var selectedVal = $(this).val();
    var my_options = $(this).find("option");
    my_options.sort(function (a, b) {
        if (a.text > b.text)
            return 1;
        else if (a.text < b.text)
            return -1;
        else
            return 0
    })
    $(this).empty().append(my_options);
    $(this).val(selectedVal);
}


function isDateString(val) {
    val = $.trim(val);
    val = val.toUpperCase();
    if (val.indexOf('JANEIRO') == 0) {
        return true;
    } else if (val.indexOf('FEVEREIRO') == 0) {
        return true;
    } else if (val.indexOf('MARÇO') == 0 || val.indexOf('MARCO') == 0) {
        return true;
    } else if (val.indexOf('ABRIL') == 0) {
        return true;
    } else if (val.indexOf('MAIO') == 0) {
        return true;
    } else if (val.indexOf('JUNHO') == 0) {
        return true;
    } else if (val.indexOf('JULHO') == 0) {
        return true;
    } else if (val.indexOf('AGOSTO') == 0) {
        return true;
    } else if (val.indexOf('SETEMBRO') == 0) {
        return true;
    } else if (val.indexOf('OUTUBRO') == 0) {
        return true;
    } else if (val.indexOf('NOVEMBRO') == 0) {
        return true;
    } else if (val.indexOf('DEZEMBRO') == 0) {
        return true;
    }
    return false;
}

$.fn.populateSelectJson = function (data)
{
    /* gera o html das options */
    isdata = true;
    var options = '<option value=""></option>';
    $.each(data, function (key, val) {
        if ($.trim(val) != '') {
            options += '<option value="' + key + '">' + val.toUpperCase() + '</option>';
            if (!isDate(val) && !isDate(reverseDate(val)) && !isDateString(val)) {
                isdata = false;
            }
        }
    });
    /* seta as options na combo turma */
    obj = $(this).empty().html(options)

    /** só ordena se os valores não forem datas */
    if (!isdata) {
        return obj.selectSorting();
    } else {
        return obj;
    }
}

function urlencode(str) {
    str = (str + '').toString();
    return encodeURIComponent(str).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').
            replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
}

function urlencodeGet(str) {
    str = (str + '').toString();
    str = str.replace(/!/g, '{1}');
    str = str_replace('/', '{2}', str);
    str = str_replace('(', '{3}', str);
    str = str_replace(')', '{4}', str);
    str = str_replace('*', '{5}', str);
    str = str_replace('%', '{6}', str);
    str = str_replace('#', '{7}', str);
    str = str_replace(' ', '{}', str);

    str = str_replace('"', '{8}', str);
    str = str_replace("'", '{9}', str);
    str = str_replace('=', '{10}', str);

    return str;
}

function arredondarNota(nota) {
    if (nota != '') {
        nota = str_replace(',', '.', nota);
        vetorNota = nota.split('.');
        $parteInteira = vetorNota[0];
        $decimal = vetorNota[1];

        if ($decimal < 25) {
            $decimal = '00';
        } else if ($decimal >= 25 && $decimal <= 74) {
            $decimal = '50';
        } else {
            $decimal = '00';
            $parteInteira = (parseInt($parteInteira) + 1);
        }

        /** monta a nota novamente */
        nota = ($parteInteira + ',' + $decimal);
        return nota;
    } else {
        return '';
    }
}

/** arredonda campos de lancamento de nota **/
$('.arredondar').blur(function () {
    nota = $.trim($(this).val());
    nota = arredondarNota(nota);
    $(this).val(nota);
});

/** arredonda campos de lancamento de nota **/
function initArredondar() {
    $('.arredondar').blur(function () {
        nota = $.trim($(this).val());
        nota = arredondarNota(nota);
        $(this).val(nota);
    });
}

function array_unique(inputArr) {
    var key = '', tmp_arr2 = {},
            val = '';

    var __array_search = function (needle, haystack) {
        var fkey = '';
        for (fkey in haystack) {
            if (haystack.hasOwnProperty(fkey)) {
                if ((haystack[fkey] + '') === (needle + '')) {
                    return fkey;
                }
            }
        }
        return false;
    };
    for (key in inputArr) {
        if (inputArr.hasOwnProperty(key)) {
            val = inputArr[key];
            if (false === __array_search(val, tmp_arr2)) {
                tmp_arr2[key] = val;
            }
        }
    }

    return tmp_arr2;
}

function isHoraValida(strHora) {
    if ($.trim(strHora) != '') {
        hora = strHora.substring(0, 2);
        minuto = strHora.substring(3, 5);

        if ($.trim(hora) == '' || $.trim(minuto) == '') {
            return false;
        } else {
            hrs = parseInt(hora, 10);
            min = parseInt(minuto, 10);
            if ((hrs < 00) || (hrs > 23) || (min < 00) || (min > 59)) {
                return false;
            }
        }
    }
    return true;
}

function converteMinutos(horario) {
    arrHora = horario.split(':');
    minutos = (60 * parseInt($.trim(arrHora[0]), 10)) + parseInt($.trim(arrHora[1]), 10);
    return minutos;
}

/** soma um mês na data. Data no formato Brasileiro */
function somaMesData(data, qtdMes) {
    data = str_replace('-', '/', data);
    data = data.split('/');
    dia = parseInt(data[0], 10);
    mes = parseInt(data[1], 10);
    ano = parseInt(data[2], 10);

    for (iM = 1; iM <= qtdMes; iM++) {
        mes++;
        if (mes > 12) {
            ano = ano + 1;
            mes = 1;
        }
    }

    nova_data = lpad(1, dia, '0') + '/' + lpad(1, mes, '0') + '/' + ano;
    dia_novo = dia;
    while (!isDate(reverseDate(nova_data)) && dia_novo > 28) {
        dia_novo = dia_novo - 1;
        nova_data = lpad(1, dia_novo, '0') + '/' + lpad(1, mes, '0') + '/' + ano;
    }
    return nova_data;
}

function isEmail(email) {
    var exclude = /[^@\-\.\w]|^[_@\.\-]|[\._\-]{2}|[@\.]{2}|(@)[^@]*\1/;
    var check = /@[\w\-]+\./;
    var checkend = /\.[a-zA-Z]{2,3}$/;
    if (((email.search(exclude) != -1) || (email.search(check)) == -1) || (email.search(checkend) == -1)) {
        return false;
    }
    else {
        return true;
    }
}

function configuraCamposTipoPessoaCliente() {
    $('#cooperado-tx_tipopessoa-F').click(function () {
        $('.PessoaFisica').show().removeClass('hide');
        $('.PessoaJuridica').hide();
        $('#lblNomeCliente').text('Nome do ' + _tratativaContratante + ':');
    });

    $('#cooperado-tx_tipopessoa-J').click(function () {
        $('.PessoaFisica').hide();
        $('.PessoaJuridica').show().removeClass('hide');
        $('#lblNomeCliente').text('Razão Social:');
    });

    $('#cooperado-tx_tipopessoa-').click(function () {
        $('.PessoaFisica').show().removeClass('hide');
        $('.PessoaJuridica').show().removeClass('hide');
        $('#lblNomeCliente').text('Nome do ' + _tratativaContratante + ':');
    });

    if ($('#cooperado-tx_tipopessoa-F').is(':checked')) {
        $('#cooperado-tx_tipopessoa-F').click();
    } else if ($('#cooperado-tx_tipopessoa-J').is(':checked')) {
        $('#cooperado-tx_tipopessoa-J').click();
    } else if ($('#cooperado-tx_tipopessoa-').is(':checked')) {
        $('.PessoaFisica').show().removeClass('hide');
        $('.PessoaJuridica').show().removeClass('hide');
    }
}

function getDatePicker(datepickerId, dataPickerOnSelect, SelectedDates) {
    /** valida se a data fornecida é menor que a data atual*/

    $("#" + datepickerId).datepicker("destroy");

    datePicker = $("#" + datepickerId).datepicker({
        buttonImage: _baseUrl + "/public/default/images/icones/calendario.gif",
        buttonImageOnly: true,
        showAnim: 'clip',
        dateFormat: 'dd/mm/yy',
        dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado', 'Domingo'],
        dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
        dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
        monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
        monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
        nextText: 'Próximo',
        prevText: 'Anterior',
        beforeShowDay: function (date)
        {
            if (SelectedDates != null || SelectedDates != undefined) {
                date = $.datepicker.formatDate('dd/mm/yy', date);
                var Highlight = SelectedDates[date];
                if (Highlight) {
                    return [true, "Highlighted", Highlight];
                }
                else {
                    return [true, '', ''];
                }
            } else {
                return [true, '', ''];
            }
        }
    }).keypress(function () {
        return false;
    });

    if (dataPickerOnSelect != null || dataPickerOnSelect != undefined) {
        datePicker.datepicker("option", "onSelect", dataPickerOnSelect);
    }

    return datePicker;
}

function getDataTables(tableId, rows) {
    //EM TODA TELE QUE USAR ESTE DATATABLE CRIAR A FUNCAO 'filtro()' PERSONALIZADA   
    $.fn.dataTableExt.afnFiltering.push(
            function (oSettings, aData, iDataIndex) {

                return filtro(aData);

            });



    oTable = $('#' + tableId).dataTable({//configuração e inicialização do plugin
        "bPaginate": true,
        "bJQueryUI": false,
        "bFilter": true,
        "bInfo": false,
        "bSort": false,
        "bAutoWidth": false,
        "bLengthChange": false,
        "iDisplayLength": rows,
        "sPaginationType": "full_numbers",
        "sDom": '<i>rt<lp>',
        "oLanguage": {
            "sProcessing": "Processando...",
            "sLengthMenu": "Exibir _MENU_ registros",
            "sZeroRecords": "Não foram encontrados resultados",
            "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Exibindo de 0 a 0 de 0 registros",
            "sInfoFiltered": "(filtrado de _MAX_ registros no total)",
            "sInfoPostFix": "",
            "sSearch": "Busca Rápida:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "««",
                "sPrevious": "«",
                "sNext": "»",
                "sLast": "»»"
            }
        }

    });
    return oTable;
}

function dateToInt(date) {
    arrDate = explode('/', $.trim(date));
    return parseInt(arrDate[2] + arrDate[1] + arrDate[0]);
}


function getTime(seconds) {

    //a day contains 60 * 60 * 24 = 86400 seconds
    //an hour contains 60 * 60 = 3600 seconds
    //a minut contains 60 seconds
    //the amount of seconds we have left
    var leftover = seconds;

    //how many full days fits in the amount of leftover seconds
    var days = Math.floor(leftover / 86400);

    //how many seconds are left
    leftover = leftover - (days * 86400);

    //how many full hours fits in the amount of leftover seconds
    var hours = Math.floor(leftover / 3600);

    //how many seconds are left
    leftover = leftover - (hours * 3600);

    //how many minutes fits in the amount of leftover seconds
    var minutes = leftover / 60;

    //how many seconds are left
    //leftover = leftover - (minutes * 60);
    return hours + ':' + minutes;
}

//FUNCOES DE BUSCA PARA DATATABLE
function isTxEquals(textA, textB) {
    if ($.trim(textA) == $.trim(textB)) {
        return true;
    } else {
        return false;
    }
}

function inTargetValue(target, start, end) {

    if (start != null && end != null) {
        if (target >= start && target <= end) {
            return true;
        } else {
            return false;
        }
    } else if (start != null) {
        if (target >= start) {
            return true;
        } else {
            return false;
        }
    } else if (end != null) {
        if (target <= end) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }

}

function imprimirContratoConfissaoDivida(id_contratorenegociacao) {
    /** url do método que gera o contrato */
    var url = _baseUrl + '/admin/contrato-confissao-divida/render-pdf-contrato/id_contratorenegociacao/' + id_contratorenegociacao;

    /**caso exista, removee o link que será clicado para evitar que sejam adicionados mais de um elemento igual */
    $('#linkImprimirContrato').remove();

    /** adiciona o link que será "clicado"*/
    $('body').append('<a href="' + url + '" target="_blank" id="linkImprimirContrato"></a>');

    /** adiciona o método para abrir a popup */
    $("#linkImprimirContrato").popupWindow({
        height: 500,
        width: 800,
        top: 50,
        left: 50
    });

    /** abre a popup */
    $("#linkImprimirContrato").click();
}


function configModalParcela(id) {
//deixei esse método sem compo aqui apenas para evitar erros em chamadas em alguma tela que ainda não foi removida
}

function closeModalParcela(id) {
    /** elimina a modal da memória */
    $("#dialog-parcelas-" + id).modal('hide');
}

function openModalParcela(id) {
    $("#dialog-parcelas-" + id).modal();
}


function minimizarModal(id) {
    $(".trParcela_" + id).hide();
    $("#maximizar_" + id).show();
    $("#minimizar_" + id).hide();
}

function maximizarModal(id) {
    $(".trParcela_" + id).show();
    $("#maximizar_" + id).hide();
    $("#minimizar_" + id).show();
}


function removeHighcharts() {
    $('*').find('tspan').each(function () {
        var text = $(this).text();
        $(this).text(text.replace('Highcharts.com', ''));
    })
}

function openModalDepartamento() {

    /** url do método que gera o recibo */
    url = _baseUrl + '/admin/departamento/formulario/ismodal/S';
    name = 'CADASTRO DE DEPARTAMENTO';
    height = 550;
    width = 850;

    //chama a modal e fica aguardando o fechamento da janela nessa linha para somente depois prosseguir com o script
    //modalWin(url, name, height, width);
    ShowMsgAguarde();
    modalWin(url, name, height, width, function () {
        limpaSessaoIsModal();

        //atualiza as combos de departamento

        ShowMsgAguarde();
        $.getJSON(_baseUrl + '/ajax/json-combo-departamento', null, function (data) {

            //para cada campo de departamento atualiza os options
            $('.addBtnCadastrarModalDepartamento').each(function () {
                valueSelected = $(this).val();
                $(this).populateSelectJson(data);
                $(this).val(valueSelected);
            });

            CloseMsgAguarde();
        });
    });
}

//addBtnCadastrarModalTipoSolicitacao

function openModalTipoSolicitacao() {

    /** url do método que gera o recibo */
    url = _baseUrl + '/admin/tipo-solicitacao/formulario/ismodal/S';
    name = 'CADASTRO DE TIPO SOLICITAÇÃO';
    height = 550;
    width = 850;

    //chama a modal e fica aguardando o fechamento da janela nessa linha para somente depois prosseguir com o script
    //modalWin(url, name, height, width);
    ShowMsgAguarde();
    modalWin(url, name, height, width, function () {
        limpaSessaoIsModal();

        //atualiza as combos de departamento

        ShowMsgAguarde();
        $.getJSON(_baseUrl + '/ajax/json-combo-tipo-solicitacao', null, function (data) {

            //para cada campo de tipo solicitacao atualiza os options
            $('.addBtnCadastrarModalTipoSolicitacao').each(function () {
                valueSelected = $(this).val();
                $(this).populateSelectJson(data);
                $(this).val(valueSelected);
            });

            CloseMsgAguarde();
        });
    });
}


function asort(inputArr, sort_flags) {
    var valArr = [],
            valArrLen = 0,
            k, i, ret, sorter, that = this,
            strictForIn = false,
            populateArr = {};

    switch (sort_flags) {
        case 'SORT_STRING':
            // compare items as strings
            sorter = function (a, b) {
                return that.strnatcmp(a, b);
            };
            break;
        case 'SORT_LOCALE_STRING':
            // compare items as strings, original by the current locale (set with i18n_loc_set_default() as of PHP6)
            var loc = this.i18n_loc_get_default();
            sorter = this.php_js.i18nLocales[loc].sorting;
            break;
        case 'SORT_NUMERIC':
            // compare items numerically
            sorter = function (a, b) {
                return (a - b);
            };
            break;
        case 'SORT_REGULAR':
            // compare items normally (don't change types)
        default:
            sorter = function (a, b) {
                var aFloat = parseFloat(a),
                        bFloat = parseFloat(b),
                        aNumeric = aFloat + '' === a,
                        bNumeric = bFloat + '' === b;
                if (aNumeric && bNumeric) {
                    return aFloat > bFloat ? 1 : aFloat < bFloat ? -1 : 0;
                } else if (aNumeric && !bNumeric) {
                    return 1;
                } else if (!aNumeric && bNumeric) {
                    return -1;
                }
                return a > b ? 1 : a < b ? -1 : 0;
            };
            break;
    }

    // BEGIN REDUNDANT
    this.php_js = this.php_js || {};
    this.php_js.ini = this.php_js.ini || {};
    // END REDUNDANT
    strictForIn = this.php_js.ini['phpjs.strictForIn'] && this.php_js.ini['phpjs.strictForIn'].local_value && this.php_js
            .ini['phpjs.strictForIn'].local_value !== 'off';
    populateArr = strictForIn ? inputArr : populateArr;

    // Get key and value arrays
    for (k in inputArr) {
        if (inputArr.hasOwnProperty(k)) {
            valArr.push([k, inputArr[k]]);
            if (strictForIn) {
                delete inputArr[k];
            }
        }
    }

    valArr.sort(function (a, b) {
        return sorter(a[1], b[1]);
    });

    // Repopulate the old array
    for (i = 0, valArrLen = valArr.length; i < valArrLen; i++) {
        populateArr[valArr[i][0]] = valArr[i][1];
    }

    return strictForIn || populateArr;
}



function sort(inputArr, sort_flags) {
    var valArr = [],
            keyArr = [],
            k = '',
            i = 0,
            sorter = false,
            that = this,
            strictForIn = false,
            populateArr = [];

    switch (sort_flags) {
        case 'SORT_STRING':
            // compare items as strings
            sorter = function (a, b) {
                return that.strnatcmp(a, b);
            };
            break;
        case 'SORT_LOCALE_STRING':
            // compare items as strings, original by the current locale (set with  i18n_loc_set_default() as of PHP6)
            var loc = this.i18n_loc_get_default();
            sorter = this.php_js.i18nLocales[loc].sorting;
            break;
        case 'SORT_NUMERIC':
            // compare items numerically
            sorter = function (a, b) {
                return (a - b);
            };
            break;
        case 'SORT_REGULAR':
            // compare items normally (don't change types)
        default:
            sorter = function (a, b) {
                var aFloat = parseFloat(a),
                        bFloat = parseFloat(b),
                        aNumeric = aFloat + '' === a,
                        bNumeric = bFloat + '' === b;
                if (aNumeric && bNumeric) {
                    return aFloat > bFloat ? 1 : aFloat < bFloat ? -1 : 0;
                } else if (aNumeric && !bNumeric) {
                    return 1;
                } else if (!aNumeric && bNumeric) {
                    return -1;
                }
                return a > b ? 1 : a < b ? -1 : 0;
            };
            break;
    }

    // BEGIN REDUNDANT
    try {
        this.php_js = this.php_js || {};
    } catch (e) {
        this.php_js = {};
    }

    this.php_js.ini = this.php_js.ini || {};
    // END REDUNDANT
    strictForIn = this.php_js.ini['phpjs.strictForIn'] && this.php_js.ini['phpjs.strictForIn'].local_value && this.php_js
            .ini['phpjs.strictForIn'].local_value !== 'off';
    populateArr = strictForIn ? inputArr : populateArr;

    for (k in inputArr) { // Get key and value arrays
        if (inputArr.hasOwnProperty(k)) {
            valArr.push(inputArr[k]);
            if (strictForIn) {
                delete inputArr[k];
            }
        }
    }

    valArr.sort(sorter);

    for (i = 0; i < valArr.length; i++) { // Repopulate the old array
        populateArr[i] = valArr[i];
    }
    return strictForIn || populateArr;
}

function initEditor(upload) {
    var fnUpload = null;

    if (upload) {
        fnUpload = function (field_name, url, type, win) {
            if (type == 'image') {
                $('#form-img-upload input').click();
            }
        };
    }

    tinymce.init({
        selector: ".tinymce",
        statusbar: false,
        relative_urls: false,
        remove_script_host: false,
        convert_urls: false,
        plugins: [
            ["advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker"],
            ["searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking"],
            ["save table contextmenu directionality emoticons template paste"],
        ],
        file_browser_callback: fnUpload,
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        }
    });
}


function logar(formulario, url) {
    //alert(url);return false;

    var urlRequest = formulario.attr('action');
    var data = formulario.serialize();
    ShowMsgAguarde();
    $.ajax({
        url: urlRequest,
        type: 'POST',
        dataType: 'json',
        data: data,
        success: function (data) {
            if (data !== undefined && data.success !== undefined && data.success == 'S') {
                window.location = url;
            }
            else if (data !== undefined && data.error !== undefined && data.error !== '') {
                Dialog.error(data.error, 'Erro');
            } else {
                Dialog.error(_erroPadraoAjax, 'Erro');
            }
        },
        error: function () {
            Dialog.error(_erroPadraoAjax, 'Erro');
        },
        complete: function () {
            CloseMsgAguarde();
        }
    });
}

function validarCPF(cpf) {
    cpf = cpf.replace(/[^\d]+/g, '');
    if (cpf == '')
        return false;
    // Elimina CPFs invalidos conhecidos    
    if (cpf.length != 11 ||
            cpf == "00000000000" ||
            cpf == "11111111111" ||
            cpf == "22222222222" ||
            cpf == "33333333333" ||
            cpf == "44444444444" ||
            cpf == "55555555555" ||
            cpf == "66666666666" ||
            cpf == "77777777777" ||
            cpf == "88888888888" ||
            cpf == "99999999999")
        return false;
    // Valida 1o digito 
    add = 0;
    for (i = 0; i < 9; i ++)
        add += parseInt(cpf.charAt(i)) * (10 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(9)))
        return false;
    // Valida 2o digito 
    add = 0;
    for (i = 0; i < 10; i ++)
        add += parseInt(cpf.charAt(i)) * (11 - i);
    rev = 11 - (add % 11);
    if (rev == 10 || rev == 11)
        rev = 0;
    if (rev != parseInt(cpf.charAt(10)))
        return false;
    return true;
}













    