//STRING
 
function strtr (str, from, to) {
    // http://kevin.vanzonneveld.net
    // +   original by: Brett Zamir (http://brett-zamir.me)
    // +      input by: uestla
    // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +      input by: Alan C
    // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +      input by: Taras Bogach
    // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
    // +      input by: jpfle
    // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
    // -   depends on: krsort
    // -   depends on: ini_set
    // *     example 1: $trans = {'hello' : 'hi', 'hi' : 'hello'};
    // *     example 1: strtr('hi all, I said hello', $trans)
    // *     returns 1: 'hello all, I said hi'
    // *     example 2: strtr('�aaba�ccasde�oo', '���','aao');
    // *     returns 2: 'aaabaaccasdeooo'
    // *     example 3: strtr('��������', '�', 'a');
    // *     returns 3: 'aaaaaaaa'
    // *     example 4: strtr('http', 'pthxyz','xyzpth');
    // *     returns 4: 'zyyx'
    // *     example 5: strtr('zyyx', 'pthxyz','xyzpth');
    // *     returns 5: 'http'
    // *     example 6: strtr('aa', {'a':1,'aa':2});
    // *     returns 6: '2'
    var fr = '',
    i = 0,
    j = 0,
    lenStr = 0,
    lenFrom = 0,
    tmpStrictForIn = false,
    fromTypeStr = '',
    toTypeStr = '',
    istr = '';
    var tmpFrom = [];
    var tmpTo = [];
    var ret = '';
    var match = false;

    // Received replace_pairs?
    // Convert to normal from->to chars
    if (typeof from === 'object') {
        tmpStrictForIn = this.ini_set('phpjs.strictForIn', false); // Not thread-safe; temporarily set to true
        from = this.krsort(from);
        this.ini_set('phpjs.strictForIn', tmpStrictForIn);

        for (fr in from) {
            if (from.hasOwnProperty(fr)) {
                tmpFrom.push(fr);
                tmpTo.push(from[fr]);
            }
        }

        from = tmpFrom;
        to = tmpTo;
    }

    // Walk through subject and replace chars when needed
    lenStr = str.length;
    lenFrom = from.length;
    fromTypeStr = typeof from === 'string';
    toTypeStr = typeof to === 'string';

    for (i = 0; i < lenStr; i++) {
        match = false;
        if (fromTypeStr) {
            istr = str.charAt(i);
            for (j = 0; j < lenFrom; j++) {
                if (istr == from.charAt(j)) {
                    match = true;
                    break;
                }
            }
        } else {
            for (j = 0; j < lenFrom; j++) {
                if (str.substr(i, from[j].length) == from[j]) {
                    match = true;
                    // Fast forward
                    i = (i + from[j].length) - 1;
                    break;
                }
            }
        }
        if (match) {
            ret += toTypeStr ? to.charAt(j) : to[j];
        } else {
            ret += str.charAt(i);
        }
    }

    return ret;
}

 
function ucfirst (str) {
    // http://kevin.vanzonneveld.net
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   bugfixed by: Onno Marsman
    // +   improved by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: ucfirst('kevin van zonneveld');
    // *     returns 1: 'Kevin van zonneveld'
    str += '';
    var f = str.charAt(0).toUpperCase();
    return f + str.substr(1);
}
    
function strtolower (str) {
    // http://kevin.vanzonneveld.net
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Onno Marsman
    // *     example 1: strtolower('Kevin van Zonneveld');
    // *     returns 1: 'kevin van zonneveld'
    return (str + '').toLowerCase();
}

function strtoupper (str) {
    // http://kevin.vanzonneveld.net
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Onno Marsman
    // *     example 1: strtoupper('Kevin van Zonneveld');
    // *     returns 1: 'KEVIN VAN ZONNEVELD'
    return (str + '').toUpperCase();
}

    
function explode(delimiter, string, limit) {

    if ( arguments.length < 2 || typeof delimiter === 'undefined' || typeof string === 'undefined' ) return null;
    if ( delimiter === '' || delimiter === false || delimiter === null) return false;
    if ( typeof delimiter === 'function' || typeof delimiter === 'object' || typeof string === 'function' || typeof string === 'object'){
        return {
            0: ''
        };
    }
    if ( delimiter === true ) delimiter = '1';

    // Here we go...
    delimiter += '';
    string += '';

    var s = string.split( delimiter );


    if ( typeof limit === 'undefined' ) return s;

    // Support for limit
    if ( limit === 0 ) limit = 1;

    // Positive limit
    if ( limit > 0 ){
        if ( limit >= s.length ) return s;
        return s.slice( 0, limit - 1 ).concat( [ s.slice( limit - 1 ).join( delimiter ) ] );
    }

    // Negative limit
    if ( -limit >= s.length ) return [];

    s.splice( s.length + limit );
    return s;
}


function implode (glue, pieces) {
    // http://kevin.vanzonneveld.net
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Waldo Malqui Silva
    // +   improved by: Itsacon (http://www.itsacon.net/)
    // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: implode(' ', ['Kevin', 'van', 'Zonneveld']);
    // *     returns 1: 'Kevin van Zonneveld'
    // *     example 2: implode(' ', {first:'Kevin', last: 'van Zonneveld'});
    // *     returns 2: 'Kevin van Zonneveld'
    var i = '',
    retVal = '',
    tGlue = '';
    if (arguments.length === 1) {
        pieces = glue;
        glue = '';
    }
    if (typeof(pieces) === 'object') {
        if (Object.prototype.toString.call(pieces) === '[object Array]') {
            return pieces.join(glue);
        }
        for (i in pieces) {
            retVal += tGlue + pieces[i];
            tGlue = glue;
        }
        return retVal;
    }
    return pieces;
}

function rtrim (str, charlist) {
    // http://kevin.vanzonneveld.net
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +      input by: Erkekjetter
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   bugfixed by: Onno Marsman
    // +   input by: rem
    // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: rtrim('    Kevin van Zonneveld    ');
    // *     returns 1: '    Kevin van Zonneveld'
    charlist = !charlist ? ' \\s\u00A0' : (charlist + '').replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '\\$1');
    var re = new RegExp('[' + charlist + ']+$', 'g');
    return (str + '').replace(re, '');
}
    
function ltrim (str, charlist) {
    // http://kevin.vanzonneveld.net
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +      input by: Erkekjetter
    // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   bugfixed by: Onno Marsman
    // *     example 1: ltrim('    Kevin van Zonneveld    ');
    // *     returns 1: 'Kevin van Zonneveld    '
    charlist = !charlist ? ' \\s\u00A0' : (charlist + '').replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '$1');
    var re = new RegExp('^[' + charlist + ']+', 'g');
    return (str + '').replace(re, '');
}

//ARRAY

function in_array (needle, haystack, argStrict) {
    // http://kevin.vanzonneveld.net
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: vlado houba
    // +   input by: Billy
    // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: in_array('van', ['Kevin', 'van', 'Zonneveld']);
    // *     returns 1: true
    // *     example 2: in_array('vlado', {0: 'Kevin', vlado: 'van', 1: 'Zonneveld'});
    // *     returns 2: false
    // *     example 3: in_array(1, ['1', '2', '3']);
    // *     returns 3: true
    // *     example 3: in_array(1, ['1', '2', '3'], false);
    // *     returns 3: true
    // *     example 4: in_array(1, ['1', '2', '3'], true);
    // *     returns 4: false
    var key = '',
    strict = !! argStrict;

    if (strict) {
        for (key in haystack) {
            if (haystack[key] === needle) {
                return true;
            }
        }
    } else {
        for (key in haystack) {
            if (haystack[key] == needle) {
                return true;
            }
        }
    }

    return false;
}

function array () {
    // http://kevin.vanzonneveld.net
    // +   original by: d3x
    // +      improved by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: array('Kevin', 'van', 'Zonneveld');
    // *     returns 1: ['Kevin', 'van', 'Zonneveld']
    // *     example 2: ini_set('phpjs.return_phpjs_arrays', 'on');
    // *     example 2: var arr = array({0:2}, {a:41}, {2:3}).change_key_case('CASE_UPPER').keys();
    // *     returns 1: [0,'A',2]

    var arrInst, e, __, that = this, PHPJS_Array = function PHPJS_Array() {},
    mainArgs = arguments, p = this.php_js = this.php_js || {},
    _indexOf = function (value, from, strict) {
        var i = from || 0, nonstrict = !strict, length = this.length;
        while (i < length) {
            if (this[i] === value || (nonstrict && this[i] == value)) {
                return i;
            }
            i++;
        }
        return -1;
    };
    // BEGIN REDUNDANT
    if (!p.Relator) {
        p.Relator = (function () {// Used this functional class for giving privacy to the class we are creating
            // Code adapted from http://www.devpro.it/code/192.html
            // Relator explained at http://webreflection.blogspot.com/2008/07/javascript-relator-object-aka.html
            // Its use as privacy technique described at http://webreflection.blogspot.com/2008/10/new-relator-object-plus-unshared.html
            // 1) At top of closure, put: var __ = Relator.$();
            // 2) In constructor, put: var _ = __.constructor(this);
            // 3) At top of each prototype method, put: var _ = __.method(this);
            // 4) Use like:  _.privateVar = 5;
            function _indexOf (value) {
                var i = 0, length = this.length;
                while (i < length) {
                    if (this[i] === value) {
                        return i;
                    }
                    i++;
                }
                return -1;
            }
            function Relator () {
                var Stack = [], Array = [];
                if (!Stack.indexOf) {
                    Stack.indexOf = _indexOf;
                }
                return {
                    // create a new relator
                    $ : function () {
                        return Relator();
                    },
                    constructor : function (that) {
                        var i = Stack.indexOf(that);
                        ~i ? Array[i] : Array[Stack.push(that) - 1] = {};
                        this.method(that).that = that;
                        return this.method(that);
                    },
                    method : function (that) {
                        return Array[Stack.indexOf(that)];
                    }
                };
            }
            return Relator();
        }());
    }
    // END REDUNDANT

    if (p && p.ini && p.ini['phpjs.return_phpjs_arrays'].local_value.toLowerCase() === 'on') {
        if (!p.PHPJS_Array) {
            // We keep this Relator outside the class in case adding prototype methods below
            // Prototype methods added elsewhere can also use this ArrayRelator to share these "pseudo-global mostly-private" variables
            __ = p.ArrayRelator = p.ArrayRelator || p.Relator.$();
            // We could instead allow arguments of {key:XX, value:YY} but even more cumbersome to write
            p.PHPJS_Array = function PHPJS_Array () {
                var _ = __.constructor(this), args = arguments, i = 0, argl, p;
                args = (args.length === 1 && args[0] && typeof args[0] === 'object' &&
                    args[0].length && !args[0].propertyIsEnumerable('length')) ? args[0] : args; // If first and only arg is an array, use that (Don't depend on this)
                if (!_.objectChain) {
                    _.objectChain = args;
                    _.object = {};
                    _.keys = [];
                    _.values = [];
                }
                for (argl = args.length; i < argl; i++) {
                    for (p in args[i]) {
                        // Allow for access by key; use of private members to store sequence allows these to be iterated via for...in (but for read-only use, with hasOwnProperty or function filtering to avoid prototype methods, and per ES, potentially out of order)
                        this[p] = _.object[p] = args[i][p];
                        // Allow for easier access by prototype methods
                        _.keys[_.keys.length] = p;
                        _.values[_.values.length] = args[i][p];
                        break;
                    }
                }
            };
            e = p.PHPJS_Array.prototype;
            e.change_key_case = function (cs) {
                var _ = __.method(this), oldkey, newkey, i = 0, kl = _.keys.length,
                case_fn = (!cs || cs === 'CASE_LOWER') ? 'toLowerCase' : 'toUpperCase';
                while (i < kl) {
                    oldkey = _.keys[i];
                    newkey = _.keys[i] = _.keys[i][case_fn]();
                    if (oldkey !== newkey) {
                        this[oldkey] = _.object[oldkey] = _.objectChain[i][oldkey] = null; // Break reference before deleting
                        delete this[oldkey];
                        delete _.object[oldkey];
                        delete _.objectChain[i][oldkey];
                        this[newkey] = _.object[newkey] = _.objectChain[i][newkey] = _.values[i]; // Fix: should we make a deep copy?
                    }
                    i++;
                }
                return this;
            };
            e.flip = function () {
                var _ = __.method(this), i = 0, kl = _.keys.length;
                while (i < kl) {
                    oldkey = _.keys[i];
                    newkey = _.values[i];
                    if (oldkey !== newkey) {
                        this[oldkey] = _.object[oldkey] = _.objectChain[i][oldkey] = null; // Break reference before deleting
                        delete this[oldkey];
                        delete _.object[oldkey];
                        delete _.objectChain[i][oldkey];
                        this[newkey] = _.object[newkey] = _.objectChain[i][newkey] = oldkey;
                        _.keys[i] = newkey;
                    }
                    i++;
                }
                return this;
            };
            e.walk = function (funcname, userdata) {
                var _ = __.method(this), obj, func, ini, i = 0, kl = 0;

                try {
                    if (typeof funcname === 'function') {
                        for (i = 0, kl = _.keys.length; i < kl; i++) {
                            if (arguments.length > 1) {
                                funcname(_.values[i], _.keys[i], userdata);
                            }
                            else {
                                funcname(_.values[i], _.keys[i]);
                            }
                        }
                    }
                    else if (typeof funcname === 'string') {
                        this.php_js = this.php_js || {};
                        this.php_js.ini = this.php_js.ini || {};
                        ini = this.php_js.ini['phpjs.no-eval'];
                        if (ini && (
                            parseInt(ini.local_value, 10) !== 0 && (!ini.local_value.toLowerCase || ini.local_value.toLowerCase() !== 'off')
                            )) {
                            if (arguments.length > 1) {
                                for (i = 0, kl = _.keys.length; i < kl; i++) {
                                    this.window[funcname](_.values[i], _.keys[i], userdata);
                                }
                            }
                            else {
                                for (i = 0, kl = _.keys.length; i < kl; i++) {
                                    this.window[funcname](_.values[i], _.keys[i]);
                                }
                            }
                        }
                        else {
                            if (arguments.length > 1) {
                                for (i = 0, kl = _.keys.length; i < kl; i++) {
                                    eval(funcname + '(_.values[i], _.keys[i], userdata)');
                                }
                            }
                            else {
                                for (i = 0, kl = _.keys.length; i < kl; i++) {
                                    eval(funcname + '(_.values[i], _.keys[i])');
                                }
                            }
                        }
                    }
                    else if (funcname && typeof funcname === 'object' && funcname.length === 2) {
                        obj = funcname[0];
                        func = funcname[1];
                        if (arguments.length > 1) {
                            for (i = 0, kl = _.keys.length; i < kl; i++) {
                                obj[func](_.values[i], _.keys[i], userdata);
                            }
                        }
                        else {
                            for (i = 0, kl = _.keys.length; i < kl; i++) {
                                obj[func](_.values[i], _.keys[i]);
                            }
                        }
                    }
                    else {
                        return false;
                    }
                }
                catch (e) {
                    return false;
                }

                return this;
            };
            // Here we'll return actual arrays since most logical and practical for these functions to do this
            e.keys = function (search_value, argStrict) {
                var _ = __.method(this), pos,
                search = typeof search_value !== 'undefined',
                tmp_arr = [],
                strict = !!argStrict;
                if (!search) {
                    return _.keys;
                }
                while ((pos = _indexOf(_.values, pos, strict)) !== -1) {
                    tmp_arr[tmp_arr.length] = _.keys[pos];
                }
                return tmp_arr;
            };
            e.values = function () {
                var _ = __.method(this);
                return _.values;
            };
            // Return non-object, non-array values, since most sensible
            e.search = function (needle, argStrict) {
                var _ = __.method(this),
                strict = !!argStrict, haystack = _.values, i, vl, val, flags;
                if (typeof needle === 'object' && needle.exec) { // Duck-type for RegExp
                    if (!strict) { // Let's consider case sensitive searches as strict
                        flags = 'i' + (needle.global ? 'g' : '') +
                        (needle.multiline ? 'm' : '') +
                        (needle.sticky ? 'y' : ''); // sticky is FF only
                        needle = new RegExp(needle.source, flags);
                    }
                    for (i=0, vl = haystack.length; i < vl; i++) {
                        val = haystack[i];
                        if (needle.test(val)) {
                            return _.keys[i];
                        }
                    }
                    return false;
                }
                for (i = 0, vl = haystack.length; i < vl; i++) {
                    val = haystack[i];
                    if ((strict && val === needle) || (!strict && val == needle)) {
                        return _.keys[i];
                    }
                }
                return false;
            };
            e.sum = function () {
                var _ = __.method(this), sum = 0, i = 0, kl = _.keys.length;
                while (i < kl) {
                    if (!isNaN(parseFloat(_.values[i]))) {
                        sum += parseFloat(_.values[i]);
                    }
                    i++;
                }
                return sum;
            };
            // Experimental functions
            e.foreach = function (handler) {
                var _ = __.method(this), i = 0, kl = _.keys.length;
                while (i < kl) {
                    if (handler.length === 1) {
                        handler(_.values[i]); // only pass the value
                    }
                    else {
                        handler(_.keys[i], _.values[i]);
                    }
                    i++;
                }
                return this;
            };
            e.list = function () {
                var key, _ = __.method(this), i = 0, argl = arguments.length;
                while (i < argl) {
                    key = _.keys[i];
                    if (key && key.length === parseInt(key, 10).toString().length && // Key represents an int
                        parseInt(key, 10) < argl) { // Key does not exceed arguments
                        that.window[arguments[key]] = _.values[key];
                    }
                    i++;
                }
                return this;
            };
            // Parallel functionality and naming of built-in JavaScript array methods
            e.forEach = function (handler) {
                var _ = __.method(this), i = 0, kl = _.keys.length;
                while (i < kl) {
                    handler(_.values[i], _.keys[i], this);
                    i++;
                }
                return this;
            };
            // Our own custom convenience functions
            e.$object = function () {
                var _ = __.method(this);
                return _.object;
            };
            e.$objectChain = function () {
                var _ = __.method(this);
                return _.objectChain;
            };
        }
        PHPJS_Array.prototype = p.PHPJS_Array.prototype;
        arrInst = new PHPJS_Array();
        p.PHPJS_Array.apply(arrInst, mainArgs);
        return arrInst;
    }
    return Array.prototype.slice.call(mainArgs);
}

function count (mixed_var, mode) {
    // http://kevin.vanzonneveld.net
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +      input by: Waldo Malqui Silva
    // +   bugfixed by: Soren Hansen
    // +      input by: merabi
    // +   improved by: Brett Zamir (http://brett-zamir.me)
    // +   bugfixed by: Olivier Louvignes (http://mg-crea.com/)
    // *     example 1: count([[0,0],[0,-4]], 'COUNT_RECURSIVE');
    // *     returns 1: 6
    // *     example 2: count({'one' : [1,2,3,4,5]}, 'COUNT_RECURSIVE');
    // *     returns 2: 6
    var key, cnt = 0;

    if (mixed_var === null || typeof mixed_var === 'undefined') {
        return 0;
    } else if (mixed_var.constructor !== Array && mixed_var.constructor !== Object) {
        return 1;
    }

    if (mode === 'COUNT_RECURSIVE') {
        mode = 1;
    }
    if (mode != 1) {
        mode = 0;
    }

    for (key in mixed_var) {
        if (mixed_var.hasOwnProperty(key)) {
            cnt++;
            if (mode == 1 && mixed_var[key] && (mixed_var[key].constructor === Array || mixed_var[key].constructor === Object)) {
                cnt += this.count(mixed_var[key], 1);
            }
        }
    }

    return cnt;
}




//FUNCOES CRIADAS PARA MANIPULAR STRINGS    

function ucName(str){
    
    str = strtr(strtolower(str), "������������������������������", "������������������������������");
    
    arrPre = array('da', 'de', 'do', 'das', 'dos', 'a', 'e', 'o', 'as', 'os','em','no','na','nos','nas');
    
    str = str + '';
    
    
    arrStr = explode(' ', str);
    
    
    for(index = 0;index < count(arrStr);index++){
        if (!in_array(arrStr[index], arrPre) || index == 0){
            arrStr[index] = ucfirst(arrStr[index]);
        }
    }
    
    str = implode(' ', arrStr);

    return str;
    
}


function empty (mixed_var) {
    // Checks if the argument variable is empty
    // undefined, null, false, number 0, empty string,
    // string "0", objects without properties and empty arrays
    // are considered empty
    //
    // http://kevin.vanzonneveld.net
    // +   original by: Philippe Baumann
    // +      input by: Onno Marsman
    // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +      input by: LH
    // +   improved by: Onno Marsman
    // +   improved by: Francesco
    // +   improved by: Marc Jansen
    // +      input by: Stoyan Kyosev (http://www.svest.org/)
    // +   improved by: Rafal Kukawski
    // *     example 1: empty(null);
    // *     returns 1: true
    // *     example 2: empty(undefined);
    // *     returns 2: true
    // *     example 3: empty([]);
    // *     returns 3: true
    // *     example 4: empty({});
    // *     returns 4: true
    // *     example 5: empty({'aFunc' : function () { alert('humpty'); } });
    // *     returns 5: false
    var undef, key, i, len;
    var emptyValues = [undef, null, false, 0, "", "0"];

    for (i = 0, len = emptyValues.length; i < len; i++) {
        if (mixed_var === emptyValues[i]) {
            return true;
        }
    }

    if (typeof mixed_var === "object") {
        for (key in mixed_var) {
            // TODO: should we check for own properties only?
            //if (mixed_var.hasOwnProperty(key)) {
            return false;
        //}
        }
        return true;
    }

    return false;
}

function geraUrlReescrita($_string) {
    $texto = strtolower($_string);
    $a = '��������������������������������������������������������������??/ +\'�`="!@#\$�%��&()_�]}��{[�^~/:;.,\\|><';
    $b = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyyby  --                              -         ';
    $texto = strtr($texto, $a, $b); //substitui letras acentuadas por &quot;normais&quot;
    $texto = str_replace('  ', ' ', $.trim($texto));/** remove espacos duplos */
    $texto = str_replace(' ', '', $.trim($texto));/** remove os espacos simples */
    return $texto;
}