/** altere o código depois acesse 
 http://jscompress.com/ para comprimir o código e colocar no arquivo */

/* adiciona maxlength no textarea */
$.fn.addMaxlength = function() {
    this.each(function() {
        var textarea = $(this);
        if (textarea.attr('maxlength') != undefined && textarea.attr('maxlength') != -1) {
            textarea.keyup(function() {
                if (textarea.val().length >= textarea.attr('maxlength')) {
                    textarea.val(textarea.val().substr(0, textarea.attr('maxlength')));
                }
            })
        }
    });
}

/* adiciona o focus na primeira tela */
$.fn.focusFirstForm = function() {
    var setFocus = false;
    if (this.is(':visible')) {
        if (this.attr('focus') != 'no') {
            this.find('input,select,textarea').filter(':enabled').each(function() {
                if (!setFocus) {
                    switch (this.getAttribute('type')) {
                        case 'hidden':
                        case 'submit':
                            break;
                        default:
                            setFocus = true;
                            this.focus();
                            return;
                    }
                }
            });
        }
    }
}


if (_Module == 'admin' || _Module == 'painel') {

    $.fn.setTinymce = function() {

        //        this.tinymce({
        //            // Location of TinyMCE script
        //            script_url : _baseUrl + '/public/library/javascripts/tiny_mce/tiny_mce.js',
        //            // General options
        //            language : "pt",
        //            theme : "advanced",
        //            plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
        //            // Theme options
        //            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect",
        //            theme_advanced_buttons2 : "bullist,numlist,|,outdent,indent,|,forecolor,backcolor,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code",
        //            //		theme_advanced_buttons3 : "hr,removeformat,visualaid,|,sub,sup,|,charmap",
        //            theme_advanced_buttons3 : "",
        //            theme_advanced_toolbar_location : "top",
        //            theme_advanced_toolbar_align : "left",
        //
        //
        //            // Example content CSS (should be your site CSS)
        //            content_css : _baseUrl + "/public/library/javascripts/tiny_mce/themes/advanced/skins/default/content.css",
        //
        //            // Drop lists for link/image/media/template dialogs
        //            template_external_list_url : "lists/template_list.js",
        //            external_link_list_url : "lists/link_list.js",
        //            external_image_list_url : "lists/image_list.js",
        //            media_external_list_url : "lists/media_list.js",
        //            file_browser_callback : "tinyBrowser",
        //            // Replace values for the template plugin
        //            template_replace_values : {
        //                username : "Some User",
        //                staffid : "991234"
        //            }
        //
        //        });
        }


    $.fn.setTinymceSimple = function() {



        //        this.tinymce({
        //            // Location of TinyMCE script
        //            script_url : _baseUrl + '/public/library/javascripts/tiny_mce/tiny_mce.js',
        //            // General options
        //            language : "pt",
        //            theme : "advanced",
        //            plugins : "safari,pagebreak,style,layer,table,save,advhr,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,gsynuhimgupload",
        //            //plugins : "pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
        //            // Theme options
        //            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,|,gsynuhimgupload",
        //            theme_advanced_buttons2 : "bullist,numlist,|,outdent,indent,|,forecolor,backcolor,|,undo,redo,|,link,unlink,anchor,cleanup,help,code",
        //            //		theme_advanced_buttons3 : "hr,removeformat,visualaid,|,sub,sup,|,charmap",
        //            theme_advanced_buttons3 : "",
        //            theme_advanced_toolbar_location : "top",
        //            theme_advanced_toolbar_align : "left",
        //            relative_urls : false,
        //            
        //            //lina adicionada dia 04/04/2013 pra submeter acentos no ajax submit, nao sei se causara erros no submit normal ou qq programação relacionada ao tiny
        //            //entity_encoding: "raw",
        //
        //            // Example content CSS (should be your site CSS)
        //            content_css : _baseUrl + "/public/library/javascripts/tiny_mce/themes/advanced/skins/default/content.css",
        //
        //            // Drop lists for link/image/media/template dialogs
        //            template_external_list_url : "lists/template_list.js",
        //            external_link_list_url : "lists/link_list.js",
        //            external_image_list_url : "lists/image_list.js",
        //            media_external_list_url : "lists/media_list.js",
        //            //file_browser_callback : "tinyBrowser",
        //            // Replace values for the template plugin
        //            template_replace_values : {
        //                username : "Some User",
        //                staffid : "991234"
        //            }

        //        });
        }

    $.fn.setTinymceReadOnly = function() {



        //        this.tinymce({
        //            // Location of TinyMCE script
        //            script_url : _baseUrl + '/public/library/javascripts/tiny_mce/tiny_mce.js',
        //            // General options
        //            language : "pt",
        //            theme : "advanced",
        //            plugins : "safari,pagebreak,style,layer,table,save,advhr,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,gsynuhimgupload",
        //            // Theme options
        //            theme_advanced_buttons1 : "",
        //            theme_advanced_buttons2 : "",
        //            //		theme_advanced_buttons3 : "hr,removeformat,visualaid,|,sub,sup,|,charmap",
        //            theme_advanced_buttons3 : "",
        //            theme_advanced_toolbar_location : "top",
        //            theme_advanced_toolbar_align : "left",
        //            relative_urls : false,
        //            readonly : true,
        //
        //            // Example content CSS (should be your site CSS)
        //            content_css : _baseUrl + "/public/library/javascripts/tiny_mce/themes/advanced/skins/default/content.css",
        //
        //            // Drop lists for link/image/media/template dialogs
        //            template_external_list_url : "lists/template_list.js",
        //            external_link_list_url : "lists/link_list.js",
        //            external_image_list_url : "lists/image_list.js",
        //            media_external_list_url : "lists/media_list.js"
        //            
        //        });
        }

}

$.fn.maskUrl = function() {
    $(this).focus(function() {
        if ($(this).val() == '') {
            $(this).val('http://');
        }
    }).blur(function() {
        if ($(this).val() == 'http://') {
            $(this).val('');
        }
    })
}

/* mensagem exibida no container */
var Message = {
    show: function(message, type, dialogShow) {
        
        if($.trim(dialogShow) == ''){
            dialogShow = '';
        }
        
        if (message != '' && message != null) {

            /** admin */
            if (_Module == 'admin' || _Module == 'painel') {

                if(type.replace('error').length != type.length){
                    type = 'alert alert-danger';                    
                }
                else if(type.replace('success').length != type.length || type.replace('msg-ok').length != type.length){
                    type = 'alert alert-success';                    
                }
                else if(type.replace('notice').length != type.length){
                    type = 'alert alert-warning';                    
                }
                

                $('#loading-form-search').remove();

                $('#containerMessage')
                .removeClass('hide')
                .removeClass('alert-success')
                .removeClass('alert-info')
                .removeClass('alert-warning')
                .removeClass('alert-danger')
                .addClass(type)
                .addClass('alert-dismissable');
                $('#containerMessage').find('p:first').html(message);
                $('#containerMessage').show();
                
                if(dialogShow != 'N'){
                    if(type.indexOf('danger') > 0){
                        Dialog.error(message, 'Atenção!');
                    }
                    else if(type.indexOf('success') > 0){
                        Dialog.success(message, 'Sucesso!');
                    }
                    else if(type.indexOf('notice') > 0){
                        Dialog.alert(message, 'Atenção!');
                    }
                }

            }
        } else {
            Message.hide()
        }
        /** fecha a mensagem de aguarde, caso ela esteja aberta */
        CloseMsgAguarde();
        _initFixedActionBar();//Reinicializa a funcao de fixar o menu bar para recalcular a distancia do elemento do topo
    },
    hide: function() {
        /** admin*/
        if (_Module == 'admin' || _Module == 'painel') {
            $('#containerMessage').addClass('hide');
            $('#loading-form-search').remove();
        }
        _initFixedActionBar();//Reinicializa a funcao de fixar o menu bar para recalcular a distancia do elemento do topo
    },
    success: function(message) {
        if (_Module == 'admin') {
            Message.show(message, 'alert alert-success');            
        } else {
            Message.show(message, 'msg-ok success');
        }
    },
    error: function(message, dialogShow) {

        if (_Module == 'admin') {
            Message.show(message, 'alert alert-danger', dialogShow);            
        } else {
            Message.show(message, 'msg-error error', dialogShow)
        }
    },
    bigError: function(message) {
        if (_Module == 'admin') {
            Message.show(message, 'alert alert-danger');            
        } else {
            Message.show(message, 'msg-error error big-error')
        }
    },
    notice: function(message) {
        if (_Module == 'admin') {
            Message.show(message, 'alert alert-warning');            
        } else {
            Message.show(message, 'msg-notice')
        }
    }
};


/* exibe a caixa de dialogo do ui jquery */
var Dialog = {
    dialog: function(message, title, buttonPositivo, callbackPositivo, buttonNegativo, callbackNegativo, classMessage, staticBackground) {
        var html = '';
        
        $('.modal-backdrop').remove();
        
        //se já existir um dialog montado remove para evitar que uma div sobreponha a outra
        if ($('#dialogModal').length > 0) {
            $('#dialogModal').modal('hide');
            $('#dialogModal').remove();
        }
        
        html = '<div id="dialogModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'
        + '<div class="modal-dialog">'
        + '<div class="modal-content" id="dialogModalContent">'
        + '<div class="modal-header" style="height: 30px;">'
        + '<div id="dialogModalLogo"></div>'
        + '<button type="button" class="close" id="btnCloseNegativo" data-dismiss="modal" aria-hidden="true">×</button>'
        + '<h3 id="dialogModalHeader">' + title + '</h3>'
        + '</div>'
        + '<div class="modal-body">'
        + '<h4 id="dialogModalBody" style="margin-top:20px!important; margin-left:30px!important;">' + message + '</h4>'
        + '</div>'
        + '<div class="modal-footer" id="dialogModalFooter">';
        
        if($.trim(buttonNegativo) != ''){
            html += buttonNegativo;
        }
        
        if($.trim(buttonPositivo) != ''){
            html += buttonPositivo;
        } 
        
        html += '</div>'
        + '</div>'
        + '</div>'
        + '</div>';

       
        $('body').append(html);
        
        if($.trim(classMessage) != ''){
            $("#dialogModalContent").addClass(classMessage);
        }        

        if($.isFunction(callbackPositivo)){
            $('#dialogModalFooter #btnPositivo').click(callbackPositivo);
        }
        
        if($.isFunction(callbackNegativo)){
            $('#dialogModalFooter #btnNegativo').click(callbackNegativo);
            $('#btnCloseNegativo').click(callbackNegativo);                
        }
        
        if(classMessage == 'error'){
            $('#dialogModalFooter #btnPositivo').removeClass('btn-primary').addClass('btn-default');
        }
        else if(classMessage == 'success'){
            $('#dialogModalFooter #btnPositivo').removeClass('btn-primary').addClass('btn-default');
        }

        if(staticBackground){
            $('#dialogModal').modal({
                backdrop:'static'
            });
        }else{
            $('#dialogModal').modal();
        }
         
    },
    
    show: function(message, title, callback, callbackNo) {        
        buttonPositivo = '<input type="button" class="btn btn-primary " id="btnPositivo" value="Ok">';
        
        callbackPositivo = function(){
            $("#dialogModal").modal('hide');
            if ($.isFunction(callback)) {
                callback();
            }
        };
        
        callbackNegativo = function(){
            $("#dialogModal").modal('hide');
            if ($.isFunction(callbackNo)) {
                callbackNo();
            }
        };
        
        
        buttonNegativo = null;
        callbackNegativo = null;
        
        Dialog.dialog(message, title, buttonPositivo, callbackPositivo, buttonNegativo, callbackNegativo, 'info', false);
        
    },
    
    
    success: function(message, title, callback) {        
        buttonPositivo = '<input type="button" class="btn btn-primary " id="btnPositivo" value="Ok">';
        
        callbackPositivo = function(){
            $("#dialogModal").modal('hide');
            if ($.isFunction(callback)) {
                callback();
            }
        };
        
        
        buttonNegativo = null;
        callbackNegativo = null;
        
        Dialog.dialog(message, title, buttonPositivo, callbackPositivo, buttonNegativo, callbackNegativo, 'success', false);
        
    },
    
    
    alert: function(message, title, callback) {        
        buttonPositivo = '<input type="button" class="btn btn-primary " id="btnPositivo" value="Ok">';
        
        callbackPositivo = function(){
            $("#dialogModal").modal('hide');
            if ($.isFunction(callback)) {
                callback();
            }
        };
        
        
        buttonNegativo = null;
        callbackNegativo = null;
        
        Dialog.dialog(message, title, buttonPositivo, callbackPositivo, buttonNegativo, callbackNegativo, 'alert', false);
        
    },
    
    error: function(message, title, callback) {
        buttonPositivo = '<input type="button" class="btn btn-primary " id="btnPositivo" value="Ok">';
        
        callbackPositivo = function(){
            $("#dialogModal").modal('hide');
            if ($.isFunction(callback)) {
                callback();
            }
        };
        
        buttonNegativo = null;
        callbackNegativo = null;
        
        Dialog.dialog(message, title, buttonPositivo, callbackPositivo, buttonNegativo, callbackNegativo, 'error', false);
        
    },
    confirm: function(message, title, callback, callbackNao) {
        buttonPositivo = '<input type="button" class="btn btn-primary " id="btnPositivo" value="Sim">';
        
        callbackPositivo = function(){
            $("#dialogModal").modal('hide');
            if ($.isFunction(callback)) {
                callback();
            }
        };
        
        buttonNegativo = '<input type="button" class="btn btn-danger " id="btnNegativo" value="Não">';
        callbackNegativo = function(){
            $("#dialogModal").modal('hide');
            if ($.isFunction(callbackNao)) {
                callbackNao();
            }
        };
        
        Dialog.dialog(message, title, buttonPositivo, callbackPositivo, buttonNegativo, callbackNegativo, 'alert', true);        
    }
};

jQuery.extend({
    handleError: function(s, xhr, status, e) {
        // If a local callback was specified, fire it
        if (s.error) {
            //alert(xhr);
            s.error(xhr, status, e);
        }
        // If we have some XML response text (e.g. from an AJAX call) then log it in the console
        else if (xhr.responseText)
            console.log(xhr.responseText);
    }
});


$.extend($.expr[':'], {
    'containsi': function(elem, i, match, array)
    {
        return (removeAcento(elem.textContent) || removeAcento(elem.innerText) || '').toLowerCase()
        .indexOf(removeAcento(match[3] || "").toLowerCase()) >= 0;
    }
});