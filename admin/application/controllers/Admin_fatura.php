<?php

/**
 * Description of Admin_fatura
 *
 * @author eric
 */
class Admin_fatura extends CI_Controller {

    private $dirView = "admin/admin-fatura";
    private $dirTemplate = "templates/admin";
    private $controller = "admin_fatura";
    private $module = "admin";
    private $data = array();
    private $id_produtor;

    public function __construct() {
        parent::__construct();
        $this->util->isLogado('cadastro');
        $this->data['module'] = $this->module;
        $this->data['controller'] = $this->controller;
        $this->data['baseUrl'] = base_url();
        $this->data['urlPadrao'] = base_url("{$this->controller}");
        $this->data['populateForm'] = '';
        $this->load->model('Pedido_model');
        $this->load->model('Pedidocotacaofornecimento_model');
        $this->load->model('Movimentacaoestoque_model');
        $this->load->model('Pedidoitem_model');
        $this->id_produtor = 1;
        if (!empty($_SESSION['admin']['id_produtor'])) {
            $this->id_produtor = $_SESSION['admin']['id_produtor'];
        }
    }

    /**
     * método principal do sistema
     */
    public function index() {
        $this->util->isLogado('index');
        $this->data['action'] = 'index';
        if (empty($_SESSION['admin']['id_admin'])) {
            redirect('admin/home');
        }


        $params = $this->input->get('venda');
        if (empty($params)) {
            $params = array();
        }
        $params['st_pedido'] = 'F';
        $this->data['populateForm'] = array('venda' => $params);
        $this->data['exibeBtnNovo'] = 'S';
        $dataGrid = $this->Pedido_model->getDataGrid($params);
        $produtos = array();
        $this->data['dataGrid'] = $dataGrid;
        if (!empty($dataGrid)) {
            foreach ($dataGrid as $key => $pedido) {
                $itens = $this->Pedidoitem_model->getDataGrid(array('id_pedido' => $pedido['id_pedido']));
                if (!empty($itens)) {
                    $produtos[$key] = $itens;
                } else {
                    $produtos[$key] = array();
                }
            }
        }
        $this->data['produtos'] = $produtos;
        $this->data['st_pedido'] = $this->Pedido_model->arrStatusPedido;
        $this->data['st_pagamento'] = $this->Pedido_model->arrStatusPagamento;
        $this->template->load($this->dirTemplate, $this->dirView . '/index', $this->data);
    }

    public function pagarProdutor() {
        $this->util->isLogado('index');
        $this->data['action'] = 'index';
        if (empty($_SESSION['admin']['id_admin'])) {
            redirect('admin/home');
        }

        $params = $this->input->get('venda');
        if (empty($params)) {
            $params = array();
        }
        $params['st_pagoprodutor'] = 'N';
        $params['st_fornecido'] = 'S';
        $this->data['populateForm'] = array('venda' => $params);
        $this->data['exibeBtnNovo'] = 'S';
        $dataGrid = $this->Pedidocotacaofornecimento_model->getDataGrid($params);
        $this->data['dataGrid'] = $dataGrid;
        /*
          echo '<pre>';
          print_r($dataGrid);
          echo '</pre>';
          die;
         * 
         */

        $this->template->load($this->dirTemplate, $this->dirView . '/pagar-produtor', $this->data);
    }

    public function salvarFechamento() {
        $fornecimento = $this->input->post('pagar');
        if (empty($_SESSION['admin']['id_admin'])) {
            redirect('admin/home');
        }
        $populateForm = array();
        $populateForm['error'] = '';
        $populateForm['success'] = '';
        try {
            if (!empty($fornecimento)) {
                /**
                  echo '<pre>';
                  print_r($fornecimento);
                  echo '</pre>';
                  die;
                 * 
                 */
                foreach ($fornecimento as $id_pedidocotacaofornecimento) {

                    $dataUpdate = array();
                    $dataUpdate['id_pedidocotacaofornecimento'] = $id_pedidocotacaofornecimento;
                    $dataUpdate['st_pagoprodutor'] = 'S';
                    $dataUpdate['dt_pago'] = date('Y-m-d');
                    $this->Pedidocotacaofornecimento_model->save($dataUpdate);
                }
                $populateForm['success'] = 'Salvo com sucesso';
            } else {
                $populateForm['error'] = 'Falha, dados requeridos não informados';
            }
        } catch (Exception $ex) {
            $populateForm['error'] = "Falha: {$ex->getMessage()}";
        }

        echo json_encode($populateForm);
    }

    public function produtorPago() {
        if (empty($_SESSION['admin']['id_admin'])) {
            redirect('admin/home');
        }
        $this->util->isLogado('index');
        $this->data['action'] = 'produtorPago';

        $params = $this->input->get('venda');
        if (empty($params)) {
            $params = array();
        }
        $params['st_pagoprodutor'] = 'S';
        $params['st_fornecido'] = 'S';
        $this->data['populateForm'] = array('venda' => $params);
        $this->data['exibeBtnNovo'] = 'S';
        $dataGrid = $this->Pedidocotacaofornecimento_model->getDataGrid($params);
        $this->data['dataGrid'] = $dataGrid;
        $this->data['statuspagamentofornecedor'] = $this->Pedido_model->arrStatusPagamentoFornecedor;

        $this->template->load($this->dirTemplate, $this->dirView . '/produtor-pago', $this->data);
    }

    public function fornecimento() {
        $this->util->isLogado('index');
        $this->data['action'] = 'fornecimento';

        if (empty($_SESSION['admin']['id_produtor'])) {
            redirect('admin/home');
        }

        $params = $this->input->get('venda');
        if (empty($params)) {
            $params = array();
        }
        $params['id_produtor'] = $this->id_produtor;
        $params['st_fornecido'] = 'S';
        $this->data['populateForm'] = array('venda' => $params);
        $this->data['exibeBtnNovo'] = 'S';
        $dataGrid = $this->Pedidocotacaofornecimento_model->getDataGrid($params);
        $this->data['dataGrid'] = $dataGrid;
        $this->data['statuspagamentofornecedor'] = $this->Pedido_model->arrStatusPagamentoFornecedor;

        $this->template->load($this->dirTemplate, $this->dirView . '/fornecimento', $this->data);
    }

}
