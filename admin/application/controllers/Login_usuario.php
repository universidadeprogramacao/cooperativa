<?php

/**
 * Description of Login_usuario
 *
 * @author eric
 */
class Login_usuario extends CI_Controller {

    private $dirView = "login-usuario/";
    private $dirTemplate = "templates/login-usuario";
    private $controller = "login_usuario";
    private $module = "admin";
    private $data = array();

    public function __construct() {
        parent::__construct();
        $this->data['module'] = $this->module;
        $this->data['controller'] = $this->controller;
        $this->data['baseUrl'] = base_url();
        $this->data['urlPadrao'] = base_url("{$this->controller}");
        $this->data['populateForm'] = '';
        $this->load->model('Usuario_model');
    }

    /**
     * método principal do sistema
     */
    public function index() {
        /**
          echo '<pre>';
          print_r($_SESSION);
          echo '</pre>';
         * 
         */
        if(!empty($_SESSION['usuario']['id_usuario'])){
            $url = base_url('painel/home');
            redirect($url);
        }
        $this->data['action'] = 'index';

        $params = $this->input->post('login');
        if (empty($params)) {
            $params = array();
        }
        $this->data['populateForm'] = array('login' => $params);
        $this->data['exibeBtnNovo'] = 'N';
        $dataGrid = array();
        $this->data['dataGrid'] = $dataGrid;
        $this->template->load($this->dirTemplate, $this->dirView . '/index', $this->data);
    }

    public function logar() {
        $params = $this->input->post('login');
        $populateForm['error'] = "";
        $populateForm['success'] = "";

        try {
            if (!empty($params)) {
                $data = $this->Usuario_model->logar($params);
                if (!empty($data) && $data == 'S') {
                    $populateForm['success'] = 'S';
                } else {
                    $populateForm['error'] = "Falha no login, usuário e ou senha incorretos.";
                }
            } else {
                $populateForm['error'] = "Falha no login, parâmetros não informados.";
            }
        } catch (Exception $exc) {
            $populateForm['error'] = "Falha no login:{$exc->getMessage()}";
        }
        /**
        echo '<pre>';
        print_r($_SESSION);
        echo '</pre>';
         * 
         */
        echo json_encode($populateForm);
    }

    public function recuperarSenha() {
        set_time_limit(0);
        $email = $this->input->post('email');
        $populateForm['error'] = "";
        $populateForm['success'] = "";
        if (empty($email)) {
            $populateForm['error'] = "Erro parâmetros não informados.";
        } else {
            try {
                $data = $this->Usuario_model->validarEmail($email);
                if (empty($data)) {
                    $populateForm['error'] = "Dados não encontrados.";
                } else {
                    $tx_senha = $this->util->gerarSenha();
                    $params['tx_assunto'] = "Dados de acesso a administração do site/sistema";
                    $params['tx_emaildestinatario'] = $data['tx_email'];
                    $mensagem = "<p>{$data['tx_nome']}</p>, segue seus dados de acesso a administração do site/sistema:";
                    $mensagem.="<p><b>Email:</b>{$data['tx_email']}</p>";
                    $mensagem.="<p><b>Senha:</b>{$tx_senha}</p>";
                    $params['tx_mensagem'] = $mensagem;
                    $isEnviado = $this->util->enviaEmail($params);
                    if (!empty($isEnviado) && $isEnviado == 'S') {
                        $dataUpdate = $data;
                        $dataUpdate['st_indsenhapadrao'] = "S";
                        $dataUpdate['tx_senha'] = $tx_senha;
                        $atualizou = $this->Usuario_model->save($dataUpdate);
                        if ($atualizou !== false) {
                            $populateForm['success'] = "{$data['tx_nome']}, os dados de acesso foram enviados para seu email({$data['tx_email']}).";
                        } else {
                            $populateForm['error'] = "{$data['tx_nome']}, o email foi enviado, mas houve um erro inesperado em nosso sistema, tente novamente, caso persista o erro contate o suporte pelo site/sistema.";
                        }
                    } else {
                        $populateForm['error'] = "{$data['tx_nome']}, o email falhou ao enviar, tente novamente, caso persista o erro contate o suporte pelo site/sistema.";
                    }
                }
            } catch (Exception $exc) {
                $populateForm['error'] = "o email falhou ao enviar, tente novamente, caso persista o erro contate o suporte pelo site/sistema:{$exc->getMessage()}";
            }
        }
        echo json_encode($populateForm);
    }

    public function salvar() {
        $populateForm['error'] = '';
        $populateForm['success'] = '';
        $populateForm['dataGrid'] = array();
        $data = $this->input->post('usuario');
        $criptografar = $this->input->post('criptografar');

        if (empty($data['dt_cadastro'])) {
            $data['dt_cadastro'] = date("Y/m/d");
        }
        if (!empty($data)) {
            if (empty($data['st_indsenhapadrao'])) {
                $data['st_indsenhapadrao'] = 'S';
            }
            if (!empty($criptografar) && $criptografar == 'S') {
                if (!empty($data['tx_senha'])) {
                    $data['tx_senha'] = $this->criptografia->gerarHash($data['tx_senha']);
                    $data['st_indsenhapadrao'] = 'N';
                }
            }
            try {
                $data['tx_email'] = strtolower($data['tx_email']);
                $dataTeste = $this->Usuario_model->save($data);
                if ($data['st_indsenhapadrao'] == 'S') {

                    $params['tx_assunto'] = "{$data['tx_nome']},  seus dados de acesso a usuarioistração do site/sistema";
                    $params['tx_emaildestinatario'] = $data['tx_email'];
                    $mensagem = "<p>{$data['tx_nome']}, segue seus dados de acesso a usuarioistração do site/sistema:</p>";
                    $mensagem.="<p><b>Email:</b>{$data['tx_email']}</p>";
                    $mensagem.="<p><b>Senha:</b>{$data['tx_senha']}</p>";
                    $params['tx_mensagem'] = $mensagem;
                    $this->util->enviaEmail($params);
                }
                if ($dataTeste !== false) {
                    $data = $dataTeste;
                    if (!empty($data['dt_cadastro'])) {
                        $data['dt_cadastro'] = $this->util->reverseDate($data['dt_cadastro']);
                    }
                    $populateForm['success'] = 'Salvo com sucesso, efetue o login.';
                    $populateForm['dataGrid'] = $data;
                    if (!empty($criptografar) && $criptografar == 'S') {
                        $_SESSION['usuario']['st_indsenhapadrao'] = 'N';
                    }
                } else {
                    $populateForm['error'] = "Falha ao salvar";
                }
            } catch (Exception $exc) {
                $populateForm['error'] = "Falha ao salvar{$exc->getMessage()}";
            }
        } else {
            $populateForm['error'] = "Falha ao salvar, dados não informados";
        }
        echo json_encode($populateForm);
    }

    public function buscarCep() {
        $cep = $this->input->post('cep');
        $populateForm = array();
        $populateForm['cep'] = "error";
        if (!empty($cep)) {
            $url = "https://viacep.com.br/ws/{$cep}/json/";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            //curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $url = curl_exec($ch);
            curl_close($ch);

            if (!empty($url)) {
                $url = json_decode($url, true);
                $populateForm['cep'] = $url;
            }
        }
        echo json_encode($populateForm);
    }

    public function validarEmail() {
        $tx_email = $this->input->post('tx_email');
        $populateForm['error'] = '';
        $populateForm['success'] = '';
        $populateForm['validou'] = '';
        if (!empty($tx_email)) {
            $tx_email = strtolower($tx_email);
            $data = $this->Usuario_model->validarEmail($tx_email);
            if (empty($data)) {
                $populateForm['success'] = 'Email válido.';
                $populateForm['validou'] = 'S';
            } else {
                $populateForm['error'] = 'Já existe esse email cadastrado.';
                $populateForm['validou'] = 'N';
            }
        }
        echo json_encode($populateForm);
    }

}
