<?php

/**
 * Description of Admin_pedido_produtor
 *
 * @author eric
 */
class Admin_pedido_produtor extends CI_Controller {

    private $dirView = "admin/admin-pedido-produtor";
    private $dirTemplate = "templates/admin";
    private $controller = "admin_pedido_produtor";
    private $module = "admin";
    private $data = array();
    private $id_produtor;

    public function __construct() {
        parent::__construct();
        $this->util->isLogado('cadastro');
        $this->data['module'] = $this->module;
        $this->data['controller'] = $this->controller;
        $this->data['baseUrl'] = base_url();
        $this->data['urlPadrao'] = base_url("{$this->controller}");
        $this->data['populateForm'] = '';
        $this->load->model('Pedido_model');
        $this->load->model('Pedidocotacaofornecimento_model');
        $this->load->model('Movimentacaoestoque_model');
        $this->load->model('Pedidoitem_model');
        $this->load->model('Produto_model');
        $this->load->model('Usuario_model');
        $this->id_produtor = 1;
        if (!empty($_SESSION['admin']['id_produtor'])) {
            $this->id_produtor = $_SESSION['admin']['id_produtor'];
        }
        
         if (empty($_SESSION['admin']['id_produtor'])) {
             redirect("admin/home");
         }
    }

   public function index() {
        $this->util->isLogado('index');
        $this->data['action'] = 'index';
      
        $params = $this->input->get('venda');
        if (empty($params)) {
            $params = array();
        }
        $this->data['populateForm'] = array('venda' => $params);
        $this->data['exibeBtnNovo'] = 'S';
        $params['id_produtor'] = $this->id_produtor;
        $dataGrid = $this->Pedido_model->getDataGrid($params);
        $produtos = array();
        $this->data['dataGrid'] = $dataGrid;
        if (!empty($dataGrid)) {
            foreach ($dataGrid as $key => $pedido) {
                $itens = $this->Pedidoitem_model->getDataGrid(array('id_pedido' => $pedido['id_pedido']));
                if (!empty($itens)) {
                    $produtos[$key] = $itens;
                } else {
                    $produtos[$key] = array();
                }
            }
        }
        $this->data['produtos'] = $produtos;
        $this->data['st_pedido'] = $this->Pedido_model->arrStatusPedido;
        $this->data['st_pagamento'] = $this->Pedido_model->arrStatusPagamento;
        $this->template->load($this->dirTemplate, $this->dirView . '/index', $this->data);
    }

    public function formulario($id_pedido = null) {

        $cliente = null;
        $pedido = null;
        $pedidoItem = null;
        $this->data['st_pedido'] = $this->Pedido_model->arrStatusPedido;
        $this->data['st_pagamento'] = $this->Pedido_model->arrStatusPagamento;

        if (!empty($id_pedido)) {
            $dataPedido = $this->Pedido_model->findByIdPedidoAndIdProdutor($id_pedido, $this->id_produtor);
            if (!empty($dataPedido)) {
                $cliente = $this->Usuario_model->findById($dataPedido['id_usuario']);
                $cliente['dt_cadastro'] = $this->util->reverseDate($cliente['dt_cadastro']);
                $pedido = $dataPedido;
                $pedidoItem = $this->Pedidoitem_model->getDataGrid(arraY('id_pedido' => $id_pedido));

                if (!empty($pedido['vl_pago'])) {
                    $pedido['vl_pago'] = $this->util->floatToMoneyV1($pedido['vl_pago']);
                }
                if (!empty($pedido['dt_pagamento'])) {
                    $pedido['dt_pagamento'] = $this->util->reverseDate($pedido['dt_pagamento']);
                }

                if (!empty($pedido['dt_entrega'])) {
                    $pedido['dt_entrega'] = $this->util->reverseDate($pedido['dt_entrega']);
                }
            }
        }

        $this->data['populateForm'] = array('admin' => $cliente, 'pedido' => $pedido);
        $this->data['pedidoItem'] = $pedidoItem;
        $this->data['exibeBtnNovo'] = 'N';
        $this->data['pedido'] = $pedido;
        $this->template->load($this->dirTemplate, $this->dirView . '/formulario', $this->data);
    }

    public function salvar() {
        $cliente = $this->input->post('admin');
        $produto = $this->input->post('produto');
        $pedido = $this->input->post('pedido');

        $id_pedidoitem = null;

        if (!empty($cliente) && !empty($produto)) {
            $dataPedido = $pedido;
            $dataPedido['id_usuario'] = $cliente['id_usuario'];
            $dataPedido['id_produtor'] = $this->id_produtor;
            if (!empty($pedido)) {
                $dataPedido['id_pedido'] = $pedido['id_pedido'];
                $this->Pedidoitem_model->deleteByIdPedido($pedido['id_pedido']);
            } else {
                $dataPedido['dt_pedido'] = date('Y-m-d');
            }


            if (!empty($pedido['vl_pago'])) {
                $dataPedido['vl_pago'] = $this->util->moneyToFloat($pedido['vl_pago']);
            }
            if (!empty($pedido['dt_pagamento'])) {
                $dataPedido['dt_pagamento'] = $this->util->reverseDate($pedido['dt_pagamento']);
            }

            if (!empty($pedido['dt_entrega'])) {
                $dataPedido['dt_entrega'] = $this->util->reverseDate($pedido['dt_entrega']);
            }

            $dataPedido = $this->Pedido_model->save($dataPedido);
            $id_pedido = $dataPedido['id_pedido'];

            foreach ($produto as $prod) {
                $produtoCorrente = $this->Produto_model->findById($prod['id_produto']);
                $pedidoItem = array();
                $pedidoItem['id_pedido'] = $dataPedido['id_pedido'];
                $pedidoItem['id_produto'] = $prod['id_produto'];
                $pedidoItem['vl_unitario'] = $produtoCorrente['vl_unitario'];
                $pedidoItem['nr_quantidade'] = $prod['quantidade'];
                $pedidoItem['vl_subtotal'] = $pedidoItem['vl_unitario'] * $pedidoItem['nr_quantidade'];
                $id_pedidoitem = $this->Pedidoitem_model->save($pedidoItem);

                //se o pedido foi finalizado / entregue então efetua uma saída no estique
                if (!empty($pedido['st_pedido']) && $pedido['st_pedido'] == 'F') {
                    $dataMovimentacao = array();
                    $dataMovimentacao['nr_qtdmovimentada'] = $pedidoItem['nr_quantidade'];
                    $dataMovimentacao['tp_movimentacao'] = 'S';
                    $dataMovimentacao['id_produto'] = $pedidoItem['id_produto'];
                    $dataMovimentacao['dt_movimentacao'] = date('Y-m-d');
                    $this->Movimentacaoestoque_model->save($dataMovimentacao);
                }
            }
        }

        if (empty($id_pedido)) {
            redirect(base_url("{$this->controller}/formulario"));
        } else {
            redirect(base_url("{$this->controller}/formulario/{$id_pedido}"));
        }
    }

}
