<?php

/**
 * Description of Admin_pedido
 *
 * @author eric
 */
class Admin_pedido extends CI_Controller {

    private $dirView = "admin/admin-pedido";
    private $dirTemplate = "templates/admin";
    private $controller = "admin_pedido";
    private $module = "admin";
    private $data = array();
    private $id_produtor;

    public function __construct() {
        parent::__construct();
        $this->util->isLogado('cadastro');
        $this->data['module'] = $this->module;
        $this->data['controller'] = $this->controller;
        $this->data['baseUrl'] = base_url();
        $this->data['urlPadrao'] = base_url("{$this->controller}");
        $this->data['populateForm'] = '';
        $this->load->model('Pedido_model');
        $this->load->model('Pedidocotacaofornecimento_model');
        $this->load->model('Movimentacaoestoque_model');
        $this->load->model('Pedidoitem_model');
        $this->id_produtor = 1;
        if (!empty($_SESSION['admin']['id_produtor'])) {
            $this->id_produtor = $_SESSION['admin']['id_produtor'];
        }
    }

    public function pendente() {
        $this->util->isLogado('pedente');
        $this->data['action'] = 'pendente';

        if (empty($_SESSION['admin']['id_produtor'])) {
            redirect('admin/home');
        }

        $params = $this->input->post('produto');
        if (empty($params)) {
            $params = array();
        }
        $this->data['populateForm'] = array('pedido' => $params);
        $this->data['exibeBtnNovo'] = 'S';
        $params['dt_fechamento'] = date('Y-m-d');
        $dataGrid = $this->Pedido_model->getPedidoPendenteProposta($params);
        foreach ($dataGrid as $key => $d) {
            $nr_qtdofertada = 0;
            $id_pedidocotacaofornecimento = null;
            $paramsPesquisa = array();
            $paramsPesquisa['id_produtor'] = $this->id_produtor;
            $paramsPesquisa['id_produto'] = $d['id_produto'];
            $paramsPesquisa['dt_fechamentooriginal'] = $d['dt_fechamento'];
            $proposta = $this->Pedidocotacaofornecimento_model->getPropostaFornecimento($paramsPesquisa);
            if (count($proposta) == 1) {
                $nr_qtdofertada = $proposta[0]['nr_qtdofertada'];
                $id_pedidocotacaofornecimento = $proposta[0]['id_pedidocotacaofornecimento'];
            }
            $dataGrid[$key]['nr_qtdofertada'] = $nr_qtdofertada;
            $dataGrid[$key]['id_pedidocotacaofornecimento'] = $id_pedidocotacaofornecimento;
        }

        $this->data['dataGrid'] = $dataGrid;
        $this->template->load($this->dirTemplate, $this->dirView . '/pendente', $this->data);
    }

    public function cotacaoEstoque() {
        $this->util->isLogado('fechamento');
        $this->data['action'] = 'cotacaoEstoque';

        if (empty($_SESSION['admin']['id_admin'])) {
            redirect('admin/home');
        }


        $params = $this->input->post('produto');
        if (empty($params)) {
            $params = array();
        }
        $this->data['populateForm'] = array('pedido' => $params);
        $this->data['exibeBtnNovo'] = 'S';
        $params['dt_fechamento'] = date('Y-m-d');
        $params['id_produtor_null'] = 'S';
        $dataGrid = $this->Pedido_model->getPedidoPendenteProposta($params);
        $produtores = array();


        foreach ($dataGrid as $key => $d) {
            if (!empty($d['id_produtor'])) {
                unset($dataGrid[$key]);
                continue;
            }
            #$produtor = array();
            $paramsPesquisa = array();
            #$paramsPesquisa['id_produtor'] = $this->id_produtor;
            $paramsPesquisa['id_produto'] = $d['id_produto'];
            $paramsPesquisa['dt_fechamentooriginal'] = $d['dt_fechamento'];
            $proposta = $this->Pedidocotacaofornecimento_model->getPropostaFornecimento($paramsPesquisa);
            /**
              echo '<pre>';
              print_r($proposta);
              echo '</pre>';
              echo '<br/>';
             * 
             */
            /**
             * recupera todas as propostas de quantidade de fornecimento
             * que os produtores responderam no site
             */
            if (count($proposta) > 0) {
                $produtores[$key] = $proposta;
            } else {
                $produtores[$key] = array();
            }
        }


        /*
          echo '<pre>';
          print_r($produtor);
          echo '</pre>';die;
         * 
         */


        $this->data['dataGrid'] = $dataGrid;
        $this->data['produtores'] = $produtores;
        $this->template->load($this->dirTemplate, $this->dirView . '/cotacao-estoque', $this->data);
    }

    /**
     * método principal do sistema
     */
    public function index() {
        $this->util->isLogado('index');
        $this->data['action'] = 'index';
        if (empty($_SESSION['admin']['id_admin'])) {
            redirect('admin/home');
        }

        $params = $this->input->get('venda');
        if (empty($params)) {
            $params = array();
        }
        $this->data['populateForm'] = array('venda' => $params);
        $this->data['exibeBtnNovo'] = 'S';
        $params['id_produtor_null'] = 'S';
        $dataGrid = $this->Pedido_model->getDataGrid($params);
        $produtos = array();
        $this->data['dataGrid'] = $dataGrid;
        if (!empty($dataGrid)) {
            foreach ($dataGrid as $key => $pedido) {
                $itens = $this->Pedidoitem_model->getDataGrid(array('id_pedido' => $pedido['id_pedido']));
                if (!empty($itens)) {
                    $produtos[$key] = $itens;
                } else {
                    $produtos[$key] = array();
                }
            }
        }
        $this->data['produtos'] = $produtos;
        $this->data['st_pedido'] = $this->Pedido_model->arrStatusPedido;
        $this->data['st_pagamento'] = $this->Pedido_model->arrStatusPagamento;
        $this->template->load($this->dirTemplate, $this->dirView . '/index', $this->data);
    }

    public function formulario($id_param = null) {
        $this->util->isLogado('formulario');
        $this->data['action'] = 'formulario';
        redirect('admin/home');

        $dataGrid = array();
        if (!empty($id_param)) {
            $dataGrid = $this->Pedido_model->findById($id_param);
            if (empty($dataGrid)) {
                $dataGrid = array();
                $this->data['error'] = "Nenhum registro encontrado";
            } else {
                $dataGrid['vl_unitario'] = $this->Pedido_model->floatToMoneyV1($dataGrid['vl_unitario']);
                $this->data['dataGrid'] = $dataGrid;
            }
        }


        $this->data['populateForm'] = array('produto' => $dataGrid);
        //echo print_r($dataGrid);
        $this->data['exibeBtnNovo'] = 'N';
        if (!empty($dataGrid['tx_foto'])) {
            $this->data['avatar'] = $this->printHtmlImagem($dataGrid['tx_foto']);
        }

        $this->template->load($this->dirTemplate, $this->dirView . '/formulario', $this->data);
    }

    public function salvarProposta() {
        $fornecimento = $this->input->post('fornecimento');
        $populateForm = array();
        $populateForm['error'] = '';
        $populateForm['success'] = '';
        try {
            if (!empty($fornecimento)) {

                foreach ($fornecimento as $f) {
                    $f['dt_proposta'] = date('Y-m-d');
                    $f['id_produtor'] = $this->id_produtor;
                    $this->Pedidocotacaofornecimento_model->save($f);
                }
                $populateForm['success'] = 'Salvo com sucesso';
            } else {
                $populateForm['error'] = 'Falha, dados requeridos não informados';
            }
        } catch (Exception $ex) {
            $populateForm['error'] = "Falha: {$ex->getMessage()}";
        }

        echo json_encode($populateForm);
    }

    public function salvarFechamento() {
        $fornecimento = $this->input->post('fornecimento');
        $populateForm = array();
        $populateForm['error'] = '';
        $populateForm['success'] = '';
        try {
            if (!empty($fornecimento)) {

                foreach ($fornecimento as $f) {
                    if ($f['nr_qtdmovimentada'] > 0) {

                        /**
                         * atualiza a quantidade real entregue pelo fornecedor/produtor
                         */
                        $dataUpdate = array();
                        $dataUpdate['id_pedidocotacaofornecimento'] = $f['id_pedidocotacaofornecimento'];
                        $dataUpdate['st_fornecido'] = 'S';
                        $dataUpdate['nr_qtdfornecida'] = $f['nr_qtdmovimentada'];
                        $this->Pedidocotacaofornecimento_model->save($dataUpdate);

                        /**
                         * Entra com a quantidade do produto no estoque, para ficar diponível ao entregar pedidos
                         */
                        $dataMovimentacao = array();
                        $dataMovimentacao['nr_qtdmovimentada'] = $f['nr_qtdmovimentada'];
                        $dataMovimentacao['tp_movimentacao'] = 'E';
                        $dataMovimentacao['id_produto'] = $f['id_produto'];
                        $dataMovimentacao['dt_movimentacao'] = date('Y-m-d');
                        $this->Movimentacaoestoque_model->save($dataMovimentacao);
                    }
                }
                $populateForm['success'] = 'Salvo com sucesso';
            } else {
                $populateForm['error'] = 'Falha, dados requeridos não informados';
            }
        } catch (Exception $ex) {
            $populateForm['error'] = "Falha: {$ex->getMessage()}";
        }

        echo json_encode($populateForm);
    }

    public function salvarPedido() {
        $fornecimento = $this->input->post('pedido');
        $populateForm = array();
        $populateForm['error'] = '';
        $populateForm['success'] = '';
        try {
            if (!empty($fornecimento)) {
                /**
                  echo '<pre>';
                  print_r($fornecimento);
                  echo '</pre>';
                  die;
                 * 
                 */
                foreach ($fornecimento as $id_pedido => $f) {

                    if (!empty($f['vl_pago'])) {
                        $f['vl_pago'] = $this->util->moneyToFloat($f['vl_pago']);
                    } else {
                        $f['vl_pago'] = null;
                    }

                    if (!empty($f['dt_pagamento'])) {
                        $f['dt_pagamento'] = $this->util->reverseDate($f['dt_pagamento']);
                        #echo $f['dt_pagamento'];die;
                    } else {
                        $f['dt_pagamento'] = null;
                    }

                    if (!empty($f['dt_entrega'])) {
                        $f['dt_entrega'] = $this->util->reverseDate($f['dt_entrega']);
                    } else {
                        $f['dt_entrega'] = null;
                    }

                    $dataUpdate = $f;
                    $dataUpdate['id_pedido'] = $id_pedido;
                    $this->Pedido_model->save($dataUpdate);

                    //se o pedido foi finalizado / entregue então efetua uma saída no estique
                    if ($f['st_pedido'] == 'F') {
                        $itens = $itens = $this->Pedidoitem_model->getDataGrid(array('id_pedido' => $id_pedido));
                        foreach ($itens as $item) {
                            $dataMovimentacao = array();
                            $dataMovimentacao['nr_qtdmovimentada'] = $item['nr_quantidade'];
                            $dataMovimentacao['tp_movimentacao'] = 'S';
                            $dataMovimentacao['id_produto'] = $item['id_produto'];
                            $dataMovimentacao['dt_movimentacao'] = date('Y-m-d');
                            $this->Movimentacaoestoque_model->save($dataMovimentacao);
                        }
                    }
                }


                $populateForm['success'] = 'Salvo com sucesso';
            } else {
                $populateForm['error'] = 'Falha, dados requeridos não informados';
            }
        } catch (Exception $ex) {
            $populateForm['error'] = "Falha: {$ex->getMessage()}";
        }

        echo json_encode($populateForm);
    }

}
