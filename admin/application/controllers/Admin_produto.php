<?php

/**
 * Description of Admin_produto
 *
 * @author eric
 */
class Admin_produto extends CI_Controller {

    private $dirView = "admin/admin-produto";
    private $dirTemplate = "templates/admin";
    private $controller = "admin_produto";
    private $module = "admin";
    private $data = array();

    public function __construct() {
        parent::__construct();
        $this->util->isLogado('cadastro');
        $this->data['module'] = $this->module;
        $this->data['controller'] = $this->controller;
        $this->data['baseUrl'] = base_url();
        $this->data['urlPadrao'] = base_url("{$this->controller}");
        $this->data['populateForm'] = '';
        $this->load->model('Produto_model');
    }

    /**
     * método principal do sistema
     */
    public function index() {
        $this->util->isLogado('index');
        $this->data['action'] = 'index';
        if (empty($_SESSION['admin']['id_admin'])) {
            redirect('admin/home');
        }

        $params = $this->input->post('produto');
        if (empty($params)) {
            $params = array();
        }
        $this->data['populateForm'] = array('produto' => $params);
        $this->data['exibeBtnNovo'] = 'S';
        $dataGrid = $this->Produto_model->getDataGrid($params);
        $this->data['dataGrid'] = $dataGrid;
        $this->template->load($this->dirTemplate, $this->dirView . '/index', $this->data);
    }

    public function formulario($id_param = null) {
        $this->util->isLogado('formulario');
        $this->data['action'] = 'formulario';
        if (empty($_SESSION['admin']['id_admin'])) {
            redirect('admin/home');
        }

        $dataGrid = array();
        if (!empty($id_param)) {
            $dataGrid = $this->Produto_model->findById($id_param);
            if (empty($dataGrid)) {
                $dataGrid = array();
                $this->data['error'] = "Nenhum registro encontrado";
            } else {
                $dataGrid['vl_unitario'] = $this->Produto_model->floatToMoneyV1($dataGrid['vl_unitario']);
                $this->data['dataGrid'] = $dataGrid;
            }
        }


        $this->data['populateForm'] = array('produto' => $dataGrid);
        //echo print_r($dataGrid);
        $this->data['exibeBtnNovo'] = 'N';
        if (!empty($dataGrid['tx_foto'])) {
            $this->data['avatar'] = $this->printHtmlImagem($dataGrid['tx_foto']);
        }

        $this->template->load($this->dirTemplate, $this->dirView . '/formulario', $this->data);
    }

    public function salvar() {
        $populateForm['error'] = '';
        $populateForm['success'] = '';
        $populateForm['dataGrid'] = array();
        $data = $this->input->post('produto');
        if (empty($_SESSION['admin']['id_admin'])) {
            redirect('admin/home');
        }

        if (!empty($data)) {

            try {
                if (!empty($_FILES['userfile'])) {
                    $config['upload_path'] = '../uploads/produto/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '4048';
                    $config['max_width'] = '2450';
                    $config['max_height'] = '1864';
                    $config['encrypt_name'] = true;
                    $config['remove_spaces'] = true;
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload()) {
                        $dataUpload = array('upload_data' => $this->upload->data());
                        if (!empty($dataUpload['upload_data']['file_name'])) {
                            $data['tx_foto'] = $dataUpload['upload_data']['file_name'];
                            $printAvatar = $this->printHtmlImagem($data['tx_foto']);
                            //echo $printAvatar;die;
                        }
                    } else {
                        /**
                          $error = array('error' => $this->upload->display_errors());
                          echo '<pre>';
                          print_r($error);
                          echo '</pre>';
                          die;
                         * 
                         */
                    }
                }

                $data['vl_unitario'] = $this->Produto_model->moneyToFloat($data['vl_unitario']);
                $dataTeste = $this->Produto_model->save($data);

                if ($dataTeste !== false) {
                    $data = $dataTeste;
                    if (!empty($data['dt_produto'])) {
                        $data['dt_produto'] = $this->util->reverseDate($data['dt_produto']);
                    }

                    $data['vl_unitario'] = $this->Produto_model->floatToMoneyV1($data['vl_unitario']);

                    if (!empty($printAvatar)) {
                        $populateForm['avatar'] = $printAvatar;
                    }
                    $populateForm['success'] = 'Salvo com sucesso.';
                    $populateForm['dataGrid'] = $data;
                } else {
                    $populateForm['error'] = "Falha ao salvar";
                }
            } catch (Exception $exc) {
                $populateForm['error'] = "Falha ao salvar{$exc->getMessage()}";
            }
        } else {
            $populateForm['error'] = "Falha ao salvar, dados não informados";
        }
        echo json_encode($populateForm);
    }

    public function excluir() {
        $id_param = $this->input->post('id_param');
        if (empty($_SESSION['admin']['id_admin'])) {
            redirect('admin/home');
        }
        $populateForm['error'] = '';
        $populateForm['success'] = '';
        if (empty($id_param)) {
            $populateForm['error'] = "Parâmetro não informado.";
        } else {
            try {
                if ($this->Produto_model->deletar($id_param)) {
                    $populateForm['success'] = 'Apagado com sucesso.';
                } else {
                    $populateForm['error'] = "Falha ao apagar.";
                }
            } catch (Exception $exc) {
                $populateForm['error'] = "Falha ao apagar:{$exc->getMessage()}.";
            }
        }
        echo json_encode($populateForm);
    }

    private function printHtmlImagem($tx_foto) {
        //$html = "";
        $html = '<img style="max-width: 50%;max-height: 150px;" src="' . base_url("../uploads/produto/{$tx_foto}") . '">';
        return $html;
    }

    public function findAll() {

        $produto = $this->input->post('produto');
        $this->data['produto'] = $this->Produto_model->getDataGrid($produto);
        $this->load->view($this->dirView . '/find-all', $this->data);
    }

}
