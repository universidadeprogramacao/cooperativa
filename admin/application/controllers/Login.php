<?php

/**
 * Description of Login
 *
 * @author eric
 */
class Login extends CI_Controller {

    private $dirView = "login/";
    private $dirTemplate = "templates/login";
    private $controller = "admin";
    private $module = "admin";
    private $data = array();

    public function __construct() {
        parent::__construct();
        $this->data['module'] = $this->module;
        $this->data['controller'] = $this->controller;
        $this->data['baseUrl'] = base_url();
        $this->data['urlPadrao'] = base_url("{$this->controller}");
        $this->data['populateForm'] = '';
        $this->load->model('Admin_model');
        $this->load->model('Produtor_model');
    }

    /**
     * método principal do sistema
     */
    public function index() {
        // $criptografar = $this->input->post('criptografar');
        // echo $this->criptografia->gerarHash('123456');



        $this->data['action'] = 'index';

        $params = $this->input->post('login');
        if (empty($params)) {
            $params = array();
        }
        $this->data['populateForm'] = array('login' => $params);
        $this->data['exibeBtnNovo'] = 'N';
        $dataGrid = $this->Admin_model->getDataGrid($params);
        $this->data['dataGrid'] = $dataGrid;
        $this->template->load($this->dirTemplate, $this->dirView . '/index', $this->data);
    }

    public function logar() {
        $params = $this->input->post('login');

        $populateForm['error'] = "";
        $populateForm['success'] = "";

        try {
            if (!empty($params)) {
                if ($params['tipo'] == 'A') {
                    $data = $this->Admin_model->logar($params);
                } else {
                    $data = $this->Produtor_model->logar($params);
                }
                if (!empty($data) && $data == 'S') {
                    $populateForm['success'] = 'S';
                } else {
                    $populateForm['error'] = "Falha no login, usuário e ou senha incorretos.";
                }
            } else {
                $populateForm['error'] = "Falha no login, parâmetros não informados.";
            }
        } catch (Exception $exc) {
            $populateForm['error'] = "Falha no login:{$exc->getMessage()}";
        }
        echo json_encode($populateForm);
    }

    public function recuperarSenha() {
        set_time_limit(0);
        $email = $this->input->post('login');
        $populateForm['error'] = "";
        $populateForm['success'] = "";
        if (empty($email['email'])) {
            $populateForm['error'] = "Erro parâmetros não informados.";
        } else {
            try {
                if ($email['tipo'] == 'A') {
                    $data = $this->Admin_model->validarEmail($email['email']);
                } else {
                    $data = $this->Produtor_model->validarEmail($email['email']);
                }

                if (empty($data)) {
                    $populateForm['error'] = "Dados não encontrados.";
                } else {
                    $tx_senha = $this->util->gerarSenha();
                    $params['tx_assunto'] = "Dados de acesso a administração do site/sistema";
                    $params['tx_emaildestinatario'] = $data['tx_email'];
                    $mensagem = "<p>{$data['tx_nome']}</p>, segue seus dados de acesso a administração do site/sistema:";
                    $mensagem.="<p><b>Email:</b>{$data['tx_email']}</p>";
                    $mensagem.="<p><b>Senha:</b>{$tx_senha}</p>";
                    $params['tx_mensagem'] = $mensagem;
                    $isEnviado = $this->util->enviaEmail($params);
                    if (!empty($isEnviado) && $isEnviado == 'S') {
                        $dataUpdate = $data;
                        $dataUpdate['st_indsenhapadrao'] = "S";
                        $dataUpdate['tx_senha'] = $tx_senha;
                        if ($email['tipo'] == 'A') {
                            $atualizou = $this->Admin_model->save($dataUpdate);
                        } else {
                            $atualizou = $this->Produtor_model->save($dataUpdate);
                        }
                        if ($atualizou !== false) {
                            $populateForm['success'] = "{$data['tx_nome']}, os dados de acesso foram enviados para seu email({$data['tx_email']}).";
                        } else {
                            $populateForm['error'] = "{$data['tx_nome']}, o email foi enviado, mas houve um erro inesperado em nosso sistema, tente novamente, caso persista o erro contate o suporte pelo site/sistema.";
                        }
                    } else {
                        $populateForm['error'] = "{$data['tx_nome']}, o email falhou ao enviar, tente novamente, caso persista o erro contate o suporte pelo site/sistema.";
                    }
                }
            } catch (Exception $exc) {
                $populateForm['error'] = "o email falhou ao enviar, tente novamente, caso persista o erro contate o suporte pelo site/sistema:{$exc->getMessage()}";
            }
        }
        echo json_encode($populateForm);
    }

}
