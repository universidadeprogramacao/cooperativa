<?php

/**
 * Description of Ajax
 *
 * @author eric
 */
class Ajax extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Usuario_model');
        $this->load->model('Telefone_model');
    }

    public function cadastrarUsuario() {

        $populateForm['mensagem'] = '';
        $populateForm['dataGrid'] = array();
        $data = $this->input->post('admin');
        #$criptografar = $this->input->post('criptografar');

        if (!empty($data['senha'])) {
            $data['tx_senha'] = $data['senha'];
            unset($data['senha']);
        }
        /**
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        die;
         * 
         */

        if (!empty($data['dt_cadastro'])) {
            $data['dt_cadastro'] = $this->util->reverseDate($data['dt_cadastro']);
            $data['tx_email'] = strtolower($data['tx_email']);
        }
        if (!empty($data)) {
            //if (empty($data['st_indsenhapadrao'])) {
            $data['st_indsenhapadrao'] = 'S';
            //}
            // $data['tx_senha'] = $this->criptografia->gerarHash($data['tx_senha']);
            //$data['st_indsenhapadrao'] = 'N';
            try {
                $dataTeste = $this->Usuario_model->save($data);
                /**
                  if ($data['st_indsenhapadrao'] == 'S') {

                  $params['tx_assunto'] = "{$data['tx_nome']},  seus dados de acesso a administração do site/sistema";
                  $params['tx_emaildestinatario'] = $data['tx_email'];
                  $mensagem = "<p>{$data['tx_nome']}, segue seus dados de acesso a administração do site/sistema:</p>";
                  $mensagem.="<p><b>Email:</b>{$data['tx_email']}</p>";
                  $mensagem.="<p><b>Senha:</b>{$data['tx_senha']}</p>";
                  $params['tx_mensagem'] = $mensagem;
                  $this->util->enviaEmail($params);
                  }
                 * 
                 */
                if ($dataTeste !== false) {
                    $data = $dataTeste;
                    if (!empty($data['dt_cadastro'])) {
                        $data['dt_cadastro'] = $this->util->reverseDate($data['dt_cadastro']);
                    }
                    $populateForm['mensagem'] = 'acao=success';
                    $populateForm['dataGrid'] = $data;
                    /* if (!empty($criptografar) && $criptografar == 'S') {
                      $_SESSION['admin']['st_indsenhapadrao'] = 'N';
                      }* */

                    $telefone = $this->input->post('telefone');
                    $this->Telefone_model->removeAll("id_usuario", $data['id_usuario']);
                    if (!empty($telefone)) {
                        foreach ($telefone as $tel) {
                            $tel['id_usuario'] = $data['id_usuario'];
                            $this->Telefone_model->save($tel);
                        }
                    }
                } else {
                    $populateForm['mensagem'] = "acao=error";
                }
            } catch (Exception $exc) {
                $populateForm['mensagem'] = "acao=error";
            }
        } else {
            $populateForm['mensagem'] = "acao=error";
        }

        echo "<script>window.location='../../cadastrar.php?" . $populateForm['mensagem'] . "'</script>";
    }

}
