<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin_depoimento
 *
 * @author eric
 */
class Admin_depoimento extends CI_Controller {

    private $dirView = "admin/admin-depoimento";
    private $dirTemplate = "templates/admin";
    private $controller = "admin_depoimento";
    private $module = "admin";
    private $data = array();

    public function __construct() {
        parent::__construct();
        $this->util->isLogado('cadastro');
        $this->data['module'] = $this->module;
        $this->data['controller'] = $this->controller;
        $this->data['baseUrl'] = base_url();
        $this->data['urlPadrao'] = base_url("{$this->controller}");
        $this->data['populateForm'] = '';
        $this->load->model('Depoimento_model');
    }

    /**
     * método principal do sistema
     */
    public function index() {
        $this->util->isLogado('index');
        $this->data['action'] = 'index';

        $params = $this->input->post('depoimento');
        if (empty($params)) {
            $params = array();
        }
        $this->data['populateForm'] = array('depoimento' => $params);
        $this->data['exibeBtnNovo'] = 'S';
        $dataGrid = $this->Depoimento_model->getDataGrid($params);
        $this->data['dataGrid'] = $dataGrid;
        $this->template->load($this->dirTemplate, $this->dirView . '/index', $this->data);
    }

    public function formulario($id_param = null) {
        $this->util->isLogado('formulario');
        $this->data['action'] = 'formulario';

        $dataGrid = array();
        if (!empty($id_param)) {
            $dataGrid = $this->Depoimento_model->findById($id_param);
            if (empty($dataGrid)) {
                $dataGrid = array();
                $this->data['error'] = "Nenhum registro encontrado";
            } else {
                //echo $noivo['dt_depoimento'];die;
                $data = $this->util->reverseDate($dataGrid['dt_depoimento']);
                if (!empty($data)) {
                    $dataGrid['dt_depoimento'] = $data;
                }
                $this->data['dataGrid'] = $dataGrid;
            }
        }


        $this->data['populateForm'] = array('depoimento' => $dataGrid);
        //echo print_r($dataGrid);
        $this->data['exibeBtnNovo'] = 'N';
        if (!empty($dataGrid['tx_avatar'])) {
            $this->data['avatar'] = $this->printHtmlImagem($dataGrid['tx_avatar']);
        }

        $this->template->load($this->dirTemplate, $this->dirView . '/formulario', $this->data);
    }

    public function salvar() {
        $populateForm['error'] = '';
        $populateForm['success'] = '';
        $populateForm['dataGrid'] = array();
        $data = $this->input->post('depoimento');


        if (!empty($data['dt_depoimento'])) {
            $data['dt_depoimento'] = $this->util->reverseDate($data['dt_depoimento']);
            $data['tx_email'] = strtolower($data['tx_email']);
        }
        if (!empty($data)) {

            try {
                if (!empty($_FILES['userfile'])) {
                    $config['upload_path'] = '../uploads/usuarios/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '4048';
                    $config['max_width'] = '2450';
                    $config['max_height'] = '1864';
                    $config['encrypt_name'] = true;
                    $config['remove_spaces'] = true;
                    $this->load->library('upload', $config);
                    if ($this->upload->do_upload()) {
                        $dataUpload = array('upload_data' => $this->upload->data());
                        if (!empty($dataUpload['upload_data']['file_name'])) {
                            $data['tx_avatar'] = $dataUpload['upload_data']['file_name'];
                            $printAvatar = $this->printHtmlImagem($data['tx_avatar']);
                            //echo $printAvatar;die;
                        }
                    }else{
                        /**
                       $error = array('error' => $this->upload->display_errors());
                       echo '<pre>';
                       print_r($error);
                       echo '</pre>';
                       die;
                         * 
                         */
                    }
                }
                $dataTeste = $this->Depoimento_model->save($data);

                if ($dataTeste !== false) {
                    $data = $dataTeste;
                    if (!empty($data['dt_depoimento'])) {
                        $data['dt_depoimento'] = $this->util->reverseDate($data['dt_depoimento']);
                    }

                    if (!empty($printAvatar)) {
                        $populateForm['avatar'] = $printAvatar;
                    }
                    $populateForm['success'] = 'Salvo com sucesso.';
                    $populateForm['dataGrid'] = $data;
                } else {
                    $populateForm['error'] = "Falha ao salvar";
                }
            } catch (Exception $exc) {
                $populateForm['error'] = "Falha ao salvar{$exc->getMessage()}";
            }
        } else {
            $populateForm['error'] = "Falha ao salvar, dados não informados";
        }
        echo json_encode($populateForm);
    }

    public function excluir() {
        $id_param = $this->input->post('id_param');
        $populateForm['error'] = '';
        $populateForm['success'] = '';
        if (empty($id_param)) {
            $populateForm['error'] = "Parâmetro não informado.";
        } else {
            try {
                if ($this->Depoimento_model->deletar($id_param)) {
                    $populateForm['success'] = 'Apagado com sucesso.';
                } else {
                    $populateForm['error'] = "Falha ao apagar.";
                }
            } catch (Exception $exc) {
                $populateForm['error'] = "Falha ao apagar:{$exc->getMessage()}.";
            }
        }
        echo json_encode($populateForm);
    }

    private function printHtmlImagem($tx_avatar) {
        //$html = "";
        $html = '<img style="max-width: 50%;max-height: 150px;" src="' . base_url("../uploads/usuarios/{$tx_avatar}") . '">';
        return $html;
    }

}
