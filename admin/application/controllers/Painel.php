<?php

class Painel extends CI_Controller {

    private $dirView = "painel/painel";
    private $dirTemplate = "templates/painel";
    private $controller = "painel";
    private $module = "admin";
    private $data = array();
    private $limit = 1000;

    public function __construct() {
        parent::__construct();
        $this->util->isLogadoUsuario('cadastro');
        $this->data['module'] = $this->module;
        $this->data['controller'] = $this->controller;
        $this->data['baseUrl'] = base_url();
        $this->data['urlPadrao'] = base_url("{$this->controller}");
        $this->data['populateForm'] = '';
        $this->load->model('Usuario_model');
        $this->load->model('Consulta_model');
        $this->load->model('Venda_model');
        $this->load->library('PagSeguroLibrary');
    }

    /**
     * método principal do sistema
     */
    public function index() {
        $this->util->isLogadoUsuario('index');
        $this->data['action'] = 'index';
        $this->redirecionarParaConcluirCompra();

        $params = $this->input->post('usuario');
        if (empty($params)) {
            $params = array();
        }
        $this->data['populateForm'] = array('usuario' => $params);
        $this->data['exibeBtnNovo'] = 'S';
        $dataGrid = $this->Usuario_model->getDataGrid($params);
        $this->data['dataGrid'] = $dataGrid;
        $this->template->load($this->dirTemplate, $this->dirView . '/index', $this->data);
    }

    public function home() {
        $this->util->isLogadoUsuario('home');
        $this->redirecionarParaConcluirCompra();
        $this->data['action'] = 'home';
        $params = array();
        $this->data['populateForm'] = array('usuario' => $params);
        $this->data['exibeBtnNovo'] = 'N';
        $this->data['dataGrid'] = array();

        if (!empty($_SESSION['retorno']['consulta']['id_consulta'])) {
            $id_consulta = $_SESSION['retorno']['consulta']['id_consulta'];
            $id_usuario = $_SESSION['usuario']['id_usuario'];
            //$data = $this->salvarVenda($id_usuario, $id_consulta);
        }
        $this->template->load($this->dirTemplate, $this->dirView . '/home', $this->data);
    }

    public function cadastro() {
        $this->util->isLogadoUsuario('cadastro');
        $this->data['action'] = 'cadastro';
        $id_param = $this->util->getIdUser();
        //echo $id_param;die;
        $dataGrid = array();
        if (!empty($id_param)) {
            $dataGrid = $this->Usuario_model->findById($id_param);
            if (empty($dataGrid)) {
                $dataGrid = array();
                $this->data['error'] = "Cadastro não encontrado";
            } else {
                //echo $noivo['dt_cadastro'];die;
                $data = $this->util->reverseDate($dataGrid['dt_cadastro']);
                if (!empty($data)) {
                    $dataGrid['dt_cadastro'] = $data;
                }
                $this->data['dataGrid'] = $dataGrid;
            }
        } else {
            $this->data['error'] = "Cadastro não encontrado";
        }


        $this->data['populateForm'] = array('usuario' => $dataGrid);
        $this->template->load($this->dirTemplate, $this->dirView . '/cadastro', $this->data);
    }

    public function salvar() {
        $populateForm['error'] = '';
        $populateForm['success'] = '';
        $populateForm['dataGrid'] = array();
        $data = $this->input->post('usuario');
        $criptografar = $this->input->post('criptografar');

        if (!empty($data['dt_cadastro'])) {
            $data['dt_cadastro'] = $this->util->reverseDate($data['dt_cadastro']);
        }
        if (!empty($data)) {
            if (empty($data['st_indsenhapadrao'])) {
                $data['st_indsenhapadrao'] = 'S';
            }
            if (!empty($criptografar) && $criptografar == 'S') {
                if (!empty($data['tx_senha'])) {
                    $data['tx_senha'] = $this->criptografia->gerarHash($data['tx_senha']);
                    $data['st_indsenhapadrao'] = 'N';
                }
            }
            try {
                $data['tx_email'] = strtolower($data['tx_email']);
                $dataTeste = $this->Usuario_model->save($data);
                if ($data['st_indsenhapadrao'] == 'S') {

                    $params['tx_assunto'] = "{$data['tx_nome']},  seus dados de acesso a usuarioistração do site/sistema";
                    $params['tx_emaildestinatario'] = $data['tx_email'];
                    $mensagem = "<p>{$data['tx_nome']}, segue seus dados de acesso a usuarioistração do site/sistema:</p>";
                    $mensagem.="<p><b>Email:</b>{$data['tx_email']}</p>";
                    $mensagem.="<p><b>Senha:</b>{$data['tx_senha']}</p>";
                    $params['tx_mensagem'] = $mensagem;
                    $this->util->enviaEmail($params);
                }
                if ($dataTeste !== false) {
                    $data = $dataTeste;
                    if (!empty($data['dt_cadastro'])) {
                        $data['dt_cadastro'] = $this->util->reverseDate($data['dt_cadastro']);
                    }
                    $populateForm['success'] = 'Salvo com sucesso.';
                    $populateForm['dataGrid'] = $data;
                    if (!empty($criptografar) && $criptografar == 'S') {
                        $_SESSION['usuario']['st_indsenhapadrao'] = 'N';
                    }
                } else {
                    $populateForm['error'] = "Falha ao salvar";
                }
            } catch (Exception $exc) {
                $populateForm['error'] = "Falha ao salvar{$exc->getMessage()}";
            }
        } else {
            $populateForm['error'] = "Falha ao salvar, dados não informados";
        }
        echo json_encode($populateForm);
    }

    public function excluir() {
        $id_param = $this->input->post('id_param');
        $populateForm['error'] = '';
        $populateForm['success'] = '';
        if (empty($id_param)) {
            $populateForm['error'] = "Parâmetro não informado.";
        } else {
            try {
                if ($this->Usuario_model->deletar($id_param)) {
                    $populateForm['success'] = 'Apagado com sucesso.';
                } else {
                    $populateForm['error'] = "Falha ao apagar.";
                }
            } catch (Exception $exc) {
                $populateForm['error'] = "Falha ao apagar:{$exc->getMessage()}.";
            }
        }
        echo json_encode($populateForm);
    }

    public function validarEmail() {
        $tx_email = $this->input->post('tx_email');
        $populateForm['error'] = '';
        $populateForm['success'] = '';
        $populateForm['validou'] = '';
        if (!empty($tx_email)) {
            $tx_email = strtolower($tx_email);
            $data = $this->Usuario_model->validarEmail($tx_email);
            if (empty($data)) {
                $populateForm['success'] = 'Email válido.';
                $populateForm['validou'] = 'S';
            } else {
                $populateForm['error'] = 'Já existe esse email cadastrado.';
                $populateForm['validou'] = 'N';
            }
        }
        echo json_encode($populateForm);
    }

    public function gerarSenha() {
        $st_indsenhapadrao = $this->input->post('st_indsenhapadrao');
        $populateForm['error'] = '';
        $populateForm['tx_senha'] = '';
        $tx_senha = $this->util->gerarSenha(15, 4);
        if (!empty($tx_senha)) {
            $populateForm['tx_senha'] = $tx_senha;
        } else {
            $populateForm['error'] = 'Falha ao gerar senha.';
        }

        echo json_encode($populateForm);
    }

    public function sair() {
        $this->util->isLogadoUsuario('sair');
        $url = base_url("login_usuario");
        unset($_SESSION['usuario']);
        unset($_SESSION['admin']);
        redirect($url);
    }

    /**
     *
      $params['tx_emailto'] = para quem vai o email
     * $params['tx_email'] = o email do usuário
     * $params['tx_senha'] = a senha que vai para ele
     * @param type $params
     */
    private function enviarEmail($params) {
        try {
            $params['tx_assunto'] = "Dados de acesso a usuarioistração do site/sistema";
            $params['tx_emailfrom'] = "eric.ferras@fatec.sp.gov.br";
            $params['tx_emailto'] = $params['tx_email'];
            $mensagem = "<p>{$params['tx_nome']}</p>, segue seus dados de acesso a usuarioistração do site/sistema:";
            $mensagem.="<p><b>Email:</b>{$params['tx_email']}</p>";
            $mensagem.="<p><b>Senha:</b>{$params['tx_senha']}</p>";
            $params['tx_boby'] = $mensagem;
            $isEnviado = $this->Usuario_model->enviarEmail($params);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function buscarCep() {
        $cep = $this->input->post('cep');
        $populateForm = array();
        $populateForm['cep'] = "error";
        /**
          if (!empty($cep)) {
          $url = "https://viacep.com.br/ws/{$cep}/json/";
          $url = file_get_contents($url);
          if (!empty($url)) {
          $url = json_decode($url, true);
          $populateForm['cep'] = $url;
          }
          }
         * 
         */
        if (!empty($cep)) {
            $url = "https://viacep.com.br/ws/{$cep}/json/";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $url = curl_exec($ch);
            curl_close($ch);

            if (!empty($url)) {
                $url = json_decode($url, true);
                $populateForm['cep'] = $url;
            }
        }

        echo json_encode($populateForm);
    }

    public function salvarVenda() {
        $populateForm = array();

        if (!empty($_SESSION['retorno']['consulta']['id_consulta']) && !empty($_POST['tp_compra'])) {
            $id_consulta = $_SESSION['retorno']['consulta']['id_consulta'];
            $id_usuario = $_SESSION['usuario']['id_usuario'];
            $tp_compra = $this->input->post('tp_compra');
            try {
                $data = $this->saveVenda($id_usuario, $id_consulta, $tp_compra);
                if (!empty($data)) {
                    $populateForm = $data;
                } else {
                    $populateForm['error'] = "Falha ao concluir licenciamento.";
                }
            } catch (Exception $ex) {
                $populateForm['error'] = "Falha ao concluir licenciamento:{$ex->getMessage()}";
            }
        } else {
            $populateForm['error'] = "parâmetros inválidos";
        }

        echo json_encode($populateForm);
    }

    private function saveVenda($id_usuario, $id_consulta, $tp_compra) {

        if (!empty($id_usuario) && !empty($id_consulta)) {

            $usuario = $this->Usuario_model->findById($id_usuario);
            $consulta = $this->Consulta_model->findById($id_consulta);

            $nome = $usuario['tx_nome'];
            //$nome = "Eric Luiz Ferras";
            $email = $usuario['tx_email'];
            $ddd = $usuario['tx_ddd'];
            $numeroTelefone = $usuario['tx_numero'];
            $cpf = $usuario['tx_cpf'];
            $cep = $usuario['tx_cep'];
            $rua = $usuario['tx_rua'];
            $complemento = $usuario['tx_complemento'];
            $estado = $usuario['tx_estado'];
            $pais = $usuario['tx_pais'];
            $numeroCasa = $usuario['tx_numerorua'];
            $bairro = $usuario['tx_bairro'];
            $cidade = $usuario['tx_cidade'];
            $tipoDocumento = "CPF";

            $vl_venda = $consulta['vl_total'];
            $vl_realpercentualpagseguroconsulta = $consulta['vl_realpercentualpagseguroconsulta'];
            $tp_pagamentopagseguro = null;
            $st_vendapagseguro = null;

            //se for parcelado, soma a taxa de parcelamento
            if ($tp_compra == 'P') {
                $vl_venda+=$vl_realpercentualpagseguroconsulta;
                $tp_pagamentopagseguro=null;
                $st_vendapagseguro = null;
            }else{
               $tp_pagamentopagseguro = 9; 
               $st_vendapagseguro = 1;
            }

            $dataArrVenda = array();
            $dataArrVenda['dt_venda'] = date('Y-m-d H:i:s');
            $dataArrVenda['id_usuario'] = $id_usuario;
            $dataArrVenda['id_consulta'] = $id_consulta;
            $dataArrVenda['tp_pagamentopagseguro'] = $tp_pagamentopagseguro;
            $dataArrVenda['st_vendapagseguro'] = $st_vendapagseguro;
            $dataArrVenda['tx_ruaentrega'] = $rua;
            $dataArrVenda['tx_complementoentrega'] = $complemento;
            $dataArrVenda['tx_numeroruaentrega'] = $numeroCasa;
            $dataArrVenda['tx_bairroentrega'] = $bairro;
            $dataArrVenda['tx_cidadeentrega'] = $cidade;
            $dataArrVenda['tx_cepentrega'] = $cep;
            $dataArrVenda['tx_estadoentrega'] = $estado;
            $dataArrVenda['vl_venda'] = $vl_venda;
            $dataArrVenda['tx_referenciatransacao'] = "Venda ref: {$id_consulta}";
            $dataArrVenda['nr_anovenda'] = date('Y');
            $dataArrVenda['nr_mesvenda'] = date('n');
            $dataArrVenda['nr_diavenda'] = date('j');
            $dataArrVenda['nr_diasemanavenda'] = date('N');
            $dataArrVenda['vl_percentualpagseguro'] = $consulta['vl_percentualpagseguroconsulta'];

            $dataVenda = $this->Venda_model->save($dataArrVenda);

            if ($tp_compra == 'P') {
                $descricao = "Licenciamento de veículo";
                $quantidade = 1;

                if (!empty($dataVenda['id_venda'])) {
                    $id_venda = $dataVenda['id_venda'];
                    $referencia = "Venda cod: {$id_venda}";
                    $_SESSION['retorno']['consulta']['id_venda'] = $id_venda;
                } else {
                    $referencia = "Venda ref: {$id_consulta}";
                }

                //Adicionando itens na requisição de pagamento. 
                $paymentRequest = new PagSeguroPaymentRequest();
                $paymentRequest->addItem($referencia, $descricao, $quantidade, $vl_venda);

                //Informando o endereço do comprador, assim como tipo do frete.
                //SEDEX, PAC OU NOT_SPECIFIED
                $tipoFrete = PagSeguroShippingType::getCodeByType('PAC');
                $paymentRequest->setShippingType($tipoFrete);

                /**
                  $paymentRequest->setShippingAddress(
                  '01452002',
                  'Av. Brig. Faria Lima',
                  '1384',
                  'apto. 114',
                  'Jardim Paulistano',
                  'São Paulo',
                  'SP',
                  'BRA'
                  );
                 * 
                 */
                $paymentRequest->setShippingAddress(
                        $cep, $rua, $numeroCasa, $complemento, $bairro, $cidade, $estado, $pais
                );

                //Caso você já tenha os dados do comprador,
                // poderá informá-los para facilitar o fluxo de pagamento. Desta forma ele não precisará informá-los novamente.
                /**
                  $paymentRequest->setSender(
                  'João Comprador',
                  'email@comprador.com.br',
                  '14', '981022741',
                  'CPF',
                  '156.009.442-76'
                  );
                 * 
                 */
                $paymentRequest->setSender(
                        $nome, $email, $ddd, $numeroTelefone, $tipoDocumento, $cpf
                );

                //Definindo a moeda a ser utilizada no pagamento. 
                $paymentRequest->setCurrency("BRL");

                //Definindo informações adicionais, que poderão ser utilizadas pelo seu sistema após o 
                //fluxo de pagamento no PagSeguro. 
                // Referenciando a transação do PagSeguro em seu sistema  
                $paymentRequest->setReference("{$referencia}");

                // URL para onde o comprador será redirecionado (GET) após o fluxo de pagamento  
                $urlRedirect = base_url("painel/compra");
                $paymentRequest->setRedirectUrl($urlRedirect);


                // URL para onde serão enviadas notificações (POST) indicando alterações no status da transação  
                $urlNotificacao = base_url("ericferraz/index");
                $paymentRequest->addParameter('notificationURL', $urlNotificacao);
                // Você pode definir percentuais de descontos a serem oferecidos com base no meio de pagamento escolhido pelo 
                // seu cliente, durante o checkout, no ambiente do PagSeguro. 
                //$paymentRequest->addPaymentMethodConfig('CREDIT_CARD', 1.00, 'DISCOUNT_PERCENT');
                //$paymentRequest->addPaymentMethodConfig('EFT', 2.90, 'DISCOUNT_PERCENT');
                //$paymentRequest->addPaymentMethodConfig('BOLETO', 10.00, 'DISCOUNT_PERCENT');
                //$paymentRequest->addPaymentMethodConfig('DEPOSIT', 3.45, 'DISCOUNT_PERCENT');
                //$paymentRequest->addPaymentMethodConfig('BALANCE', 0.01, 'DISCOUNT_PERCENT');

                try {

                    $credentials = PagSeguroConfig::getAccountCredentials(); // getApplicationCredentials()  
                    $checkoutUrl = $paymentRequest->register($credentials);
                    $populateForm['error'] = '';
                    $populateForm['url'] = $checkoutUrl;
                } catch (PagSeguroServiceException $e) {
                    $populateForm['error'] = $e->getMessage();
                    $populateForm['url'] = '';
                }
            } else {
                $populateForm['url'] = '';
                $populateForm['error'] = '';
                $populateForm['success'] = 'Salvo com sucesso, não esqueça de entrar em contato conosco!';
                unset($_SESSION['retorno']);
            }
        } else {
            $populateForm['url'] = '';
            $populateForm['error'] = "Ops, paramêtros inválidos";
            $populateForm['success'] = '';
        }
        return $populateForm;
    }

    public function compra() {

        $this->util->isLogadoUsuario('compra');
        $this->data['action'] = 'index';
        if (empty($params)) {
            $params = array();
        }

        if (!empty($_GET['id_transacao']) && !empty($_SESSION['retorno']['consulta']['id_venda'])) {

            $id_transacao = $_GET['id_transacao'];
            //$id_transacao = str_replace("-","", $id_transacao);
            $dataUpdate = array();
            $dataUpdate['id_transacaopagseguro'] = $id_transacao;
            $dataUpdate['id_venda'] = $_SESSION['retorno']['consulta']['id_venda'];
            $this->Venda_model->save($dataUpdate);
            unset($_SESSION['retorno']['consulta']['id_consulta']);
        }

        if (!empty($_SESSION['retorno']['consulta']['id_consulta'])) {
            $dataGrid = $this->Consulta_model->findById($_SESSION['retorno']['consulta']['id_consulta']);
            if (!empty($dataGrid)) {
                $this->data['dataGrid'] = $dataGrid;
            }
        }
        $this->data['populateForm'] = array('compra' => $params);
        $this->data['exibeBtnNovo'] = 'N';
        $this->template->load($this->dirTemplate, $this->dirView . '/compra', $this->data);
    }

    private function redirecionarParaConcluirCompra() {

        if (!empty($_SESSION['retorno']['consulta']['id_consulta'])) {
            $url = base_url('painel/compra');
            redirect($url);
        }
    }

    public function teste($id_transacao) {
        $dataVenda = $this->Venda_model->getVendaByCodigoTransacao($id_transacao);
        $data = $dataVenda;
        $data['id_venda'] = $dataVenda['id_venda'];
        $data['dt_modificacaostatus'] = date('Y-m-d H:i:s');
        $this->Venda_model->save($data);
    }

    public function loadVenda() {
        $params = $this->input->post('venda');
        $params['limit'] = $this->limit;
        $dataGrid = $this->Venda_model->getDataGrid($params);
        $this->data['dataGrid'] = $dataGrid;
        $this->load->view($this->dirView . '/load-venda', $this->data);
    }

    public function loadVendaById() {
        $id_venda = $this->input->post('id_venda');
        $dataGrid = array();
        if (!empty($id_venda)) {
            $dataGrid = $this->Venda_model->getDataGrid(array('id_venda' => $id_venda, 'load-venda' => 'S', 'frontend' => 'S'));
            if (!empty($dataGrid)) {
                $dataGrid = $dataGrid[0];
                if (!empty($dataGrid['tx_numero']) && !empty($dataGrid['tx_ddd'])) {
                    $dataGrid['tx_numero'] = sprintf("(%s) %s", $dataGrid['tx_ddd'], $dataGrid['tx_numero']);
                }
            }
        }
        //$this->data['dataGrid'] = $dataGrid;
        $this->data['populateFormVendaEdicao'] = array('vendaedicao' => $dataGrid);
        $this->load->view($this->dirView . '/loadVendaById', $this->data);
    }

    public function pdf($id_venda = null) {
        $this->util->isLogadoUsuario('cadastro');
        $this->data['action'] = 'pdf';
        $params = $this->input->post('venda');
        if (!empty($id_venda)) {
            $params['id_venda'] = $id_venda;
        }

        $params['limit'] = $this->limit;
        $params['frontend'] = 'S';
        $dataGrid = $this->Venda_model->getDataGrid($params);
        $this->data['dataGrid'] = $dataGrid;
        $this->load->view($this->dirView . '/pdf', $this->data);
    }

}
