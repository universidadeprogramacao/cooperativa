<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin_consulta
 *
 * @author eric
 */
class Admin_consulta extends CI_Controller {
    private $dirView = "admin/admin-consulta";
    private $dirTemplate = "templates/admin";
    private $controller = "Admin_consulta";
    private $module = "consulta";
    private $data = array();

    public function __construct() {
        parent::__construct();
        $this->util->isLogado();
        $this->data['module'] = $this->module;
        $this->data['controller'] = $this->controller;
        $this->data['baseUrl'] = base_url();
        $this->data['urlPadrao'] = base_url("{$this->controller}");
        $this->data['populateForm'] = '';
        $this->load->model('Consulta_model');
        $this->load->library('phpexcel');
    }

    /**
     * método principal do sistema
     */
    public function index() {
        
        $this->util->isLogado('index');
        $this->data['action'] = 'index';

        $params = $this->input->get('consulta');
        if (empty($params)) {
            $params = array();
        }
        $this->data['populateForm'] = array('consulta' => $params);
        $this->data['exibeBtnNovo'] = 'S';
        $dataGrid = $this->Consulta_model->getDataGrid($params);

        //EXPORTAR
        $exportar = $this->input->get('exportar');
        if (!empty($exportar)) {
            // Instanciamos a classe
              $objPHPExcel = new PHPExcel();

              // Definimos o estilo da fonte
              $objPHPExcel->getActiveSheet()->getStyle('A1:J1')->getFont()->setBold(true);

              // Criamos as colunas
              $objPHPExcel->setActiveSheetIndex(0)
                          ->setCellValue('A1', "Nome" )
                          ->setCellValue('B1', "Email " )
                          ->setCellValue('C1', "Revavam " )
                          ->setCellValue('D1', "Data da consulta " )
                          ->setCellValue('E1', "Valor total " );
                      
              // Podemos configurar diferentes larguras paras as colunas como padrão
                $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
                $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
                $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
                $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
                $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
                $i=2;
                foreach ($dataGrid as $resultado) {
                    $data_consulta = $this->util->reverseDate($resultado['dt_consulta']);
                    $total         = $this->util->floatToMoneyV1($resultado['vl_total']);
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $i, $resultado['tx_nome']); 
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $i, $resultado['tx_email']); 
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $i, $resultado['tx_renavam']); 
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $i, $data_consulta); 
                    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $i, $total); 
                 $i++;
                }

                 $objPHPExcel->getActiveSheet()->setTitle('CONSULTAS');

                  $arquivo = 'consulta_'.date('Y_m_d').'.xls';
                  // Cabeçalho do arquivo para ele baixar
                  header('Content-Type: application/vnd.ms-excel');
                  header("Content-Disposition: attachment;filename='$arquivo'");
                  header('Cache-Control: max-age=0');
                  // Se for o IE9, isso talvez seja necessário
                  header('Cache-Control: max-age=1');

                  // Acessamos o 'Writer' para poder salvar o arquivo
                  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

                  // Salva diretamente no output, poderíamos mudar arqui para um nome de arquivo em um diretório ,caso não quisessemos jogar na tela
                  $objWriter->save('php://output'); 
        }
        $this->data['dataGrid'] = $dataGrid;
        $this->data['dataTotal'] = $this->Consulta_model->getTotal();
        $this->template->load($this->dirTemplate, $this->dirView . '/index', $this->data);
    }
    
    public function excluir() {
        $id_param = $this->input->post('id_param');
        $populateForm['error'] = '';
        $populateForm['success'] = '';
        if (empty($id_param)) {
            $populateForm['error'] = "Parâmetro não informado.";
        } else {
            try {
                if ($this->Consulta_model->deletar($id_param)) {
                    $populateForm['success'] = 'Apagado com sucesso.';
                } else {
                    $populateForm['error'] = "Falha ao apagar.";
                }
            } catch (Exception $exc) {
                $populateForm['error'] = "Falha ao apagar:{$exc->getMessage()}.";
            }
        }
        echo json_encode($populateForm);
    }
}
