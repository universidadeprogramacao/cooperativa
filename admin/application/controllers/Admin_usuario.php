<?php

/**
 * Description of Admin_usuario
 *
 * @author eric
 */
class Admin_usuario extends CI_Controller {

    private $dirView = "admin/usuario";
    private $dirTemplate = "templates/admin";
    private $controller = "admin_usuario";
    private $module = "admin";
    private $data = array();

    public function __construct() {
        parent::__construct();
        $this->util->isLogado('cadastro');
        $this->data['module'] = $this->module;
        $this->data['controller'] = $this->controller;
        $this->data['baseUrl'] = base_url();
        $this->data['urlPadrao'] = base_url("{$this->controller}");
        $this->data['populateForm'] = '';
        $this->load->model('Usuario_model');
        $this->load->model('Telefone_model');
    }

    /**
     * método principal do sistema
     */
    public function index() {
        $this->util->isLogado('index');
        $this->data['action'] = 'index';

        $params = $this->input->post('admin');
        if (empty($params)) {
            $params = array();
        }
        $this->data['populateForm'] = array('admin' => $params);
        $this->data['exibeBtnNovo'] = 'S';
        $dataGrid = $this->Usuario_model->getDataGrid($params);
        $this->data['dataGrid'] = $dataGrid;
        $this->template->load($this->dirTemplate, $this->dirView . '/index', $this->data);
    }

    public function home() {
        $this->util->isLogado('home');
        $this->data['action'] = 'home';
        $params = array();
        $this->data['populateForm'] = array('admin' => $params);
        $this->data['exibeBtnNovo'] = 'N';
        $this->data['dataGrid'] = array();
        $this->template->load($this->dirTemplate, $this->dirView . '/home', $this->data);
    }

    public function cadastro() {
        $this->util->isLogado('cadastro');
        $this->data['action'] = 'cadastro';
        $id_param = $this->util->getIdUsuario();
        $dataGrid = array();
        if (!empty($id_param)) {
            $dataGrid = $this->Usuario_model->findById($id_param);
            if (empty($dataGrid)) {
                $dataGrid = array();
                $this->data['error'] = "Cadastro não encontrado";
            } else {
                //echo $noivo['dt_cadastro'];die;
                $data = $this->util->reverseDate($dataGrid['dt_cadastro']);
                if (!empty($data)) {
                    $dataGrid['dt_cadastro'] = $data;
                }
                $this->data['dataGrid'] = $dataGrid;
            }
        } else {
            $this->data['error'] = "Cadastro não encontrado";
        }


        $this->data['populateForm'] = array('admin' => $dataGrid);
        $this->template->load($this->dirTemplate, $this->dirView . '/cadastro', $this->data);
    }

    public function formulario($id_param = null) {
        $this->util->isLogado('formulario');
        $this->data['action'] = 'formulario';

        $dataGrid = array();
        $this->data['telefone'] = array();
        if (!empty($id_param)) {
            $dataGrid = $this->Usuario_model->findById($id_param);
            if (empty($dataGrid)) {
                $dataGrid = array();
                $this->data['error'] = "Nenhum registro encontrado";
            } else {
                //echo $noivo['dt_cadastro'];die;
                $data = $this->util->reverseDate($dataGrid['dt_cadastro']);
                if (!empty($data)) {
                    $dataGrid['dt_cadastro'] = $data;
                }
                $this->data['dataGrid'] = $dataGrid;
                $telefone = $this->Telefone_model->fetchAll("id_usuario", $id_param);
                /**
                  echo '<pre>';
                  print_r($telefone);
                  echo '</pre>';
                 * 
                 */
                if (!empty($telefone)) {
                    $this->data['telefone'] = $telefone;
                }
            }
        }


        $this->data['populateForm'] = array('admin' => $dataGrid);
        //echo print_r($dataGrid);
        $this->data['exibeBtnNovo'] = 'N';

        $this->template->load($this->dirTemplate, $this->dirView . '/formulario', $this->data);
    }

    public function salvar() {
        $populateForm['error'] = '';
        $populateForm['success'] = '';
        $populateForm['dataGrid'] = array();
        $data = $this->input->post('admin');
        $criptografar = $this->input->post('criptografar');

        if (!empty($data['dt_cadastro'])) {
            $data['dt_cadastro'] = $this->util->reverseDate($data['dt_cadastro']);
            $data['tx_email'] = strtolower($data['tx_email']);
        }
        if (!empty($data)) {
            //if (empty($data['st_indsenhapadrao'])) {
            $data['st_indsenhapadrao'] = 'S';
            //}
            // if (!empty($criptografar) && $criptografar == 'S') {
            //     if (!empty($data['tx_senha'])) {
            //         $data['tx_senha'] = $this->criptografia->gerarHash($data['tx_senha']);
            //         $data['st_indsenhapadrao'] = 'N';
            //     }
            // }
            //$data['tx_senha'] = $this->criptografia->gerarHash($data['tx_senha']);
            //$data['st_indsenhapadrao'] = 'N';
            try {
                $dataTeste = $this->Usuario_model->save($data);
                /**
                  if($data['st_indsenhapadrao'] =='S'){

                  $params['tx_assunto'] = "{$data['tx_nome']},  seus dados de acesso a administração do site/sistema";
                  $params['tx_emaildestinatario'] = $data['tx_email'];
                  $mensagem = "<p>{$data['tx_nome']}, segue seus dados de acesso a administração do site/sistema:</p>";
                  $mensagem.="<p><b>Email:</b>{$data['tx_email']}</p>";
                  $mensagem.="<p><b>Senha:</b>{$data['tx_senha']}</p>";
                  $params['tx_mensagem'] = $mensagem;
                  $this->util->enviaEmail($params);

                  }
                 * 
                 */
                if ($dataTeste !== false) {
                    $data = $dataTeste;
                    if (!empty($data['dt_cadastro'])) {
                        $data['dt_cadastro'] = $this->util->reverseDate($data['dt_cadastro']);
                    }
                    $populateForm['success'] = 'Salvo com sucesso.';
                    $populateForm['dataGrid'] = $data;
                    /**
                      if (!empty($criptografar) && $criptografar == 'S') {
                      $_SESSION['admin']['st_indsenhapadrao'] = 'N';
                      }
                     * 
                     */
                    $telefone = $this->input->post('telefone');
                    $this->Telefone_model->removeAll("id_usuario", $data['id_usuario']);
                    if (!empty($telefone)) {
                        foreach ($telefone as $tel) {
                            $tel['id_usuario'] = $data['id_usuario'];
                            $this->Telefone_model->save($tel);
                        }
                    }
                } else {
                    $populateForm['error'] = "Falha ao salvar";
                }
            } catch (Exception $exc) {
                $populateForm['error'] = "Falha ao salvar{$exc->getMessage()}";
            }
        } else {
            $populateForm['error'] = "Falha ao salvar, dados não informados";
        }
        echo json_encode($populateForm);
    }

    public function excluir() {
        $id_param = $this->input->post('id_param');
        $populateForm['error'] = '';
        $populateForm['success'] = '';
        if (empty($id_param)) {
            $populateForm['error'] = "Parâmetro não informado.";
        } else {
            try {
                if ($this->Usuario_model->deletar($id_param)) {
                    $populateForm['success'] = 'Apagado com sucesso.';
                } else {
                    $populateForm['error'] = "Falha ao apagar.";
                }
            } catch (Exception $exc) {
                $populateForm['error'] = "Falha ao apagar:{$exc->getMessage()}.";
            }
        }
        echo json_encode($populateForm);
    }

    public function validarEmail() {
        $tx_email = $this->input->post('tx_email');
        $populateForm['error'] = '';
        $populateForm['success'] = '';
        $populateForm['validou'] = '';
        if (!empty($tx_email)) {
            $tx_email = strtolower($tx_email);
            $data = $this->Usuario_model->validarEmail($tx_email);
            if (empty($data)) {
                $populateForm['success'] = 'Email válido.';
                $populateForm['validou'] = 'S';
            } else {
                $populateForm['error'] = 'Já existe esse email cadastrado.';
                $populateForm['validou'] = 'N';
            }
        }
        echo json_encode($populateForm);
    }

    public function gerarSenha() {
        $st_indsenhapadrao = $this->input->post('st_indsenhapadrao');
        $populateForm['error'] = '';
        $populateForm['tx_senha'] = '';
        $tx_senha = $this->util->gerarSenha(15, 4);
        if (!empty($tx_senha)) {
            $populateForm['tx_senha'] = $tx_senha;
        } else {
            $populateForm['error'] = 'Falha ao gerar senha.';
        }

        echo json_encode($populateForm);
    }

    public function sair() {
        $this->util->isLogado('sair');
        $url = base_url("login");
        unset($_SESSION['admin']);
        redirect($url);
    }

    /**
     *
      $params['tx_emailto'] = para quem vai o email
     * $params['tx_email'] = o email do usuário
     * $params['tx_senha'] = a senha que vai para ele
     * @param type $params
     */
    private function enviarEmail($params) {
        try {
            $params['tx_assunto'] = "Dados de acesso a administração do site/sistema";
            $params['tx_emailfrom'] = "eric@inovasie.com.br";
            $params['tx_emailto'] = $params['tx_email'];
            $mensagem = "<p>{$params['tx_nome']}</p>, segue seus dados de acesso a administração do site/sistema:";
            $mensagem.="<p><b>Email:</b>{$params['tx_email']}</p>";
            $mensagem.="<p><b>Senha:</b>{$params['tx_senha']}</p>";
            $params['tx_boby'] = $mensagem;
            $isEnviado = $this->Usuario_model->enviarEmail($params);
        } catch (Exception $ex) {
            throw $ex;
        }
    }

    public function findAll() {

        $usuario = $this->input->post('admin');
        $this->data['dataGrid'] = $this->Usuario_model->getDataGrid($usuario);
        $this->load->view($this->dirView . '/find-all', $this->data);
    }

    public function findById() {

        $id_usuario = $this->input->post('id_usuario');
        $params = array('id_usuario' => $id_usuario);
        $dataGrid = $this->Usuario_model->getDataGrid($params);
        if (!empty($dataGrid)) {
            $dataGrid = $dataGrid[0];
        }
        $this->data['populateForm'] = array('admin' => $dataGrid);
        $this->load->view($this->dirView . '/find-by-id', $this->data);
    }

}
