<?php

/**
 * Description of Depoimento_model
 *
 * @author eric
 */
class Depoimento_model extends Base_model {

    protected $tbl = "tbdepoimento";
    protected $id_tabela = "id_depoimento";

    public function __construct() {
        parent::__construct();
    }
    
    public function getDataGrid($params = null) {
        try {

            $this->db->select("depoimento.*");
            $this->db->from("{$this->tbl}  depoimento");
            $this->db->order_by("depoimento.dt_depoimento", "desc");

            if (!empty($params['tx_nome'])) {
                $this->db->like('depoimento.tx_nome', $params['tx_nome']);
            }

            if (!empty($params['tx_email'])) {
                $this->db->like('depoimento.tx_email', $params['tx_email']);
            }

            if (!empty($params['dt_depoimentoinicio'])) {
                $params['dt_depoimentoinicio'] = $this->util->reverseDate($params['dt_depoimentoinicio']);
                $this->db->where('depoimento.dt_depoimento >=', $params['dt_depoimentoinicio']);
            }

             if (!empty($params['dt_depoimentofim'])) {
                $params['dt_depoimentofim'] = $this->util->reverseDate($params['dt_depoimentofim']);
                $this->db->where('depoimento.dt_depoimento <=', $params['dt_depoimentoinicio']);
            }


            if (!empty($params['st_aprovado'])) {
                $this->db->where('depoimento.st_aprovado', $params['st_aprovado']);
            }

            

            $data = $this->db->get()->result_array();

            return $data;
        } catch (Exception $exc) {
            throw $e;
        }
    }

}
