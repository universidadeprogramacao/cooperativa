<?php

/**
 * Description of Movimentacaoestoque_model
 *
 * @author eric
 */
class Movimentacaoestoque_model extends Base_model {

    protected $tbl = "tbmovimentacaoestoque";
    protected $id_tabela = "id_movimentacaoestoque";

    public function __construct() {
        parent::__construct();
    }

    public function save($data) {
        if (empty($data['dt_movimentacao'])) {
            $data['dt_movimentacao'] = date('Y-m-d');
        }
        return parent::save($data);
    }

}
