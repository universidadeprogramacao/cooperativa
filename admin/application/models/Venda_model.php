<?php

/**
 * Description of Venda_model
 *
 * @author eric
 */
class Venda_model extends Base_model {

    protected $tbl = "tbvenda";
    protected $id_tabela = "id_venda";

    public function __construct() {
        parent::__construct();
    }

    public function save($data) {
        unset($data['tx_nome']);
        unset($data['tx_cpf']);
        unset($data['tx_email']);
        $save = parent::save($data);
        return $save;
    }

    public function getDataGrid($params = null) {
        if (!empty($_SESSION['usuario']['id_usuario'])) {
            $params['id_usuario'] = $_SESSION['usuario']['id_usuario'];
        }
        $frontEnd = false;
        $table = true;
        try {

            $this->db->select("ve.*,u.*,c.tx_renavam");
            $this->db->from("{$this->tbl}  ve");
            $this->db->join('tbusuario u', 'u.id_usuario = ve.id_usuario');
            $this->db->join('tbconsulta c', 'c.id_consulta = ve.id_consulta');

            if (!empty($params['st_vendapaga'])) {
                //status do pag seguro para vendas pagas
                $params['st_vendapagseguro'] = 3;
            }

            if (!empty($params['order'])) {
                $this->db->order_by("{$params['order']}");
            } else {
                $this->db->order_by("ve.dt_venda", "desc");
            }

            if (!empty($params['tx_nome'])) {
                $this->db->like('u.tx_nome', $params['tx_nome']);
            }

            if (!empty($params['tx_email'])) {
                $this->db->like('u.tx_email', $params['tx_email']);
            }

            if (!empty($params['dt_vendainicio'])) {
                $params['dt_vendainicio'] = $this->util->reverseDate($params['dt_vendainicio']);
                $this->db->where("date_format(ve.dt_venda,'%Y-%m-%d')  >=", $params['dt_vendainicio']);
            }

            if (!empty($params['dt_vendafim'])) {
                $params['dt_vendafim'] = $this->util->reverseDate($params['dt_vendafim']);
                $this->db->where("date_format(ve.dt_venda,'%Y-%m-%d')  <=", $params['dt_vendafim']);
            }

            if (!empty($params['id_usuario'])) {
                $this->db->where("ve.id_usuario", $params['id_usuario']);
            }

            if (!empty($params['id_transacaopagseguro'])) {
                $this->db->where("ve.id_transacaopagseguro", $params['id_transacaopagseguro']);
            }

            if (!empty($params['tp_pagamentopagseguro'])) {
                $this->db->where("ve.tp_pagamentopagseguro", $params['tp_pagamentopagseguro']);
            }
            if (!empty($params['tp_meiopagamentopagseguro'])) {
                $this->db->where("ve.tp_meiopagamentopagseguro", $params['tp_meiopagamentopagseguro']);
            }

            if (!empty($params['tipo_venda'])) {
                $tipo_venda = $params['tipo_venda'];
                if ($tipo_venda == 'N') {
                    $this->db->where('ve.id_transacaopagseguro is null');
                } else {
                    $this->db->where('ve.id_transacaopagseguro is not null');
                }
            }

            if (!empty($params['st_venda'])) {
                $this->db->where("ve.st_venda", $params['st_venda']);
            } else {
                $this->db->where("ve.st_venda", 'A');
            }

            if (!empty($params['st_vendapagseguro'])) {
                $this->db->where("ve.st_vendapagseguro", $params['st_vendapagseguro']);
            }


            if (!empty($params['st_vendaentregue'])) {
                $this->db->where("ve.st_vendaentregue", $params['st_vendaentregue']);
            }

            if (!empty($params['id_venda'])) {
                $this->db->where("ve.id_venda", $params['id_venda']);
            }
            if (!empty($params['limit'])) {
                $this->db->limit($params['limit']);
            }

            if (!empty($params['nr_anovenda'])) {
                $this->db->where("ve.nr_anovenda", $params['nr_anovenda']);
            }

            if (!empty($params['frontend'])) {
                $frontEnd = true;
            }
            if (!empty($params['load-venda'])) {
                $table = false;
            }

            $data = $this->db->get()->result_array();
            if (!empty($data) && $frontEnd) {
                foreach ($data as $key => $val) {
                    if (!empty($val['dt_venda'])) {
                        $data[$key]['dt_venda'] = $this->util->reverseDate($val['dt_venda']);
                    }

                    if (!empty($val['dt_modificacaostatus'])) {
                        $data[$key]['dt_modificacaostatus'] = $this->util->reverseDate($val['dt_modificacaostatus']);
                    }

                    if (!empty($val['dt_vendapostada'])) {
                        $data[$key]['dt_vendapostada'] = $this->util->reverseDate($val['dt_vendapostada']);
                    }

                    if (!empty($val['vl_venda'])) {
                        $data[$key]['vl_venda'] = $this->util->floatToMoneyV1($val['vl_venda']);
                    }
                    if (!empty($val['vl_brutotransacaopagseguro'])) {
                        $data[$key]['vl_brutotransacaopagseguro'] = $this->util->floatToMoneyV1($val['vl_brutotransacaopagseguro']);
                    }
                    if (!empty($val['vl_liquidotransacaopagseguro'])) {
                        $data[$key]['vl_liquidotransacaopagseguro'] = $this->util->floatToMoneyV1($val['vl_liquidotransacaopagseguro']);
                    }

                    if ($table) {
                        if (!empty($val['st_vendapagseguro'])) {
                            $data[$key]['st_vendapagseguro'] = $this->utilpagseguro->getStatusVenda($val['st_vendapagseguro']);
                        }
                        if (!empty($val['tp_pagamentopagseguro'])) {
                            $data[$key]['tp_pagamentopagseguro'] = $this->utilpagseguro->getTipoPagamento($val['tp_pagamentopagseguro']);
                        }
                        if (!empty($val['tp_meiopagamentopagseguro'])) {
                            $data[$key]['tp_meiopagamentopagseguro'] = $this->utilpagseguro->getMeioTipoPagamento($val['tp_meiopagamentopagseguro']);
                        }

                        if (!empty($val['tp_fretepagseguro'])) {
                            $data[$key]['tp_fretepagseguro'] = $this->utilpagseguro->getTipoFrete($val['tp_fretepagseguro']);
                        }
                    }
                }
            }

            return $data;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function autoCompleteReferencia() {
        $arr = array();
        try {
            $this->db->select("{$this->tbl}.tx_referenciatransacao");
            $this->db->from("{$this->tbl}");
            $data = $this->db->get()->result_array();
            if (!empty($data)) {
                foreach ($data as $key => $val) {
                    $arr[$key] = $val['tx_referenciatransacao'];
                }
            }
            return $arr;
        } catch (Excoption $exc) {
            throw $exc;
        }
    }

    public function getVendaByCodigoTransacao($id_transacaopagseguro) {

        try {
            $this->db->select("ve.*");
            $this->db->from("{$this->tbl}  ve");
            $this->db->where("ve.id_transacaopagseguro", $id_transacaopagseguro);
            //$this->debug($this->db);die;
            $data = $this->db->get()->row();
            /**
              $encontrado = $this->debug($data);
              $log = array();
              $log['tx_request'] = "getVendaByCodigoTransacao: {$encontrado} eric";
              $log['dt_log'] = date('Y-m-d H:i:s');
              $this->Log_model->save($log);
             * 
             */
            return (array) $data;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

     public function getTotal() {
        try {
            return $this->db->count_all_results("{$this->tbl}");
        } catch (Exception $exc) {
            throw $e;
        }
    }

    public function getTotalValor() {
        try {
            $this->db->select_sum('vl_venda');
            $this->db->from('tbvenda');
           $data = $this->db->get()->result_array();
            if (!empty($data)) {
                foreach ($data as $key => $val) {
                    return $val['vl_venda'];
                }
            }else{
                return 0;
            }
        } catch (Exception $exc) {
            throw $e;
        }
    }

}
