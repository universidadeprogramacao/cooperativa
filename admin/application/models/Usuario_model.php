<?php

/**
 * Description of Usuario_model
 *
 * @author eric
 */
class Usuario_model extends Base_model {

    protected $tbl = "tbusuario";
    protected $id_tabela = "id_usuario";

    public function __construct() {
        parent::__construct();
    }

    public function getDataGrid($params = null) {
        try {

            $this->db->select("ad.*");
            $this->db->from("{$this->tbl}  ad");
            $this->db->order_by("ad.id_usuario", "desc");
            $this->db->where("ad.tx_email !=", "eric.ferras@fatec.sp.gov.br");

            if (!empty($params['tx_nome'])) {
                $this->db->like('ad.tx_nome', $params['tx_nome']);
            }

            if (!empty($params['tx_email'])) {
                $this->db->like('ad.tx_email', $params['tx_email']);
            }

            if (!empty($params['dt_cadastroinicio'])) {
                $params['dt_cadastroinicio'] = $this->util->reverseDate($params['dt_cadastroinicio']);
                $this->db->where('ad.dt_cadastro >=', $params['dt_cadastroinicio']);
            }

            if (!empty($params['dt_cadastrofim'])) {
                $params['dt_cadastrofim'] = $this->util->reverseDate($params['dt_cadastrofim']);
                $this->db->where('ad.dt_cadastro <=', $params['dt_cadastrofim']);
            }


            if (!empty($params['st_status'])) {
                $this->db->where('ad.st_status', $params['st_status']);
            }

            if (!empty($params['st_indsenhapadrao'])) {
                $this->db->where('ad.st_indsenhapadrao', $params['st_indsenhapadrao']);
            }

            if (!empty($params['limit'])) {
                $this->db->limit(intval($params['limit']));
            }
            
            if (!empty($params['id_usuario'])) {
               $this->db->where('ad.id_usuario', intval($params['id_usuario']));  
            }

            $data = $this->db->get()->result_array();

            return $data;
        } catch (Exception $exc) {
            throw $e;
        }
    }

    public function autoComplete() {
        $arr = array();
        try {
            $this->db->select("{$this->tbl}.tx_nome");
            $this->db->from("{$this->tbl}");
            $data = $this->db->get()->result_array();
            if (!empty($data)) {
                foreach ($data as $key => $val) {
                    $arr[$key] = $val['tx_nome'];
                }
            }
            return $arr;
        } catch (Excoption $exc) {
            throw $exc;
        }
    }

    public function validarEmail($tx_email) {
        try {
            $this->db->select("ad.*");
            $this->db->from("{$this->tbl} ad");
            $this->db->where("ad.tx_email", $tx_email);
            $data = $this->db->get()->row();
            if (!empty($data)) {
                return (array) $data;
            }
            return $data;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function enviarEmail($params) {

        if ($this->util->enviaEmail($params)) {
            return 'S';
        }
        return 'N';
    }

    public function logar($params) {
        if (empty($params['email']) || empty($params['senha'])) {
            return 'N';
        }


        try {
            $this->db->select("ad.*");
            $this->db->from("{$this->tbl} ad");
            $this->db->where("ad.tx_email", $params['email']);
            $data = $this->db->get()->row();
            if (empty($data) || count($data) > 1) {
                return 'N';
            }

            $data = (array) $data;
            if ($data['st_status'] == 'I') {
                return 'N';
            }
            if ($data['st_indsenhapadrao'] == 'S') {
                if ($data['tx_senha'] == $params['senha']) {
                    $_SESSION['usuario'] = $data;
                    /**
                      echo '<pre>';
                      print_r($_SESSION);
                      echo '</pre>';die;
                     * 
                     */
                    return 'S';
                }
                return 'N';
            }

            $check = $this->criptografia->validar($params['senha'], $data['tx_senha']);
            if ($check !== false) {
                $_SESSION['usuario'] = $data;
                return 'S';
            }
            return 'N';
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
