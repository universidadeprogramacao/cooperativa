<?php

/**
 * Description of Admin_model
 *
 * @author eric
 */
class Admin_model extends Base_model {

    protected $tbl = "tbadmin";
    protected $id_tabela = "id_admin";

    public function __construct() {
        parent::__construct();
    }

    public function getDataGrid($params = null) {
        try {

            $this->db->select("ad.*");
            $this->db->from("{$this->tbl}  ad");
            $this->db->order_by("ad.id_admin", "desc");
            $this->db->where("ad.tx_email !=", "eric.ferras@fatec.sp.gov.br");

            if (!empty($params['tx_nome'])) {
                $this->db->like('ad.tx_nome', $params['tx_nome']);
            }

            if (!empty($params['tx_email'])) {
                $this->db->like('ad.tx_email', $params['tx_email']);
            }

            if (!empty($params['dt_cadastroinicio'])) {
                $params['dt_cadastroinicio'] = $this->util->reverseDate($params['dt_cadastroinicio']);
                $this->db->where('ad.dt_cadastro >=', $params['dt_cadastroinicio']);
            }

            if (!empty($params['dt_cadastrofim'])) {
                $params['dt_cadastrofim'] = $this->util->reverseDate($params['dt_cadastrofim']);
                $this->db->where('ad.dt_cadastro <=', $params['dt_cadastrofim']);
            }


            if (!empty($params['st_status'])) {
                $this->db->where('ad.st_status', $params['st_status']);
            }

            if (!empty($params['st_indsenhapadrao'])) {
                $this->db->where('ad.st_indsenhapadrao', $params['st_indsenhapadrao']);
            }

            $data = $this->db->get()->result_array();

            return $data;
        } catch (Exception $exc) {
            throw $e;
        }
    }

    public function validarEmail($tx_email) {
        try {
            $this->db->select("ad.*");
            $this->db->from("{$this->tbl} ad");
            $this->db->where("ad.tx_email", $tx_email);
            $data = $this->db->get()->row();
            if (!empty($data)) {
                return (array) $data;
            }
            return $data;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function enviarEmail($params) {

        if ($this->util->enviaEmail($params)) {
            return 'S';
        }
        return 'N';
    }

    public function logar($params) {
        if (empty($params['email']) || empty($params['senha'])) {
            return 'N';
        }


        try {
            $this->db->select("ad.*");
            $this->db->from("{$this->tbl} ad");
            $this->db->where("ad.tx_email", $params['email']);
            $data = $this->db->get()->row();
            if (empty($data) || count($data) > 1) {
                return 'N';
            }

            $data = (array) $data;
            if ($data['st_status'] == 'I') {
                return 'N';
            }
            if ($data['st_indsenhapadrao'] == 'S') {
                if ($data['tx_senha'] == $params['senha']) {
                    $data['tipo'] = 'A';
                    $_SESSION['admin'] = $data;
                    return 'S';
                }
                return 'N';
            }

            $check = $this->criptografia->validar($params['senha'], $data['tx_senha']);
            if ($check !== false) {
                $data['tipo'] = 'A';
                $_SESSION['admin'] = $data;
                return 'S';
            }
            return 'N';
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
