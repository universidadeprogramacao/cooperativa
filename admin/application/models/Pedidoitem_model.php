<?php

/**
 * Description of Pedidoitem_model
 *
 * @author eric
 */
class Pedidoitem_model extends Base_model {

    protected $tbl = "tbpedidoitem";
    protected $id_tabela = "id_pedidoitem";

    public function __construct() {
        parent::__construct();
    }

    public function getDataGrid($params = null) {
        try {

            $this->db->select("pi.*,p.tx_produto,p.tx_foto");
            $this->db->from("{$this->tbl}  pi");
            $this->db->join("tbproduto p", "p.id_produto = pi.id_produto");

            if (!empty($params['id_pedido'])) {
                $this->db->where('pi.id_pedido', $params['id_pedido']);
            }
            if (!empty($params['tx_produto'])) {
                $this->db->like('produto.tx_produto', $params['tx_produto']);
            }


            $data = $this->db->get()->result_array();

            return $data;
        } catch (Exception $exc) {
            throw $e;
        }
    }

    public function deleteByIdPedido($id_pedido) {
        try {
            $this->db->where("id_pedido", $id_pedido);
            return $this->db->delete($this->tbl);
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
