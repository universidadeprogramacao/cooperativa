<?php

/**
 * Description of MY_model
 *
 * @author eric
 */
class Base_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function save($data) {

        $key = false;
        try {

            if (!empty($data[$this->id_tabela])) {
                $procura = $data[$this->id_tabela];
                $key = array_search($procura, $data);
            }

            if ($key === false) {
                if ($this->db->insert($this->tbl, $data)) {
                    $data[$this->id_tabela] = $this->getPrimaryKey();
                    return $data;
                }
            }

            $valorChavePrimaria = $procura;
            if (empty($valorChavePrimaria)) {
                if ($this->db->insert($this->tbl, $data)) {
                    $data[$this->id_tabela] = $this->getPrimaryKey();
                    return $data;
                }
                return false;
            }
            unset($data[$this->id_tabela]);
            $this->db->where($this->id_tabela, $valorChavePrimaria);

            if ($this->db->update($this->tbl, $data)) {
                $data[$this->id_tabela] = $valorChavePrimaria;
                return $data;
            }
            return false;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function deletar($id_param) {
        try {
            $this->db->where("{$this->id_tabela}", $id_param);
            return $this->db->delete($this->tbl);
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function removeAll($chave, $valor) {
        try {
            $this->db->where($chave, $valor);
            return $this->db->delete($this->tbl);
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function findById($id_param) {
        try {
            $this->db->select("{$this->tbl}.*");
            $this->db->from("{$this->tbl}");
            $this->db->where("{$this->tbl}.{$this->id_tabela}", $id_param);
            $data = $this->db->get()->row();
            if (!empty($data)) {
                return (array) $data;
            }
            return $data;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function fetchAll($chave, $valor) {
        try {
            $this->db->select("{$this->tbl}.*");
            $this->db->from("{$this->tbl}");
            $this->db->where("{$this->tbl}.{$chave}", $valor);
            $data = $this->db->get()->result_array();

            return $data;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function getPrimaryKey() {
        try {
            $this->db->select("{$this->tbl}.{$this->id_tabela}");
            $this->db->from("{$this->tbl}");
            $this->db->order_by("{$this->id_tabela} desc");
            $this->db->limit(1);

            $data = $this->db->get()->row();
            if (!empty($data)) {
                $data = (array) $data;
                return $data[$this->id_tabela];
            }
            return $data;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function countRows() {
        return $this->db->count_all_results("{$this->tbl}");
    }

    public function saveLog() {
        
    }

    public function debug($val) {
        echo '<pre>';
        print_r($val);
        echo '</pre>';
        echo '<br/>';
    }

    public function moneyToFloat($vl) {
        $vl = str_replace('.', '', $vl);
        return str_replace(',', '.', $vl);
    }

    public function floatToMoneyV1($vl, $decimals = 2) {
        return number_format($vl, $decimals, ',', '.');
    }

    public function reverseDate($date) {
        $arrDate = explode(' ', $date);

        $date = $arrDate[0];
        $time = @$arrDate[1];

        if (stripos($date, '/') !== false) {
            return trim(implode('-', array_reverse(explode('/', $date))) . ' ' . $time);
        } else {
            return trim(implode('/', array_reverse(explode('-', $date))) . ' ' . $time);
        }
    }

}
