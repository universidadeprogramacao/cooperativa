<?php

/**
 * Description of Telefoneprodutor_model
 *
 * @author eric
 */
class Telefoneprodutor_model extends Base_model {

    protected $tbl = "tbtelefoneprodutor";
    protected $id_tabela = "id_telefoneprodutor";

    public function __construct() {
        parent::__construct();
    }

}
