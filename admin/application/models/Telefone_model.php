<?php

/**
 * Description of Telefone_model
 *
 * @author eric
 */
class Telefone_model extends Base_model {
    protected $tbl = "tbtelefone";
    protected $id_tabela = "id_telefone";

    public function __construct() {
        parent::__construct();
    }
}
