<?php

/**
 * Description of Produto_model
 *
 * @author eric
 */
class Produto_model extends Base_model {
    
    protected $tbl = "tbproduto";
    protected $id_tabela = "id_produto";

    public function __construct() {
        parent::__construct();
    }
    
    public function getDataGrid($params = null) {
        try {

            $this->db->select("produto.*");
            $this->db->from("{$this->tbl}  produto");
            $this->db->order_by("produto.tx_produto", "asc");

            if (!empty($params['tx_produto'])) {
                $this->db->like('produto.tx_produto', $params['tx_produto']);
            }


            $data = $this->db->get()->result_array();

            return $data;
        } catch (Exception $exc) {
            throw $e;
        }
    }
    
    public function validarEmail($tx_email) {
        try {
            $this->db->select("ad.*");
            $this->db->from("{$this->tbl} ad");
            $this->db->where("ad.tx_email", $tx_email);
            $data = $this->db->get()->row();
            if (!empty($data)) {
                return (array) $data;
            }
            return $data;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
   
}
