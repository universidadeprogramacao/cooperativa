<?php

/**
 * Description of Pedido_model
 *
 * @author eric
 */
class Pedido_model extends Base_model {

    protected $tbl = "tbpedido";
    protected $id_tabela = "id_pedido";
    public $arrStatusPedido = array(
        "A" => "Ativo",
        "P" => "Pendente de entrega",
        "C" => "Cancelado",
        "F" => "Finalizado / Entregue"
    );
    public $arrStatusPagamento = array(
        "A" => "Ativo",
        "P" => "Pago",
        "C" => "Cancelado"
    );
    public $arrStatusPagamentoFornecedor = array(
        "" => "Todos",
        "S" => "Pago",
        "N" => "Não pago"
    );

    public function __construct() {
        parent::__construct();
    }

    public function autoCompleteReferencia() {
        $arr = array();
        try {
            $this->db->select("{$this->tbl}.tx_referenciatransacao");
            $this->db->from("{$this->tbl}");
            $data = $this->db->get()->result_array();
            if (!empty($data)) {
                foreach ($data as $key => $val) {
                    $arr[$key] = $val['tx_referenciatransacao'];
                }
            }
            return $arr;
        } catch (Excoption $exc) {
            throw $exc;
        }
    }

    public function getVendaByCodigoTransacao($id_transacaopagseguro) {

        try {
            $this->db->select("ve.*");
            $this->db->from("{$this->tbl}  ve");
            $this->db->where("ve.id_transacaopagseguro", $id_transacaopagseguro);
            //$this->debug($this->db);die;
            $data = $this->db->get()->row();
            /**
              $encontrado = $this->debug($data);
              $log = array();
              $log['tx_request'] = "getVendaByCodigoTransacao: {$encontrado} eric";
              $log['dt_log'] = date('Y-m-d H:i:s');
              $this->Log_model->save($log);
             * 
             */
            return (array) $data;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function getTotal() {
        try {
            return $this->db->count_all_results("{$this->tbl}");
        } catch (Exception $exc) {
            throw $e;
        }
    }

    public function getTotalValor() {
        try {
            $this->db->select_sum('vl_venda');
            $this->db->from('tbvenda');
            $data = $this->db->get()->result_array();
            if (!empty($data)) {
                foreach ($data as $key => $val) {
                    return $val['vl_venda'];
                }
            } else {
                return 0;
            }
        } catch (Exception $exc) {
            throw $e;
        }
    }

    //daqui pra baixo

    public function save($data) {
        if (isset($data['tx_nome'])) {
            unset($data['tx_nome']);
        }
        if (isset($data['tx_cpf'])) {
            unset($data['tx_cpf']);
        }
        if (isset($data['tx_email'])) {
            unset($data['tx_email']);
        }
        $save = parent::save($data);
        return $save;
    }

    public function getDataGrid($params = null) {

        try {
            /**
              echo '<pre>';
              print_r($params);
              echo '</pre>';
              die;
             * 
             */
            $this->db->select("p.*,u.*");
            $this->db->from("{$this->tbl} p");
            $this->db->join('tbusuario u', 'u.id_usuario = p.id_usuario');
            if (!empty($params['st_pedido'])) {
                $this->db->where("p.st_pedido", "{$params['st_pedido']}");
            }

            if (!empty($params['st_pedidoin'])) {
                $this->db->where("p.st_pedido IN({$params['st_pedidoin']})");
            }

            if (!empty($params['dt_vendainicio'])) {
                $params['dt_vendainicio'] = $this->reverseDate($params['dt_vendainicio']);
                $this->db->where("p.dt_pagamento >=", "{$params['dt_vendainicio']}");
            }
            if (!empty($params['dt_vendafim'])) {
                $params['dt_vendafim'] = $this->reverseDate($params['dt_vendafim']);
                $this->db->where("p.dt_pagamento <=", "{$params['dt_vendafim']}");
            }

            if (!empty($params['id_produtor_null'])) {
                $this->db->where('p.id_produtor is null');
            }
            if (!empty($params['id_produtor'])) {
                $this->db->where('p.id_produtor', $params['id_produtor']);
            }

            $data = $this->db->get()->result_array();

            return $data;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function getPedidoPendenteProposta($params = null) {

        $this->db->select("p.*,date_format(p.dt_fechamento,'%d/%m/%Y') as data_fechamento,sum(pi.nr_quantidade) as quantidade,sum(pi.vl_subtotal) as subtotal,prod.tx_produto,prod.id_produto,pi.vl_unitario");
        $this->db->from("{$this->tbl}  p");
        $this->db->join('tbpedidoitem pi', 'pi.id_pedido = p.id_pedido');
        $this->db->join('tbproduto prod', 'prod.id_produto = pi.id_produto');
        $this->db->where("p.st_pedido", "A");
        $this->db->where("datediff(dt_fechamento,'{$params['dt_fechamento']}') IS NULL OR datediff(dt_fechamento,'{$params['dt_fechamento']}') >=0");

        //if (!empty($params['id_produtor_null'])) {
            //$this->db->where('p.id_produtor',null);
        //}

        $this->db->group_by("p.dt_fechamento");
        $this->db->group_by("pi.id_produto");



        $data = $this->db->get()->result_array();
        //print_r($data);die;
        return $data;
    }

    public function findByIdPedidoAndIdProdutor($id_param, $id_produtor) {
        try {
            $this->db->select("{$this->tbl}.*");
            $this->db->from("{$this->tbl}");
            $this->db->where("{$this->tbl}.{$this->id_tabela}", $id_param);
            $this->db->where("{$this->tbl}.id_produtor", $id_produtor);
            $data = $this->db->get()->row();
            if (!empty($data)) {
                return (array) $data;
            }
            return $data;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
