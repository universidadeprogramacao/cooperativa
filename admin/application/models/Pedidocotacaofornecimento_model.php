<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pedidocotacaofornecimento_model
 *
 * @author eric
 */
class Pedidocotacaofornecimento_model extends Base_model {

    protected $tbl = "tbpedidocotacaofornecimento";
    protected $id_tabela = "id_pedidocotacaofornecimento";

    public function getPropostaFornecimento($params = null) {

        try {
            $this->db->select("pf.*,produtor.tx_nome,produtor.tx_email,produtor.tx_ddd,produtor.tx_numero");
            $this->db->from("{$this->tbl}  pf");
            $this->db->join("tbprodutor  produtor", "produtor.id_produtor = pf.id_produtor");
            if (!empty($params['id_produto'])) {
                $this->db->where("pf.id_produto", "{$params['id_produto']}");
            }
            if (!empty($params['id_produtor'])) {
                $this->db->where("pf.id_produtor", "{$params['id_produtor']}");
            }
            /**
              if (!empty($params['nr_qtdoriginal'])) {
              $this->db->where("pf.nr_qtdoriginal", "{$params['nr_qtdoriginal']}");
              }
             * 
             */
            if (!empty($params['dt_fechamentooriginal'])) {
                $this->db->where("pf.dt_fechamentooriginal", "{$params['dt_fechamentooriginal']}");
            }

            $data = $this->db->get()->result_array();
            return $data;
        } catch (Exception $ex) {
            throw $e;
        }
    }

    public function getDataGrid($params = null) {
        try {

            $this->db->select("pc.*,prod.tx_produto,f.*");
            $this->db->from("{$this->tbl}  pc");
            $this->db->join("tbproduto prod", "prod.id_produto = pc.id_produto");
            $this->db->join("tbprodutor f", "f.id_produtor = pc.id_produtor");
            $this->db->order_by("f.tx_nome", "asc");

            if (!empty($params['tx_nome'])) {
                $this->db->like('f.tx_nome', $params['tx_nome']);
            }

            if (!empty($params['tx_email'])) {
                $this->db->like('f.tx_email', $params['tx_email']);
            }

            if (!empty($params['tx_cpf'])) {
                $this->db->like('f.tx_cpf', $params['tx_cpf']);
            }

            if (!empty($params['tx_cpf'])) {
                $this->db->like('f.tx_cpf', $params['tx_cpf']);
            }

            if (!empty($params['dt_vendainicio'])) {
                $params['dt_vendainicio'] = $this->reverseDate($params['dt_vendainicio']);
                $this->db->where('pc.dt_fechamentooriginal >=', $params['dt_vendainicio']);
            }
            if (!empty($params['dt_vendafim'])) {
                $params['dt_vendafim'] = $this->reverseDate($params['dt_vendafim']);
                $this->db->where('pc.dt_fechamentooriginal <=', $params['dt_vendafim']);
            }
            if(!empty($params['st_pagoprodutor'])){
                $this->db->where("pc.st_pagoprodutor","{$params['st_pagoprodutor']}");
            }
            if(!empty($params['st_fornecido'])){
                $this->db->where("pc.st_fornecido","{$params['st_fornecido']}");
            }
            if (!empty($params['id_produtor'])) {
                $this->db->where("pc.id_produtor", "{$params['id_produtor']}");
            }

            $data = $this->db->get()->result_array();

            return $data;
        } catch (Exception $exc) {
            throw $e;
        }
    }

}
