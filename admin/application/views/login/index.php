<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="login-panel panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Login</h3>
            </div>
            <div class="panel-body">
                <form role="form" method="post" class="populate form-horizontal" id="validate" action="<?php echo base_url() . "login/logar"; ?>">
                    <fieldset>
                        <div class="form-group">
                            <label class="col-sm-2 col-xs-5 control-label"><?php echo CAMPO_OBRIGATORIO; ?>Email</label>
                            <div class="col-xs-10 col-sm-8">
                                <div class="input-group">
                                    <input class="form-control validate[required,custom[email]]" id="login-email" name="login[email]" type="text" maxlength="60" autofocus>
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-xs-5 control-label"><?php echo CAMPO_OBRIGATORIO; ?>Senha</label>
                            <div class="col-xs-10 col-sm-8">
                                <div class="input-group">
                                    <input class="form-control validate[required]" name="login[senha]" id="login-senha" type="password" maxlength="80">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-lock"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        
                         <div class="form-group">
                            <label class="col-sm-2 col-xs-5 control-label"><?php echo CAMPO_OBRIGATORIO; ?>Perfil</label>
                            <div class="col-xs-10 col-sm-8">
                                
                                <select name="login[tipo]" id="login-tipo" class="form-control validate[required]">
                                    <option value="A">ADMINISTRADOR</option>
                                    <option value="P">PRODUTOR  </option>
                                </select>
                                
                            </div>
                        </div>

                        <div class="form-group">
                            <div  class="col-sm-4 col-xs-7 col-sm-offset-2">

                                <button style="width: 100%;" type="button" id="logar" class="btn btn-success">
                                    <span class="glyphicon glyphicon-send"></span>
                                    Logar
                                </button>

                            </div>
                            
                           
                            <div class="col-sm-4 col-xs-7 col-sm-offset-0">

                                <button type="button" id="recuperar" class="btn btn-info">
                                    <span class="glyphicon glyphicon-info-sign"></span>
                                    Esqueci minha senha
                                </button>
                            </div>
                        </div>
                           


                    </fieldset>
                </form>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="modelRecuperarSenha" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Recuperação de senha</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="formRecuperarSenha" action="<?php echo base_url() . 'login/recuperarSenha'; ?>">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Email:</label>
                        <div class="input-group">
                            <input type="text" class="form-control validate[required,custom[email]]" name="email" id="email">
                            <span class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </span>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">

                <button type="button" id="btnEnviar" class="btn btn-success">
                    <span class="glyphicon glyphicon-send"></span>
                    Enviar
                </button>

                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <span class="glyphicon glyphicon-off"></span>
                    Fechar
                </button>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
       
        var formulario = $('#validate');
        var formRecuperarSenha = $('#validate');
        $('#logar').click(function () {
            if (formulario.validationEngine('validate')) {
                logar(formulario, "<?php echo base_url() . 'admin/home'; ?>");
            }
        });
        
        /**
        $('#btnEnviar').click(function () {
            if (formRecuperarSenha.validationEngine('validate')) {
                recuperarSenha(formRecuperarSenha);
            }
        });
        **/
        
        
        $('#recuperar').click(function (){
           if($("#login-email").val() ===''){
               Dialog.error('informe o e-mail',"Atenção!");
           }else{
             recuperarSenha(formRecuperarSenha);  
           }
        });
        

    });

    function recuperarSenha(formulario) {
       
        var urlRequest =' <?php echo base_url() . 'login/recuperarSenha'; ?>';
        var data = formulario.serialize();
        ShowMsgAguarde();
        $.ajax({
            url: urlRequest,
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (data) {
                if(data !== undefined && data.success !== undefined && data.success !==''){
                    Dialog.success(data.success,'Sucesso.');
                }
                else if(data !== undefined && data.error !== undefined && data.error !== ''){
                    Dialog.error(data.error,'Erro');
                }else{
                    Dialog.error(_erroPadraoAjax,'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax,'Erro');
            },
            complete: function () {
                CloseMsgAguarde();
            }
        });
        
    }

</script>