<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <link rel="stylesheet" href="<?php echo base_url() . 'public/default/' ?>style.css" type="text/css" />

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta name="author" content="Venerando Viagens - Index"/>
        <meta name="keywords" content="passagens aereas, lua de mel, passagens de lua de mal, intercambio"/>
        <meta name="description" content="A Ag&ecirc;ncia Tatuap&eacute; TZ Viagens oferece servi&ccedil;os com excelente padr&atilde;o de qualidade no atendimento, e na consultoria de seus clientes, com &oacute;timos pre&ccedil;os, e melhor custo x benef&iacute;cio do mercado." />

        <meta name="viewport" content="width=device-width,initial-scale=1"/>
        <meta charset="UTF-8"/>
        <title>A Ag&ecirc;ncia Tatuap&eacute; TZ Viagens</title>
        <link href='http://fonts.googleapis.com/css?family=Cabin:400,500,400italic,600,700|Sanchez' rel='stylesheet' type='text/css'/>
        <script src="https://code.jquery.com/jquery-1.11.3.js"></script>


        <meta xml:lang="pt-br"/>
    </head>

    <body>

        <div id="principal">

            <div id="header">

                <div id="logo">
                    <a href="<?php echo base_url('index'); ?>">
                        <img src="<?php echo base_url() . 'public/default/' ?>img/2.png" width="100%"/>
                    </a>
                </div>

                <div id="topInfo">

                    Entre em contato:<br/>
                    (11) 3530-5077 / 3530-5177<br/>
                    reservas@sptp.tzviagens.com.br<br/>
                    Rua Euclides Pacheco, 411 - Tatuap&eacute;

                </div>

            </div>		
            <div id="lineRed"></div>



            <?php
            if (empty($_SESSION['noivos'])) {
                ?>
                <div id="contentHeader">

                    <img src="<?php echo base_url() . 'public/default/' ?>img/1111.png" width="100%"/>

                </div>

                <div id="lineWhite"></div>		
                <div id="menu">
                    <a href="<?php echo base_url('index'); ?>" id="btnMenu">
                        HOME
                    </a>
                    <a href="<?php echo base_url('sobre'); ?>" id="btnMenu">
                        SOBRE A TZ VIAGENS
                    </a>
                    <a href="<?php echo base_url('formaPagamento'); ?>" id="btnMenu">
                        FORMAS DE PAGAMENTO
                    </a>
                    <a href="<?php echo base_url('noivados'); ?>" id="btnMenu">
                        NOIVADOS
                    </a>
                    <!--		<a href="index5.php" id="btnMenu">
                                            PARCEIROS
                                    </a> -->
                </div>
            <?php } else{
                 $tx_slug = $_SESSION['noivos']['tx_slug'];
                 $dt_cadastrocerimoniarelogiosa = !empty($_SESSION['noivos']['dt_cadastrocerimoniarelogiosa']) ? $this->util->reverseDate($_SESSION['noivos']['dt_cadastrocerimoniarelogiosa']) : '';
                 $dt_hoje = date('Y-m-d');
 
                 $mensagemDias = "";
                 if(!empty($dt_cadastrocerimoniarelogiosa)){
                     $dt_cerimonia = $_SESSION['noivos']['dt_cadastrocerimoniarelogiosa'];
                     $diasRestantes = $this->util->getDiferencaDatas($dt_hoje,$dt_cerimonia);
                     
                     if($diasRestantes<0){
                         $mensagemDias = "Já ocorreu";
                     }
                     else if($diasRestantes ==0){
                        $mensagemDias = "É hoje";  
                     }
                     else if($diasRestantes ==1){
                       $mensagemDias = "Falta 1 dia"; 
                     }else{
                        $mensagemDias = "Faltam {$diasRestantes} dias";   
                     }
                 }
                ?>
                 <div id="contentHeader">
		
			<div id="nomeNoivos"><?php echo $_SESSION['noivos']['tx_noivo'] ?> e <?php echo $_SESSION['noivos']['tx_noiva'] ?></div>
			
                        <div id="contador">- <?php echo $dt_cadastrocerimoniarelogiosa; ?> -
			
				<div id="diasRestantes"><?php echo $mensagemDias; ?></div>
			
			</div>
		
		</div>		
		<div id="lineWhite"></div>
		
		<div id="menu">
			<a href="<?php echo base_url("noivados/ver/{$tx_slug}"); ?>">
				<div id="btnMenu">SOBRE OS NOIVOS</div>
			</a>
			<a href="<?php echo base_url("cerimonia/index/{$tx_slug}"); ?>">
				<div id="btnMenu">CERIMONIA</div>
			</a>
			<a href="<?php echo base_url("festa/index/{$tx_slug}"); ?>">
				<div id="btnMenu">FESTA</div>
			</a>
			<a href="<?php echo base_url("cotas/index/{$tx_slug}"); ?>">
				<div id="btnMenu">COTAS DE LUA-DE-MEL</div>
			</a>
		</div>	
                
                <?php
                 }
                ?>




            <?php echo $contents; ?>
            <?php
            $tx_slug = !empty($_SESSION['noivos']) ? $_SESSION['noivos']['tx_slug'] : null;
            ?>

            <div id="lineRed"></div>

            <div id="footer">

                <div id="col1">

                    <ul>
                        <?php
                        if (!empty($tx_slug)) {
                            ?>
                            <li><a href="<?php echo base_url("noivados/ver/{$tx_slug}"); ?>">Sobre os Noivos</a></li>
                            <li><a href="<?php echo base_url("cerimonia/index/{$tx_slug}"); ?>">Cerimonia</a></li>
                            <li><a href="<?php echo base_url("festa/index/{$tx_slug}"); ?>">Festa</a></li>
                            <li><a href="<?php echo base_url("cotas/index/{$tx_slug}"); ?>">Cotas de Lua-de-mel</a></li>
                            <li><a href="<?php echo base_url("mensagem/index/{$tx_slug}"); ?>">Mensagem aos noivos</a></li>
                            <?php
                        } else {
                            ?>

                            <li><a href="<?php echo base_url("noivados/"); ?>">Sobre os Noivos</a></li>
                            <li><a href="<?php echo base_url("cerimonia/"); ?>">Cerimonia</a></li>
                            <li><a href="<?php echo base_url("festa/"); ?>">Festa</a></li>
                            <li><a href="<?php echo base_url("cotas/"); ?>">Cotas de Lua-de-mel</a></li>
                            <li><a href="<?php echo base_url("mensagem/"); ?>">Mensagem aos noivos</a></li>

                            <?php
                        }
                        ?>

                    </ul>

                </div>

                <div id="col2">

                    <ul>

                        <li><a href="<?php echo base_url('sobre'); ?>">Sobre a TZ Viagens Tatuapé</a></li>
                        <li><a href="<?php echo base_url('formaPagamento'); ?>">Formas de Pagamento</a></li>
                        <li><a href="<?php echo base_url('noivados'); ?>">Noivados</a></li>
                        <li><a href="<?php echo base_url('parceiros'); ?>">Parceiros</a></li>
                        <li><a href="<?php echo base_url('index'); ?>">Promoções</a></li>

                    </ul>

                </div>

                <div id="col3" style="text-align:center">

                    <div style="width: 100%; margin-top:20px; text-align:center">
                        <!-- Page plugin's width will be 180px -->
                        <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com/TZ-VIAGENS-SP-Tatuapé-1421821448108167/timeline/;width=300&amp;height=170&amp;colorscheme=dark&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100%; height:170px;" allowTransparency="true"></iframe>
                    </div>
                    <br/>
                    (11)3530-5077 / 3530-5177<br/>
                    E-mail: reservas@sptp.tzviagens.com.br

                </div>

                <div id="lineWhite"></div>

                <div class="footer">

                    &copy; 2015 - Todos os direitos reservados - desenvolvido por <a href="http://www.climaagencia.com.br" target="_blank">Clima Agência</a>

                </div>

            </div>

        </div>
        <script type="text/javascript">
            function setaImagem() {
                var settings = {
                    primeiraImg: function () {
                        elemento = document.querySelector("#slider a:first-child");
                        elemento.classList.add("ativo");
                        this.legenda(elemento);
                    },
                    slide: function () {
                        elemento = document.querySelector(".ativo");

                        if (elemento.nextElementSibling) {
                            elemento.nextElementSibling.classList.add("ativo");
                            settings.legenda(elemento.nextElementSibling);
                            elemento.classList.remove("ativo");
                        } else {
                            elemento.classList.remove("ativo");
                            settings.primeiraImg();
                        }

                    },
                    proximo: function () {
                        clearInterval(intervalo);
                        elemento = document.querySelector(".ativo");

                        if (elemento.nextElementSibling) {
                            elemento.nextElementSibling.classList.add("ativo");
                            settings.legenda(elemento.nextElementSibling);
                            elemento.classList.remove("ativo");
                        } else {
                            elemento.classList.remove("ativo");
                            settings.primeiraImg();
                        }
                        intervalo = setInterval(settings.slide, 4000);
                    },
                    anterior: function () {
                        clearInterval(intervalo);
                        elemento = document.querySelector(".ativo");

                        if (elemento.previousElementSibling) {
                            elemento.previousElementSibling.classList.add("ativo");
                            settings.legenda(elemento.previousElementSibling);
                            elemento.classList.remove("ativo");
                        } else {
                            elemento.classList.remove("ativo");
                            elemento = document.querySelector("a:last-child");
                            elemento.classList.add("ativo");
                            this.legenda(elemento);
                        }
                        intervalo = setInterval(settings.slide, 4000);
                    },
                    legenda: function (obj) {
                        var legenda = obj.querySelector("img").getAttribute("alt");
                        document.querySelector("figcaption").innerHTML = legenda;
                    }

                }

                //chama o slide
                settings.primeiraImg();

                //chama a legenda
                settings.legenda(elemento);

                //chama o slide Ã  um determinado tempo
                var intervalo = setInterval(settings.slide, 4000);
                document.querySelector(".next").addEventListener("click", settings.proximo, false);
                document.querySelector(".prev").addEventListener("click", settings.anterior, false);
            }

            window.addEventListener("load", setaImagem, false);
        </script>


    </body>


</html>
