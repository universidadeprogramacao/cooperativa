<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Administração do portal</title>

        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>public/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="<?php echo base_url(); ?>public/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="<?php echo base_url(); ?>public/dist/css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>public/dist/css/sb-admin-2.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="<?php echo base_url(); ?>public/bower_components/morrisjs/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?php echo base_url(); ?>public/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>public/admin/css/styles.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>public/library/javascripts/data-table/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>public/library/javascripts/select2-3.5.4/select2.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>public/library/javascripts/select2-3.5.4/select2-bootstrap.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script>
            var onlyRenderContente = 'N';
            var _Module = '<?php echo $module; ?>';
            var _controller = '<?php echo $controller; ?>';
            var _baseUrl = '<?php echo $baseUrl; ?>';
            var _urlPadrao = '<?php echo $urlPadrao; ?>';
            var _returnSubmit = [];
            var _populateForm = <?php echo json_encode($populateForm); ?>;
            var _dataAtual = '<?php echo date('d/m/Y'); ?>';
            var _acao = '';
<?php
$exibeBtnNovo = !empty($exibeBtnNovo) ? $exibeBtnNovo : 'N';
?>
            var _exibeBtnNovo = '<?php echo $exibeBtnNovo; ?>';

            var _permissaoCadastrar = 'S';
            var _permissaoExcluir = 'S';
<?php $action = !empty($action) ? $action : ''; ?>
            var _action = '<?php echo $action; ?>';
            var _erroPadraoAjax = '<?php echo ERRO_PADRAO_AJAX; ?>';
        </script>

        <!-- jQuery -->
        <script src="<?php echo base_url(); ?>public/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/jquery/jquery-migrate-1.2.1.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo base_url(); ?>public/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="<?php echo base_url(); ?>public/bower_components/metisMenu/dist/metisMenu.min.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/jquery-ui/jquery-ui-1.8.11.custom.min.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/jquery.populate/jquery.populate.pack.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/jquery.alphanumeric/jquery.alphanumeric.pack.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/jquery.maskedinput/jquery.maskedinput-1.2.2.min.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/jquery.maskmoney/jquery.maskMoney.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/validateEngine/languages/jquery.validationEngine-pt.min.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/validateEngine/jquery.validationEngine.js"></script>

        <script src="<?php echo base_url(); ?>public/library/javascripts/jquery.autocomplete/jquery.autocomplete.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/jquery.numeric/jquery.numeric.pack.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/jquery.floatnumber/jquery.floatnumber.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/highchart-4.1.9/js/highcharts.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/highchart-4.1.9/js/modules/exporting.js"></script>
        <!--
        <script src="<?php echo base_url(); ?>public/library/javascripts/tinymce4.1/tinymce.min.js"></script>
        -->
        <script src="//tinymce.cachefly.net/4.2/tinymce.min.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/base/php_js.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/base/form.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/base/global.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/jquery.form.min.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/data-table/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/select2-3.5.4/select2.min.js"></script>

        <!-- Morris Charts JavaScript -->
        <!--
        <script src="<?php echo base_url(); ?>public/bower_components/raphael/raphael-min.js"></script>
        <script src="<?php echo base_url(); ?>public/bower_components/morrisjs/morris.min.js"></script>
        <script src="<?php echo base_url(); ?>public/js/morris-data.js"></script>
        -->

    </head>

    <body>

        <div id="wrapper">


            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url() . "admin/home"; ?>">Sistema administrativo</a>
                </div>
                <!-- /.navbar-header -->


                <ul class="nav navbar-top-links navbar-right">




                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="<?php echo base_url() . "admin/cadastro"; ?>"><i class="fa fa-user fa-fw"></i>Meus dados</a>
                            </li>
                            <!--
                            <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                            </li>
                            -->

                            <li class="divider"></li>
                            <li><a title="Sair do sistema" id="sairSistema" href="<?php echo base_url() . "admin/sair"; ?>"><i class="fa fa-sign-out fa-fw"></i> Sair</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->


                </ul>
                <!-- /.navbar-top-links -->


                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">

                            <li>
                                <a href="<?php echo base_url() . "admin/home"; ?>"><i class="fa fa-home fa-fw"></i> Home</a>
                            </li>



                            <?php
                            if (!empty($_SESSION['admin']['id_admin'])) {
                                ?>
                                <li>
                                    <a href="#"><i class="fa fa-user fa-fw"></i> Admin<span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_url() . "admin/formulario/"; ?>">Novo</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url() . "admin"; ?>">Listar</a>
                                        </li>
                                    </ul>

                                </li>
                                <?php
                            }
                            ?>

                            <li>
                                <a href="#"><i class="fa fa-university fa-fw"></i>Clientes<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="<?php echo base_url() . "admin_usuario/formulario/"; ?>">Novo</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() . "admin_usuario/"; ?>">Listar</a>
                                    </li>

                                </ul>

                            </li>

                            <?php
                            if (!empty($_SESSION['admin']['id_admin'])) {
                                ?>
                                <li>
                                    <a href="#"><i class="fa fa-university fa-fw"></i>Produtor<span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_url() . "admin_produtor/formulario/"; ?>">Novo</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url() . "admin_produtor/"; ?>">Listar</a>
                                        </li>

                                    </ul>

                                </li>
                                <?php
                            }
                            ?>

                            <?php
                            if (!empty($_SESSION['admin']['id_admin'])) {
                                ?>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-caret-down fa-fw"></i> Produtos<span class="fa arrow"></span>
                                    </a>

                                    <ul class="nav nav-second-level">
                                        <li>
                                            <a href="<?php echo base_url() . "admin_produto/formulario"; ?>">Novo</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo base_url() . "admin_produto/"; ?>">Listar</a>
                                        </li>

                                    </ul>

                                </li>
                                <?php
                            }
                            ?>


                            <li>
                                <a href="#">
                                    <i class="fa fa-caret-down fa-fw"></i> Pedidos<span class="fa arrow"></span>
                                </a>

                                <ul class="nav nav-second-level">
                                    
                                    
                                    <?php
                                    if (!empty($_SESSION['admin']['id_produtor'])) {
                                        ?>
                                        <li>
                                            <a href="<?php echo base_url() . "admin_pedido_produtor"; ?>">Pedidos únicos</a>
                                        </li>
                                        <?php
                                    }
                                    ?>

                                    <?php
                                    if (!empty($_SESSION['admin']['id_admin'])) {
                                        ?>


                                        <li>
                                            <a href="<?php echo base_url() . "admin_pedido/"; ?>">Ver</a>
                                        </li>

                                        <?php
                                    }
                                    ?>



                                    <?php
                                    if (!empty($_SESSION['admin']['id_produtor'])) {
                                        ?>
                                        <li>
                                            <a href="<?php echo base_url() . "admin_pedido/pendente"; ?>">Pedidos pendentes</a>
                                        </li>

                                        <?php
                                    }
                                    ?>

                                    <?php
                                    if (!empty($_SESSION['admin']['id_admin'])) {
                                        ?>
                                        <li>
                                            <a href="<?php echo base_url() . "admin_pedido/cotacaoEstoque"; ?>">Fechar cotação estoque</a>
                                        </li>
                                        <?php
                                    }
                                    ?>

                                </ul>

                            </li>




                            <li>
                                <a href="#">
                                    <i class="fa fa-money fa-fw"></i> Faturas<span class="fa arrow"></span>
                                </a>

                                <ul class="nav nav-second-level">

                                    <?php
                                    if (!empty($_SESSION['admin']['id_admin'])) {
                                        ?>
                                        <li>
                                            <a href="<?php echo base_url() . "admin_fatura/"; ?>">Pedidos pagos</a>
                                        </li>
                                        <?php
                                    }
                                    ?>

                                    <?php
                                    if (!empty($_SESSION['admin']['id_admin'])) {
                                        ?>
                                        <li>
                                            <a href="<?php echo base_url() . "admin_fatura/pagarProdutor"; ?>">Pagar produtor</a>
                                        </li>

                                        <li>
                                            <a href="<?php echo base_url() . "admin_fatura/produtorPago"; ?>">Produtores pagos</a>
                                        </li>
                                        <?php
                                    }
                                    ?>

                                    <?php
                                    if (!empty($_SESSION['admin']['id_produtor'])) {
                                        ?>
                                        <li>
                                            <a href="<?php echo base_url() . "admin_fatura/fornecimento"; ?>">Meus fornecimentos</a>
                                        </li>
                                        <?php
                                    }
                                    ?>

                                </ul>

                            </li>



                        </ul>


                    </div>

                </div>

            </nav>


            <!-- conteudo -->
            <?php echo $contents; ?>
            <!-- fim conteudo -->

        </div>
        <!-- /#wrapper -->



        <!-- Custom Theme JavaScript -->
        <script src="<?php echo base_url(); ?>public/dist/js/sb-admin-2.js"></script>
        <style type="text/css">
            #divBotoes button{
                margin-right: 8px;
            }
        </style>
        <script>
            $(function () {
                //$('li').removeClass('active');
                $('#sairSistema').click(function (e) {
                    e.preventDefault();
                    var link = $(this).attr('href');
                    Dialog.confirm("Você irá ser deslogado do sistema, confirma operação?", "Confirmação", function () {
                        window.location = link;
                    });
                });
            });
        </script>

    </body>

</html>