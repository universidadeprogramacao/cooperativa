<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="eric ferraz|eric.ferras@fatec.sp.gov.br">

        <title>Login</title>

        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>public/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="<?php echo base_url(); ?>public/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="<?php echo base_url(); ?>public/dist/css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>public/dist/css/sb-admin-2.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="<?php echo base_url(); ?>public/bower_components/morrisjs/morris.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?php echo base_url(); ?>public/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>public/admin/css/styles.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>public/library/javascripts/data-table/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
         <link href="<?php echo base_url(); ?>public/library/javascripts/select2-3.5.4/select2.css" rel="stylesheet" type="text/css">
         <link href="<?php echo base_url(); ?>public/library/javascripts/select2-3.5.4/select2-bootstrap.css" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script>
            var onlyRenderContente = 'N';
            var _Module = '<?php echo $module; ?>';
            var _controller = '<?php echo $controller; ?>';
            var _baseUrl = '<?php echo $baseUrl; ?>';
            var _urlPadrao = '<?php echo $urlPadrao; ?>';
            var _returnSubmit = [];
            var _populateForm = <?php echo json_encode($populateForm); ?>;
            var _dataAtual = '<?php echo date('d/m/Y'); ?>';
            var _acao = '';
<?php
$exibeBtnNovo = !empty($exibeBtnNovo) ? $exibeBtnNovo : 'N';
?>
            var _exibeBtnNovo = '<?php echo $exibeBtnNovo; ?>';

            var _permissaoCadastrar = 'S';
            var _permissaoExcluir = 'S';
<?php $action = !empty($action) ? $action : ''; ?>
            var _action = '<?php echo $action; ?>';
            var _erroPadraoAjax = '<?php echo ERRO_PADRAO_AJAX; ?>';
        </script>

        <!-- jQuery -->
        <script src="<?php echo base_url(); ?>public/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/jquery/jquery-migrate-1.2.1.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo base_url(); ?>public/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="<?php echo base_url(); ?>public/bower_components/metisMenu/dist/metisMenu.min.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/jquery-ui/jquery-ui-1.8.11.custom.min.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/jquery.populate/jquery.populate.pack.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/jquery.alphanumeric/jquery.alphanumeric.pack.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/jquery.maskedinput/jquery.maskedinput-1.2.2.min.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/jquery.maskmoney/jquery.maskMoney.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/validateEngine/languages/jquery.validationEngine-pt.min.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/validateEngine/jquery.validationEngine.js"></script>

        <script src="<?php echo base_url(); ?>public/library/javascripts/jquery.autocomplete/jquery.autocomplete.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/jquery.numeric/jquery.numeric.pack.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/jquery.floatnumber/jquery.floatnumber.js"></script>
        <!--
        <script src="<?php echo base_url(); ?>public/library/javascripts/tinymce4.1/tinymce.min.js"></script>
        -->
        <script src="//tinymce.cachefly.net/4.2/tinymce.min.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/base/php_js.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/base/form.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/base/global.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/jquery.form.min.js"></script>
        <script src="<?php echo base_url(); ?>public/library/javascripts/data-table/jquery.dataTables.min.js"></script>
          <script src="<?php echo base_url(); ?>public/library/javascripts/select2-3.5.4/select2.min.js"></script>

     

    </head>

    <body>

    <div class="container">
       <?php echo $contents; ?>
    </div>
        <!-- Custom Theme JavaScript -->
        <script src="<?php echo base_url(); ?>public/dist/js/sb-admin-2.js"></script>
        <style type="text/css">
            #divBotoes button{
                margin-right: 8px;
            }
        </style>

    </body>

</html>