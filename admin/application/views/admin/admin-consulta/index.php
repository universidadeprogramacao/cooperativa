<div id="page-wrapper">
    <div class="row" style="margin-bottom: 10px;margin-top: 5px;" id="divBotoes"></div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Pesquisar Consultas efetuadas</h3>
                </div>
                <div class="panel-body">

                    <form method="get" class="form-horizontal populate" action="<?php echo "{$urlPadrao}"; ?>" id="validate">
                        
                         <div class="form-group">
                            <label class="col-xs-8 col-sm-1 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Renavam:
                            </label>
                            <div class="col-xs-10 col-sm-8">
                                <input type="text" name="consulta[tx_renavam]" id="consulta-tx_renavam" class="form-control" maxlength="40">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-xs-8 col-sm-1 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Nome:
                            </label>
                            <div class="col-xs-10 col-sm-8">
                                <input type="text" name="consulta[tx_nome]" id="consulta-tx_nome" class="form-control" maxlength="60">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-xs-8 col-sm-1 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Email:
                            </label>

                            <div class="col-xs-10 col-sm-7">
                                <div class="input-group">
                                    <input type="text" name="consulta[tx_email]" id="consulta-tx_email" class="form-control validate" maxlength="60">
                                    <span class="input-group-addon">
                                        @
                                    </span>
                                </div>
                            </div>

                        </div>



                        <div class="form-group">
                            <label class="col-xs-8 col-sm-1 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Dia:
                            </label>

                            <div class="col-xs-3 col-sm-1">
                                <input type="text" name="consulta[nr_dia]" id="consulta-nr_dia" class="form-control numeric validate" maxlength="2">
                            </div>

                            <label class="col-xs-8 col-sm-1 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Mes:
                            </label>

                            <div class="col-xs-3 col-sm-2">
                                <?php
                                $meses = $this->util->getDataComboMes();
                                ?>

                                <select name="consulta[nr_mes]" id="consulta-nr_mes" class="form-control validate">
                                    <?php
                                    foreach ($meses as $key => $val) {
                                        ?>
                                        <option value="<?php echo $key; ?>" title="<?php echo $val; ?>"><?php echo $val; ?></option>

                                        <?php
                                    }
                                    ?>
                                </select>

                            </div>

                            <label class="col-xs-8 col-sm-1 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Ano:
                            </label>

                            <div class="col-xs-3 col-sm-2">
                                <input type="text" name="consulta[nr_ano]" id="consulta-nr_ano" class="form-control numeric validate" maxlength="4">
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="col-xs-5 col-sm-5 col-sm-offset-1">
                                <button type="submit" class="btn btn-info">
                                    <span class="glyphicon glyphicon-search"></span>
                                    Pesquisar
                                </button>
                                <?php 
                                    $parametros   = $_SERVER['QUERY_STRING'];
                                    $UrlAtual     = base_url().'Admin_consulta?' . $parametros;
                                    //echo $UrlAtual;
                                ?>
                                <a href="<?php echo $UrlAtual;?>&exportar=1" class="btn btn-success">Exportar</a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

            Total de consultas: <strong><?php echo $dataTotal; ?></strong>

            <table class="table table-bordered table-striped" id="dataTable">
                <thead>
                    <tr>
                        <th style="width: 8%;">&nbsp;</th>
                        <th style="width: 25%;">Nome</th>
                        <th style="width: 25%;">Email</th>
                        <th style="width: 20%;">Revavam</th>
                        <th style="width: 15%;">Data da consulta</th>
                        <th style="width: 10%;">Valor total</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if (!empty($dataGrid)) {
                        foreach ($dataGrid as $resultado) {
                            ?>
                            <tr id="linhaCadastro-<?php echo $resultado['id_consulta']; ?>">
                                <td>
                                    <div class="btn-group">
                                        <a href="#" id_consulta="<?php echo $resultado['id_consulta']; ?>"  title="Ver mais dados" style="margin: 3px;" class="btn btn-xs btn-info id_consulta" >
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </a>

                                        <button id_param="<?php echo $resultado['id_consulta']; ?>" title="Excluir" type="button" style="margin: 3px;" class="btn btn-xs btn-danger buttonDelete">
                                            <span class="glyphicon glyphicon-remove"></span>
                                        </button>

                                    </div>
                                </td>

                                <td><?php echo $resultado['tx_nome']; ?></td>
                                <td><?php echo $resultado['tx_email']; ?></td>
                                <td><?php echo $resultado['tx_renavam']; ?></td>
                                <td><?php echo $this->util->reverseDate($resultado['dt_consulta']); ?></td>
                                <td><?php echo $this->util->floatToMoneyV1($resultado['vl_total']); ?></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                             <td>&nbsp;</td>
                            <td> <div class="alert alert-warning">
                                    Nenhum registro encontrado
                                </div>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>

</div>
<!-- /#page-wrapper -->


<div id="htmlConsultaGeral" style="display: none;">

    <?php
    if (!empty($dataGrid)) {
        foreach ($dataGrid as $resultado) {
            ?>
            <div id="consulta-<?php echo $resultado['id_consulta'] ?>">
                <table class="table">
                    <tr>
                        <td>

                            <b>Data da consulta:</b> <?php echo $this->util->reverseDate($resultado['dt_consulta']); ?>
                            <br/>
                            <b>IP:</b> <?php echo $resultado['tx_ip']; ?>
                            <br/>

                            <b>Nome:</b> <?php echo $resultado['tx_nome']; ?>
                            <br/>
                            <b>Email:</b>  <?php echo $resultado['tx_email']; ?>
                            <br/>

                            <b>Renavam:</b>  <?php echo $resultado['tx_renavam']; ?>
                            <br/>

                        </td>
                    </tr>

                    <tr>
                        <td>
                            <br/>
                            <?php echo $resultado['tx_consulta']; ?>
                            <br/>
                        </td>
                    </tr>
                </table>
            </div>

            <?php
        }
    }
    ?>

</div>

<div class="modal fade bs-example-modal-sm print" id="modalConsulta"  tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 50%;">
        <div class="modal-content">
            <div class="modal-header">
                <button  type="button" class="close no-print" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Detalhamento de consulta</h4>
            </div>
            <div class="modal-body" id="load-consulta">
            </div>
            <div class="modal-footer no-print" style="margin-top: 10px;">
                <button onclick="imprimir('modalConsulta');" type="button" title="Imprimir" class="btn btn-primary">
                    <span class="glyphicon glyphicon-print"></span>
                    Imprimir
                </button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">
                    <span class="glyphicon glyphicon-eye-close"></span>
                    Fechar
                </button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    $(document).ready(function () {

        initBtnPageFormulario();
        $('#btnNovo').hide();
        $('#btnNovo').click(function () {
            window.location = _urlPadrao + '/formulario';
        });

        $('.buttonDelete').click(function () {
            var id_param = $(this).attr('id_param');
            Dialog.confirm("Deseja realmente excluir esse registro", "Confirma", function () {
                excluir(id_param);
            });
        });

        $('.id_consulta').click(function (e) {
            e.preventDefault();
            var consulta = $('#consulta-' + $(this).attr('id_consulta')).html();
            _initModalDetalharConsulta(consulta);
        });

    });

    function _initModalDetalharConsulta(htmlConsulta) {
        $('#load-consulta').html(htmlConsulta);
        $('#modalConsulta').modal();
    }

    function imprimir(objeto) {

        var conteudo = $('#' + objeto).html();
        impressao = window.open('about:blank');
        impressao.document.write(conteudo);
        impressao.window.print();
        impressao.window.close();

    }

    function excluir(id_param) {
        ShowMsgAguarde();
        $.ajax({
            url: _baseUrl + _controller + '/excluir',
            type: 'POST',
            dataType: 'json',
            data: {id_param: id_param},
            success: function (data) {
                if (data.success !== undefined && data.success !== '') {
                    var linhaCadastro = $('#linhaCadastro-' + id_param);
                    $("#consulta-" + id_param).remove();
                    if (linhaCadastro.length > 0) {
                        linhaCadastro.fadeOut(800, function () {
                            $(this).remove();
                        });
                    }

                    Dialog.success(data.success, 'Sucesso');
                }
                else if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.error, 'Erro');
                }
                else {
                    Dialog.error(_erroPadraoAjax, 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }

        });
    }

</script>

<style>

    @media print {
        .no-print {
            visibility: hidden;
        }

    }


</style>