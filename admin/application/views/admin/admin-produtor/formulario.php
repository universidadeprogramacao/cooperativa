
<div id="page-wrapper">
    <div class="row" style="margin-bottom: 10px;margin-top: 5px;" id="divBotoes"></div>
    <div class="row">
        <?php
        if (!empty($error)) {
            ?>
            <div class="col-sm-12 col-xs-12">
                <div class="alert alert-danger"><?php echo $error; ?></div>
            </div>

            <?php
        }
        ?>

        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Cadastro de Produtor</h3>
                </div>
                <div class="panel-body">
                 
                     <form method="post" class="form-horizontal populate" action="<?php echo "{$urlPadrao}/salvar"; ?>" id="validate">
                    
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a data-toggle="tab" href="#cliente">Produtor</a></li>
                    <li>
                        <a data-toggle="tab" href="#telefone">Telefone</a>
                    </li>
                   
                </ul>
                    
                     <div class="tab-content">
                    <div id="cliente" class="tab-pane fade in active">
                        <?php include_once 'tab-usuario.php'; ?>
                    </div>
                    <div id="telefone" class="tab-pane fade">
                         <?php include_once 'tab-telefone.php'; ?>
                    </div>
                   
                </div>
                    
                    
                   
                       

                     </form>
                </div>
                
            </div>


        </div>
    </div>
</div>

<!-- /#page-wrapper -->

<script>
    var st_indsenhapadrao = $('#admin-st_indsenhapadrao');
    var aviso = $('#aviso');
    var senha = $('#senha');
    $(document).ready(function () {

        initBtnPageFormulario();

        if (st_indsenhapadrao.val() !== '' && st_indsenhapadrao.val() == 'N') {
            aviso.show();
            senha.val('123456');
        } else {
            senha.val($.trim($('#admin-tx_senha').val()));
        }
        $('#btnNovo').click(function () {
            window.location = _urlPadrao + '/formulario';
        });
        var dt_cadastro = $.trim($('#admin-dt_cadastro').val());
        if (dt_cadastro == '') {
            $('#admin-dt_cadastro').val(_dataAtual);
        }

        $('#admin-tx_email').blur(function () {

            tx_email = $.trim($(this).val());
            //alert(tx_slug);
            if (tx_email !== '') {
                validarEmail(tx_email);
            }
        });

        $('#btnGerarSenha').click(function () {
            gerarSenha();
        });

        $('#btnSalvar').click(function () {

            var formulario = $('#validate');

            if (formulario.validationEngine('validate')) {
                salvar(formulario);
            }
        });
    });

    function validarEmail(tx_email) {
        ShowMsgAguarde();
        $.ajax({
            url: _baseUrl + _controller + '/validarEmail',
            type: 'POST',
            dataType: 'json',
            data: {tx_email: tx_email},
            success: function (data) {
                if (data.validou !== undefined && data.validou == 'N') {
                    $('#admin-tx_email').val('');
                }

                if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.error, 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }

        });
    }

    function gerarSenha() {
        ShowMsgAguarde();

        $.ajax({
            url: _baseUrl + _controller + '/gerarSenha',
            type: 'POST',
            dataType: 'json',
            data: {st_indsenhapadrao: st_indsenhapadrao.val()},
            success: function (data) {
                if (data.tx_senha !== undefined && data.tx_senha !== '') {
                    senha.val(data.tx_senha);
                    $('#admin-tx_senha').val(data.tx_senha);
                    st_indsenhapadrao.val('S');
                    aviso.fadeOut(500);
                }
                else if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.error, 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }

        });
    }



    function salvar(formulario) {
        ShowMsgAguarde();
        //formulario.submit();return false;
        formulario.ajaxSubmit({
            success: function (data) {
                data = $.parseJSON(data);

                if (data.success !== undefined && data.success !== '') {
                    if (data.dataGrid.id_produtor !== undefined && data.dataGrid.id_produtor !== '') {
                        $("#admin-id_produtor").val(data.dataGrid.id_produtor);
                        if (data.dataGrid.st_indsenhapadrao !== undefined && data.dataGrid.st_indsenhapadrao == 'N') {
                            aviso.show();
                            senha.val('123456');
                        } else {
                            senha.val(data.dataGrid.tx_senha);
                        }
                    }
                    Dialog.success(data.success, 'Sucesso');
                } else if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.erro, 'Erro');
                } else {
                    Dialog.error('Falha ao salvar', 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax, 'Erro');
            },
            complete: function () {
                CloseMsgAguarde();
            }
        });

    }

</script>