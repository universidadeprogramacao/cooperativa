<br/>
<div class="form-group">
                        <div class="col-sm-2">
                            <button class="btn btn-success" type="button" onclick="addTelefone();">
                                <span class="glyphicon glyphicon-plus"></span>
                                Add
                            </button>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>

                                <tr>
                                    <td style="width: 70%;">
                                        Telefone
                                    </td>

                                    <td style="width: 20%;">
                                        Tipo
                                    </td>

                                    <td style="width: 10%;">
                                        Ação
                                    </td>
                                </tr>

                            </thead>

                            <tbody id="corpo-telefones">
                                <?php
                                if(!empty($telefone)){
                                    foreach ($telefone as $key=> $tel){
                                ?>
                                
                                <tr>
                                    <td>
                                        <input class="form-control" type="text" name="telefone[<?php echo $key ?>][tx_telefone]" value="<?php echo $tel['tx_telefone'] ?>" maxlength="45">
                                    </td>
                                    <td>
                                        <select class="form-control" id="telefone-0-tx_tipo" name="telefone[<?php echo $key; ?>][tx_tipo]">
                                            <?php
                                            if($tel['tx_tipo'] =='FIXO'){
                                            ?>
                                            <option selected="selected" value="FIXO">FIXO</option>
                                            <option value="CELULAR">CELULAR</option>
                                            <?php
                                    }else{
                                            ?>
                                            
                                             <option  selected="selected" value="CELULAR">CELULAR</option>
                                             <option value="FIXO">FIXO</option>
                                           
                                            <?php
                                    }
                                            ?>
                                        </select>

                                    </td>


                                    <td>
                                        <button type="button" class="btn btn-danger" onclick="removerTelefone($(this));">
                                            <span class="glyphicon glyphicon-remove"></span>

                                        </button>
                                    </td>
                                </tr>
                                <?php
                                    }
                                }
                                ?>
                                

                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
        var totalTelefones = 1;

        function addTelefone() {
            totalTelefones++;
            var html = '<tr>';
            html += '<td>';
            html += '<input class="form-control" id="telefone-' + totalTelefones + '-tx_telefone" type="text" name="telefone[' + totalTelefones + '][tx_telefone]" maxlength="45">';
            html += '</td>';
            html += '<td>';
            html += '<select class="form-control" id="telefone-' + totalTelefones + '-tx_tipo" name="telefone['+totalTelefones+'][tx_tipo]">';
            html += '<option value="FIXO">FIXO</option>';
            html += '<option value="CELULAR">CELULAR</option>'
            html += '</select>';
            html += '</td>';
            html += '<td>';
            html += '<button type="button" class="btn btn-danger" onclick="removerTelefone($(this));">';
            html += '<span class="glyphicon glyphicon-remove"></span>';
            html += '</button>';
            html += '</td>';
            html += '</tr>';
            $("#corpo-telefones").append(html);
        }

        function removerTelefone(objeto) {
            objeto.parents('tr').remove();
        }

    </script>