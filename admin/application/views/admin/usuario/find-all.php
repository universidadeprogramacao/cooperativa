<div class="col-sm-12">
    <div class="alert alert-warning">
        *A consulta é limitada a 5 registros, utilize os filtros para simplificar a busca.
    </div>
</div>
<table class="table table-bordered table-striped" id="dataTable">
    <thead>
        <tr>
            <th style="width: 10%;">&nbsp;</th>
            <th style="width: 30%;">Nome</th>
            <th style="width: 30%;">Email</th>
            <th style="width: 15%;">Data de cadastro</th>
            <th style="width: 15%;">Status</th>
        </tr>
    </thead>

    <tbody>
        <?php
        if (!empty($dataGrid)) {
            foreach ($dataGrid as $resultado) {
                ?>
                <tr id="linhaCadastro-<?php echo $resultado['id_usuario']; ?>">
                    <td>
                        <div class="btn-group">
                            <a id_usuario="<?php echo $resultado['id_usuario'] ?>" href="<?php echo "{$urlPadrao}/formulario/{$resultado['id_usuario']}/{$this->util->gerarSlug($resultado['tx_nome'])}"; ?>"  title="Selecionar" style="margin: 3px;" class="btn btn-xs btn-info selecaoBuscaUsuario" >
                                <span class="glyphicon glyphicon-thumbs-up"></span>
                            </a>

                        </div>
                    </td>

                    <td><?php echo $resultado['tx_nome']; ?></td>
                    <td><?php echo $resultado['tx_email']; ?></td>
                    <td><?php echo $this->util->reverseDate($resultado['dt_cadastro']); ?></td>
                    <td><?php echo $this->util->getStatus($resultado['st_status']); ?></td>
                </tr>
                <?php
            }
        } else {
            ?>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td> <div class="alert alert-warning">
                        Nenhum registro encontrado
                    </div>
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

<script>
    $(function () {
        $(".selecaoBuscaUsuario").click(function (e) {
            e.preventDefault();
            var id_usuario = $(this).attr('id_usuario');
            findClienteById(id_usuario);
        });
    });

    function findClienteById(id_usuario) {
        ShowMsgAguarde();
        $.ajax({
            url: '<?php echo base_url('admin_usuario/findById'); ?>',
            data: {id_usuario: id_usuario},
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                $('#renderUsuario').html(data);
                $("#modalConsultaCliente").modal('hide');
            },
            error: function () {
                Dialog.error(_erroPadraoAjax, 'Erro');
            }, complete: function () {
                CloseMsgAguarde();
            }

        });
    }
</script>