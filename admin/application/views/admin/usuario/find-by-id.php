<div class="form-group">
    <div class="btn-group">
        <button id="btnExibirMaisInformacoesUsuario" style="margin-right: 5px;" type="button" title="Exibir mais informações" class="btn btn-info">
            <span class="glyphicon glyphicon-eye-open"></span>
            Exibir mais informações
        </button>

        <button id="btnOcultarMaisInformacoesUsuario" type="button" title="Ocultar mais informações" class="btn btn-default">
            <span class="glyphicon glyphicon-eye-close"></span>
            Ocultar mais informações
        </button>
    </div>
</div>

<div id="usuarioDesabilitarCampos" class="populate">
    <input type="hidden" name="admin[id_usuario]" id="admin-id_usuario">

<div class="form-group">
    <label class="col-xs-8 col-sm-1 control-label">
    
        Status:
    </label>
    <div class="col-xs-7 col-sm-6">


        <span class="radio radio-inline">
            <label>
                <input type="radio" name="admin[st_status]"  id="admin-st_status-A" value="A">Cadastro Ativo
            </label>
        </span>

        <span class="radio radio-inline">
            <label>
                <input type="radio" name="admin[st_status]" id="admin-st_status-I" value="I">Cadastro Inativo
            </label>
        </span>

    </div>
</div>

<div class="form-group">
    <label class="col-xs-8 col-sm-2 control-label">
    
        Data de cadastro:
    </label>

    <div class="col-xs-5 col-sm-2">
        <div class="input-group">
            <input type="text" name="admin[dt_cadastro]" id="admin-dt_cadastro" class="form-control date">
            <span class="input-group-addon">
                <span class="fa fa-calendar"></span>
            </span>
        </div>
    </div>

</div>


<div class="form-group">
    <label class="col-sm-1 control-label">
       
        Nome:
    </label>
    <div class="col-sm-6">
        <input type="text" name="admin[tx_nome]"  id="admin-tx_nome" class="form-control validate" maxlength="60">
    </div>

    <label class="col-sm-2 control-label">
       
        CPF Ou CNPJ:
    </label>
    <div class="col-sm-2">
        <input type="text" name="admin[tx_cpf]"  id="admin-tx_cpf" class="form-control validate" maxlength="30">
    </div>
</div>



<div class="form-group">
    <label class="col-sm-1 control-label">
       
        Sexo:
    </label>
    <div class="col-sm-4">
        <span class="radio radio-inline">
            <label>
                <input type="radio" name="admin[tx_sexo]"  id="admin-tx_sexo-M" value="M">Masculino
            </label>
        </span>

        <span class="radio radio-inline">
            <label>
                <input type="radio" name="admin[tx_sexo]" id="admin-tx_sexo-F" value="F">Feminino
            </label>
        </span>
    </div>

    <label class="col-sm-1 control-label">
       
        Tipo:
    </label>
    <div class="col-sm-4">
        <span class="radio radio-inline">
            <label>
                <input type="radio" name="admin[tx_tipopessoa]"  id="admin-tx_tipopessoa-F" value="F">Pessoa Física
            </label>
        </span>

        <span class="radio radio-inline">
            <label>
                <input type="radio" name="admin[tx_tipopessoa]" id="admin-tx_tipopessoa-J" value="J">Pessoa Jurídica
            </label>
        </span>
    </div>
</div>


<div class="form-group">
    <label class="col-xs-8 col-sm-1 control-label">
       
        Email:
    </label>

    <div class="col-xs-8 col-sm-5">
        <div class="input-group">
            <input type="text" name="admin[tx_email]"  id="admin-tx_email" class="form-control lower validate[custom[email]]" maxlength="60">
            <span class="input-group-addon">
                @
            </span>
        </div>
    </div>

</div>


<div id="mais-informacoes" style="display: none;">
    <div class="form-group">
        <label class="col-xs-8 col-sm-1 control-label">
           
            DDD:
        </label>

        <div class="col-sm-1">
            <input type="text" name="admin[tx_ddd]" id="admin-tx_ddd" class="form-control numeric validate" maxlength="2">
        </div>

        <label class="col-sm-1 control-label">
           
            Tel:
        </label>

        <div class="col-sm-3">
            <input type="text" name="admin[tx_numero]" id="admin-tx_numero" class="form-control numeric validate" maxlength="20">
            <div class="help-block">
                *Sem espaços ou acentos
            </div>
        </div>

    </div>

    <div class="form-group">
        <label class="col-sm-1 control-label">
           
            CEP:
        </label>

        <div class="col-sm-4">
            <input type="text"  name="admin[tx_cep]" id="admin-tx_cep" class="form-control numeric validate" maxlength="8">
        </div>

        <label class="col-sm-1 control-label">
           
            Bairro:
        </label>

        <div class="col-sm-5">
            <input type="text" name="admin[tx_bairro]" id="admin-tx_bairro" class="form-control validate" maxlength="40">
        </div>

    </div>

    <div class="form-group">
        <label class="col-sm-1 control-label">
           
            Cidade:
        </label>

        <div class="col-sm-10">
            <input type="text" name="admin[tx_cidade]" id="admin-tx_cidade" class="form-control validate" maxlength="40">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-1 control-label">
           
            Rua:
        </label>

        <div class="col-sm-7">
            <input type="text" name="admin[tx_rua]" id="admin-tx_rua" class="form-control validate" maxlength="150">
        </div>

        <label class="col-sm-1 control-label">
           
            Número:
        </label>

        <div class="col-sm-2">
            <input type="text" name="admin[tx_numerorua]" id="admin-tx_numerorua" class="form-control validate" maxlength="10">
        </div>
    </div>


    <div class="form-group">
        <label class="col-sm-2 control-label">
           
            Complemento:
        </label>

        <div class="col-sm-4">
            <input type="text" name="admin[tx_complemento]" id="admin-tx_complemento" class="form-control validate" maxlength="20">
            <div class="help-block">
                Exemplo: Casa / Apartamento xxx
            </div>
        </div>

        <label class="col-sm-2 control-label">
           
            Estado:
        </label>

        <div class="col-sm-3">
            <select name="admin[tx_estado]" id="admin-tx_estado" class="form-control validate">
                <option value="">Selecione</option>
                <option value="AC">AC</option>
                <option value="AL">AL</option>
                <option value="AM">AM</option>
                <option value="AP">AP</option>
                <option value="BA">BA</option>
                <option value="CE">CE</option>
                <option value="DF">DF</option>
                <option value="ES">ES</option>
                <option value="GO">GO</option>
                <option value="MA">MA</option>
                <option value="MG">MG</option>
                <option value="MS">MS</option>
                <option value="MT">MT</option>
                <option value="PA">PA</option>
                <option value="PB">PB</option>
                <option value="PE">PE</option>
                <option value="PI">PI</option>
                <option value="PR">PR</option>
                <option value="RJ">RJ</option>
                <option value="RN">RN</option>
                <option value="RS">RS</option>
                <option value="RO">RO</option>
                <option value="RR">RR</option>
                <option value="SC">SC</option>
                <option value="SE">SE</option>
                <option value="SP">SP</option>
                <option value="TO">TO</option>
            </select>
        </div>

    </div>

</div>
    
</div>

<script>
    $(function () {
        _populateForm = <?php echo json_encode($populateForm); ?>;
        $('.populate').populate(_populateForm);
        //$("#usuarioDesabilitarCampos input, select, textArea").attr('disabled',true);
        //$("#admin-id_usuario").attr('disabled',false);
        
        $("#btnExibirMaisInformacoesUsuario").click(function () {
            $("#mais-informacoes").fadeIn(1000);
        });

        $("#btnOcultarMaisInformacoesUsuario").click(function () {
               $("#mais-informacoes").fadeOut(1000);
        });
    });
</script>



