<div id="page-wrapper">
    <div class="row" style="margin-bottom: 10px;margin-top: 5px;" id="divBotoes"></div>
    <div class="row">
        <div class="col-xs-12 col-sm-22 col-lg-12 col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Pesquisar</h3>
                </div>
                <div class="panel-body">

                    <form method="get" class="form-horizontal populate" action="<?php echo "{$urlPadrao}/pagarProdutor"; ?>" id="validate">


                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Produtor:
                            </label>

                            <div class="col-xs-10 col-sm-10">
                                <input type="text" name="venda[tx_nome]" id="venda-tx_nome" class="form-control validate" maxlength="60">
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                E-mail:
                            </label>

                            <div class="col-xs-5 col-sm-5">
                                <input type="text" name="venda[tx_email]" id="venda-tx_email" class="form-control validate" maxlength="60">
                            </div>

                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                CPF / CNPJ:
                            </label>

                            <div class="col-xs-3 col-sm-3">
                                <input type="text" name="venda[tx_cpf]" id="venda-tx_cpf" class="form-control validate" maxlength="30">
                            </div>

                        </div>


                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Data:
                            </label>

                            <div class="col-xs-5 col-sm-2">
                                <input type="text" name="venda[dt_vendainicio]" id="venda-dt_vendainicio" class="form-control date validate[custom[date]]">
                            </div>
                            <div class="col-sm-1 col-xs-2">
                                A
                            </div>
                            <div class="col-xs-5 col-sm-2">
                                <input type="text" name="venda[dt_vendafim]" id="venda-dt_vendafim" class="form-control date validate[custom[date]]">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-5 col-sm-5 col-sm-offset-2">
                                <button type="submit"  class="btn btn-info">
                                    <span class="glyphicon glyphicon-search"></span>
                                    Pesquisar
                                </button>

                            </div>
                        </div>

                    </form>


                </div>



            </div>
        </div>

        <div class="col-sm-12">
            <h3>Produtos fornecedidos</h3>
            <form method="post" id="formSalvarPedido" class="form-horizontal populate" action="<?php echo "{$urlPadrao}/salvarFechamento"; ?>" >
                <table class="table table-bordered" id="datatable">
                    <thead>
                        <tr style="background-color: #CCC;">
                            <th style="width: 30%;">
                                Fornecedor
                            </th>

                            <th style="width: 10%;">
                                Data
                            </th>

                            <th style="width: 20%;">
                                Produto
                            </th>
                            <th style="width: 10%;">
                                Valor Unitário
                            </th>
                            <th style="width: 10%;">
                                Quantidade Fornecida
                            </th>
                            <th style="width: 10%;">
                                Sub Total
                            </th>
                            <th style="width: 10%;">
                                <p>Pagar</p>
                                <p>
                                    <button id="btnMarcarComoPagos" onclick="acaoMarcarPago();" type="button" title="Marcar todos como pagos" class="btn btn-info">Marcar todos como pagos</button>
                                </p>
                            </th>

                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        $totalPago = 0;
                        $totalGeral = 0;
                        $k = 1;
                        $id_funcAntigo = null;
                        $exibeSalvar = false;
                        if (!empty($dataGrid)) {
                            $totalRegistros = count($dataGrid);
                            $exibeSalvar = true;
                            foreach ($dataGrid as $key => $pedido) {

                                $pedido['dt_fechamentooriginal'] = $this->util->reverseDate($pedido['dt_fechamentooriginal']);
                                $totalParcial = $pedido['vl_unitariooriginal'] * $pedido['nr_qtdfornecida'];
                                $totalGeral+=$totalParcial;
                                ?>

                                <?php
                                if ($id_funcAntigo != $pedido['id_produtor'] && $id_funcAntigo <> null) {
                                    ?>

                                    <tr style="background-color: #CCC;">
                                        <td>
                                            &nbsp;
                                        </td>

                                        <td>
                                            &nbsp;
                                        </td>

                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            Total
                                        </td>
                                        <td>
                                            <?php echo $this->util->floatToMoneyV1($totalPago); ?>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>

                                    </tr>


                                    <?php
                                    $totalPago = 0;
                                }
                                $id_funcAntigo = $pedido['id_produtor'];
                                $totalPago+=$totalParcial;
                                ?>



                                <tr>

                                    <td>
                                        <p><b>Nome:</b> <?php echo $pedido['tx_nome']; ?></p>
                                        <p><b>Cpf/Cnpj:</b> <?php echo $pedido['tx_cpf']; ?></p>
                                        <p><b>Email:</b> <?php echo $pedido['tx_email']; ?></p>
                                        <p><b>Tel:</b> ( <?php echo $pedido['tx_ddd']; ?>) <?php echo $pedido['tx_numero']; ?></p>
                                        <p><b>Rua:</b> <?php echo $pedido['tx_rua']; ?></p>
                                        <p><b>Número:</b> <?php echo $pedido['tx_numerorua']; ?></p>
                                        <p><b>Bairro:</b> <?php echo $pedido['tx_bairro']; ?></p>
                                        <p><b>Cep:</b> <?php echo $pedido['tx_cep']; ?></p>
                                        <p><b>Cidade - Estado:</b> <?php echo $pedido['tx_cidade']; ?> - <?php echo $pedido['tx_estado']; ?> </p>

                                    </td>
                                    <td>
                                        <?php echo $pedido['dt_fechamentooriginal']; ?>
                                    </td>

                                    <td>
                                        <?php echo $pedido['tx_produto']; ?>
                                    </td>
                                    <td>
                                        <?php echo $this->util->floatToMoneyV1($pedido['vl_unitariooriginal']); ?>
                                    </td>
                                    <td>
                                        <?php echo $pedido['nr_qtdfornecida']; ?>
                                    </td>

                                    <td>
                                        <?php echo $this->util->floatToMoneyV1($totalParcial); ?>
                                    </td>
                                    <td>
                                        <input title="Pagar" type="checkbox" name="pagar[<?php echo $pedido['id_pedidocotacaofornecimento']; ?>]" class="form-control pagarFornecedor" value="<?php echo $pedido['id_pedidocotacaofornecimento']; ?>">

                                    </td>

                                </tr>

                                <?php
                                if ($k == $totalRegistros) {
                                    ?>

                                    <tr style="background-color: #CCC;">
                                        <td>
                                            &nbsp;
                                        </td>

                                        <td>
                                            &nbsp;
                                        </td>

                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td>
                                            Total
                                        </td>
                                        <td>
                                            <?php echo $this->util->floatToMoneyV1($totalPago); ?>
                                        </td>
                                        <td>
                                            &nbsp;
                                        </td>

                                    </tr>


                                    <?php
                                }
                                ?>




                                <?php
                                $k++;
                            }
                        }
                        ?>
                    </tbody>

                </table>

                <?php
                if ($exibeSalvar) {
                    ?>

                    <div class="form-group">
                        <div class="col-sm-4">
                            <div class="col-sm-4">
                                <button type="button" id="btnSalvarPagamento" class="btn btn-success">
                                    Salvar
                                </button>
                            </div>
                        </div>
                    </div>

                    <?php
                }
                ?>

            </form>
        </div>




    </div>
</div>

</div>
<!-- /#page-wrapper -->

<div class="modal fade" id="modalVenda"  style="width: 100% !important;overflow: auto;height:auto;">
    <div class="modal-dialog"  style="width: 100% !important;">
        <div class="modal-content"  style="width: 100% !important;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Detalhamento da venda</h4>
            </div>
            <div class="modal-body" id="corpoVenda"  style="width: 100% !important;">
            </div>
            <div class="modal-footer">


                <button type="button" class="btn btn-danger" data-dismiss="modal">
                    <span class="glyphicon glyphicon-eye-close"></span>
                    Fechar
                </button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<form method="get" id="formularioAcoes"></form>

<script>

    $(document).ready(function () {


        // _initButtonDelete();
        $('#btnNovo').hide();
        $('#btnConsultar').click(function () {
            if ($('#validate').validationEngine('validate')) {
                loadVenda();
            }
        });

        $("#btnSalvarPagamento").click(function () {
            salvarPagamento();
        });

    });

    function _initModalVenda() {
        $('#modalVenda').modal();
    }
    function closeModalvenda() {
        $('#modalVenda').modal('hide');
    }

    function acaoMarcarPago() {
        var objeto = $('.pagarFornecedor');

        if (objeto.length > 0) {

            objeto.each(function () {
                if ($(this).is(':checked')) {
                    $(this).prop('checked', false);
                } else {
                    $(this).prop('checked', true);
                }
            });
        }
    }

    function loadVendaById(id_venda) {
        ShowMsgAguarde();

        $.ajax({
            url: _baseUrl + _controller + '/loadVendaById',
            type: 'POST',
            dataType: 'html',
            data: {id_venda: id_venda},
            success: function (data) {
                $('#corpoVenda').html(data);
                _initModalVenda();
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }
        });
    }

    function loadVenda() {
        ShowMsgAguarde();
        var data = $('#validate').serialize();
        $.ajax({
            url: _baseUrl + _controller + '/loadVenda',
            type: 'POST',
            dataType: 'html',
            data: data,
            success: function (data) {
                $('#load-venda').html(data);
                // _initButtonDelete();
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }

        });
    }

    function geraPDF() {
        var id_venda = $('#vendaedicao-id_venda').val();
        var url = _baseUrl + _controller + '/pdf/' + id_venda;
        var formulario = $('#formularioAcoes');
        formulario.attr('action', url);
        formulario.attr('target', '_blank');
        formulario.submit();
    }

    function salvarPagamento() {
        var formulario = $('#formSalvarPedido');
        var url = formulario.attr('action');
        var data = formulario.serialize();
        ShowMsgAguarde();
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (data) {
                if (data.success !== undefined && data.success !== '') {
                    Dialog.success(data.success, 'Sucesso', function () {
                        $("#validate").submit();
                    });
                } else if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.error, 'Erro');
                } else {
                    Dialog.error(_erroPadraoAjax, 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }

        });
    }

    function excluir(id_param) {
        ShowMsgAguarde();
        $.ajax({
            url: _baseUrl + _controller + '/excluir',
            type: 'POST',
            dataType: 'json',
            data: {id_param: id_param},
            success: function (data) {
                if (data.success !== undefined && data.success !== '') {
                    var linhaCadastro = $('#linhaCadastro-' + id_param);
                    if (linhaCadastro.length > 0) {
                        linhaCadastro.fadeOut(800, function () {
                            $(this).remove();
                        });
                    }
                    Dialog.success(data.success, 'Sucesso');
                } else if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.error, 'Erro');
                } else {
                    Dialog.error(_erroPadraoAjax, 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }

        });
    }

    function _initButtonDelete() {
        var buttonDelete = $('.buttonDelete');
        buttonDelete.unbind('click');
        buttonDelete.click(function () {
            var id_param = $(this).attr('id_param');
            Dialog.confirm("Deseja realmente excluir esse registro", "Confirma", function () {
                excluir(id_param);
            });
        });
    }

</script>