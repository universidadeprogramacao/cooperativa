<div id="page-wrapper">
    <div class="row" style="margin-bottom: 10px;margin-top: 5px;" id="divBotoes"></div>
    <div class="row">
        <div class="col-xs-12 col-sm-22 col-lg-12 col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Pesquisar</h3>
                </div>
                <div class="panel-body">

                    <form method="get" class="form-horizontal populate" action="<?php echo "{$urlPadrao}/fornecimento"; ?>" id="validate">

                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Status:
                            </label>
                            <div class="col-sm-5">
                                
                                <label>
                                    <input type="radio" name="venda[st_pagoprodutor]" value="" id="venda-st_pagoprodutor-">Todos
                                </label>
                                <label>
                                    <input type="radio" name="venda[st_pagoprodutor]" value="S" id="venda-st_pagoprodutor-S">Pago
                                </label>


                                <label>
                                    <input type="radio" name="venda[st_pagoprodutor]" value="N" id="venda-st_pagoprodutor-N">Não pago
                                </label>

                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Data:
                            </label>

                            <div class="col-xs-5 col-sm-2">
                                <input type="text" name="venda[dt_vendainicio]" id="venda-dt_vendainicio" class="form-control date validate[custom[date]]">
                            </div>
                            <div class="col-sm-1 col-xs-2">
                                A
                            </div>
                            <div class="col-xs-5 col-sm-2">
                                <input type="text" name="venda[dt_vendafim]" id="venda-dt_vendafim" class="form-control date validate[custom[date]]">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-5 col-sm-5 col-sm-offset-2">
                                <button type="submit"  class="btn btn-info">
                                    <span class="glyphicon glyphicon-search"></span>
                                    Pesquisar
                                </button>

                            </div>
                        </div>

                    </form>


                </div>



            </div>
        </div>

        <div class="col-sm-12">
           
            <table class="table table-bordered" id="datatable">
                <thead>
                    <tr style="background-color: #CCC;">
                        <th style="width: 30%;">
                            Fornecedor
                        </th>

                        <th style="width: 10%;">
                            Data
                        </th>

                        <th style="width: 20%;">
                            Produto
                        </th>
                        <th style="width: 10%;">
                            Valor Unitário
                        </th>
                        <th style="width: 10%;">
                            Quantidade Fornecida
                        </th>
                        <th style="width: 10%;">
                            Sub Total
                        </th>
                        <th style="width: 10%;">
                           Status
                        </th>

                    </tr>
                </thead>

                <tbody>
                    <?php
                    $totalPagoGeral = 0;
                    if (!empty($dataGrid)) {
                        $totalRegistros = count($dataGrid);
                        foreach ($dataGrid as $key => $pedido) {

                            $pedido['dt_fechamentooriginal'] = $this->util->reverseDate($pedido['dt_fechamentooriginal']);
                            $totalParcial = $pedido['vl_unitariooriginal'] * $pedido['nr_qtdfornecida'];
                            $totalPagoGeral+=$totalParcial;
                            ?>

                            <tr>

                                <td>
                                    <p><b>Nome:</b> <?php echo $pedido['tx_nome']; ?></p>
                                    <p><b>Cpf/Cnpj:</b> <?php echo $pedido['tx_cpf']; ?></p>
                                    <p><b>Email:</b> <?php echo $pedido['tx_email']; ?></p>
                                    <p><b>Tel:</b> ( <?php echo $pedido['tx_ddd']; ?>) <?php echo $pedido['tx_numero']; ?></p>
                                    <p><b>Rua:</b> <?php echo $pedido['tx_rua']; ?></p>
                                    <p><b>Número:</b> <?php echo $pedido['tx_numerorua']; ?></p>
                                    <p><b>Bairro:</b> <?php echo $pedido['tx_bairro']; ?></p>
                                    <p><b>Cep:</b> <?php echo $pedido['tx_cep']; ?></p>
                                    <p><b>Cidade - Estado:</b> <?php echo $pedido['tx_cidade']; ?> - <?php echo $pedido['tx_estado']; ?> </p>

                                </td>
                                <td>
                                    <?php echo $pedido['dt_fechamentooriginal']; ?>
                                </td>

                                <td>
                                    <?php echo $pedido['tx_produto']; ?>
                                </td>
                                <td>
                                    <?php echo $this->util->floatToMoneyV1($pedido['vl_unitariooriginal']); ?>
                                </td>
                                <td>
                                    <?php echo $pedido['nr_qtdfornecida']; ?>
                                </td>

                                <td>
                                    <?php echo $this->util->floatToMoneyV1($totalParcial); ?>
                                </td>
                                <td>
                                    <?php echo $statuspagamentofornecedor[$pedido['st_pagoprodutor']]; ?>

                                </td>

                            </tr>

                            <?php
                          
                        }
                    }

                    if ($totalPagoGeral > 0) {
                        ?>

                        <tr style="background-color: #CCC;">
                            <td>
                                &nbsp;
                            </td>

                            <td>
                                &nbsp;
                            </td>

                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                Total Pago
                            </td>
                            <td>
                                <?php echo $this->util->floatToMoneyV1($totalPagoGeral); ?>
                            </td>
                            <td>
                                &nbsp;
                            </td>

                        </tr>

                        <?php
                    }
                    ?>
                </tbody>

            </table>

            </form>
        </div>




    </div>
</div>

</div>
<!-- /#page-wrapper -->



<script>

    $(document).ready(function () {

        // _initButtonDelete();
        $('#btnNovo').hide();
        $('#btnConsultar').click(function () {
            if ($('#validate').validationEngine('validate')) {
                loadVenda();
            }
        });

    });


</script>