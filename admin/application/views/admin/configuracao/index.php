
<div id="page-wrapper">
    <div class="row" style="margin-bottom: 10px;margin-top: 5px;" id="divBotoes"></div>
    <div class="row">
        <?php
        if (!empty($error)) {
            ?>
            <div class="col-sm-12 col-xs-12">
                <div class="alert alert-danger"><?php echo $error; ?></div>
            </div>

            <?php
        }
        ?>

        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Configuração do portal</h3>
                </div>
                <div class="panel-body">
                    <form method="post" class="form-horizontal populate" action="<?php echo "{$urlPadrao}/salvar"; ?>" id="validate">
                        <input type="hidden" name="configuracao[id_configuracao]" id="configuracao-id_configuracao">
                      
                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Nome do serviço:
                            </label>
                            <div class="col-xs-10 col-sm-8">
                                <input type="text" name="configuracao[tx_nomeservico]" id="configuracao-tx_nomeservico" class="form-control validate[required]" maxlength="100">
                                <div class="help-block">
                                    Exemplo: Honorário
                                </div>
                            </div>
                        </div>

                         <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Valor:
                            </label>

                            <div class="col-xs-2 col-sm-2">
                                <div class="input-group">
                                    <input type="text" name="configuracao[vl_servico]" id="configuracao-vl_servico" class="form-control money vl_servico validate[required]" maxlength="15">
                                    <span class="input-group-addon">
                                        R$
                                    </span>
                                </div>
                            </div>

                        </div>
                        
                        
                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Percentual pag seguro:
                            </label>

                            <div class="col-xs-2 col-sm-2">
                                <div class="input-group">
                                    <input type="text" name="configuracao[vl_percentualpagseguro]" id="configuracao-vl_percentualpagseguro" class="form-control money vl_percentualpagseguro validate[required,min[0.01],max[100.00]]" >
                                    <span class="input-group-addon">
                                        R$
                                    </span>
                                </div>
                                <div class="help-block">
                                    *Percentual a ser calculado em cima de cada operação.
                                </div>
                            </div>

                        </div>

                    </form>
                </div>
            </div>


        </div>
    </div>
</div>

<!-- /#page-wrapper -->

<script>
   
    $(document).ready(function () {

        initBtnPageFormulario();
        $('#btnNovo').hide();
        $('#btnSalvar').show();
      
        $('#btnSalvar').click(function () {

            var formulario = $('#validate');

            if (formulario.validationEngine('validate')) {
                salvar(formulario);
            }
        });
    });


    function salvar(formulario) {
        ShowMsgAguarde();
        //formulario.submit();return false;
        formulario.ajaxSubmit({
            success: function (data) {
                data = $.parseJSON(data);

                if (data.success !== undefined && data.success !== '') {
                    if (data.dataGrid.id_configuracao !== undefined && data.dataGrid.id_configuracao !== '') {
                        $("#configuracao-id_configuracao").val(data.dataGrid.id_configuracao);
                    }
                    Dialog.success(data.success, 'Sucesso');
                } else if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.erro, 'Erro');
                } else {
                    Dialog.error('Falha ao salvar', 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax, 'Erro');
            },
            complete: function () {
                CloseMsgAguarde();
            }
        });

    }

</script>