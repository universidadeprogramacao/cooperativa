<div id="page-wrapper">
    <div class="row" style="margin-bottom: 10px;margin-top: 5px;" id="divBotoes"></div>
    <div class="row">
        <?php
        if (!empty($error)) {
            ?>
            <div class="col-sm-12 col-xs-12">
                <div class="alert alert-danger"><?php echo $error; ?></div>
            </div>

            <?php
        }

        $disaBled = "";
        if (!empty($pedido['st_pedido']) && $pedido['st_pedido'] == 'F') {
            $disaBled = "disabled='disabled'";
        }
        ?>

        <form enctype="multipart/form-data" method="post" class="form-horizontal populate" action="<?php echo "{$urlPadrao}/salvar"; ?>" id="validate">

            <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Cadastro de pedidos</h3>
                    </div>

                    <div class="panel-body">
                        <input type="hidden" name="pedido[id_pedido]" id="pedido-id_pedido">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Status</label>
                            <div class="col-sm-3">
                                <select <?php echo $disaBled; ?> id="pedido-st_pedido"  name="pedido[st_pedido]" class="form-control">
                                    <?php
                                    foreach ($st_pedido as $indice => $status) {
                                       
                                        ?>
                                        <option  title="<?php echo $status; ?>"  value="<?php echo $indice; ?>"><?php echo $status; ?></option>
                                        <?php
                                    }
                                    ?>

                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Status pgto.</label>
                            <div class="col-sm-3">
                                <select  <?php echo $disaBled; ?> id="pedido-st_pagamento"  name="pedido[st_pagamento]" class="form-control">
                                    <?php
                                    foreach ($st_pagamento as $indice => $status) {
                                       
                                        ?>

                                        <option title="<?php echo $status; ?>" value="<?php echo $indice; ?>"><?php echo $status; ?></option>
                                        <?php
                                    }
                                    ?>

                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Vl pago</label>
                            <div class="col-sm-2">
                                <input  <?php echo $disaBled; ?> id="pedido-vl_pago"   type="text" name="pedido[vl_pago]" class="form-control decimal">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Dt. pgto </label>
                            <div class="col-sm-2">
                                <input  <?php echo $disaBled; ?>   type="text" id="pedido-dt_pagamento" name="pedido[dt_pagamento]" class="form-control date">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Dt. entrega </label>
                            <div class="col-sm-2">
                                <input  <?php echo $disaBled; ?>   type="text" id="pedido-dt_entrega" name="pedido[dt_entrega]" class="form-control date">
                            </div>
                        </div>
                    </div>

                </div>


                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Cliente</h3>
                    </div>
                    <div class="panel-body">


                        <div class="form-group">
                            <div class="col-sm-3">
                                <button id="btnOpenModalPequisarCliente" title="Pesquisar Cliente" type="button" class="btn btn-success">
                                    Pesquisar Cliente
                                </button>
                            </div>
                        </div>

                        <div class="form-group" id="renderUsuario">
                            <?php include_once 'tab-usuario.php'; ?>
                        </div>
                    </div>

                </div>


                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Produto(s)</h3>
                    </div>
                    <div class="panel-body">


                        <div class="form-group">
                            <div class="col-sm-3">
                                <button id="btnOpenModalPequisaProduto" title="Pesquisar Produto" type="button" class="btn btn-success">
                                    Pesquisar Produto
                                </button>
                            </div>
                        </div>

                        <table id="tabela-produto" class="table table-bordered">

                            <thead>
                                <tr style="background-color: #CCC;">
                                    <td style="width:10%;">
                                        Imagem
                                    </td>
                                    <td style="width: 40%;">
                                        Produto
                                    </td>
                                    <td style="width: 10%;">
                                        Vl. Unitário
                                    </td>
                                    <td style="width: 5%;">
                                        Qtd
                                    </td>
                                    <td style="width: 10%;">
                                        Sub Total
                                    </td>

                                    <td style="width: 10%;">
                                        Acao
                                    </td>
                                </tr>
                            </thead>

                            <tbody id="corpo-add-produto">
                                <?php
                                if (!empty($pedidoItem)) {
                                    foreach ($pedidoItem as $produto) {
                                        ?>
                                        <tr class="linha-produto-<?php echo $produto['id_produto']; ?>"   tx_foto="<?php echo $produto['tx_foto']; ?>"  tx_produto="<?php echo $produto['tx_produto']; ?>" vl_unitario="<?php echo $produto['vl_unitario']; ?>"  id_produto="<?php echo $produto['id_produto']; ?>">
                                            <td class="tx_foto">
                                                <input class="id_produtoselecionado" type="hidden" name="produto[<?php echo $produto['id_produto']; ?>][id_produto]" value="<?php echo $produto['id_produto']; ?>">
                                                <img style="width: 95%;height:80px;text-align: center;" src="<?php echo base_url("../uploads/produto/{$produto['tx_foto']}"); ?>"  alt="<?php echo $produto['tx_produto'] ?>" />
                                            </td>
                                            <td>
                                                <?php echo $produto['tx_produto']; ?>
                                            </td>
                                            <td class="vl_unitario">
                                                <?php echo $produto['vl_unitario']; ?>
                                            </td>
                                            <td class="qtd">
                                                <input class="quantidade-produto" type="text" name="produto[<?php echo $produto['id_produto']; ?>][quantidade]" value="<?php echo $produto['nr_quantidade']; ?>">
                                            </td>
                                            <td class="subtotal">
                                                <?php echo $produto['vl_subtotal']; ?>
                                            </td>

                                            <td>
                                                <p>
                                                    <button class="btn btn-info btbRecalcularProduto" type="button">
                                                        <span class="glyphicon glyphicon-check"></span>
                                                        Recalcular
                                                    </button>
                                                </p>
                                                <p>
                                                    <button class="btn btn-danger btnRemoverProduto" type="button">
                                                        <span class="glyphicon glyphicon-remove"></span>
                                                        Remover
                                                    </button>
                                                </p>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>

                        </table>



                    </div>
                </div>


            </div>
        </form>
    </div>
</div>

<div class="modal fade bs-example-modal-sm print" id="modalConsulta"  tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button  type="button" class="close no-print" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Pesquisa de produto</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="form-pesquisar-produto" class="form-horizontal" action="<?php echo base_url('admin_produto/findAll'); ?>">
                    <div class="form-group">
                        <label class="col-sm-1 control-label">
                            <?php //echo CAMPO_OBRIGATORIO; ?>
                            Produto:
                        </label>
                        <div class="col-sm-11">
                            <input type="text" name="produto[tx_produto]" id="produto-tx_produto" class="form-control" maxlength="60">
                        </div>
                    </div>
                </form>

                <div class="table-responsive" id="load-consulta-produto"></div>
            </div>
            <div class="modal-footer no-print" style="margin-top: 10px;">
                <button id="btnPesquisarProduto" type="button" title="Pesquisar" class="btn btn-info">
                    <span class="glyphicon glyphicon-serch"></span>
                    Pesquisar
                </button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade bs-example-modal-sm" id="modalConsultaCliente"  tabindex="-1" role="dialog">
    <div class="modal-dialog" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header">
                <button  type="button" class="close no-print" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Pesquisa de cliente</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="form-pesquisar-cliente" class="form-horizontal" action="<?php echo base_url('admin_usuario/findAll'); ?>">
                    <input type="hidden" name="admin[limit]" id="admin-limit" value="5">
                    <div class="form-group">
                        <label class="col-xs-8 col-sm-1 control-label">
                            <?php //echo CAMPO_OBRIGATORIO; ?>
                            Nome:
                        </label>
                        <div class="col-xs-10 col-sm-8">
                            <input type="text" name="admin[tx_nome]" id="admin-tx_nome" class="form-control" maxlength="60">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-8 col-sm-1 control-label">
                            <?php //echo CAMPO_OBRIGATORIO; ?>
                            Email:
                        </label>

                        <div class="col-xs-8 col-sm-5">
                            <div class="input-group">
                                <input type="text" name="admin[tx_email]" id="admin-tx_email" class="form-control lower validate" maxlength="60">
                                <span class="input-group-addon">
                                    @
                                </span>
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-xs-8 col-sm-1 control-label">
                            <?php //echo CAMPO_OBRIGATORIO; ?>
                            Status:
                        </label>
                        <div class="col-xs-7 col-sm-6">
                            <span class="radio radio-inline">
                                <label>
                                    <input type="radio" name="admin[st_status]" id="admin-st_status-T" value="">Todos
                                </label>
                            </span>

                            <span class="radio radio-inline">
                                <label>
                                    <input type="radio" name="admin[st_status]" id="admin-st_status-A" value="A">Cadastro Ativo
                                </label>
                            </span>

                            <span class="radio radio-inline">
                                <label>
                                    <input type="radio" name="admin[st_status]" id="admin-st_status-I" value="I">Cadastro Inativo
                                </label>
                            </span>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-8 col-sm-2 control-label">
                            <?php //echo CAMPO_OBRIGATORIO; ?>
                            Data de cadastro:
                        </label>

                        <div class="col-xs-5 col-sm-2">
                            <input type="text" name="admin[dt_cadastroinicio]" id="admin-dt_cadastroinicio" class="form-control date validate[custom[date]]">
                        </div>
                        <div class="col-sm-1 col-xs-2">
                            A
                        </div>
                        <div class="col-xs-5 col-sm-2">
                            <input type="text" name="admin[dt_cadastrofim]" id="admin-dt_cadastrofim" class="form-control date validate[custom[date]]">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-2 col-sm-offset-1">
                            <button id="btnPesquisarCliente" type="button" title="Pesquisar" class="btn btn-info">
                                <span class="glyphicon glyphicon-serch"></span>
                                Pesquisar
                            </button>
                        </div>
                    </div>
                </form>

                <div class="table-responsive" id="load-consulta-cliente"></div>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- /#page-wrapper -->

<script src="http://malsup.github.com/jquery.form.js"></script>
<script>

    $(document).ready(function () {

        initBtnPageFormulario();
        $("#btnSalvar").show();
        $('#btnNovo').click(function () {
            window.location = _urlPadrao + '/formulario';
        });

        recalcularProduto();
        removerProduto();

        $('#btnOpenModalPequisaProduto').click(function () {
            openModalConsultarProduto();
        });

        $('#btnOpenModalPequisarCliente').click(function () {
            openModalConsultaCliente();
        });

        $('#btnPesquisarProduto').click(function () {
            var formulario = $('#form-pesquisar-produto');
            findProduto(formulario);
        });

        $('#btnPesquisarCliente').click(function () {
            var formulario = $('#form-pesquisar-cliente');
            findCliente(formulario);
        });


        $('#btnSalvar').click(function () {
            var formulario = $('#validate');
            formulario.submit();
            /**
             if (formulario.validationEngine('validate')) {
             salvar(formulario);
             }
             **/
        });
    });

    function salvar(formulario) {
        ShowMsgAguarde();

        formulario.ajaxSubmit({
            dataType: 'json',
            success: function (data) {
                if (data.success !== undefined && data.success !== '') {
                    if (data.dataGrid.id_produto !== undefined && data.dataGrid.id_produto !== '') {
                        $("#produto-id_produto").val(data.dataGrid.id_produto);
                        //alert(data.avatar);
                        if (data.avatar !== undefined && data.avatar !== '') {
                            //alert(data.avatar);
                            $("#avatar").html(data.avatar);
                        }
                    }
                    Dialog.success(data.success, 'Sucesso');
                } else if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.erro, 'Erro');
                } else {
                    Dialog.error('Falha ao salvar', 'Erro');
                }
            }, error: function () {
                Dialog.error(_erroPadraoAjax, 'Erro');
            },
            complete: function () {
                CloseMsgAguarde();
            }

        });


    }

    function openModalConsultarProduto() {
        $('#modalConsulta').modal();
    }

    function openModalConsultaCliente() {
        $('#modalConsultaCliente').modal();
    }

    function findProduto(formulario) {
        ShowMsgAguarde();
        $.ajax({
            url: formulario.attr('action'),
            data: formulario.serialize(),
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                $('#load-consulta-produto').html(data);
            },
            error: function () {
                Dialog.error(_erroPadraoAjax, 'Erro');
            }, complete: function () {
                CloseMsgAguarde();
            }


        });
    }

    function findCliente(formulario) {
        ShowMsgAguarde();
        $.ajax({
            url: formulario.attr('action'),
            data: formulario.serialize(),
            type: 'POST',
            dataType: 'html',
            success: function (data) {
                $('#load-consulta-cliente').html(data);
            },
            error: function () {
                Dialog.error(_erroPadraoAjax, 'Erro');
            }, complete: function () {
                CloseMsgAguarde();
            }


        });
    }

    function recalcularProduto() {
        var subTotal = 0;
        var button = $('.btbRecalcularProduto');
        button.unbind('click');

        button.click(function () {
            var objeto = $(this).parents('tr');
            var tx_foto = objeto.attr('tx_foto');
            var tx_produto = objeto.attr('tx_produto');
            var vl_unitario = objeto.attr('vl_unitario');
            var id_produto = objeto.attr('id_produto');
            var quantidade = objeto.find('.quantidade-produto').val();

            if ($.isNumeric(quantidade)) {
                subTotal = quantidade * vl_unitario;

            }
            objeto.find('.subtotal').text(subTotal);
        });
    }

    function removerProduto() {
        var button = $('.btnRemoverProduto');
        button.unbind('click');

        button.click(function () {
            var trRemover = $(this).parents('tr');
            Dialog.confirm("Deseja realmente remover esse produto do pedido?", "Confirmação", function () {
                trRemover.remove();
            });

        });
    }


</script>

