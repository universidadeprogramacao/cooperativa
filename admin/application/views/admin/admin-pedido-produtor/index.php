<div id="page-wrapper">
    <div class="row" style="margin-bottom: 10px;margin-top: 5px;" id="divBotoes"></div>
    <div class="row">
        <div class="col-xs-12 col-sm-22 col-lg-12 col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Pesquisar Pedidos</h3>
                </div>
                <div class="panel-body">

                    <form method="get" class="form-horizontal populate" action="<?php echo "{$urlPadrao}"; ?>" id="validate">


                        <label class="col-xs-8 col-sm-2 control-label">
                            Status:
                        </label>

                        <div class="col-sm-5">
                            <label>
                                <input type="radio" name="venda[st_pedido]" value="" id="venda-st_pedido-">Todos
                            </label>
                            <label>
                                <input type="radio" name="venda[st_pedido]" value="A" id="venda-st_pedido-A">Ativos
                            </label>


                            <label>
                                <input type="radio" name="venda[st_pedido]" value="P" id="venda-st_pedido-P">Pendente de entrega
                            </label>

                            <label>
                                <input type="radio" name="venda[st_pedido]" value="P" id="venda-st_pedido-F">Finalizo / entregue
                            </label>
                            <label>
                                <input type="radio" name="venda[st_pedido]" value="C" id="venda-st_pedido-C">Cancelado
                            </label>

                        </div>

                        <div class="form-group">
                            <div class="col-xs-5 col-sm-5 col-sm-offset-1">
                                <button type="submit"  class="btn btn-info">
                                    <span class="glyphicon glyphicon-search"></span>
                                    Pesquisar
                                </button>

                            </div>
                        </div>

                    </form>


                </div>



            </div>
        </div>

        <div class="col-sm-12">
            <h3>Pedidos feitos pelo site</h3>
            <form method="post" id="formSalvarPedido" class="form-horizontal populate"  >
                <table class="table table-bordered" id="datatable">
                    <thead>
                        <tr style="background-color: #CCC;">

                            <th style="width: 10%;">
                                Data pedido
                            </th>
                            <th style="width: 30%;">
                                Cliente
                            </th>
                            <th style="width: 40%;">
                                Produtos
                            </th>
                            <th style="width: 40%;">
                                Ação
                            </th>

                        </tr>
                    </thead>

                    <tbody>
                        <?php
                        if (!empty($dataGrid)) {
                            foreach ($dataGrid as $key => $pedido) {

                                $disaBled = "";
                                if ($pedido['st_pedido'] == 'F') {
                                    $disaBled = "disabled='disabled'";
                                }

                                if (!empty($pedido['vl_pago'])) {
                                    $pedido['vl_pago'] = $this->util->floatToMoneyV1($pedido['vl_pago']);
                                }
                                if (!empty($pedido['dt_pagamento'])) {
                                    $pedido['dt_pagamento'] = $this->util->reverseDate($pedido['dt_pagamento']);
                                }
                                if (!empty($pedido['dt_entrega'])) {
                                    $pedido['dt_entrega'] = $this->util->reverseDate($pedido['dt_entrega']);
                                }
                                ?>
                                <tr>
                                    <td><?php echo $this->util->reverseDate($pedido['dt_pedido']); ?></td>
                                    <td>
                                        <p><b>Nome:</b> <?php echo $pedido['tx_nome']; ?></p>
                                        <p><b>Cpf/Cnpj:</b> <?php echo $pedido['tx_cpf']; ?></p>
                                        <p><b>Email:</b> <?php echo $pedido['tx_email']; ?></p>
                                        <p><b>Tel:</b> ( <?php echo $pedido['tx_ddd']; ?>) <?php echo $pedido['tx_numero']; ?></p>
                                        <p><b>Rua:</b> <?php echo $pedido['tx_rua']; ?></p>
                                        <p><b>Número:</b> <?php echo $pedido['tx_numerorua']; ?></p>
                                        <p><b>Bairro:</b> <?php echo $pedido['tx_bairro']; ?></p>
                                        <p><b>Cep:</b> <?php echo $pedido['tx_cep']; ?></p>
                                        <p><b>Cidade - Estado:</b> <?php echo $pedido['tx_cidade']; ?> - <?php echo $pedido['tx_estado']; ?> </p>

                                    </td>
                                    <td>
                                        <table style="width: 100%;">
                                            <?php
                                            $cor = "#B0E0E6";
                                            if (!empty($produtos[$key])) {
                                                $totalProdutos = count($produtos[$key]);
                                                $limite = 0;
                                                $valorTotalPedido = 0.0;
                                                foreach ($produtos[$key] as $produto) {
                                                    $valorTotalPedido+=$produto['vl_subtotal'];
                                                    $limite++;
                                                    if ($cor == "#B0E0E6") {
                                                        $cor = "#E0FFFF";
                                                    } else {
                                                        $cor = "#B0E0E6";
                                                    }
                                                    ?>
                                                    <tr style="background-color:<?php echo $cor; ?>;">
                                                        <td>
                                                            <p><b>Produto: </b> <?php echo $produto['tx_produto']; ?></p>
                                                            <p><b>Unitário: </b> <?php echo $produto['vl_unitario']; ?></p>
                                                            <p><b>Qtd: </b> <?php echo $produto['nr_quantidade']; ?></p>
                                                            <p><b>Sub Total: </b> <?php echo $produto['vl_subtotal']; ?></p>
                                                        </td>
                                                    </tr>

                                                    <?php
                                                    if ($limite == $totalProdutos) {
                                                        ?>
                                                        <tr style="background-color:#ccc;">
                                                            <td>
                                                                Total: <?php echo $valorTotalPedido; ?>
                                                            </td>
                                                        </tr>

                                                        <?php
                                                    }
                                                    ?>


                                                    <?php
                                                }
                                            }
                                            ?>
                                        </table>

                                    </td>
                                    <td>
                                        <a href="<?php echo base_url("admin_pedido_produtor/formulario/{$pedido['id_pedido']}"); ?>" class="btn btn-info">
                                            <span class="glyphicon glyphicon-edit"></span>
                                            Ver
                                        </a>
                                    </td>
                                </tr>

                                <?php
                            }
                        }
                        ?>
                    </tbody>

                </table>

                </div>
            </form>
        </div>




    </div>
</div>

</div>
<!-- /#page-wrapper -->

<div class="modal fade" id="modalVenda"  style="width: 100% !important;overflow: auto;height:auto;">
    <div class="modal-dialog"  style="width: 100% !important;">
        <div class="modal-content"  style="width: 100% !important;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Detalhamento da venda</h4>
            </div>
            <div class="modal-body" id="corpoVenda"  style="width: 100% !important;">
            </div>
            <div class="modal-footer">


                <button type="button" class="btn btn-danger" data-dismiss="modal">
                    <span class="glyphicon glyphicon-eye-close"></span>
                    Fechar
                </button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<form method="get" id="formularioAcoes"></form>

<script>

    $(document).ready(function () {

        initBtnPageFormulario();
        // _initButtonDelete();
        $('#btnNovo').click(function (){
            window.location = _urlPadrao+'/formulario';
        });
        
        $('#btnConsultar').click(function () {
            if ($('#validate').validationEngine('validate')) {
                loadVenda();
            }
        });

        $("#btnSalvarPedido").click(function () {
            salvarPedido();
        });

    });

    function _initModalVenda() {
        $('#modalVenda').modal();
    }
    function closeModalvenda() {
        $('#modalVenda').modal('hide');
    }

    function loadVendaById(id_venda) {
        ShowMsgAguarde();

        $.ajax({
            url: _baseUrl + _controller + '/loadVendaById',
            type: 'POST',
            dataType: 'html',
            data: {id_venda: id_venda},
            success: function (data) {
                $('#corpoVenda').html(data);
                _initModalVenda();
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }
        });
    }

    function loadVenda() {
        ShowMsgAguarde();
        var data = $('#validate').serialize();
        $.ajax({
            url: _baseUrl + _controller + '/loadVenda',
            type: 'POST',
            dataType: 'html',
            data: data,
            success: function (data) {
                $('#load-venda').html(data);
                // _initButtonDelete();
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }

        });
    }

    function geraPDF() {
        var id_venda = $('#vendaedicao-id_venda').val();
        var url = _baseUrl + _controller + '/pdf/' + id_venda;
        var formulario = $('#formularioAcoes');
        formulario.attr('action', url);
        formulario.attr('target', '_blank');
        formulario.submit();
    }

    function salvarPedido() {
        var formulario = $('#formSalvarPedido');
        var url = formulario.attr('action');
        var data = formulario.serialize();
        ShowMsgAguarde();
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (data) {
                if (data.success !== undefined && data.success !== '') {
                    Dialog.success(data.success, 'Sucesso', function () {
                        $("#validate").submit();
                    });
                } else if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.error, 'Erro');
                } else {
                    Dialog.error(_erroPadraoAjax, 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }

        });
    }

    function excluir(id_param) {
        ShowMsgAguarde();
        $.ajax({
            url: _baseUrl + _controller + '/excluir',
            type: 'POST',
            dataType: 'json',
            data: {id_param: id_param},
            success: function (data) {
                if (data.success !== undefined && data.success !== '') {
                    var linhaCadastro = $('#linhaCadastro-' + id_param);
                    if (linhaCadastro.length > 0) {
                        linhaCadastro.fadeOut(800, function () {
                            $(this).remove();
                        });
                    }
                    Dialog.success(data.success, 'Sucesso');
                } else if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.error, 'Erro');
                } else {
                    Dialog.error(_erroPadraoAjax, 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }

        });
    }

    function _initButtonDelete() {
        var buttonDelete = $('.buttonDelete');
        buttonDelete.unbind('click');
        buttonDelete.click(function () {
            var id_param = $(this).attr('id_param');
            Dialog.confirm("Deseja realmente excluir esse registro", "Confirma", function () {
                excluir(id_param);
            });
        });
    }

</script>