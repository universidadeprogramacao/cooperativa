<div id="page-wrapper">
    <div class="row" style="margin-bottom: 10px;margin-top: 5px;" id="divBotoes"></div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Pesquisar </h3>
                </div>
                <div class="panel-body">
                    
                    <form method="post" class="form-horizontal populate" action="<?php echo "{$urlPadrao}"; ?>" id="validate">
                        <div class="form-group">
                            <label class="col-xs-8 col-sm-1 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Produto:
                            </label>
                            <div class="col-xs-10 col-sm-8">
                                <input type="text" name="produto[tx_produto]" id="produto-tx_produto" class="form-control" maxlength="60">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-5 col-sm-5">
                                <button type="submit" class="btn btn-info">
                                    <span class="glyphicon glyphicon-search"></span>
                                    Pesquisar
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
                
            </div>


            <table class="table table-bordered table-striped" id="dataTable">
                <thead>
                    <tr>
                        <th style="width: 10%;">&nbsp;</th>
                        <th style="width: 90%;">Produto</th>
                        
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if (!empty($dataGrid)) {
                        foreach ($dataGrid as $resultado) {
                            ?>
                    <tr id="linhaCadastro-<?php echo $resultado['id_produto']; ?>">
                                <td>
                                    <div class="btn-group">
                                        <a href="<?php echo "{$urlPadrao}/formulario/{$resultado['id_produto']}/{$this->util->gerarSlug($resultado['tx_produto'])}"; ?>"  title="Editar" style="margin: 3px;" class="btn btn-xs btn-info" >
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </a>
                                        
                                        
                                        <button id_param="<?php echo $resultado['id_produto']; ?>" title="Excluir" type="button" style="margin: 3px;" class="btn btn-xs btn-danger buttonDelete">
                                            <span class="glyphicon glyphicon-remove"></span>
                                        </button>

                                    </div>
                                </td>

                                <td><?php echo $resultado['tx_produto']; ?></td>
                                 
                            </tr>
                        <?php }
                    } else { ?>
                        <tr>
                            <td>&nbsp;</td>
                           
                             <td> <div class="alert alert-warning">
                                    Nenhum registro encontrado
                                </div>
                             </td>
                             
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>

</div>
<!-- /#page-wrapper -->

<script>
    $(document).ready(function () {

        initBtnPageFormulario();
        $('#btnNovo').click(function () {
            window.location = _urlPadrao + '/formulario';
        });
        
        $('.buttonDelete').click(function (){
            var id_param = $(this).attr('id_param');
            Dialog.confirm("Deseja realmente excluir esse registro","Confirma",function (){
                excluir(id_param);
            });
        });

    });

    function excluir(id_param) {
        ShowMsgAguarde();
        $.ajax({
            url: _baseUrl + _controller + '/excluir',
            type: 'POST',
            dataType: 'json',
            data: {id_param: id_param},
            success: function (data) {
                if (data.success !== undefined && data.success !=='') {
                    var linhaCadastro = $('#linhaCadastro-'+id_param);
                    if(linhaCadastro.length>0){
                        linhaCadastro.fadeOut(800,function (){
                            $(this).remove();
                        });
                    }
                    Dialog.success(data.success,'Sucesso');
                }
                else if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.error, 'Erro');
                }
                else {
                    Dialog.error(_erroPadraoAjax, 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }

        });
    }

</script>