
<table class="table table-bordered table-striped" id="dataTable">
    <thead>
        <tr>
            <th style="width: 10%;">&nbsp;</th>
            <th style="width: 90%;">Produto</th>
            <th style="width: 10%;">
                Vl. Unitário
            </th>
            <th style="width: 10%;">
                Selecionar
            </th>
        </tr>
    </thead>

    <tbody>
        <?php
        if (!empty($produto)) {
            foreach ($produto as $resultado) {
                ?>
                <tr id="linhaCadastro-<?php echo $resultado['id_produto']; ?>"
                    tx_foto="<?php echo base_url("../uploads/produto/{$resultado['tx_foto']}"); ?>"
                    tx_produto="<?php echo $resultado['tx_produto']; ?>"
                    vl_unitario="<?php echo $resultado['vl_unitario']; ?>"
                    id_produto="<?php echo $resultado['id_produto']; ?>"
                    >
                    <td>
                        <img alt="<?php echo $resultado['tx_produto']; ?>" style="max-width: 50%;max-height: 150px;" src="<?php echo base_url("../uploads/produto/{$resultado['tx_foto']}"); ?>">
                    </td>


                    <td>
                        <?php echo $resultado['tx_produto']; ?>
                    </td>
                    <td>
                        <?php echo $this->util->floatToMoneyV1($resultado['vl_unitario']); ?>
                    </td>
                    <td>
                        <button onclick="prototipoSelecionarProduto($(this).parents('tr'));" type="button" class="btn btn-info" title="Selecionar">
                            <span class="glyphicon glyphicon-ok-sign"></span>
                        </button>
                    </td>

                </tr>
                <?php
            }
        } else {
            ?>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>

                <td> <div class="alert alert-warning">
                        Nenhum registro encontrado
                    </div>
                </td>
                <td>&nbsp;</td>

            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

<script>
 
    function prototipoSelecionarProduto(objeto) {
        var tx_foto = objeto.attr('tx_foto');
        var tx_produto = objeto.attr('tx_produto');
        var vl_unitario = objeto.attr('vl_unitario');
        var id_produto = objeto.attr('id_produto');


        var existeProduto = $('#corpo-add-produto .linha-produto-' + id_produto);
        if (existeProduto.length > 0) {
            Dialog.error("Esse produto  já foi selecionado", "Atenção!");
        } else {
            var quantidadeDefault = 1;
            var subTotal = quantidadeDefault * vl_unitario;
            var html = '<tr  style="padding: 4px;margin: 5px;" class="linha-produto-' + id_produto + '"   tx_foto="' + tx_foto + '"  tx_produto="' + tx_produto + '" vl_unitario="' + vl_unitario + '"  id_produto="' + id_produto + '">';
            html += '<input class="id_produtoselecionado" type="hidden" name="produto[' + id_produto + '][id_produto]" value="'+id_produto+'">';
            html += '<td class="tx_foto">';
            html += '<img style="width: 95%;height:80px;text-align: center;" src="' + tx_foto + '"  alt="' + tx_produto + '" />';
            html += '</td>';
            html += '<td class="tx_produto">';
            html += tx_produto;
            html += '</td>';
            html += '<td class="vl_unitario">';
            html += vl_unitario;
            html += '</td>';
            html += '<td class="qtd">';
            html += '<input class="quantidade-produto" type="text" name="produto[' + id_produto + '][quantidade]" value="' + quantidadeDefault + '">';
            html += '</td>';
            html += '<td class="subtotal">';
            html += subTotal;
            html += '</td>';
            html += '<td>';
            html += '<p>';
            html += '<button class="btn btn-info btbRecalcularProduto" type="button">';
            html += '<span class="glyphicon glyphicon-check"></span>';
            html += 'Recalcular';
            html += '</button>';
            html += '</p>';
            html += '<p>';
            html += '<button class="btn btn-danger btnRemoverProduto" type="button">';
            html += '<span class="glyphicon glyphicon-remove"></span>';
            html += 'Remover';
            html += '</button>';
            html += '</p>';
            html += '</td>';
            html += '</tr>';
            $("#corpo-add-produto").prepend(html);
            recalcularProduto();
            removerProduto();
        
        }

    }

</script>