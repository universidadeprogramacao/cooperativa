
<div id="page-wrapper">
    <div class="row" style="margin-bottom: 10px;margin-top: 5px;" id="divBotoes"></div>
    <div class="row">
        <?php
        if (!empty($error)) {
            ?>
            <div class="col-sm-12 col-xs-12">
                <div class="alert alert-danger"><?php echo $error; ?></div>
            </div>

            <?php
        }
        ?>

        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Cadastro de produto</h3>
                </div>
                <div class="panel-body">
                    <form enctype="multipart/form-data" method="post" class="form-horizontal populate" action="<?php echo "{$urlPadrao}/salvar"; ?>" id="validate">
                        <input type="hidden" name="produto[id_produto]" id="produto-id_produto">


                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Produto:
                            </label>
                            <div class="col-sm-6">
                                <input type="text" name="produto[tx_produto]"  id="produto-tx_produto" class="form-control validate[required]" maxlength="60">
                            </div>
                            
                             <label class="col-sm-2 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Valor unitário:
                            </label>
                            <div class="col-sm-2">
                                <input type="text" name="produto[vl_unitario]"  id="produto-vl_unitario" class="form-control money validate[required,decimal]" maxlength="10">
                            </div>
                            
                        </div>
                        
                        <div class="form-group">

                            <label class="col-sm-2 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Descrição:
                            </label>

                             <div class="col-sm-10">
                                <input type="text" name="produto[tx_descricao]"  id="produto-tx_descricao" class="form-control validate[required]" maxlength="255">
                            </div>

                        </div>


                       

                        <div class="form-group">

                            <label class="col-sm-2 control-label">
                                Foto(Opcional):
                            </label>

                            <div class="col-sm-5">
                                <input type="file" name="userfile" id="userfile">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-5" id="avatar">
                                <?php
                                if(!empty($avatar)){
                                    echo $avatar;
                                }
                                ?>
                              
                            </label>

                        </div>



                    </form>
                </div>
            </div>


        </div>
    </div>
</div>

<!-- /#page-wrapper -->

<script src="http://malsup.github.com/jquery.form.js"></script>
<script>

                                            $(document).ready(function () {

                                                initBtnPageFormulario();
                                                $('#btnNovo').click(function () {
                                                    window.location = _urlPadrao + '/formulario';
                                                });

                                              
                                                $('#btnSalvar').click(function () {

                                                    var formulario = $('#validate');
                                                    if (formulario.validationEngine('validate')) {
                                                        salvar(formulario);
                                                    }
                                                });
                                            });

                                            function salvar(formulario) {
                                                ShowMsgAguarde();

                                                formulario.ajaxSubmit({
                                                    dataType: 'json',
                                                    success: function (data) {
                                                        if (data.success !== undefined && data.success !== '') {
                                                            if (data.dataGrid.id_produto !== undefined && data.dataGrid.id_produto !== '') {
                                                                $("#produto-id_produto").val(data.dataGrid.id_produto);
                                                                //alert(data.avatar);
                                                                if(data.avatar !==undefined && data.avatar !==''){
                                                                    //alert(data.avatar);
                                                                    $("#avatar").html(data.avatar);
                                                                }
                                                            }
                                                            Dialog.success(data.success, 'Sucesso');
                                                        } else if (data.error !== undefined && data.error !== '') {
                                                            Dialog.error(data.erro, 'Erro');
                                                        } else {
                                                            Dialog.error('Falha ao salvar', 'Erro');
                                                        }
                                                    }, error: function () {
                                                         Dialog.error(_erroPadraoAjax, 'Erro');
                                                    },
                                                    complete: function () {
                                                        CloseMsgAguarde();
                                                    }

                                                });

                                                /**
                                                 formulario.ajaxSubmit({
                                                 success: function (data) {
                                                 data = $.parseJSON(data);
                                                 if (data.success !== undefined && data.success !== '') {
                                                 if (data.dataGrid.id_produto !== undefined && data.dataGrid.id_produto !== '') {
                                                 $("#produto-id_produto").val(data.dataGrid.id_produto);
                                                 }
                                                 Dialog.success(data.success, 'Sucesso');
                                                 } else if (data.error !== undefined && data.error !== '') {
                                                 Dialog.error(data.erro, 'Erro');
                                                 } else {
                                                 Dialog.error('Falha ao salvar', 'Erro');
                                                 }
                                                 },
                                                 error: function () {
                                                 Dialog.error(_erroPadraoAjax, 'Erro');
                                                 },
                                                 complete: function () {
                                                 CloseMsgAguarde();
                                                 }
                                                 });
                                                 
                                                 **/
                                            }

</script>

