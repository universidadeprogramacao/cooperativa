
<div id="page-wrapper">
    <div class="row" style="margin-bottom: 10px;margin-top: 5px;" id="divBotoes"></div>
    <div class="row">
        <?php
        if (!empty($error)) {
            ?>
            <div class="col-sm-12 col-xs-12">
                <div class="alert alert-danger"><?php echo $error; ?></div>
            </div>

            <?php
        }
        ?>

        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Cadastro de Administrador do portal</h3>
                </div>
                <div class="panel-body">
                    <form method="post" class="form-horizontal populate" action="<?php echo "{$urlPadrao}/salvar"; ?>" id="validate">
                        <input type="hidden" name="admin[id_admin]" id="admin-id_admin">
                        <input type="hidden" name="admin[st_indsenhapadrao]" id="admin-st_indsenhapadrao">
                        <input type="hidden" name="admin[tx_senha]" id="admin-tx_senha">



                        <div class="form-group">
                            <label class="col-xs-8 col-sm-1 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Nome:
                            </label>
                            <div class="col-xs-10 col-sm-8">
                                <input type="text" name="admin[tx_nome]" id="admin-tx_nome" class="form-control validate[required]" maxlength="60">
                            </div>
                        </div>

                         <div class="form-group">
                            <label class="col-xs-8 col-sm-1 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Email:
                            </label>

                            <div class="col-xs-8 col-sm-5">
                                <div class="input-group">
                                    <input type="text" name="admin[tx_email]" id="admin-tx_email" class="form-control lower validate[required,custom[email]]" maxlength="60">
                                    <span class="input-group-addon">
                                        @
                                    </span>
                                </div>
                            </div>

                        </div>


                        <div class="form-group">
                            <label class="col-xs-8 col-sm-1 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Status:
                            </label>
                            <div class="col-xs-7 col-sm-6">


                                <span class="radio radio-inline">
                                    <label>
                                        <input type="radio" name="admin[st_status]"  id="admin-st_status-A" value="A">Cadastro Ativo
                                    </label>
                                </span>

                                <span class="radio radio-inline">
                                    <label>
                                        <input type="radio" name="admin[st_status]" id="admin-st_status-I" value="I">Cadastro Inativo
                                    </label>
                                </span>

                            </div>
                        </div>

                       

                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Data de cadastro:
                            </label>

                            <div class="col-xs-5 col-sm-2">
                                <div class="input-group">
                                    <input type="text" name="admin[dt_cadastro]" id="admin-dt_cadastro" class="form-control date validate[required,custom[date]]">
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                </div>
                            </div>

                        </div>

                        <div class="form-group" id="aviso" style="display: none;">
                            <div class="col-sm-10 col-xs-11 col-sm-offset-1">
                                <div class="alert alert-danger">
                                    <p>
                                        Aviso: Esse usuário já possui senha cadastrada, caso você clique em "Gerar senha", a senha passará a ser padrão, necessitando o usuário alterá-la no primeiro acesso.
                                    </p>
                                    <p>
                                        "123456" Não é a senha real do usuário.
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-5 col-xs-8 col-sm-offset-1">
                                <button type="button" id="btnGerarSenha" class="btn btn-success">
                                    <span class="glyphicon glyphicon-lock"></span>
                                    Gerar senha
                                </button>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-1 col-xs-5 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?> Senha:
                            </label>
                            <div class="col-sm-4 col-xs-8">
                                <div class="input-group">
                                    <input type="text" name="senha"  readonly="readonly" id="senha" class="form-control validate[required]">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-lock"></span>
                                    </span>
                                </div>
                            </div>
                        </div>



                    </form>
                </div>
            </div>


        </div>
    </div>
</div>

<!-- /#page-wrapper -->

<script>
    var st_indsenhapadrao = $('#admin-st_indsenhapadrao');
    var aviso = $('#aviso');
    var senha = $('#senha');
    $(document).ready(function () {

        initBtnPageFormulario();

        if (st_indsenhapadrao.val() !== '' && st_indsenhapadrao.val() == 'N') {
            aviso.show();
            senha.val('123456');
        } else {
            senha.val($.trim($('#admin-tx_senha').val()));
        }
        $('#btnNovo').click(function () {
            window.location = _urlPadrao + '/formulario';
        });
        var dt_cadastro = $.trim($('#admin-dt_cadastro').val());
        if (dt_cadastro == '') {
            $('#admin-dt_cadastro').val(_dataAtual);
        }

        $('#admin-tx_email').blur(function () {

            tx_email = $.trim($(this).val());
            //alert(tx_slug);
            if (tx_email !== '') {
                validarEmail(tx_email);
            }
        });

        $('#btnGerarSenha').click(function () {
            gerarSenha();
        });

        $('#btnSalvar').click(function () {

            var formulario = $('#validate');

            if (formulario.validationEngine('validate')) {
                salvar(formulario);
            }
        });
    });

    function validarEmail(tx_email) {
        ShowMsgAguarde();
        $.ajax({
            url: _baseUrl + _controller + '/validarEmail',
            type: 'POST',
            dataType: 'json',
            data: {tx_email: tx_email},
            success: function (data) {
                if (data.validou !== undefined && data.validou == 'N') {
                    $('#admin-tx_email').val('');
                }

                if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.error, 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }

        });
    }

    function gerarSenha() {
        ShowMsgAguarde();

        $.ajax({
            url: _baseUrl + _controller + '/gerarSenha',
            type: 'POST',
            dataType: 'json',
            data: {st_indsenhapadrao: st_indsenhapadrao.val()},
            success: function (data) {
                if (data.tx_senha !== undefined && data.tx_senha !== '') {
                    senha.val(data.tx_senha);
                    $('#admin-tx_senha').val(data.tx_senha);
                    st_indsenhapadrao.val('S');
                    aviso.fadeOut(500);
                }
                else if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.error, 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }

        });
    }



    function salvar(formulario) {
        ShowMsgAguarde();
        //formulario.submit();return false;
        formulario.ajaxSubmit({
            success: function (data) {
                data = $.parseJSON(data);

                if (data.success !== undefined && data.success !== '') {
                    if (data.dataGrid.id_admin !== undefined && data.dataGrid.id_admin !== '') {
                        $("#admin-id_admin").val(data.dataGrid.id_admin);
                        if (data.dataGrid.st_indsenhapadrao !== undefined && data.dataGrid.st_indsenhapadrao == 'N') {
                            aviso.show();
                            senha.val('123456');
                        } else {
                            senha.val(data.dataGrid.tx_senha);
                        }
                    }
                    Dialog.success(data.success, 'Sucesso');
                } else if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.erro, 'Erro');
                } else {
                    Dialog.error('Falha ao salvar', 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax, 'Erro');
            },
            complete: function () {
                CloseMsgAguarde();
            }
        });

    }

</script>