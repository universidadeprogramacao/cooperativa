<div id="page-wrapper">
    <div class="row" style="margin-bottom: 10px;margin-top: 5px;" id="divBotoes"></div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Página inicial</h3>
                </div>
                <div class="panel-body">
                    <div class="alert alert-success">
                        <?php echo!empty($_SESSION['admin']['tx_nome']) ? "{$_SESSION['admin']['tx_nome']}," : ""; ?> bem vindo(a) a área administrativa do portal.
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
     if(!empty($dataComboAno)){
    ?>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Gráfico de consultas X Vendas</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-1 control-label">
                            Ano:
                        </label>

                        <div class="col-sm-3">
                            <select name="ano" onchange="loadGrafico($(this).val());" id="ano" class="form-control input-sm">
                                <?php
                                    foreach ($dataComboAno as $key=>$val){
                                ?>
                                <option value="<?php echo $key; ?>" title="<?php echo $val; ?>"><?php echo $val; ?></option>
                              <?php
                                    }
                              ?>
                            </select>
                        </div>
                    </div>
                    <br/>
                     <div class="form-group">
                        <label class="col-sm-1 control-label">
                            tipo:
                        </label>
                         <div class="col-sm-3">
                             <label>
                                 <input type="checkbox" name="st_vendapaga" onchange="loadGrafico($('#ano').val())" id="st_vendapaga" value="S">Somente vendas pagas
                             </label>
                         </div>
                     </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div id="grafico-consultas"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
     }
    ?>

</div>

<script>
    $(function () {
        //loadGrafico('<?php echo date('Y'); ?>');
    });

    function loadGrafico(ano) {
        var selecionadoVendaPaga = $('#st_vendapaga:checked');
        var st_vendapaga = '';
        if(selecionadoVendaPaga.length>0){
            st_vendapaga = 'S';
        }
        ShowMsgAguarde();
        var url = "<?php echo base_url(); ?>admin/grafico";
        $.ajax({
            url: url,
            type: 'POST',
            data: {ano: ano,st_vendapaga:st_vendapaga},
            dataType: 'html',
            success: function (data) {
                $('#grafico-consultas').html(data);
            }, error: function () {
                Dialog.error(_erroPadraoAjax, 'Erro!');
            }, complete: function (jqXHR, textStatus) {
                CloseMsgAguarde();
            }
        });
    }

</script>