<div id="page-wrapper">
    <div class="row" style="margin-bottom: 10px;margin-top: 5px;" id="divBotoes"></div>
    <div class="row">
        <?php
        if (!empty($error)) {
            ?>
            <div class="col-sm-12 col-xs-12">
                <div class="alert alert-danger"><?php echo $error; ?></div>
            </div>

            <?php
        }
        ?>

        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Meus dados cadastrais</h3>
                </div>
                <div class="panel-body">


                    <form method="post" class="form-horizontal populate" action="<?php echo base_url() . "admin/salvar"; ?>" id="validate">
                        <?php
                        if ($this->util->isAdmin()) {
                            ?>
                            <input type="hidden" name="admin[id_admin]" id="admin-id_admin">
                            <?php
                        } else {
                           
                            ?>
                             <input type="hidden" name="admin[id_produtor]" id="admin-id_produtor">
                            <?php
                        }
                        ?>


                        <input type="hidden" name="admin[st_indsenhapadrao]" id="admin-st_indsenhapadrao">
                        <input type="hidden" name="criptografar" value="S">

                        <?php
                        if ($this->util->isSenhaPadrao() !== FALSE) {
                            ?>
                            <div class="form-group">
                                <div class="col-sm-10 col-xs-10">
                                    <div class="alert alert-danger">
                                        Aviso: Você está com a senha padrão gerada pelo sistema, para prosseguir é necessário trocá-la, evite senhas fáceis, como datas de aniversário, telefones, etc.
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                            <label class="col-xs-8 col-sm-1 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Nome:
                            </label>
                            <div class="col-xs-10 col-sm-8">
                                <input type="text" name="admin[tx_nome]" readonly="readonly" id="admin-tx_nome" class="form-control validate[required]" maxlength="60">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-xs-8 col-sm-1 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Email:
                            </label>

                            <div class="col-xs-8 col-sm-5">
                                <div class="input-group">
                                    <input type="text" name="admin[tx_email]" readonly="readonly" id="admin-tx_email" class="form-control lower validate[required,custom[email]]" maxlength="60">
                                    <span class="input-group-addon">
                                        @
                                    </span>
                                </div>
                            </div>

                        </div>


                        <div class="form-group">
                            <label class="col-xs-8 col-sm-1 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Senha:
                            </label>

                            <div class="col-xs-8 col-sm-5">
                                <div class="input-group">
                                    <input type="text" name="admin[tx_senha]" id="admin-tx_senha" class="form-control validate[required]" maxlength="80">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-lock"></span>
                                    </span>
                                </div>
                            </div>

                        </div>


                        <div class="form-group">
                            <label class="col-xs-8 col-sm-1 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Status:
                            </label>
                            <div class="col-xs-7 col-sm-6">


                                <span class="radio radio-inline">
                                    <label>
                                        <input type="radio" readonly="readonly" name="admin[st_status]"  id="admin-st_status-A" value="A">Cadastro Ativo
                                    </label>
                                </span>

                                <span class="radio radio-inline">
                                    <label>
                                        <input type="radio" readonly="readonly" name="admin[st_status]" id="admin-st_status-I" value="I">Cadastro Inativo
                                    </label>
                                </span>

                            </div>
                        </div>



                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Data de cadastro:
                            </label>

                            <div class="col-xs-5 col-sm-2">
                                <div class="input-group">
                                    <input type="text" name="admin[dt_cadastro]" readonly="readonly" id="admin-dt_cadastro" class="form-control date validate[required,custom[date]]">
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                </div>
                            </div>

                        </div>


                    </form>

                </div>
            </div>


        </div>
    </div>
</div>

<!-- /#page-wrapper -->

<script>

    $(document).ready(function () {
        initBtnPageFormulario();
        $('#btnNovo').hide();
        $('#btnListar').hide();
        $('#btnSalvar').show();
        $('#admin-tx_senha').val('');

        $('#admin-tx_email').blur(function () {

            tx_email = $.trim($(this).val());

            if (tx_email !== '') {
                //validarEmail(tx_email);
            }
        });

        $('#btnSalvar').click(function () {

            var formulario = $('#validate');

            if (formulario.validationEngine('validate')) {
                var tx_senha = $('#admin-tx_senha').val();
                if ($.trim(tx_senha).length < 6 || tx_senha == '123456') {
                    Dialog.error("A senha deve conter no mínimo 6 caracteres e evite senha do tipo 123456", "Atenção");
                    return  false;
                }
                salvar(formulario);
            }
        });
    });

    function validarEmail(tx_email) {
        ShowMsgAguarde();
        $.ajax({
            url: _baseUrl + _controller + '/validarEmail',
            type: 'POST',
            dataType: 'json',
            data: {tx_email: tx_email},
            success: function (data) {
                if (data.validou !== undefined && data.validou == 'N') {
                    $('#admin-tx_email').val('');
                }

                if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.error, 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }

        });
    }


    function salvar(formulario) {
        ShowMsgAguarde();
        //formulario.submit();return false;
        formulario.ajaxSubmit({
            success: function (data) {
                data = $.parseJSON(data);
                if (data.success !== undefined && data.success !== '') {
                    Dialog.success(data.success, 'Sucesso');
                } else if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.erro, 'Erro');
                } else {
                    Dialog.error('Falha ao salvar', 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax, 'Erro');
            },
            complete: function () {
                CloseMsgAguarde();
            }
        });

    }

</script>