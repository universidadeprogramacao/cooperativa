<?php
$consultaAno = array();
$arrMeses = array(
    0 => 'Jan',
    1 => 'Fev',
    2 => 'Mar',
    3 => 'Abr',
    4 => 'Mai',
    5 => 'Jun',
    6 => 'Jul',
    7 => 'Ago',
    8 => 'Set',
    9 => 'Out',
    10 => 'Nov',
    11 => 'Dez'
);

$arrSomaMesesConsulta = array(
    0 => 0,
    1 => 0,
    2 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    10 => 0,
    11 => 0 
);

$arrSomaMesesVenda = array(
    0 => 0,
    1 => 0,
    2 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
    9 => 0,
    10 => 0,
    11 => 0 
);

$categoriesMes = array();
$data = array();
if (!empty($dataGridConsulta)) {
 
    foreach ($dataGridConsulta as $valConsulta){
        $nr_mes = $valConsulta['nr_mes']-1;
        $arrSomaMesesConsulta[$nr_mes]+=1;
    }
    
      
}
if(!empty($dataGridvenda)){
  foreach ($dataGridvenda as $valVenda){
        $nr_mesvenda = $valVenda['nr_mesvenda']-1;
        $arrSomaMesesVenda[$nr_mesvenda]+=1;
    }  
}

?>
<script>
    $(function () {

        $('#grafico-consultas').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Consultas X Vendas'
            },
            subtitle: {
                text: 'Fonte: http://despachantemarcel.com.br/'
            },
            xAxis: {
                categories: <?php echo json_encode($arrMeses); ?>,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Consultas X Vendas'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                    name: 'Consultas',
                    data: <?php echo json_encode($arrSomaMesesConsulta); ?>

                },
                {
                    name: 'Vendas',
                    data: <?php echo json_encode($arrSomaMesesVenda); ?>

                }]
        });
    });


</script>