<div id="page-wrapper">
    <div class="row" style="margin-bottom: 10px;margin-top: 5px;" id="divBotoes"></div>
    <div class="row">
        <div class="col-xs-12 col-sm-22 col-lg-12 col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Pesquisar Venda</h3>
                </div>
                <div class="panel-body">

                    <form method="post" class="form-horizontal populate" action="<?php echo base_url('admin_venda/pdf'); ?>" target="_blank" id="validate">

                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Código
                            </label>
                            <div class="col-sm-2">
                                <input type="text" name="venda[id_venda]" id="venda-id_venda" class="form-control numeric" maxlength="20">
                            </div>

                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Referência:
                            </label>
                            <div class="col-xs-10 col-sm-6">
                                <input type="text" name="venda[tx_referenciatransacao]" id="venda-tx_referenciatransacao" class="form-control" maxlength="210">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Data da venda:
                            </label>

                            <div class="col-xs-5 col-sm-2">
                                <input type="text" name="venda[dt_vendainicio]" id="venda-dt_vendainicio" class="form-control date validate[custom[date]]">
                            </div>
                            <div class="col-sm-1 col-xs-2">
                                A
                            </div>
                            <div class="col-xs-5 col-sm-2">
                                <input type="text" name="venda[dt_vendafim]" id="venda-dt_vendafim" class="form-control date validate[custom[date]]">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Tipo Pagamento:
                            </label>
                            <div class="col-xs-10 col-sm-3">
                                <select name="venda[tp_pagamentopagseguro]" id="venda-tp_pagamentopagseguro" class="form-control">
                                    <option value="" title="Selecione">Selecione</option>
                                    <?php
                                    $tipoPagamento = $this->utilpagseguro->getTipoPagamento('', true);
                                    if (!empty($tipoPagamento)) {
                                        foreach ($tipoPagamento as $key => $val) {
                                            ?>
                                            <option value="<?php echo $key; ?>" title="<?php echo $val; ?>"><?php echo $val; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>

                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Meio Pagamento:
                            </label>
                            <div class="col-xs-10 col-sm-5">
                                <select name="venda[tp_meiopagamentopagseguro]" id="venda-tp_meiopagamentopagseguro" class="form-control">
                                    <option value="" title="Selecione">Selecione</option>
                                    <?php
                                    $meioPagamento = $this->utilpagseguro->getMeioTipoPagamento('', true);
                                    if (!empty($meioPagamento)) {
                                        foreach ($meioPagamento as $key => $val) {
                                            ?>
                                            <option value="<?php echo $key; ?>" title="<?php echo $val; ?>"><?php echo $val; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Status Venda:
                            </label>
                            <div class="col-xs-10 col-sm-3">
                                <select name="venda[st_vendapagseguro]" id="venda-st_vendapagseguro" class="form-control">
                                    <option value="" title="Selecione">Selecione</option>
                                    <?php
                                    $statusVendaPagSeguro = $this->utilpagseguro->getStatusVenda('', true);
                                    if (!empty($statusVendaPagSeguro)) {
                                        foreach ($statusVendaPagSeguro as $key => $val) {
                                            ?>
                                            <option value="<?php echo $key; ?>" title="<?php echo $val; ?>"><?php echo $val; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>


                            <label class="col-xs-8 col-sm-2 control-label">
                                Tipo:
                            </label>

                            <div class="col-sm-5">
                                <label>
                                    <input type="radio" name="venda[tipo_venda]" value="" id="venda-tipo_venda-">Todas
                                </label>
                                <label>
                                    <input type="radio" name="venda[tipo_venda]" value="N" id="venda-tipo_venda-N">Não iniciadas
                                </label>


                                <label>
                                    <input type="radio" name="venda[tipo_venda]" value="S" id="venda-tipo_venda-S">Somente iniciadas
                                </label>
                            </div>


                        </div>


                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Usuário:
                            </label>
                            <div class="col-xs-10 col-sm-10">
                                <input type="text" name="venda[tx_nome]" id="venda-tx_nome" class="form-control" maxlength="60">
                                <div class="help-block">
                                    *Quem comprou pelo site
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Email:
                            </label>

                            <div class="col-xs-8 col-sm-5">
                                <div class="input-group">
                                    <input type="text" name="venda[tx_email]" id="venda-tx_email" class="form-control lower validate" maxlength="60">
                                    <span class="input-group-addon">
                                        @
                                    </span>
                                </div>
                            </div>

                        </div>


                        <div class="form-group">
                            <div class="col-xs-5 col-sm-5 col-sm-offset-2">
                                <button type="submit"  class="btn btn-success">
                                    <span class="glyphicon glyphicon-print"></span>
                                    Gerar relatório
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

      
        </div>
    </div>

</div>
<!-- /#page-wrapper -->


<script>
    autoCompleteUsuario = <?php echo!empty($autocompleteUsuario) ? json_encode($autocompleteUsuario) : '' ?>;
    $(document).ready(function () {
      
        $('#venda-tx_nome').autocomplete(autoCompleteUsuario);
 
    });

</script>