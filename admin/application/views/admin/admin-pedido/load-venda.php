
<table class="table table-bordered table-striped" id="dataTable">
    <thead>
        <tr>
            <th style="width: 15%;">&nbsp;</th>
            <th style="width: 30%;">Referência</th>
            <th style="width: 18%;">Data</th>
            <th style="width: 20%;">Usuário</th>
            <th style="width: 15%;">Email</th>
            <th style="width: 15%;">Status</th>
            <th style="width: 10%;">Entregue</th>
        </tr>
    </thead>

    <tbody>
        <?php
  
        if (!empty($dataGrid)) {
            foreach ($dataGrid as $resultado) {
                ?>
                <tr id="linhaCadastro-<?php echo $resultado['id_venda']; ?>">
                    <td>
                        <div class="btn-group">
                            <a  href="<?php echo "{$urlPadrao}/pdf/{$resultado['id_venda']}"; ?>"  title="Gerar PDF" style="margin: 3px;" class="btn btn-xs btn-success" target="_blank">
                                <span class="glyphicon glyphicon-print"></span>
                            </a>

                            <a onclick="loadVendaById('<?php echo $resultado['id_venda'] ?>')"   title="Ver" style="margin: 3px;" class="btn btn-xs btn-info" >
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>

                            <button id_param="<?php echo $resultado['id_venda']; ?>" title="Excluir" type="button" style="margin: 3px;" class="btn btn-xs btn-danger buttonDelete">
                                <span class="glyphicon glyphicon-remove"></span>
                            </button>

                        </div>
                    </td>

                    <td><?php echo $resultado['tx_referenciatransacao']; ?></td>
                    <td><?php echo $this->util->reverseDate($resultado['dt_venda']); ?></td>
                    <td><?php echo $resultado['tx_nome']; ?></td>
                    <td><?php echo $resultado['tx_email']; ?></td>
                    <td><?php echo $this->utilpagseguro->getStatusVenda($resultado['st_vendapagseguro']); ?></td>
                    <td><?php echo $this->util->getStVendaEntregue($resultado['st_vendaentregue']); ?></td>
                </tr>
                <?php
            }
        } else {
            ?>
            <tr>
                <td colspan="7"> 
                    <div class="alert alert-warning">
                        Nenhum registro encontrado
                    </div>
                </td>
               
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>