
<div id="page-wrapper">
    <div class="row" style="margin-bottom: 10px;margin-top: 5px;" id="divBotoes"></div>
    <div class="row">
        <?php
        if (!empty($error)) {
            ?>
            <div class="col-sm-12 col-xs-12">
                <div class="alert alert-danger"><?php echo $error; ?></div>
            </div>

            <?php
        }
        ?>

        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Pedido pendente de fornecimento</h3>
                </div>
                <div class="panel-body">
                    <form method="post" class="form-horizontal populate" action="<?php echo "{$urlPadrao}/salvarProposta"; ?>" id="validate">
                        <input type="hidden" name="produtor[id_produtor]" id="produtor-id_produtor">
                        <div style="display: none;" id="html-dinamico"></div>

                        <table class="table table-bordered" id="datatable">
                            <thead>
                                <tr style="background-color: #CCC;">

                                    <th style="width: 50%;">
                                        Produto
                                    </th>
                                    <th style="width: 10%;">
                                        Qtd Pedida
                                    </th>
                                    <th style="width: 10%;">
                                        Sub Total
                                    </th>
                                    <th style="width: 10%;">
                                        Data fechamento
                                    </th>
                                    <th style="width: 20%;">
                                        Qtd Fornecimento
                                    </th>

                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                if (!empty($dataGrid)) {
                                    foreach ($dataGrid as $key => $pedido) {
                                        $valorTotalPedido = 0.00;
                                        ?>

                                        <tr>
                                            <td><?php echo $pedido['tx_produto']; ?></td>
                                            <td><?php echo $pedido['quantidade']; ?></td>
                                            <td><?php echo $pedido['subtotal']; ?></td>
                                            <td><?php echo $pedido['data_fechamento']; ?></td>
                                            <td>
                                                <select  name="produto[<?php echo $key; ?>]" class="form-control fornecimento">
                                                    <?php
                                                    for ($i = 0; $i <= $pedido['quantidade']; $i++) {
                                                        $selected = "";
                                                        if ($pedido['nr_qtdofertada'] == $i) {
                                                            $selected = "selected='selected'";
                                                        }
                                                        ?>
                                                        <option <?php echo $selected; ?> vl_unitariooriginal="<?php echo $pedido['vl_unitario']; ?>" id_pedidocotacaofornecimento="<?php echo $pedido['id_pedidocotacaofornecimento']; ?>" id_produto="<?php echo $pedido['id_produto']; ?>" sub_totaloriginal="<?php echo $pedido['subtotal']; ?>" nr_qtdoriginal="<?php echo $pedido['quantidade']; ?>" dt_fechamentooriginal="<?php echo $pedido['dt_fechamento']; ?>" title="<?php echo $i; ?>" value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>

                                            </td>

                                        </tr>

                                        <?php
                                    }
                                } else {
                                    ?>
                                <div class="alert alert-warning">Nenhum pedido pendente de fornecimento</div>

                                <?php
                            }
                            ?>

                            </tbody>
                        </table>


                    </form>
                </div>
            </div>


        </div>
    </div>
</div>

<!-- /#page-wrapper -->

<script>

    $(document).ready(function () {

        initBtnPageFormulario();
        $("#btnNovo").hide();
        $("#btnSalvar").show();

        $('#btnSalvar').click(function () {

            var fornecimento = $(".fornecimento");
            var selecionado = '';
            var html = '';
            var indice = 0;
            if (fornecimento.length > 0) {

                fornecimento.each(function () {
                    selecionado = $(this).find("option:selected");
                    if (selecionado.length > 0) {
                        html += '<input type="hidden" name="fornecimento[' + indice + '][vl_unitariooriginal]" value="' + selecionado.attr('vl_unitariooriginal') + '" >';
                        html += '<input type="hidden" name="fornecimento[' + indice + '][id_pedidocotacaofornecimento]" value="' + selecionado.attr('id_pedidocotacaofornecimento') + '" >';
                        html += '<input type="hidden" name="fornecimento[' + indice + '][id_produto]" value="' + selecionado.attr('id_produto') + '" >';
                        html += '<input type="hidden" name="fornecimento[' + indice + '][sub_totaloriginal]" value="' + selecionado.attr('sub_totaloriginal') + '" >';
                        html += '<input type="hidden" name="fornecimento[' + indice + '][nr_qtdoriginal]" value="' + selecionado.attr('nr_qtdoriginal') + '" >';
                        html += '<input type="hidden" name="fornecimento[' + indice + '][dt_fechamentooriginal]" value="' + selecionado.attr('dt_fechamentooriginal') + '" >';
                        html += '<input type="hidden" name="fornecimento[' + indice + '][nr_qtdofertada]" value="' + selecionado.val() + '" >';
                        indice++;
                    }
                });
            }

            $("#html-dinamico").html(html);
            salvar($("#validate"));

        });
    });


    function salvar(formulario) {
        ShowMsgAguarde();
        //formulario.submit();return false;
        formulario.ajaxSubmit({
            success: function (data) {
                data = $.parseJSON(data);
                //Dialog.success(data.success, 'Sucesso');
            },
            error: function () {
                Dialog.error(_erroPadraoAjax, 'Erro');
            },
            complete: function () {
                CloseMsgAguarde();
            }
        });

    }

</script>