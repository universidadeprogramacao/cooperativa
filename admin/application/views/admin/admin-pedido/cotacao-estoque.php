
<div id="page-wrapper">
    <div class="row" style="margin-bottom: 10px;margin-top: 5px;" id="divBotoes"></div>
    <div class="row">
        <?php
        if (!empty($error)) {
            ?>
            <div class="col-sm-12 col-xs-12">
                <div class="alert alert-danger"><?php echo $error; ?></div>
            </div>

            <?php
        }
        ?>

        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Fechamento de cotação do estoque</h3>
                </div>
                <div class="panel-body">
                    <form method="post" class="form-horizontal populate" action="<?php echo "{$urlPadrao}/salvarFechamento"; ?>" id="validate">

                        <div style="display: none;" id="html-dinamico"></div>
                        <h3>Pedidos feitos pelo site</h3>
                        <table class="table table-bordered" id="datatable">
                            <thead>
                                <tr style="background-color: #CCC;">

                                    <th style="width: 30%;">
                                        Produto
                                    </th>
                                    <th style="width: 10%;">
                                        Qtd Pedida
                                    </th>
                                    <th style="width: 10%;">
                                        Sub Total
                                    </th>
                                    <th style="width: 10%;">
                                        Data fechamento
                                    </th>
                                    <th style="width: 40%;">
                                        Fornecedores
                                    </th>

                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                if (!empty($dataGrid)) {
                                    foreach ($dataGrid as $key => $pedido) {
                                        /*
                                          echo '<pre>';
                                          print_r($pedido);
                                          echo '</pre>';
                                          die;
                                         * 
                                         */

                                        $valorTotalPedido = 0.00;
                                        ?>

                                        <tr>
                                            <td><?php echo $pedido['tx_produto']; ?></td>
                                            <td><?php echo $pedido['quantidade']; ?></td>
                                            <td><?php echo $pedido['subtotal']; ?></td>
                                            <td><?php echo $pedido['data_fechamento']; ?></td>
                                            <td>
                                                <table>

                                                    <tr>
                                                        <td style="width: 100%;">

                                                            <table style="width: 100%;">
                                                                <?php
                                                                if (!empty($produtores[$key])) {
                                                                    $totalFornecedores = 0;
                                                                    $totalLimite = count($produtores[$key]);
                                                                    foreach ($produtores[$key] as $produtor) {
                                                                        $totalFornecedores++;
                                                                        ?>
                                                                        <tr>
                                                                            <td>
                                                                                <p>
                                                                                    <b>Fornecedor:</b> <?php echo $produtor['tx_nome']; ?>
                                                                                </p>
                                                                                <p>
                                                                                    <b>E-mail:</b> <?php echo $produtor['tx_email']; ?>
                                                                                </p>
                                                                                <p>
                                                                                    <b>Tel:</b> (<?php echo $produtor['tx_ddd']; ?>) <?php echo $produtor['tx_numero']; ?>
                                                                                </p>
                                                                            </td>
                                                                            <td>
                                                                                <select  name="produto[]" class="form-control fornecimento">
                                                                                    <?php
                                                                                    for ($i = 0; $i <= $pedido['quantidade']; $i++) {
                                                                                        $selected = "";
                                                                                        if ($produtor['nr_qtdofertada'] == $i) {
                                                                                            $selected = "selected='selected'";
                                                                                        }
                                                                                        ?>
                                                                                        <option <?php echo $selected; ?> id_produto="<?php echo $produtor['id_produto']; ?>"  id_pedidocotacaofornecimento="<?php echo $produtor['id_pedidocotacaofornecimento']; ?>" dt_movimentacao="<?php echo date('Y-m-d'); ?>" title="<?php echo $i; ?>" value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                                                        <?php
                                                                                    }
                                                                                    ?>
                                                                                </select>
                                                                            </td>
                                                                        </tr>

                                                                        <?php
                                                                        if ($totalFornecedores == $totalLimite) {
                                                                            ?>
                                                                            <tr>
                                                                                <td>
                                                                                    <b>Qtd fornecedores:</b> <?php echo $totalFornecedores; ?>
                                                                                </td>
                                                                                <td>
                                                                                    <b>Qtd produtos ideal:</b> <?php echo round($pedido['quantidade']/$totalFornecedores); ?>
                                                                                </td>
                                                                            </tr>


                                                                            <?php
                                                                        }
                                                                        ?>


                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </td>

                                        </tr>

        <?php
    }
} else {
    ?>
                                <div class="alert alert-warning">Nenhum pedido pendente de fornecimento</div>

                                    <?php
                                }
                                ?>

                            </tbody>
                        </table>


                    </form>
                </div>
            </div>


        </div>
    </div>
</div>

<!-- /#page-wrapper -->

<script>

    $(document).ready(function () {

        initBtnPageFormulario();
        $("#btnNovo").hide();
        $("#btnSalvar").show();

        $('#btnSalvar').click(function () {

            var fornecimento = $(".fornecimento");
            var selecionado = '';
            var html = '';
            var indice = 0;
            if (fornecimento.length > 0) {

                fornecimento.each(function () {
                    selecionado = $(this).find("option:selected");
                    if (selecionado.length > 0) {
                        html += '<input type="hidden" name="fornecimento[' + indice + '][id_pedidocotacaofornecimento]" value="' + selecionado.attr('id_pedidocotacaofornecimento') + '" >';
                        html += '<input type="hidden" name="fornecimento[' + indice + '][dt_movimentacao]" value="' + selecionado.attr('dt_movimentacao') + '" >';
                        html += '<input type="hidden" name="fornecimento[' + indice + '][id_produto]" value="' + selecionado.attr('id_produto') + '" >';
                        html += '<input type="hidden" name="fornecimento[' + indice + '][nr_qtdmovimentada]" value="' + selecionado.val() + '" >';
                        indice++;
                    }
                });
            }

            $("#html-dinamico").html(html);
            salvar($("#validate"));

        });
    });


    function salvar(formulario) {
        ShowMsgAguarde();
        //formulario.submit();return false;
        formulario.ajaxSubmit({
            success: function (data) {
                data = $.parseJSON(data);
                Dialog.success(data.success, 'Sucesso');
            },
            error: function () {
                Dialog.error(_erroPadraoAjax, 'Erro');
            },
            complete: function () {
                CloseMsgAguarde();
            }
        });

    }

</script>