<?php
ob_start();
if(!empty($dataGrid)){
    /**
    echo '<pre>';
    print_r($dataGrid);
    echo '</pre>';
    die;
     * 
     */
    $i=1;
    foreach ($dataGrid as $key=> $val){
        $telefone = sprintf("(%s) %s",$val['tx_ddd'],$val['tx_numero']);
?>

<table border="1" style="width: 100%;border-collapse: collapse;margin-top: 10px;">
    <tr>
        <th colspan="2" style="background-color: #cccccc;">
            Dados da venda
        </th>
    </tr>

    <tr>
        <td>
            Código: <?php echo $val['id_venda']; ?>
        </td>
        
        <td>
            Referência: <?php echo $val['tx_referenciatransacao']; ?>
        </td>
    </tr>
    
    
     <tr>
         <td colspan="2">
            Renavam: <?php echo $val['tx_renavam']; ?>
        </td>
      
    </tr>
    
    
    <tr>
        <td>
            Data da venda: <?php echo $val['dt_venda']; ?>
        </td>
        
        <td>
            Status: <?php echo $val['st_vendapagseguro']; ?>
        </td>
    </tr>
    
    <tr>
        <td>
            Valor: <?php echo $val['vl_venda']; ?>
        </td>
        
        <td>
           Valor líquido: <?php echo $val['vl_liquidotransacaopagseguro']; ?>
        </td>
    </tr>
    
    <tr>
        <td>
            Tipo pagamento: <?php echo $val['tp_pagamentopagseguro']; ?>
        </td>
        
        <td>
           Meio pagamento: <?php echo $val['tp_meiopagamentopagseguro']; ?>
        </td>
    </tr>
    
     <tr>
        <td>
            Data postagem: <?php echo $val['dt_vendapostada']; ?>
        </td>
        
        <td>
          Código rastreamento: <?php echo $val['tx_codigocorreios']; ?>
        </td>
    </tr>
    
  
</table>

<table border="1" style="width: 100%;border-collapse: collapse;margin-top: 10px;">
      <tr>
          <th colspan="3" style="background-color: #cccccc;">
            Dados do usuário
        </th>
    </tr>
    
    <tr>
        <td colspan="2">
            Nome: <?php echo $val['tx_nome']; ?>
        </td>
        <td>
            CPF: <?php echo $val['tx_cpf']; ?>
        </td>
    </tr>
    
      <tr>
          <td colspan="2">
            Email: <?php echo $val['tx_email']; ?>
        </td>
        <td>
            Telefone: <?php echo $telefone; ?>
        </td>
    </tr>
    
</table>

<table border="1" style="width: 100%;border-collapse: collapse;margin-top: 10px;">
    <tr>
        <th colspan="3" style="background-color: #cccccc;">
            Dados da entrega
        </th>
    </tr>
    
    <tr>
        <td>
           Rua: <?php echo $val['tx_ruaentrega']; ?>
        </td>
        <td>
            Número: <?php echo $val['tx_numeroruaentrega']; ?>
        </td>
        <td>
            Complemento: <?php echo $val['tx_complementoentrega']; ?>
        </td>
    </tr>
    
      <tr>
          <td colspan="2">
            Cidade: <?php echo $val['tx_cidadeentrega']; ?>
        </td>
        <td>
            Bairro: <?php echo $val['tx_bairroentrega']; ?>
        </td>
    </tr>
    <tr>
       <td>
            CEP: <?php echo $val['tx_cepentrega']; ?>
        </td>
        <td colspan="2">
            Estado: <?php echo $val['tx_estadoentrega']; ?>
        </td>
    </tr>
    
</table>
<div style="width: 100%;margin-top: 20px;height: 1px;border: 1px solid #000;"></div>

<?php 
     if($i%2 ==0 && $i !=0 && $i<$key){
         echo '<page_break>';
     }
     $i++;
    }
}else{
?>
<table border="1" style="width: 100%;border-collapse: collapse;margin-top: 10px;">
    <tr>
        <th colspan="1" style="background-color: #cccccc;">
          Nenhum registro encontrado
        </th>
    </tr>
</table>


<?php
}

$conteudo = ob_get_contents();
ob_clean();

$conf['mode'] = 'c';
$conf['format'] = 'A4';
$conf['font_size'] = '12';
$conf['font'] = 'arial';
$conf['m_left'] = 5;
$conf['m_right'] = 5;
$conf['m_top'] = 5;
$conf['m_bottom'] = 5;
$conf['m_header'] = 5;
$conf['m_footer'] = 5;
$conf['orientation'] = 'P';

$date = date('Y-m-d H-i-s');
$date = str_replace("", "-", $date);
$pdf['name'] = sprintf("RELATORIO_VENDAS_%s.pdf",$date);
$pdf['content'] = $conteudo;
$this->utilpdf->mpdf($pdf,$conf);
?>