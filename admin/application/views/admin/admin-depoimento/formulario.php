
<div id="page-wrapper">
    <div class="row" style="margin-bottom: 10px;margin-top: 5px;" id="divBotoes"></div>
    <div class="row">
        <?php

        function getDataComboDepoimento($status = null) {
            $arr = array(
                5 => "Excelente",
                4 => "Muito bom",
                3 => "Bom",
                2 => "Regular",
                1 => "Ruim"
            );

            if (!empty($arr[$status])) {
                return $arr[$status];
            }
            return $arr;
        }

        if (!empty($error)) {
            ?>
            <div class="col-sm-12 col-xs-12">
                <div class="alert alert-danger"><?php echo $error; ?></div>
            </div>

            <?php
        }
        ?>

        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Cadastro de Usuários comuns do site</h3>
                </div>
                <div class="panel-body">
                    <form enctype="multipart/form-data" method="post" class="form-horizontal populate" action="<?php echo "{$urlPadrao}/salvar"; ?>" id="validate">
                        <input type="hidden" name="depoimento[id_depoimento]" id="depoimento-id_depoimento">



                        <div class="form-group">
                            <label class="col-xs-8 col-sm-1 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Status:
                            </label>
                            <div class="col-xs-7 col-sm-6">


                                <span class="radio radio-inline">
                                    <label>
                                        <input type="radio" name="depoimento[st_aprovado]"  id="depoimento-st_aprovado-S" value="S">Aprovado
                                    </label>
                                </span>

                                <span class="radio radio-inline">
                                    <label>
                                        <input type="radio" name="depoimento[st_aprovado]" id="depoimento-st_aprovado-N" value="N">Pendente
                                    </label>
                                </span>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Data de cadastro:
                            </label>

                            <div class="col-xs-5 col-sm-2">
                                <div class="input-group">
                                    <input type="text" name="depoimento[dt_depoimento]" id="depoimento-dt_depoimento" class="form-control date validate[required,custom[date]]">
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </span>
                                </div>
                            </div>

                        </div>


                        <div class="form-group">
                            <label class="col-sm-1 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Nome:
                            </label>
                            <div class="col-sm-5">
                                <input type="text" name="depoimento[tx_nome]"  id="depoimento-tx_nome" class="form-control validate[required]" maxlength="60">
                            </div>

                            <label class="col-sm-2 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Email:
                            </label>

                            <div class="col-sm-4">
                                <div class="input-group">
                                    <input type="text" name="depoimento[tx_email]"  id="depoimento-tx_email" class="form-control lower validate[required,custom[email]]" maxlength="60">
                                    <span class="input-group-addon">
                                        @
                                    </span>
                                </div>
                            </div>

                        </div>


                        <div class="form-group">
                            <label class="col-sm-1 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Cidade:
                            </label>

                            <div class="col-sm-6">
                                <input type="text" name="depoimento[tx_cidade]" id="depoimento-tx_cidade" class="form-control validate[required]" maxlength="100">
                            </div>

                            <label class="col-sm-2 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Estado:
                            </label>

                            <div class="col-sm-3">
                                <select name="depoimento[tx_estado]" id="depoimento-tx_estado" class="form-control validate[required] ">
                                    <option value="">Selecione</option>
                                    <option value="AC">AC</option>
                                    <option value="AL">AL</option>
                                    <option value="AM">AM</option>
                                    <option value="AP">AP</option>
                                    <option value="BA">BA</option>
                                    <option value="CE">CE</option>
                                    <option value="DF">DF</option>
                                    <option value="ES">ES</option>
                                    <option value="GO">GO</option>
                                    <option value="MA">MA</option>
                                    <option value="MG">MG</option>
                                    <option value="MS">MS</option>
                                    <option value="MT">MT</option>
                                    <option value="PA">PA</option>
                                    <option value="PB">PB</option>
                                    <option value="PE">PE</option>
                                    <option value="PI">PI</option>
                                    <option value="PR">PR</option>
                                    <option value="RJ">RJ</option>
                                    <option value="RN">RN</option>
                                    <option value="RS">RS</option>
                                    <option value="RO">RO</option>
                                    <option value="RR">RR</option>
                                    <option value="SC">SC</option>
                                    <option value="SE">SE</option>
                                    <option value="SP">SP</option>
                                    <option value="TO">TO</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">

                            <label class="col-sm-2 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Avaliação:
                            </label>

                            <div class="col-sm-3">
                                <select name="depoimento[tx_avaliacaoservico]" id="depoimento-tx_avaliacaoservico"
                                        class="form-control validate[required]"
                                        onchange="pintarEstrelasAvaliacao($(this).val());">

                                    <option value="">Como avalia nosso serviço?</option>
                                    <?php
                                    foreach (getDataComboDepoimento() as $key => $val) {
                                        ?>
                                        <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                        <?php
                                    }
                                    ?>

                                </select>
                            </div>

                            <label id="estrelas">
                                <i class="fa fa-star-o estrela"></i>
                                <i class="fa fa-star-o estrela"></i>
                                <i class="fa fa-star-o estrela"></i>
                                <i class="fa fa-star-o estrela"></i>
                                <i class="fa fa-star-o estrela"></i>
                            </label>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">
                                <?php echo CAMPO_OBRIGATORIO; ?>
                                Depoimento:
                            </label>

                            <div class="col-sm-10">
                                <textarea class="form-control validate[required]"
                                          name="depoimento[tx_depoimento]" id="depoimento-tx_depoimento" rows="5" placeholder="Depoimento"
                                          data-toggle="tooltip" title="Depoimento obrigatório"
                                          ></textarea>
                            </div>

                        </div>

                        <div class="form-group">

                            <label class="col-sm-2 control-label">
                                Foto(Opcional):
                            </label>

                            <div class="col-sm-5">
                                <input type="file" name="userfile" id="userfile">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-5" id="avatar">
                                <?php
                                if(!empty($avatar)){
                                    echo $avatar;
                                }
                                ?>
                              
                            </label>

                        </div>



                    </form>
                </div>
            </div>


        </div>
    </div>
</div>

<!-- /#page-wrapper -->

<script src="http://malsup.github.com/jquery.form.js"></script>
<script>

                                            $(document).ready(function () {

                                                initBtnPageFormulario();
                                                $('#btnNovo').click(function () {
                                                    window.location = _urlPadrao + '/formulario';
                                                });

                                                $("#depoimento-tx_avaliacaoservico").change();
                                                var dt_depoimento = $.trim($('#depoimento-dt_depoimento').val());
                                                if (dt_depoimento == '') {
                                                    $('#depoimento-dt_depoimento').val(_dataAtual);
                                                }

                                                $('#btnSalvar').click(function () {

                                                    var formulario = $('#validate');
                                                    if (formulario.validationEngine('validate')) {
                                                        salvar(formulario);
                                                    }
                                                });
                                            });

                                            function salvar(formulario) {
                                                ShowMsgAguarde();

                                                formulario.ajaxSubmit({
                                                    dataType: 'json',
                                                    success: function (data) {
                                                        if (data.success !== undefined && data.success !== '') {
                                                            if (data.dataGrid.id_depoimento !== undefined && data.dataGrid.id_depoimento !== '') {
                                                                $("#depoimento-id_depoimento").val(data.dataGrid.id_depoimento);
                                                                //alert(data.avatar);
                                                                if(data.avatar !==undefined && data.avatar !==''){
                                                                    //alert(data.avatar);
                                                                    $("#avatar").html(data.avatar);
                                                                }
                                                            }
                                                            Dialog.success(data.success, 'Sucesso');
                                                        } else if (data.error !== undefined && data.error !== '') {
                                                            Dialog.error(data.erro, 'Erro');
                                                        } else {
                                                            Dialog.error('Falha ao salvar', 'Erro');
                                                        }
                                                    }, error: function () {
                                                         Dialog.error(_erroPadraoAjax, 'Erro');
                                                    },
                                                    complete: function () {
                                                        CloseMsgAguarde();
                                                    }

                                                });

                                                /**
                                                 formulario.ajaxSubmit({
                                                 success: function (data) {
                                                 data = $.parseJSON(data);
                                                 if (data.success !== undefined && data.success !== '') {
                                                 if (data.dataGrid.id_depoimento !== undefined && data.dataGrid.id_depoimento !== '') {
                                                 $("#depoimento-id_depoimento").val(data.dataGrid.id_depoimento);
                                                 }
                                                 Dialog.success(data.success, 'Sucesso');
                                                 } else if (data.error !== undefined && data.error !== '') {
                                                 Dialog.error(data.erro, 'Erro');
                                                 } else {
                                                 Dialog.error('Falha ao salvar', 'Erro');
                                                 }
                                                 },
                                                 error: function () {
                                                 Dialog.error(_erroPadraoAjax, 'Erro');
                                                 },
                                                 complete: function () {
                                                 CloseMsgAguarde();
                                                 }
                                                 });
                                                 
                                                 **/
                                            }

                                            function  pintarEstrelasAvaliacao(a) {

                                                var y = 1;
                                                var est = '';
                                                for (var x = 1; x <= a; x++) {
                                                    est += '<i class="fa fa-star estrela"></i> ';
                                                    y++;
                                                }

                                                for (var x = y; x <= 5; x++) {
                                                    est += '<i class="fa fa-star-o estrela"></i> ';
                                                }

                                                $("#estrelas").html(est);
                                            }

</script>

<style type="text/css">
    .estrela {
        color: #eedf32;
        font-size: 27px;
        padding: 5px;
    }
</style>