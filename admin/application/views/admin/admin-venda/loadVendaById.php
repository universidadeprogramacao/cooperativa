<form method="post" action="<?php echo base_url("admin_venda/salvarEdicao"); ?>" id="formularioVendaEdicao">
<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">Dados da venda</h3>
    </div>
    <div class="panel-body form-horizontal">
        <div class="form-group">
            <div class="btn-group">

                <button type="button" onclick="salvarVendaEdicao();" class="btn btn-success" style="margin-right: 5px;">
                    <span class="glyphicon glyphicon-save"></span>
                    Salvar
                </button>


                <button type="button" onclick="geraPDF();" class="btn btn-info" style="margin-left: 5px;margin-right: 5px;">
                    <span class="glyphicon glyphicon-print"></span>
                    Imprimir
                </button>

                <button type="button" class="btn btn-danger" data-dismiss="modal">
                    <span class="glyphicon glyphicon-eye-close"></span>
                    Fechar
                </button>
            </div>
        </div>

        <div class="form-group">
            <label class="col-xs-8 col-sm-2 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                Código
            </label>
            <div class="col-sm-2">
                <input type="text" name="vendaedicao[id_venda]" id="vendaedicao-id_venda" class="readonly form-control numeric" maxlength="20">
            </div>

            <label class="col-xs-8 col-sm-2 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                Referência:
            </label>
            <div class="col-xs-10 col-sm-6">
                <input type="text" name="vendaedicao[tx_referenciatransacao]" id="vendaedicao-tx_referenciatransacao" class=" form-control" maxlength="210">
            </div>
        </div>
        
        <div class="form-group">
             <label class="col-xs-8 col-sm-2 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                Renavam:
            </label>
            <div class="col-xs-2 col-sm-2">
                <input type="text" disabled="disabled" name="vendaedicao[tx_renavam]" id="vendaedicao-tx_renavam" class=" form-control" maxlength="210">
            </div>
            
        </div>

        <div class="form-group">
            <label class="col-xs-8 col-sm-2 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                Data da venda:
            </label>

            <div class="col-xs-5 col-sm-2">
                <input type="text" name="vendaedicao[dt_venda]" id="vendaedicao-dt_venda" class="desabilitar form-control date validate[custom[date]]">
            </div>

            <label class="col-xs-8 col-sm-2 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                Status Venda:
            </label>
            <div class="col-xs-10 col-sm-3">
                <select name="vendaedicao[st_vendapagseguro]" id="vendaedicao-st_vendapagseguro" class=" form-control">
                    <option value="" title="Selecione">Selecione</option>
                    <?php
                    $statusVendaPagSeguro = $this->utilpagseguro->getStatusVenda('', true);
                    if (!empty($statusVendaPagSeguro)) {
                        foreach ($statusVendaPagSeguro as $key => $val) {
                            ?>
                            <option value="<?php echo $key; ?>" title="<?php echo $val; ?>"><?php echo $val; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>

        </div>

        <div class="form-group">
            <label class="col-xs-8 col-sm-2 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                Valor:
            </label>
            <div class="col-sm-2">
                <div class="input-group">
                    <input type="text" name="vendaedicao[vl_venda]" id="vendaedicao-vl_venda" class="form-control money validate[money]">
                    <span class="input-group-addon">
                        R$
                    </span>
                </div>
            </div>

            <label class="col-xs-8 col-sm-2 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                Valor bruto:
            </label>
            <div class="col-sm-2">
                <div class="input-group">
                    <input type="text" name="vendaedicao[vl_brutotransacaopagseguro]" id="vendaedicao-vl_brutotransacaopagseguro" class="money form-control  validate[money]">
                    <span class="input-group-addon">
                        R$
                    </span>
                </div>
            </div>

            <label class="col-xs-8 col-sm-2 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                Valor líquido:
            </label>
            <div class="col-sm-2">
                <div class="input-group">
                    <input type="text" name="vendaedicao[vl_liquidotransacaopagseguro]" id="vendaedicao-vl_liquidotransacaopagseguro" class="money form-control validate[money]">
                    <span class="input-group-addon">
                        R$
                    </span>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-xs-8 col-sm-2 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                Tipo Pagamento:
            </label>
            <div class="col-xs-10 col-sm-3">
                <select name="vendaedicao[tp_pagamentopagseguro]" id="vendaedicao-tp_pagamentopagseguro" class=" form-control">
                    <option value="" title="Selecione">Selecione</option>
                    <?php
                    $tipoPagamento = $this->utilpagseguro->getTipoPagamento('', true);
                    if (!empty($tipoPagamento)) {
                        foreach ($tipoPagamento as $key => $val) {
                            ?>
                            <option value="<?php echo $key; ?>" title="<?php echo $val; ?>"><?php echo $val; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>

            <label class="col-xs-8 col-sm-2 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                Meio Pagamento:
            </label>
            <div class="col-xs-10 col-sm-5">
                <select name="vendaedicao[tp_meiopagamentopagseguro]" id="vendaedicao-tp_meiopagamentopagseguro" class=" form-control">
                    <option value="" title="Selecione">Selecione</option>
                    <?php
                    $meioPagamento = $this->utilpagseguro->getMeioTipoPagamento('', true);
                    if (!empty($meioPagamento)) {
                        foreach ($meioPagamento as $key => $val) {
                            ?>
                            <option value="<?php echo $key; ?>" title="<?php echo $val; ?>"><?php echo $val; ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-xs-8 col-sm-2 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                Produto postado?
            </label>
            <div class="col-sm-2">
                <label>
                    <input type="radio" name="vendaedicao[st_vendaentregue]" value="S" id="vendaedicao-st_vendaentregue-S" class="st_postado">Sim
                </label>

                <label>
                    <input type="radio" name="vendaedicao[st_vendaentregue]" value="N" id="vendaedicao-st_vendaentregue-N" class="st_postado">Não
                </label>
            </div>

        </div>

        <div class="form-group postagem">
            <label class="col-xs-8 col-sm-2 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                Data postagem:
            </label>
            <div class="col-xs-5 col-sm-2">
                <input type="text" name="vendaedicao[dt_vendapostada]" id="vendaedicao-dt_vendapostada" class="form-control date validate[custom[date]]">
                <div class="help-block">
                    *Data em que o produto foi enviado.
                </div>
            </div>


            <label class="col-xs-8 col-sm-2 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                Cód Rastreamento
            </label>
            <div class="col-xs-9 col-sm-6">
                <input type="text" name="vendaedicao[tx_codigocorreios]" id="vendaedicao-tx_codigocorreios" class="form-control validate" maxlength="30">
                <div class="help-block">
                    *Código fornecido pelos correios.
                </div>
            </div>
        </div>
    </div>
</div>


<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">Dados do usuário</h3>
    </div>
    <div class="panel-body form-horizontal">
        <div class="form-group">

            <label class="col-xs-8 col-sm-2 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                Nome:
            </label>
            <div class="col-xs-10 col-sm-7">
                <input type="text" name="vendaedicao[tx_nome]" id="vendaedicao-tx_nome" class=" form-control" maxlength="60">
                <div class="help-block">
                    *Quem comprou pelo site
                </div>
            </div>

            <label class="col-xs-8 col-sm-1 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                CPF:
            </label>
            <div class="col-xs-10 col-sm-2">
                <input type="text" name="vendaedicao[tx_cpf]" id="vendaedicao-tx_cpf" class=" form-control" maxlength="20">

            </div>

        </div>

        <div class="form-group">
            <label class="col-xs-8 col-sm-2 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                Email:
            </label>

            <div class="col-xs-8 col-sm-5">
                <div class="input-group">
                    <input type="text" name="vendaedicao[tx_email]" id="vendaedicao-tx_email" class=" form-control lower validate" maxlength="60">
                    <span class="input-group-addon">
                        @
                    </span>
                </div>
            </div>


            <label class="col-xs-8 col-sm-2 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                Tel:
            </label>


            <div class="col-xs-8 col-sm-3">
                <div class="input-group">
                    <input type="text" name="vendaedicao[tx_numero]" id="vendaedicao-tx_numero" class="desabilitar form-control validate">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-phone"></span>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">Dados da entrega</h3>
    </div>
    <div class="panel-body form-horizontal">
        <div class="form-group">
            <label class="col-xs-8 col-sm-2 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                Rua:
            </label>
            <div class="col-xs-10 col-sm-4">
                <input type="text" name="vendaedicao[tx_ruaentrega]" id="vendaedicao-tx_ruaentrega" class=" form-control" >
            </div>

            <label class="col-xs-8 col-sm-1 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                Número:
            </label>
            <div class="col-xs-10 col-sm-1">
                <input type="text" name="vendaedicao[tx_numeroruaentrega]" id="vendaedicao-tx_numeroruaentrega" class=" form-control" >
            </div>

            <label class="col-xs-8 col-sm-2 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                Complemento:
            </label>
            <div class="col-xs-10 col-sm-2">
                <input type="text" name="vendaedicao[tx_complementoentrega]" id="vendaedicao-tx_complementoentrega" class=" form-control" >
            </div>
        </div>

        <div class="form-group">
            <label class="col-xs-8 col-sm-2 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                Cidade:
            </label>
            <div class="col-xs-10 col-sm-5">
                <input type="text" name="vendaedicao[tx_cidadeentrega]" id="vendaedicao-tx_cidadeentrega" class=" form-control" >
            </div>

            <label class="col-xs-8 col-sm-2 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                Bairro:
            </label>
            <div class="col-xs-10 col-sm-3">
                <input type="text" name="vendaedicao[tx_bairroentrega]" id="vendaedicao-tx_bairroentrega" class=" form-control" >
            </div>

        </div>

        <div class="form-group">
            <label class="col-xs-8 col-sm-2 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                CEP:
            </label>
            <div class="col-xs-10 col-sm-4">
                <input type="text" name="vendaedicao[tx_cepentrega]" id="vendaedicao-tx_cepentrega" class=" form-control" >
            </div>

            <label class="col-xs-8 col-sm-2 control-label">
                <?php //echo CAMPO_OBRIGATORIO; ?>
                Estado:
            </label>
            <div class="col-xs-10 col-sm-4">
                <input type="text" name="vendaedicao[tx_estadoentrega]" id="vendaedicao-tx_estadoentrega" class=" form-control" >
            </div>
        </div>

    </div>
</div>
</form>


<script>
    var _populateVendaEdicao = <?php echo !empty($populateFormVendaEdicao) ? json_encode($populateFormVendaEdicao) : ''; ?>;
    var aviso = $('#aviso');
    var senha = $('#senha');
    $(document).ready(function () {
        _initMask();
        $('.desabilitar').attr('disabled', true);
        $('.readonly').attr('readonly', true);
        initBtnPageFormulario();
        if(_populateVendaEdicao !== undefined && _populateVendaEdicao !==''){
            $('#formularioVendaEdicao').populate(_populateVendaEdicao);
        }
        
        $('#btnSalvar').click(function () {

            var formulario = $('#validate');

            if (formulario.validationEngine('validate')) {
                salvar(formulario);
            }
        });
    });

    function validarEmail(tx_email) {
        ShowMsgAguarde();
        $.ajax({
            url: _baseUrl + _controller + '/validarEmail',
            type: 'POST',
            dataType: 'json',
            data: {tx_email: tx_email},
            success: function (data) {
                if (data.validou !== undefined && data.validou == 'N') {
                    $('#admin-tx_email').val('');
                }

                if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.error, 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }

        });
    }

    function gerarSenha() {
        ShowMsgAguarde();

        $.ajax({
            url: _baseUrl + _controller + '/gerarSenha',
            type: 'POST',
            dataType: 'json',
            data: {st_indsenhapadrao: st_indsenhapadrao.val()},
            success: function (data) {
                if (data.tx_senha !== undefined && data.tx_senha !== '') {
                    senha.val(data.tx_senha);
                    $('#admin-tx_senha').val(data.tx_senha);
                    st_indsenhapadrao.val('S');
                    aviso.fadeOut(500);
                }
                else if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.error, 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }

        });
    }



    function salvar(formulario) {
        ShowMsgAguarde();
        //formulario.submit();return false;
        formulario.ajaxSubmit({
            success: function (data) {
                data = $.parseJSON(data);

                if (data.success !== undefined && data.success !== '') {
                    if (data.dataGrid.id_admin !== undefined && data.dataGrid.id_admin !== '') {
                        $("#admin-id_admin").val(data.dataGrid.id_admin);
                        if (data.dataGrid.st_indsenhapadrao !== undefined && data.dataGrid.st_indsenhapadrao == 'N') {
                            aviso.show();
                            senha.val('123456');
                        } else {
                            senha.val(data.dataGrid.tx_senha);
                        }
                    }
                    Dialog.success(data.success, 'Sucesso');
                } else if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.erro, 'Erro');
                } else {
                    Dialog.error('Falha ao salvar', 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax, 'Erro');
            },
            complete: function () {
                CloseMsgAguarde();
            }
        });

    }

</script>