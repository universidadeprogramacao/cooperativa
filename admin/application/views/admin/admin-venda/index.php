<div id="page-wrapper">
    <div class="row" style="margin-bottom: 10px;margin-top: 5px;" id="divBotoes"></div>
    <div class="row">
        <div class="col-xs-12 col-sm-22 col-lg-12 col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Pesquisar Venda</h3>
                </div>
                <div class="panel-body">

                    <form method="get" class="form-horizontal populate" action="<?php echo "{$urlPadrao}"; ?>" id="validate">

                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Código
                            </label>
                            <div class="col-sm-2">
                                <input type="text" name="venda[id_venda]" id="venda-id_venda" class="form-control numeric" maxlength="20">
                            </div>

                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Referência:
                            </label>
                            <div class="col-xs-10 col-sm-6">
                                <input type="text" name="venda[tx_referenciatransacao]" id="venda-tx_referenciatransacao" class="form-control" maxlength="210">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Data da venda:
                            </label>

                            <div class="col-xs-5 col-sm-2">
                                <input type="text" name="venda[dt_vendainicio]" id="venda-dt_vendainicio" class="form-control date validate[custom[date]]">
                            </div>
                            <div class="col-sm-1 col-xs-2">
                                A
                            </div>
                            <div class="col-xs-5 col-sm-2">
                                <input type="text" name="venda[dt_vendafim]" id="venda-dt_vendafim" class="form-control date validate[custom[date]]">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Tipo Pagamento:
                            </label>
                            <div class="col-xs-10 col-sm-3">
                                <select name="venda[tp_pagamentopagseguro]" id="venda-tp_pagamentopagseguro" class="form-control">
                                    <option value="" title="Selecione">Selecione</option>
                                    <?php
                                    $tipoPagamento = $this->utilpagseguro->getTipoPagamento('', true);
                                    if (!empty($tipoPagamento)) {
                                        foreach ($tipoPagamento as $key => $val) {
                                            ?>
                                            <option value="<?php echo $key; ?>" title="<?php echo $val; ?>"><?php echo $val; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>

                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Meio Pagamento:
                            </label>
                            <div class="col-xs-10 col-sm-5">
                                <select name="venda[tp_meiopagamentopagseguro]" id="venda-tp_meiopagamentopagseguro" class="form-control">
                                    <option value="" title="Selecione">Selecione</option>
                                    <?php
                                    $meioPagamento = $this->utilpagseguro->getMeioTipoPagamento('', true);
                                    if (!empty($meioPagamento)) {
                                        foreach ($meioPagamento as $key => $val) {
                                            ?>
                                            <option value="<?php echo $key; ?>" title="<?php echo $val; ?>"><?php echo $val; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Status Venda:
                            </label>
                            <div class="col-xs-10 col-sm-3">
                                <select name="venda[st_vendapagseguro]" id="venda-st_vendapagseguro" class="form-control">
                                    <option value="" title="Selecione">Selecione</option>
                                    <?php
                                    $statusVendaPagSeguro = $this->utilpagseguro->getStatusVenda('', true);
                                    if (!empty($statusVendaPagSeguro)) {
                                        foreach ($statusVendaPagSeguro as $key => $val) {
                                            ?>
                                            <option value="<?php echo $key; ?>" title="<?php echo $val; ?>"><?php echo $val; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>


                            <label class="col-xs-8 col-sm-2 control-label">
                                Tipo:
                            </label>

                            <div class="col-sm-5">
                                <label>
                                    <input type="radio" name="venda[tipo_venda]" value="" id="venda-tipo_venda-">Todas
                                </label>
                                <label>
                                    <input type="radio" name="venda[tipo_venda]" value="N" id="venda-tipo_venda-N">Não iniciadas
                                </label>


                                <label>
                                    <input type="radio" name="venda[tipo_venda]" value="S" id="venda-tipo_venda-S">Somente iniciadas
                                </label>
                            </div>


                        </div>


                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Usuário:
                            </label>
                            <div class="col-xs-10 col-sm-10">
                                <input type="text" name="venda[tx_nome]" id="venda-tx_nome" class="form-control" maxlength="60">
                                <div class="help-block">
                                    *Quem comprou pelo site
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Email:
                            </label>

                            <div class="col-xs-8 col-sm-5">
                                <div class="input-group">
                                    <input type="text" name="venda[tx_email]" id="venda-tx_email" class="form-control lower validate" maxlength="60">
                                    <span class="input-group-addon">
                                        @
                                    </span>
                                </div>
                            </div>

                        </div>



                        <div class="form-group">
                            <div class="col-xs-5 col-sm-5 col-sm-offset-1">
                                <button type="button" id="btnConsultar" class="btn btn-info">
                                    <span class="glyphicon glyphicon-search"></span>
                                    Pesquisar
                                </button>
                                <?php 
                                    $parametros   = $_SERVER['QUERY_STRING'];
                                    $UrlAtual     = base_url().'admin_venda?' . $parametros;
                                    //echo $UrlAtual;
                                ?>
                                <!-- <a href="<?php echo $UrlAtual;?>&exportar=1" class="btn btn-success">Exportar</a> -->
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            

            Total de vendas: <strong><?php echo $dataTotal; ?></strong> 
            <br> Total de receita: <strong><?php echo 'R$ '. number_format($dataTotalValor, 2, ',', '.'); ?></strong>
            <br> Total de receita de honorários: <strong><?php $resulH = $dataTotal*130;
            echo 'R$ '. number_format($resulH, 2, ',', '.');  ?></strong>


            <div id="load-venda"></div>

        </div>
    </div>

</div>
<!-- /#page-wrapper -->

<div class="modal fade" id="modalVenda"  style="width: 100% !important;overflow: auto;height:auto;">
    <div class="modal-dialog"  style="width: 100% !important;">
        <div class="modal-content"  style="width: 100% !important;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Detalhamento da venda</h4>
            </div>
            <div class="modal-body" id="corpoVenda"  style="width: 100% !important;">
            </div>
            <div class="modal-footer">


                <button type="button" class="btn btn-danger" data-dismiss="modal">
                    <span class="glyphicon glyphicon-eye-close"></span>
                    Fechar
                </button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<form method="get" id="formularioAcoes"></form>

<script>
    autoCompleteUsuario = <?php echo!empty($autocompleteUsuario) ? json_encode($autocompleteUsuario) : '' ?>;
    $(document).ready(function () {

        $('#btnNovo').click(function () {
            window.location = _urlPadrao + '/formulario';
        });
        $('#venda-tx_nome').autocomplete(autoCompleteUsuario);
        _initButtonDelete();
         $('#btnNovo').hide();
        $('#btnConsultar').click(function () {
            if ($('#validate').validationEngine('validate')) {
                loadVenda();
            }
        });

    });

    function _initModalVenda() {
        $('#modalVenda').modal();
    }
    function closeModalvenda() {
        $('#modalVenda').modal('hide');
    }

    function loadVendaById(id_venda) {
        ShowMsgAguarde();

        $.ajax({
            url: _baseUrl + _controller + '/loadVendaById',
            type: 'POST',
            dataType: 'html',
            data: {id_venda: id_venda},
            success: function (data) {
                $('#corpoVenda').html(data);
                _initModalVenda();
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }
        });
    }

    function loadVenda() {
        ShowMsgAguarde();
        var data = $('#validate').serialize();
        $.ajax({
            url: _baseUrl + _controller + '/loadVenda',
            type: 'POST',
            dataType: 'html',
            data: data,
            success: function (data) {
                $('#load-venda').html(data);
                _initButtonDelete();
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }

        });
    }
    
    function geraPDF(){
        var id_venda = $('#vendaedicao-id_venda').val();
        var url = _baseUrl + _controller + '/pdf/'+id_venda;
        var formulario = $('#formularioAcoes');
        formulario.attr('action',url);
        formulario.attr('target','_blank');
        formulario.submit();
    }
    
    function salvarVendaEdicao(){
        var formulario = $('#formularioVendaEdicao');
        var url = formulario.attr('action');
        var data = formulario.serialize();
        ShowMsgAguarde();
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data:data,
            success: function (data) {
                if (data.success !== undefined && data.success !== '') {
                    Dialog.success(data.success, 'Sucesso');
                }
                else if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.error, 'Erro');
                }
                else {
                    Dialog.error(_erroPadraoAjax, 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }

        }); 
    }

    function excluir(id_param) {
        ShowMsgAguarde();
        $.ajax({
            url: _baseUrl + _controller + '/excluir',
            type: 'POST',
            dataType: 'json',
            data: {id_param: id_param},
            success: function (data) {
                if (data.success !== undefined && data.success !== '') {
                    var linhaCadastro = $('#linhaCadastro-' + id_param);
                    if (linhaCadastro.length > 0) {
                        linhaCadastro.fadeOut(800, function () {
                            $(this).remove();
                        });
                    }
                    Dialog.success(data.success, 'Sucesso');
                }
                else if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.error, 'Erro');
                }
                else {
                    Dialog.error(_erroPadraoAjax, 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }

        });
    }

    function _initButtonDelete() {
        var buttonDelete = $('.buttonDelete');
        buttonDelete.unbind('click');
        buttonDelete.click(function () {
            var id_param = $(this).attr('id_param');
            Dialog.confirm("Deseja realmente excluir esse registro", "Confirma", function () {
                excluir(id_param);
            });
        });
    }

</script>