<?php
$consultaAno = array();

for ($i = 1; $i <= 12; $i++) {
    $valor = rand(5, 800);
    $consultaAno[0]['name'] = "Total do mês";
    $consultaAno[0]['type'] = "column";
    $consultaAno[0]['yAxis'] = 1;
    $consultaAno[0]['data'][$i - 1] = $valor;
    

    $consultaAno[1]['name'] = "Média";
    $consultaAno[1]['type'] = "spline";
    $consultaAno[1]['data'][$i - 1] = $valor;
}
?>
<script>
    $(function () {
      
        $('#grafico-consultas').highcharts({
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: 'Gráfico de consultas efetuadas no portal'
            },
            xAxis: [{
                    categories: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
                        'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                    crosshair: true
                }],
            yAxis: [{// Primary yAxis
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    },
                    title: {
                        text: 'Total',
                        style: {
                            color: Highcharts.getOptions().colors[1]
                        }
                    }
                }, {// Secondary yAxis
                    title: {
                        text: 'Média',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    labels: {
                        format: '{value}',
                        style: {
                            color: Highcharts.getOptions().colors[0]
                        }
                    },
                    opposite: true
                }],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 120,
                verticalAlign: 'top',
                y: 100,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            series:<?php echo json_encode($consultaAno); ?>
        });
    });


</script>