<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="login-panel panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Login</h3>
            </div>
            <div class="panel-body">
                <form role="form" method="post" class="populate form-horizontal" id="validate" action="<?php echo base_url() . "login_usuario/logar"; ?>">
                    <fieldset>
                        <div class="form-group">
                            <label class="col-sm-2 col-xs-5 control-label"><?php echo CAMPO_OBRIGATORIO; ?>Email</label>
                            <div class="col-xs-10 col-sm-8">
                                <div class="input-group">
                                    <input class="form-control validate[required,custom[email]]" id="login-email" name="login[email]" type="text" maxlength="60" autofocus>
                                    <span class="input-group-addon">
                                        <i class="fa fa-user"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-xs-5 control-label"><?php echo CAMPO_OBRIGATORIO; ?>Senha</label>
                            <div class="col-xs-10 col-sm-8">
                                <div class="input-group">
                                    <input class="form-control validate[required]" name="login[senha]" id="login-senha" type="password" maxlength="80">
                                    <span class="input-group-addon">
                                        <span class="glyphicon glyphicon-lock"></span>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div  class="col-sm-4 col-xs-7 col-sm-offset-2">

                                <button style="width: 100%;" type="button" id="logar" class="btn btn-success">
                                    <span class="glyphicon glyphicon-send"></span>
                                    Logar
                                </button>

                            </div>

                            <div class="col-sm-4 col-xs-7 col-sm-offset-0">

                                <button type="button" id="recuperar" class="btn btn-info">
                                    <span class="glyphicon glyphicon-info-sign"></span>
                                    Esqueci minha senha
                                </button>
                            </div>
                        </div>

                        <div class="form-group">
                            <div  class="col-sm-4 col-xs-7 col-sm-offset-2">

                                <button style="width: 100%;" type="button" id="btnNovoCadastro" class="btn btn-primary">
                                    <span class="glyphicon glyphicon-send"></span>
                                    Não sou cadastrado
                                </button>

                            </div>
                        </div>


                    </fieldset>
                </form>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="modelRecuperarSenha" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Recuperação de senha</h4>
            </div>
            <div class="modal-body">
                <form method="post" id="formRecuperarSenha" action="<?php echo base_url() . 'login_usuario/recuperarSenha'; ?>">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Email:</label>
                        <div class="input-group">
                            <input type="text" class="form-control validate[required,custom[email]]" name="email" id="email">
                            <span class="input-group-addon">
                                <i class="fa fa-user"></i>
                            </span>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">

                <button type="button" id="btnEnviar" class="btn btn-success">
                    <span class="glyphicon glyphicon-send"></span>
                    Enviar
                </button>

                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <span class="glyphicon glyphicon-off"></span>
                    Fechar
                </button>
            </div>
        </div>
    </div>
</div>




<div class="modal fade" id="modalUsuario" tabindex="-1" role="dialog" style="height: auto;">
    <div class="modal-dialog" role="document" style="width: 100% !important;">
        <div class="modal-content" style="width: 100% !important;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Cadastro</h4>
            </div>
            <div class="modal-body">
                <form method="post" class="form-horizontal" id="formUsuario" action="<?php echo base_url() . 'login_usuario/salvar'; ?>">

                    <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Meus dados cadastrais</h3>
                            </div>
                            <div class="panel-body">

                                <input type="hidden" name="usuario[id_usuario]" id="usuario-id_usuario">
                                <input type="hidden" name="usuario[st_indsenhapadrao]" value="N" id="usuario-st_indsenhapadrao">
                                <input type="hidden" name="criptografar" value="S">
                                <input type="hidden" name="usuario[tx_ibge]" id="usuario-tx_ibge">
                                <input type="hidden" name="usuario[tx_gia]" id="usuario-tx_gia">

                                <?php
                                if ($this->util->isSenhaPadraoUsuario() !== FALSE) {
                                    ?>
                                    <div class="form-group">
                                        <div class="col-sm-10 col-xs-10">
                                            <div class="alert alert-danger">
                                                Aviso: Você está com a senha padrão gerada pelo sistema, para prosseguir é necessário trocá-la, evite senhas fáceis, como datas de aniversário, telefones, etc.
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label">
                                        <?php echo CAMPO_OBRIGATORIO; ?>
                                        Nome:
                                    </label>
                                    <div class="col-sm-7">
                                        <input type="text" name="usuario[tx_nome]"  id="usuario-tx_nome" class="form-control validate[required]" maxlength="60">
                                    </div>

                                    <label class="col-sm-1 control-label">
                                        <?php echo CAMPO_OBRIGATORIO; ?>
                                        CPF:
                                    </label>
                                    <div class="col-sm-2">
                                        <input type="text" name="usuario[tx_cpf]"  id="usuario-tx_cpf" class="form-control validate[required]" maxlength="20">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-xs-8 col-sm-1 control-label">
                                        <?php echo CAMPO_OBRIGATORIO; ?>
                                        Email:
                                    </label>

                                    <div class="col-xs-8 col-sm-5">
                                        <div class="input-group">
                                            <input type="text" name="usuario[tx_email]"  id="usuario-tx_email" class="form-control lower validate[required,custom[email]]" maxlength="60">
                                            <span class="input-group-addon">
                                                @
                                            </span>
                                        </div>
                                    </div>

                                </div>


                                <div class="form-group">
                                    <label class="col-xs-8 col-sm-1 control-label">
                                        <?php echo CAMPO_OBRIGATORIO; ?>
                                        Senha:
                                    </label>

                                    <div class="col-xs-8 col-sm-5">
                                        <div class="input-group">
                                            <input type="text" name="usuario[tx_senha]" id="usuario-tx_senha" class="form-control validate[required]" maxlength="80">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-lock"></span>
                                            </span>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-xs-8 col-sm-1 control-label">
                                        <?php echo CAMPO_OBRIGATORIO; ?>
                                        DDD:
                                    </label>

                                    <div class="col-sm-1">
                                        <input type="text" name="usuario[tx_ddd]" id="usuario-tx_ddd" class="form-control numeric validate[required]" maxlength="2">
                                    </div>

                                    <label class="col-sm-1 control-label">
                                        <?php echo CAMPO_OBRIGATORIO; ?>
                                        Tel:
                                    </label>

                                    <div class="col-sm-3">
                                        <input type="text" name="usuario[tx_numero]" id="usuario-tx_numero" class="form-control numeric validate[required]" maxlength="20">
                                        <div class="help-block">
                                            *Sem espaços ou acentos
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label">
                                        <?php echo CAMPO_OBRIGATORIO; ?>
                                        CEP:
                                    </label>

                                    <div class="col-sm-4">
                                        <input type="text"  name="usuario[tx_cep]" id="usuario-tx_cep" class="form-control numeric validate[required]" maxlength="8">
                                    </div>

                                    <label class="col-sm-1 control-label">
                                        <?php echo CAMPO_OBRIGATORIO; ?>
                                        Bairro:
                                    </label>

                                    <div class="col-sm-5">
                                        <input type="text" name="usuario[tx_bairro]" id="usuario-tx_bairro" class="form-control validate[required]" maxlength="40">
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label">
                                        <?php echo CAMPO_OBRIGATORIO; ?>
                                        Cidade:
                                    </label>

                                    <div class="col-sm-10">
                                        <input type="text" name="usuario[tx_cidade]" id="usuario-tx_cidade" class="form-control validate[required]" maxlength="40">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-1 control-label">
                                        <?php echo CAMPO_OBRIGATORIO; ?>
                                        Rua:
                                    </label>

                                    <div class="col-sm-7">
                                        <input type="text" name="usuario[tx_rua]" id="usuario-tx_rua" class="form-control validate[required]" maxlength="150">
                                    </div>

                                    <label class="col-sm-1 control-label">
                                        <?php echo CAMPO_OBRIGATORIO; ?>
                                        Número:
                                    </label>

                                    <div class="col-sm-2">
                                        <input type="text" name="usuario[tx_numerorua]" id="usuario-tx_numerorua" class="form-control validate[required]" maxlength="10">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-2 control-label">
                                        <?php echo CAMPO_OBRIGATORIO; ?>
                                        Complemento:
                                    </label>

                                    <div class="col-sm-4">
                                        <input type="text" name="usuario[tx_complemento]" id="usuario-tx_complemento" class="form-control validate[required]" maxlength="20">
                                        <div class="help-block">
                                            Exemplo: Casa / Apartamento xxx
                                        </div>
                                    </div>

                                    <label class="col-sm-2 control-label">
                                        <?php echo CAMPO_OBRIGATORIO; ?>
                                        Estado:
                                    </label>

                                    <div class="col-sm-3">
                                        <select name="usuario[tx_estado]" id="usuario-tx_estado" class="form-control validate[required] ">
                                            <option value="">Selecione</option>
                                            <option value="AC">AC</option>
                                            <option value="AL">AL</option>
                                            <option value="AM">AM</option>
                                            <option value="AP">AP</option>
                                            <option value="BA">BA</option>
                                            <option value="CE">CE</option>
                                            <option value="DF">DF</option>
                                            <option value="ES">ES</option>
                                            <option value="GO">GO</option>
                                            <option value="MA">MA</option>
                                            <option value="MG">MG</option>
                                            <option value="MS">MS</option>
                                            <option value="MT">MT</option>
                                            <option value="PA">PA</option>
                                            <option value="PB">PB</option>
                                            <option value="PE">PE</option>
                                            <option value="PI">PI</option>
                                            <option value="PR">PR</option>
                                            <option value="RJ">RJ</option>
                                            <option value="RN">RN</option>
                                            <option value="RS">RS</option>
                                            <option value="RO">RO</option>
                                            <option value="RR">RR</option>
                                            <option value="SC">SC</option>
                                            <option value="SE">SE</option>
                                            <option value="SP">SP</option>
                                            <option value="TO">TO</option>
                                        </select>
                                    </div>

                                </div>


                                </form>

                            </div>
                        </div>


                    </div>

                </form>
            </div>
            <div class="modal-footer">

                <button type="button" id="btnSalvarUsuario" class="btn btn-success">
                    <span class="glyphicon glyphicon-send"></span>
                    Enviar
                </button>

                <button type="button" class="btn btn-default" data-dismiss="modal">
                    <span class="glyphicon glyphicon-off"></span>
                    Fechar
                </button>
            </div>
        </div>
    </div>

</div>

<script>
    $(function () {

        var formulario = $('#validate');
        var formRecuperarSenha = $('#formRecuperarSenha');
        $("#usuario-tx_cpf").mask('999.999.999-99');
        $('#logar').click(function () {
            if (formulario.validationEngine('validate')) {
                logar(formulario, "<?php echo base_url() . 'painel/home'; ?>");
            }
        });

        $('#btnEnviar').click(function () {
            if (formRecuperarSenha.validationEngine('validate')) {
                recuperarSenha(formRecuperarSenha);
            }
        });

        $('#recuperar').click(function () {
            $('#modelRecuperarSenha').modal();
        });

        $('#btnNovoCadastro').click(function () {
            _initModalCadastrarUsuario();
        });

        $('#btnSalvarUsuario').click(function () {
            var formulario = $('#formUsuario');
            if (formulario.validationEngine('validate')) {
                var cpf = $('#usuario-tx_cpf').val();
                if (!validarCPF(cpf)) {
                    Dialog.error("CPF inválido", 'Erro.');
                }else if($('#usuario-tx_nome').val().length<8){
                    Dialog.error("Informe seu nome completo", 'Erro.'); 
                } else {
                    salvar(formulario);
                }
            }
        });


        $('#usuario-tx_email').blur(function () {
            var tx_email = $.trim($(this).val());
            if (tx_email !== '') {
                validarEmail(tx_email);
            }
        });

        $('#usuario-tx_cep').blur(function () {
            var cep = $(this).val();
            buscarCep(cep);
        });

    });

    function _initModalCadastrarUsuario() {
        $('#modalUsuario').modal();
    }
    function _closeModalCadastrarUsuario() {
        $('#modalUsuario').modal('hide');
    }

    function recuperarSenha(formulario) {

        var urlRequest = formulario.attr('action');
        var data = formulario.serialize();
        ShowMsgAguarde();
        $.ajax({
            url: urlRequest,
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (data) {
                if (data !== undefined && data.success !== undefined && data.success !== '') {
                    Dialog.success(data.success, 'Sucesso.');
                }
                else if (data !== undefined && data.error !== undefined && data.error !== '') {
                    Dialog.error(data.error, 'Erro');
                } else {
                    Dialog.error(_erroPadraoAjax, 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax, 'Erro');
            },
            complete: function () {
                CloseMsgAguarde();
            }
        });

    }


    function salvar(formulario) {

        var urlRequest = formulario.attr('action');
        var data = formulario.serialize();
        ShowMsgAguarde();
        $.ajax({
            url: urlRequest,
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (data) {
                if (data !== undefined && data.success !== undefined && data.success !== '') {
                    _closeModalCadastrarUsuario();
                    Dialog.success(data.success, 'Sucesso.',function (){
                        var email = $('#usuario-tx_email').val();
                        var senha = $('#usuario-tx_senha').val();
                        $('#login-email').val(email);
                        $('#login-senha').val(senha);
                        $('#logar').click();
                    });
                }
                else if (data !== undefined && data.error !== undefined && data.error !== '') {
                    Dialog.error(data.error, 'Erro');
                } else {
                    Dialog.error(_erroPadraoAjax, 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax, 'Erro');
            },
            complete: function () {
                CloseMsgAguarde();
            }
        });

    }

    function validarEmail(tx_email) {
        ShowMsgAguarde();
        $.ajax({
            url: _baseUrl + _controller + '/validarEmail',
            type: 'POST',
            dataType: 'json',
            data: {tx_email: tx_email},
            success: function (data) {
                if (data.validou !== undefined && data.validou == 'N') {
                    $('#usuario-tx_email').val('');
                }

                if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.error, 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }

        });
    }

    function buscarCep(cep) {
        ShowMsgAguarde();
        $.ajax({
            url: _baseUrl + _controller + '/buscarCep',
            type: 'POST',
            dataType: 'json',
            data: {cep: cep},
            success: function (data) {
                if (data.cep == 'error') {
                    //Dialog.error("Cep não encontrado", 'Erro.');
                } else {
                    var cep = data.cep;
                    $("#usuario-tx_rua").val(cep.logradouro);
                    $("#usuario-tx_bairro").val(cep.bairro);
                    $("#usuario-tx_cidade").val(cep.localidade);
                    $("#usuario-tx_complemento").val(cep.complemento);
                    $("#usuario-tx_gia").val(cep.gia);
                    $("#usuario-tx_ibge").val(cep.ibge);
                    $('#usuario-tx_estado option[value="' + cep.uf + '"]').attr('selected', 'selected');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }

        });
    }

</script>