<div id="page-wrapper">
    <div class="row" style="margin-bottom: 10px;margin-top: 5px;" id="divBotoes"></div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Página inicial</h3>
                </div>
                <div class="panel-body">
                    <div class="alert alert-success">
                        <?php echo !empty($_SESSION['admin']['tx_nome']) ? "{$_SESSION['admin']['tx_nome']}," : ""; ?> bem vindo(a) a área administrativa do portal.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>