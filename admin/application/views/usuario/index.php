<div id="page-wrapper">
    <div class="row" style="margin-bottom: 10px;margin-top: 5px;" id="divBotoes"></div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Pesquisar </h3>
                </div>
                <div class="panel-body">
                    
                    <form method="post" class="form-horizontal populate" action="<?php echo "{$urlPadrao}"; ?>" id="validate">
                        <div class="form-group">
                            <label class="col-xs-8 col-sm-1 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Nome:
                            </label>
                            <div class="col-xs-10 col-sm-8">
                                <input type="text" name="admin[tx_nome]" id="admin-tx_nome" class="form-control" maxlength="60">
                            </div>
                        </div>

                       <div class="form-group">
                            <label class="col-xs-8 col-sm-1 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Email:
                            </label>

                            <div class="col-xs-8 col-sm-5">
                                <div class="input-group">
                                    <input type="text" name="admin[tx_email]" id="admin-tx_email" class="form-control lower validate" maxlength="60">
                                    <span class="input-group-addon">
                                        @
                                    </span>
                                </div>
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="col-xs-8 col-sm-1 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Status:
                            </label>
                            <div class="col-xs-7 col-sm-6">
                                <span class="radio radio-inline">
                                    <label>
                                        <input type="radio" name="admin[st_status]" id="admin-st_status-T" value="">Todos
                                    </label>
                                </span>

                                <span class="radio radio-inline">
                                    <label>
                                        <input type="radio" name="admin[st_status]" id="admin-st_status-A" value="A">Cadastro Ativo
                                    </label>
                                </span>

                                <span class="radio radio-inline">
                                    <label>
                                        <input type="radio" name="admin[st_status]" id="admin-st_status-I" value="I">Cadastro Inativo
                                    </label>
                                </span>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-xs-8 col-sm-2 control-label">
                                <?php //echo CAMPO_OBRIGATORIO; ?>
                                Data de cadastro:
                            </label>

                            <div class="col-xs-5 col-sm-2">
                                <input type="text" name="admin[dt_cadastroinicio]" id="admin-dt_cadastroinicio" class="form-control date validate[custom[date]]">
                            </div>
                            <div class="col-sm-1 col-xs-2">
                                A
                            </div>
                            <div class="col-xs-5 col-sm-2">
                                <input type="text" name="admin[dt_cadastrofim]" id="admin-dt_cadastrofim" class="form-control date validate[custom[date]]">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-5 col-sm-5">
                                <button type="submit" class="btn btn-info">
                                    <span class="glyphicon glyphicon-search"></span>
                                    Pesquisar
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>


            <table class="table table-bordered table-striped" id="dataTable">
                <thead>
                    <tr>
                        <th style="width: 10%;">&nbsp;</th>
                        <th style="width: 30%;">Nome</th>
                        <th style="width: 30%;">Email</th>
                        <th style="width: 15%;">Data de cadastro</th>
                        <th style="width: 15%;">Status</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    if (!empty($dataGrid)) {
                        foreach ($dataGrid as $resultado) {
                            ?>
                    <tr id="linhaCadastro-<?php echo $resultado['id_usuario']; ?>">
                                <td>
                                    <div class="btn-group">
                                        <a href="<?php echo "{$urlPadrao}/formulario/{$resultado['id_usuario']}/{$this->util->gerarSlug($resultado['tx_nome'])}"; ?>"  title="Editar" style="margin: 3px;" class="btn btn-xs btn-info" >
                                            <span class="glyphicon glyphicon-edit"></span>
                                        </a>

                                        <button id_param="<?php echo $resultado['id_usuario']; ?>" title="Excluir" type="button" style="margin: 3px;" class="btn btn-xs btn-danger buttonDelete">
                                            <span class="glyphicon glyphicon-remove"></span>
                                        </button>

                                    </div>
                                </td>

                                <td><?php echo $resultado['tx_nome']; ?></td>
                                <td><?php echo $resultado['tx_email']; ?></td>
                                <td><?php echo $this->util->reverseDate($resultado['dt_cadastro']); ?></td>
                                <td><?php echo $this->util->getStatus($resultado['st_status']); ?></td>
                            </tr>
                        <?php }
                    } else { ?>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                             <td> <div class="alert alert-warning">
                                    Nenhum registro encontrado
                                </div>
                             </td>
                              <td>&nbsp;</td>
                               <td>&nbsp;</td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>

</div>
<!-- /#page-wrapper -->

<script>
    $(document).ready(function () {

        initBtnPageFormulario();
        $('#btnNovo').click(function () {
            window.location = _urlPadrao + '/formulario';
        });
        
        $('.buttonDelete').click(function (){
            var id_param = $(this).attr('id_param');
            Dialog.confirm("Deseja realmente excluir esse registro","Confirma",function (){
                excluir(id_param);
            });
        });

    });

    function excluir(id_param) {
        ShowMsgAguarde();
        $.ajax({
            url: _baseUrl + _controller + '/excluir',
            type: 'POST',
            dataType: 'json',
            data: {id_param: id_param},
            success: function (data) {
                if (data.success !== undefined && data.success !=='') {
                    var linhaCadastro = $('#linhaCadastro-'+id_param);
                    if(linhaCadastro.length>0){
                        linhaCadastro.fadeOut(800,function (){
                            $(this).remove();
                        });
                    }
                    Dialog.success(data.success,'Sucesso');
                }
                else if (data.error !== undefined && data.error !== '') {
                    Dialog.error(data.error, 'Erro');
                }
                else {
                    Dialog.error(_erroPadraoAjax, 'Erro');
                }
            },
            error: function () {
                Dialog.error(_erroPadraoAjax);
            },
            complete: function () {
                CloseMsgAguarde();
            }

        });
    }

</script>