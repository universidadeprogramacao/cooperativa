<?php

/**
 * Description of Util
 *
 * @author eric
 */
class Util {

    protected $valor;

    public function getStVendaEntregue($st_vendaentregue = '', $combo = false) {
        $arr = array(
            "" => "",
            "S" => "SIM",
            "N" => "NÃO"
        );

        if ($combo) {
            return $arr;
        }

        if (!empty($arr[$st_vendaentregue])) {
            return $arr[$st_vendaentregue];
        }
        return '';
    }

    public function moneyToFloat($vl) {
        $vl = str_replace('.', '', $vl);
        return str_replace(',', '.', $vl);
    }

    public function floatToMoneyV1($vl, $decimals = 2) {
        return number_format($vl, $decimals, ',', '.');
    }

    public function converteValorAutomatico($valor) {
        $converte = false;
        if (strpos($valor, ",") !== false) {
            $search = ",";
            $replace = ".";
            $converte = true;
        } else if (strpos($valor, ".") !== false) {
            $search = ".";
            $replace = ",";
            $converte = true;
        }

        if ($converte) {
            $valor = str_replace($search, $replace, $valor);
        }
        return $valor;
    }

    public function parseIntegerToDecimal($numero, $separator = ',') {
        $len = strlen($numero);
        $inicio = substr($numero, 0, $len - 2);
        $fim = substr($numero, -2, 2);
        $number = "{$inicio}{$separator}{$fim}";
        return $number;
    }

    public function debug($val) {
        echo '<pre>';
        print_r($val);
        echo '</pre>';
        echo '<hr/><br/>';
    }

    public function reverseDate($date) {
        $arrDate = explode(' ', $date);

        $date = $arrDate[0];
        $time = @$arrDate[1];

        if (stripos($date, '/') !== false) {
            return trim(implode('-', array_reverse(explode('/', $date))) . ' ' . $time);
        } else {
            return trim(implode('/', array_reverse(explode('-', $date))) . ' ' . $time);
        }
    }

    public function getStatus($valor) {
        $status = array("A" => "Ativo", "I" => "Inativo", "" => "Todos");
        if (isset($status[$valor])) {
            return $status[$valor];
        }
        return $valor;
    }

    public function getAprovado($valor) {
        $status = array("A" => "Aprovado", "N" => "Aguardando Aprovação", "I" => "Não Aprovado", "" => "Todos");
        if (isset($status[$valor])) {
            return $status[$valor];
        }
        return $valor;
    }

    public function getFormaPagamento($tx_tipopagamento = null) {
        $pagamentos = array("B" => "Boleto", "C" => "Cartão de crétido", "T" => "Transfêrencia Bancária");
        if (isset($pagamentos[$tx_tipopagamento])) {
            return $pagamentos[$tx_tipopagamento];
        }
        return $pagamentos;
    }

    public function floatToMoney($valor, $numeroCasas = 2) {

        $valor = (floatval($valor));
        $delimitador = ".";
        if (strpos($valor, ",") !== false) {
            $delimitador = ".";
        } else if (strpos($valor, ".") !== false) {
            $delimitador = ",";
        }
        $this->valor = number_format($valor, $numeroCasas, $delimitador);
        return $this->valor;
    }

    public function gerarSenha($tamanho = 9, $forca = 0) {
        $vogais = 'aeuy';
        $consoantes = 'bdghjmnpqrstvz';
        if ($forca >= 1) {
            $consoantes .= 'BDGHJLMNPQRSTVWXZ';
        }
        if ($forca >= 2) {
            $vogais .= "AEUY";
        }
        if ($forca >= 4) {
            $consoantes .= '23456789';
        }
        if ($forca >= 8) {
            $vogais .= '@#$%';
        }

        $senha = '';
        $alt = time() % 2;
        for ($i = 0; $i < $tamanho; $i++) {
            if ($alt == 1) {
                $senha .= $consoantes[(rand() % strlen($consoantes))];
                $alt = 0;
            } else {
                $senha .= $vogais[(rand() % strlen($vogais))];
                $alt = 1;
            }
        }
        return $senha;
    }

    public function removerAcentos($string) {
        $procurar = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ');
        $substituir = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y');
        return str_replace($procurar, $substituir, $string);
    }

//função que gera um slug com base no título
    public function gerarSlug($string) {
        $string = $this->removerAcentos($string);
        return url_title($string, '-', TRUE);
    }
    
    public function isAdmin(){
        if (!empty($_SESSION['admin']['id_produtor'])) {
            return false;
        }
        if (!empty($_SESSION['admin']['id_admin'])) {
            return true;
        } 
        return false;
    }

    public function getIdUsuario() {

        if (!empty($_SESSION['admin']['id_produtor'])) {
            return $_SESSION['admin']['id_produtor'];
        }
        if (!empty($_SESSION['admin']['id_admin'])) {
            return $_SESSION['admin']['id_admin'];
        }
        return null;
    }

    public function getIdUser() {
        return !empty($_SESSION['usuario']['id_usuario']) ? $_SESSION['usuario']['id_usuario'] : null;
    }

    public function isLogado($action = 'todos') {
        #$this->debug($_SESSION);
        if (empty($_SESSION['admin'])) {
            $url = base_url("login");
            redirect($url);
        }

        if ($_SESSION['admin']['st_indsenhapadrao'] == 'S') {
            if ($action !== 'cadastro') {
                $url = base_url("Admin/cadastro");
                redirect($url);
            }
        }
        return true;
    }

    public function isLogadoUsuario($action = 'todos') {

        if (empty($_SESSION['usuario'])) {
            $url = base_url("login_usuario");
            redirect($url);
        }

        if ($_SESSION['usuario']['st_indsenhapadrao'] == 'S') {
            if ($action !== 'cadastro') {
                $url = base_url("Painel/cadastro");
                redirect($url);
            }
        }
        return true;
    }

    public function isSenhaPadrao() {
        if (empty($_SESSION['admin'])) {
            return FALSE;
        }
        if ($_SESSION['admin']['st_indsenhapadrao'] == 'S') {
            return TRUE;
        }
        return FALSE;
    }

    public function isSenhaPadraoUsuario() {
        if (empty($_SESSION['usuario'])) {
            return FALSE;
        }
        if ($_SESSION['usuario']['st_indsenhapadrao'] == 'S') {
            return TRUE;
        }
        return FALSE;
    }

    public function validarNoivos() {
        if (empty($_SESSION['noivos'])) {
            $baseUrl = base_url("noivados");
            redirect($baseUrl);
        }
    }

    public function getIdNoivo() {
        if (!empty($_SESSION['noivos']['id_noivo'])) {
            return $_SESSION['noivos']['id_noivo'];
        }
        return null;
    }

    public function getDataExtenso($data) {
        date_default_timezone_set('America/Sao_Paulo');
        setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        return utf8_encode(strftime('%A, %d de %B de %Y', strtotime(date($data))));
    }

    public function getDataComboMes() {
        $arr[''] = "Todos";
        $arr[1] = "Janeiro";
        $arr[2] = "Fevereiro";
        $arr[3] = "Março";
        $arr[4] = "Abril";
        $arr[5] = "Maio";
        $arr[6] = "Junho";
        $arr[7] = "Julho";
        $arr[8] = "Agosto";
        $arr[9] = "Setembro";
        $arr[10] = "Outubro";
        $arr[11] = "Novembro";
        $arr[12] = "Dezembro";
        return $arr;
    }

    /**
     * 
     * @param type $data1 no formatao AAAA-MM-DD
     * @param type $data2 no formatao AAAA-MM-DD
     */
    public function getDiferencaDatas($data1, $data2) {
        // Usa a função strtotime() e pega o timestamp das duas datas:
        $tempoInicial = strtotime($data1);
        $tempoFinal = strtotime($data2);

        // Calcula a diferença de segundos entre as duas datas:
        $diferenca = $tempoFinal - $tempoInicial; // 19522800 segundos
        // Calcula a diferença de dias
        $dias = (int) round($diferenca / (60 * 60 * 24));
        return $dias;
    }

    public function isLocalHost() {
        if ($_SERVER['HTTP_HOST'] == 'localhost') {
            return true;
        }
        return false;
    }

    public function enviaEmail($params) {



        $params['tx_emailfrom'] = TX_EMAILDESTINOFROM;

        // Aqui vamos configurar o cabeçalho (header) do e-mail, formatos, remetentes, destinatários de cópias
        $headers = "MIME-Version: 1.0\r\n";
        // Aqui definirmos o formato padrão HTML e a codificação do Texto
        $headers.= "Content-type: text/html; charset=utf-8\r\n";

        // Abaixo estabelecemos o Remetente do E-mail com o From:
        $headers.= "From: {$params['tx_emailfrom']}\r\n";
        // Caso se queira especificar o e-mail de Resposta, utilizamos o Reply-To:, caso você não queira, basta excluir a linha abaixo
        $headers.= "Reply-To: {$params['tx_emailfrom']}\r\n";
        // Se desejar enviar cópia para outro e-mail utiliza-se o Bcc:, especificando o e-mail de destino, se não quiser mandar essa cópia, basta remover a linha abaixo
        //$headers.= "Bcc: roberto_carlos@hotmail.com\r\n";
        // Nesta linha abaixo, configuramos o e-mail do destinatário, caso queira especificar mais de 1 destinatário, basta colocar uma virgula (,) e o outro e-mail, ex: fulano@gmail.com, fulano2@gmail.com
        $destinatario = "{$params['tx_emaildestinatario']}";
        // Definimos o assuntos do nosso e-mail
        $assunto = "{$params['tx_assunto']}";

        // Abaixo, vamos colocar o corpo da mensagem, lembrando que como definimos que será em padrão HTML acima no cabeçalho, você terá de utilizar tags HTML abaixo, veja o exemplo
        $mensagem = $params['tx_mensagem'];

        $body = "<html>
                <head>
                <title>{$assunto}</title>
                    <meta charset='utf-8'>
                </head>
                <body>
                  {$mensagem}
                </body>
                </html>
                ";

// Definido todos os parametros, agora podemos proceder com o envio, conforme a linha abaixo, lembre-se sempre de verificar se todos os parâmetros estão corretamente especificados
        return mail($destinatario, $assunto, $body, $headers);
    }

}
