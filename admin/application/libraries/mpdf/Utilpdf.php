<?php


/**
 * Description of utilpdf
 *
 * @author eric
 */
class Utilpdf {
   public function mpdf(array $pdf, $conf = array()) {

        /**
          conversion HTML => PDF */
        //include_once("mpdf/mpdf.php");
        include_once 'mpdf.php';

        if (!empty($conf['orientation']) && !empty($conf['format'])) {
            if ($conf['orientation'] == 'L') {
                $conf['format'] = $conf['format'] . '-L';
            }
        }

        $mpdf = new mPDF(
                        $conf['mode'] ? $conf['mode'] : 'c',
                        $conf['format'] ? $conf['format'] : 'A4',
                        $conf['font_size'] ? $conf['font_size'] : '',
                        $conf['font'] ? $conf['font'] : '',
                        $conf['m_left'] ? $conf['m_left'] : '',
                        $conf['m_right'] ? $conf['m_right'] : '',
                        $conf['m_top'] ? $conf['m_top'] : '',
                        $conf['m_bottom'] ? $conf['m_bottom'] : '',
                        $conf['m_header'] ? $conf['m_header'] : '',
                        $conf['m_footer'] ? $conf['m_footer'] : '',
                        $conf['orientation'] ? $conf['orientation'] : 'P'
        );

        if (isset($pdf['header'])) {
            $mpdf->SetHTMLHeader($pdf['header']);
        }
        if (isset($pdf['footer'])) {
            $mpdf->SetHTMLFooter($pdf['footer']);
        }

        if (isset($pdf['title'])) {
            $mpdf->SetTitle($pdf['title']);
        }

        $mpdf->WriteHTML($pdf['content']);

        if(empty($pdf['name'])){
            $pdf['name'] = null;
        }
        
        $mpdf->Output($pdf['name'],'I');

        exit;
    }
}
