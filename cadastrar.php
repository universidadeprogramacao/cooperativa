<?php
session_start();
require_once './model/BaseDb.php';
require_once './model/Tbproduto.php';
?>

<!DOCTYPE html>
<!--[if lte IE 8]> <html class="oldie" lang="en"> <![endif]-->
<!--[if IE 9]> <html class="ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="pt-br"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="format-detection" content="telephone=no">
        <title>Carrinho</title>
        <link rel="stylesheet" href="css/fancySelect.css" />
        <link rel="stylesheet" href="css/uniform.css" />
        <link rel="stylesheet" href="css/all.css" />
        <link media="screen" rel="stylesheet" type="text/css" href="css/screen.css" />
        <!--[if lt IE 9]>
                <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/jquery.bxslider.min.js"></script>
        <script type="text/javascript" src="js/jquery.placeholder.js"></script>
        <script type="text/javascript" src="/jquery.uniform.min.js"></script>
        <script type="text/javascript" src="js/fancySelect.js"></script>
        <script type="text/javascript" src="js/main.js"></script>

    </head>
    <body>
        <div id="wrapper">
            <div class="wrapper-holder">
                <?php
                include 'includes/cabecalhoTodasPaginas.php';
                ?>
                <section class="bar">
                    <div class="bar-frame">
                        <ul class="breadcrumbs">
                            <li><a href="index.php">Home</a></li>
                            <li>Cadastre-se</li>
                        </ul>
                    </div>
                </section>
                <section id="main">


                    <div class="row">
                        <?php
                        if (!empty($_GET['acao'])) {
                            if ($_GET['acao'] == 'error') {
                                ?>
                                <div class="col-sm-12 col-xs-12">
                                    <div class="alert alert-danger">
                                        Ocorreu um erro ao se cadastrar, tente novamente
                                    </div>
                                </div>


                                <?php
                            } else if ($_GET['acao'] == 'success') {
                                ?>

                                <div class="col-sm-12 col-xs-12">
                                    <div class="alert alert-success">
                                        Cadastrado com sucesso, você sera redirecionado para entrar no sistema em instantes.
                                    </div>
                                </div>

                                <script>
                                    window.location = "finanalizar-compra.php";
                                </script>


                                <?php
                            }
                            ?>

                            <?php
                        }
                        ?>

                        <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Cadastro de Clientes</h3>
                                </div>
                                <div class="panel-body">

                                    <form method="post" class="form-horizontal populate" action="admin/ajax/cadastrarUsuario" id="validate">

                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a data-toggle="tab" href="#cliente">Cliente</a></li>
                                            <li>
                                                <a data-toggle="tab" href="#telefone">Telefone</a>
                                            </li>

                                        </ul>

                                        <div class="tab-content">
                                            <div id="cliente" class="tab-pane fade in active">
                                                <?php include_once 'includes/tab-usuario.php'; ?>
                                            </div>
                                            <div id="telefone" class="tab-pane fade">
                                                <?php include_once 'includes/tab-telefone.php'; ?>
                                            </div>

                                        </div>


                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <button id="btnSalvar" title="Enviar Cadastrao" type="submit" class="btn btn-success">
                                                    <span class="glyphicon glyphicon-save"></span>
                                                    Enviar Cadastrao
                                                </button>
                                            </div>
                                        </div>


                                    </form>
                                </div>

                            </div>


                        </div>

                </section>
            </div>
            <?php
            include 'includes/RodapesTodasAsPaginas.html';
            ?>
        </div>       
    </body>
</html>

<script>
    $(function () {
        /**
         $("#btnSalvar").click(function (){
         var formulario = $("#validate");
         if(formulario.validationEngine("validate")){
         alert("Validou");
         }else{
         alert("Informe os campos obrigatórios");
         }
         });
         **/
    });

</script>