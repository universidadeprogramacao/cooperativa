<?php
#session_start();
if (empty($_SESSION['total'])) {
    $_SESSION['total']['qtd'] = 0;
    $_SESSION['total']['valor'] = 0;
}
require_once 'model/BaseDb.php';
define('CAMPO_OBRIGATORIO', '<span style="color:#961500;font-family:Verdana;font-size:11px;font-weight:700;" title="" class="required" data-original-title="Campo obrigatório!">*</span>');
define('ERRO_PADRAO_AJAX', "Ocorreu um erro ao executar sua solicitação, tente novamente, caso persista contate a administração do sistema");

?>

<link rel="stylesheet" href="http://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" type="text/css">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="admin/public/library/javascripts/validateEngine/jquery.validationEngine.js"></script>
<script src="http://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>


<header id="header">
    <span>
        <a href="index.php"><h2>SGCR</h2></a>
    </span>
    <ul class="tools-nav tools-nav-mobile">
        <li class="items"><a href="carrinho.php"><span><?php echo $_SESSION['total']['qtd']; ?></span> Itens, <strong>R$<?php echo BaseDB::floatToMoneyStatic($_SESSION['total']['valor']); ?></strong></a></li>
        <li class="login"><a href="finanalizar-compra.php">Entrar</a> / <a href="cadastrar.php">Cadastro</a></li>
    </ul>
    <a class="menu_trigger" href="#">menu</a>
    <div class="bar-holder">
        <nav id="nav">
            <ul>
                <li><a href="produtos.php">Produtos</a></li>
                <!--
                <li><a href="produtos.php">Asters</a></li>
                <li><a href="produtos.php">Crisantemo</a></li>
                <li><a href="produtos.php">Outras</a></li>
                -->
            </ul>
        </nav>
        <ul class="tools-nav">
            <li class="items"><a href="carrinho.php"><span><?php echo $_SESSION['total']['qtd']; ?></span> Itens, 
                    <strong>R$<?php echo BaseDB::floatToMoneyStatic($_SESSION['total']['valor']); ?></strong></a></li>
                    <li class="login"><a href="finanalizar-compra.php">Entrar</a> / <a href="cadastrar.php">Cadastro</a></li>
        </ul>
    </div>
</header>
