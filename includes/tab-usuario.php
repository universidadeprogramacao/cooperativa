<input type="hidden" name="admin[id_usuario]" id="admin-id_usuario">
<input type="hidden" name="admin[st_indsenhapadrao]" id="admin-st_indsenhapadrao" value="S">
<input type="hidden" name="admin[st_status]" id="admin-st_status" value="A">

<br/>
<div class="form-group">
    <label class="col-xs-8 col-sm-2 control-label">
        <?php echo CAMPO_OBRIGATORIO; ?>
        Data de cadastro:
    </label>

    <div class="col-xs-5 col-sm-2">

        <input required="required" readonly="readonly" type="text" value="<?php echo date("d/m/Y"); ?>" name="admin[dt_cadastro]" id="admin-dt_cadastro" class="form-control date validate[required,custom[date]]">

    </div>

</div>


<div class="form-group">
    <label class="col-sm-1 control-label">
        <?php echo CAMPO_OBRIGATORIO; ?>
        Nome:
    </label>
    <div class="col-sm-6">
        <input required="required" type="text" name="admin[tx_nome]"  id="admin-tx_nome" class="form-control validate[required]" maxlength="60">
    </div>

    <label class="col-sm-2 control-label">
        <?php echo CAMPO_OBRIGATORIO; ?>
        CPF Ou CNPJ:
    </label>
    <div class="col-sm-2">
        <input required="required" type="text" name="admin[tx_cpf]"  id="admin-tx_cpf" class="form-control validate[required]" maxlength="30">
    </div>
</div>



<div class="form-group">
    <label class="col-sm-1 control-label">
        <?php echo CAMPO_OBRIGATORIO; ?>
        Sexo:
    </label>
    <div class="col-sm-4">
        <span class="radio radio-inline">
            <label>
                <input required="required" type="radio" name="admin[tx_sexo]"  id="admin-tx_sexo-M" value="M">Masculino
            </label>
        </span>

        <span class="radio radio-inline">
            <label>
                <input required="required" type="radio" name="admin[tx_sexo]" id="admin-tx_sexo-F" value="F">Feminino
            </label>
        </span>
    </div>

    <label class="col-sm-1 control-label">
        <?php echo CAMPO_OBRIGATORIO; ?>
        Tipo:
    </label>
    <div class="col-sm-4">
        <span class="radio radio-inline">
            <label>
                <input required="required" type="radio" name="admin[tx_tipopessoa]"  id="admin-tx_tipopessoa-F" value="F">Pessoa Física
            </label>
        </span>

        <span class="radio radio-inline">
            <label>
                <input required="required" type="radio" name="admin[tx_tipopessoa]" id="admin-tx_tipopessoa-J" value="J">Pessoa Jurídica
            </label>
        </span>
    </div>
</div>


<div class="form-group">
    <label class="col-xs-8 col-sm-1 control-label">
        <?php echo CAMPO_OBRIGATORIO; ?>
        Email:
    </label>

    <div class="col-xs-8 col-sm-5">
        <div class="input-group">
            <input required="required" type="text" name="admin[tx_email]"  id="admin-tx_email" class="form-control lower validate[required,custom[email]]" maxlength="60">
            <span class="input-group-addon">
                @
            </span>
        </div>
    </div>

</div>


<div class="form-group">
    <label class="col-xs-8 col-sm-1 control-label">
        <?php echo CAMPO_OBRIGATORIO; ?>
        DDD:
    </label>

    <div class="col-sm-1">
        <input required="required" type="text" name="admin[tx_ddd]" id="admin-tx_ddd" class="form-control numeric validate[required]" maxlength="2">
    </div>

    <label class="col-sm-1 control-label">
        <?php echo CAMPO_OBRIGATORIO; ?>
        Tel:
    </label>

    <div class="col-sm-3">
        <input required="required" type="text" name="admin[tx_numero]" id="admin-tx_numero" class="form-control numeric validate[required]" maxlength="20">
        <div class="help-block">
            *Sem espaços ou acentos
        </div>
    </div>

</div>

<div class="form-group">
    <label class="col-sm-1 control-label">
        <?php echo CAMPO_OBRIGATORIO; ?>
        CEP:
    </label>

    <div class="col-sm-4">
        <input required="required" type="text"  name="admin[tx_cep]" id="admin-tx_cep" class="form-control numeric validate[required]" maxlength="8">
    </div>

    <label class="col-sm-1 control-label">
        <?php echo CAMPO_OBRIGATORIO; ?>
        Bairro:
    </label>

    <div class="col-sm-5">
        <input required="required" type="text" name="admin[tx_bairro]" id="admin-tx_bairro" class="form-control validate[required]" maxlength="40">
    </div>

</div>

<div class="form-group">
    <label class="col-sm-1 control-label">
        <?php echo CAMPO_OBRIGATORIO; ?>
        Cidade:
    </label>

    <div class="col-sm-10">
        <input required="required" type="text" name="admin[tx_cidade]" id="admin-tx_cidade" class="form-control validate[required]" maxlength="40">
    </div>
</div>

<div class="form-group">
    <label class="col-sm-1 control-label">
        <?php echo CAMPO_OBRIGATORIO; ?>
        Rua:
    </label>

    <div class="col-sm-7">
        <input required="required" type="text" name="admin[tx_rua]" id="admin-tx_rua" class="form-control validate[required]" maxlength="150">
    </div>

    <label class="col-sm-1 control-label">
        <?php echo CAMPO_OBRIGATORIO; ?>
        Número:
    </label>

    <div class="col-sm-2">
        <input required="required" type="text" name="admin[tx_numerorua]" id="admin-tx_numerorua" class="form-control validate[required]" maxlength="10">
    </div>
</div>


<div class="form-group">
    <label class="col-sm-2 control-label">
        <?php echo CAMPO_OBRIGATORIO; ?>
        Complemento:
    </label>

    <div class="col-sm-4">
        <input required="required" type="text" name="admin[tx_complemento]" id="admin-tx_complemento" class="form-control validate[required]" maxlength="20">
        <div class="help-block">
            Exemplo: Casa / Apartamento xxx
        </div>
    </div>

    <label class="col-sm-2 control-label">
        <?php echo CAMPO_OBRIGATORIO; ?>
        Estado:
    </label>

    <div class="col-sm-3">
        <select required="required" name="admin[tx_estado]" id="admin-tx_estado" class="form-control validate[required] ">
            <option value="">Selecione</option>
            <option value="AC">AC</option>
            <option value="AL">AL</option>
            <option value="AM">AM</option>
            <option value="AP">AP</option>
            <option value="BA">BA</option>
            <option value="CE">CE</option>
            <option value="DF">DF</option>
            <option value="ES">ES</option>
            <option value="GO">GO</option>
            <option value="MA">MA</option>
            <option value="MG">MG</option>
            <option value="MS">MS</option>
            <option value="MT">MT</option>
            <option value="PA">PA</option>
            <option value="PB">PB</option>
            <option value="PE">PE</option>
            <option value="PI">PI</option>
            <option value="PR">PR</option>
            <option value="RJ">RJ</option>
            <option value="RN">RN</option>
            <option value="RS">RS</option>
            <option value="RO">RO</option>
            <option value="RR">RR</option>
            <option value="SC">SC</option>
            <option value="SE">SE</option>
            <option value="SP">SP</option>
            <option value="TO">TO</option>
        </select>
    </div>

</div>



<div class="form-group">
    <label class="col-sm-1 col-xs-5 control-label">
        <?php echo CAMPO_OBRIGATORIO; ?> Senha:
    </label>
    <div class="col-sm-4 col-xs-8">
        <div class="input-group">
            <input required="required" type="text" name="admin[tx_senha]"  id="admin-tx_senha" class="form-control validate[required]">
            <span class="input-group-addon">
                <span class="glyphicon glyphicon-lock"></span>
            </span>
        </div>
    </div>
</div>

