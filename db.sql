-- MySQL dump 10.13  Distrib 5.6.31, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: db_cooperativa
-- ------------------------------------------------------
-- Server version	5.6.31-0ubuntu0.15.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbadmin`
--

DROP TABLE IF EXISTS `tbadmin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbadmin` (
  `id_admin` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_nome` varchar(60) NOT NULL,
  `tx_email` varchar(60) NOT NULL,
  `st_status` char(1) NOT NULL DEFAULT 'A',
  `dt_cadastro` date NOT NULL,
  `tx_senha` varchar(80) NOT NULL,
  `st_indsenhapadrao` char(1) NOT NULL DEFAULT 'S',
  PRIMARY KEY (`id_admin`),
  UNIQUE KEY `tx_email_UNIQUE` (`tx_email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbadmin`
--

LOCK TABLES `tbadmin` WRITE;
/*!40000 ALTER TABLE `tbadmin` DISABLE KEYS */;
INSERT INTO `tbadmin` VALUES (1,'ERIC FERRAZ','eric.ferras@fatec.sp.gov.br','A','2016-11-26','$2a$08$MTc3MjY3OTEyMDU4MzliNu41aNO7SlOzQuuYQ6VL8K0V8apux5ipO','N'),(2,'bruno','bruno@gmail.com','A','2016-11-29','uNeLugUBe9APADE','S');
/*!40000 ALTER TABLE `tbadmin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbdepoimento`
--

DROP TABLE IF EXISTS `tbdepoimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbdepoimento` (
  `id_depoimento` int(11) NOT NULL AUTO_INCREMENT,
  `tx_nome` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tx_cidade` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tx_estado` varchar(2) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tx_email` varchar(60) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `tx_avaliacaoservico` int(1) NOT NULL,
  `tx_depoimento` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `dt_depoimento` date NOT NULL,
  `st_aprovado` char(1) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT 'N',
  `tx_avatar` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id_depoimento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbdepoimento`
--

LOCK TABLES `tbdepoimento` WRITE;
/*!40000 ALTER TABLE `tbdepoimento` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbdepoimento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbmovimentacaoestoque`
--

DROP TABLE IF EXISTS `tbmovimentacaoestoque`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbmovimentacaoestoque` (
  `id_movimentacaoestoque` bigint(20) NOT NULL AUTO_INCREMENT,
  `nr_qtdmovimentada` int(11) NOT NULL,
  `tp_movimentacao` char(1) NOT NULL DEFAULT 'E',
  `id_produto` bigint(20) NOT NULL,
  `dt_movimentacao` date NOT NULL,
  PRIMARY KEY (`id_movimentacaoestoque`),
  KEY `id_produto` (`id_produto`),
  CONSTRAINT `tbmovimentacaoestoque_ibfk_1` FOREIGN KEY (`id_produto`) REFERENCES `tbproduto` (`id_produto`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbmovimentacaoestoque`
--

LOCK TABLES `tbmovimentacaoestoque` WRITE;
/*!40000 ALTER TABLE `tbmovimentacaoestoque` DISABLE KEYS */;
INSERT INTO `tbmovimentacaoestoque` VALUES (1,14,'E',2,'2016-11-29'),(2,14,'E',2,'2016-11-29'),(3,10,'E',3,'2016-11-29'),(4,10,'E',3,'2016-11-29'),(5,1,'E',2,'2016-11-29');
/*!40000 ALTER TABLE `tbmovimentacaoestoque` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbpedido`
--

DROP TABLE IF EXISTS `tbpedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbpedido` (
  `id_pedido` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_usuario` bigint(20) NOT NULL,
  `dt_pedido` date NOT NULL,
  `dt_fechamento` date NOT NULL,
  `st_pedido` char(1) NOT NULL DEFAULT 'A' COMMENT 'A = ATIVO \nP = PENDENTE DE ENTREGA\nC = CANCELADO\nF = FINALIZADO / ENTREGUE',
  `dt_entrega` date DEFAULT NULL,
  `dt_pagamento` date DEFAULT NULL,
  `st_pagamento` char(1) NOT NULL DEFAULT 'A' COMMENT 'A = ATIVO\nP = PAGO\nC = CANCELADO',
  `vl_pago` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`id_pedido`),
  KEY `id_usuario` (`id_usuario`),
  CONSTRAINT `tbpedido_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `tbusuario` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbpedido`
--

LOCK TABLES `tbpedido` WRITE;
/*!40000 ALTER TABLE `tbpedido` DISABLE KEYS */;
INSERT INTO `tbpedido` VALUES (1,2,'2016-11-28','2016-11-29','A',NULL,NULL,'A',NULL),(2,2,'2016-11-28','2016-11-29','A',NULL,NULL,'A',NULL),(3,2,'2016-11-28','2016-11-29','A',NULL,NULL,'A',NULL),(4,2,'2016-11-28','2016-11-29','A',NULL,NULL,'A',NULL),(5,2,'2016-11-28','2016-11-29','A',NULL,NULL,'A',NULL),(6,2,'2016-11-28','2016-11-29','A',NULL,NULL,'A',NULL),(7,2,'2016-11-28','2016-11-29','A',NULL,NULL,'A',NULL),(8,2,'2016-11-28','2016-11-29','A',NULL,NULL,'A',NULL),(9,2,'2016-11-28','2016-11-29','A',NULL,NULL,'A',NULL),(10,2,'2016-11-28','2016-11-29','A',NULL,NULL,'A',NULL),(11,2,'2016-11-28','2016-11-29','A',NULL,NULL,'A',NULL),(12,2,'2016-11-29','2016-11-30','A',NULL,NULL,'A',NULL);
/*!40000 ALTER TABLE `tbpedido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbpedidocotacaofornecimento`
--

DROP TABLE IF EXISTS `tbpedidocotacaofornecimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbpedidocotacaofornecimento` (
  `id_pedidocotacaofornecimento` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_produtor` bigint(20) NOT NULL,
  `id_produto` bigint(20) NOT NULL,
  `sub_totaloriginal` decimal(12,2) NOT NULL,
  `nr_qtdoriginal` int(11) NOT NULL COMMENT 'a quantidade original do pedido',
  `dt_proposta` date NOT NULL,
  `st_fornecido` char(1) NOT NULL COMMENT 'S = SIM FOI FORNECIDO\nN = NÃO FOI FORNECIDO NADA',
  `nr_qtdfornecida` int(11) NOT NULL DEFAULT '0' COMMENT 'se foi fornecido, qual a quantidade real que foi feita durante a divisão',
  `dt_fechamentooriginal` date NOT NULL,
  `nr_qtdofertada` int(11) NOT NULL DEFAULT '0' COMMENT 'Quantos produtos que o produtor colocou que pode oferecer',
  `vl_unitariooriginal` decimal(12,2) NOT NULL DEFAULT '0.00',
  `st_pagoprodutor` char(1) NOT NULL DEFAULT 'N' COMMENT 'Se o produtor entregou o pedido e recebeu',
  `dt_pago` date DEFAULT NULL COMMENT 'Se o produtor foi pago, vai contar a data de pagamento',
  PRIMARY KEY (`id_pedidocotacaofornecimento`),
  KEY `id_produtor` (`id_produtor`),
  KEY `id_produto` (`id_produto`),
  CONSTRAINT `tbpedidocotacaofornecimento_ibfk_1` FOREIGN KEY (`id_produtor`) REFERENCES `tbprodutor` (`id_produtor`),
  CONSTRAINT `tbpedidocotacaofornecimento_ibfk_2` FOREIGN KEY (`id_produto`) REFERENCES `tbproduto` (`id_produto`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbpedidocotacaofornecimento`
--

LOCK TABLES `tbpedidocotacaofornecimento` WRITE;
/*!40000 ALTER TABLE `tbpedidocotacaofornecimento` DISABLE KEYS */;
INSERT INTO `tbpedidocotacaofornecimento` VALUES (1,1,2,54.00,27,'2016-11-29','S',14,'2016-11-29',3,2.00,'N',NULL),(2,1,3,38.00,19,'2016-11-29','S',10,'2016-11-29',16,2.00,'N',NULL),(3,1,2,2.00,1,'2016-11-29','S',1,'2016-11-30',1,2.00,'N',NULL),(4,2,2,54.00,27,'2016-11-29','S',14,'2016-11-29',3,2.00,'N',NULL),(5,2,3,38.00,19,'2016-11-29','S',10,'2016-11-29',4,2.00,'N',NULL),(6,2,2,2.00,1,'2016-11-29','',0,'2016-11-30',1,2.00,'N',NULL);
/*!40000 ALTER TABLE `tbpedidocotacaofornecimento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbpedidoitem`
--

DROP TABLE IF EXISTS `tbpedidoitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbpedidoitem` (
  `id_pedidoitem` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_pedido` bigint(20) NOT NULL,
  `id_produto` bigint(20) NOT NULL,
  `vl_unitario` decimal(12,2) NOT NULL,
  `nr_quantidade` int(11) NOT NULL,
  `vl_subtotal` decimal(12,2) NOT NULL,
  PRIMARY KEY (`id_pedidoitem`),
  KEY `id_pedido` (`id_pedido`),
  KEY `id_produto` (`id_produto`),
  CONSTRAINT `tbpedidoitem_ibfk_1` FOREIGN KEY (`id_pedido`) REFERENCES `tbpedido` (`id_pedido`),
  CONSTRAINT `tbpedidoitem_ibfk_2` FOREIGN KEY (`id_produto`) REFERENCES `tbproduto` (`id_produto`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbpedidoitem`
--

LOCK TABLES `tbpedidoitem` WRITE;
/*!40000 ALTER TABLE `tbpedidoitem` DISABLE KEYS */;
INSERT INTO `tbpedidoitem` VALUES (1,1,2,2.00,3,6.00),(2,1,3,2.00,3,6.00),(3,2,2,2.00,3,6.00),(4,2,3,2.00,3,6.00),(5,3,2,2.00,3,6.00),(6,3,3,2.00,3,6.00),(7,4,2,2.00,3,6.00),(8,4,3,2.00,3,6.00),(9,5,2,2.00,3,6.00),(10,5,3,2.00,3,6.00),(11,6,2,2.00,3,6.00),(12,6,3,2.00,3,6.00),(13,7,2,2.00,5,10.00),(14,8,2,2.00,1,2.00),(15,8,3,2.00,1,2.00),(16,9,2,2.00,1,2.00),(17,10,2,2.00,1,2.00),(18,11,2,2.00,1,2.00),(19,12,2,2.00,1,2.00);
/*!40000 ALTER TABLE `tbpedidoitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbproduto`
--

DROP TABLE IF EXISTS `tbproduto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbproduto` (
  `id_produto` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_produto` varchar(60) NOT NULL,
  `tx_descricao` varchar(255) NOT NULL,
  `tx_foto` varchar(80) DEFAULT NULL,
  `vl_unitario` decimal(12,2) NOT NULL,
  PRIMARY KEY (`id_produto`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbproduto`
--

LOCK TABLES `tbproduto` WRITE;
/*!40000 ALTER TABLE `tbproduto` DISABLE KEYS */;
INSERT INTO `tbproduto` VALUES (2,'rosa vermelha','rosa vermelha promocional','b69f1038c6d545737bb019ff97efc0fe.jpg',2.00),(3,'rosa azul','rosa azul promocional','9d8b5da19b81fcfbaf0b212c46fd2696.jpg',2.00);
/*!40000 ALTER TABLE `tbproduto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbprodutor`
--

DROP TABLE IF EXISTS `tbprodutor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbprodutor` (
  `id_produtor` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_nome` varchar(60) NOT NULL,
  `tx_email` varchar(60) NOT NULL,
  `st_status` char(1) NOT NULL DEFAULT 'A',
  `dt_cadastro` date NOT NULL,
  `tx_senha` varchar(80) NOT NULL,
  `st_indsenhapadrao` char(1) NOT NULL DEFAULT 'S',
  `tx_ddd` varchar(2) DEFAULT NULL,
  `tx_numero` varchar(20) DEFAULT NULL,
  `tx_cpf` varchar(30) DEFAULT NULL,
  `tx_cep` varchar(8) DEFAULT NULL,
  `tx_rua` varchar(150) DEFAULT NULL,
  `tx_complemento` varchar(20) DEFAULT NULL,
  `tx_estado` varchar(2) DEFAULT NULL,
  `tx_pais` varchar(3) NOT NULL DEFAULT 'BRA',
  `tx_numerorua` varchar(10) DEFAULT NULL,
  `tx_bairro` varchar(40) DEFAULT NULL,
  `tx_cidade` varchar(40) DEFAULT NULL,
  `tx_ibge` varchar(40) DEFAULT NULL,
  `tx_gia` varchar(40) DEFAULT NULL,
  `tx_tipopessoa` char(1) NOT NULL DEFAULT 'F',
  `tx_sexo` char(1) NOT NULL DEFAULT 'M',
  PRIMARY KEY (`id_produtor`),
  UNIQUE KEY `tx_email_UNIQUE` (`tx_email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbprodutor`
--

LOCK TABLES `tbprodutor` WRITE;
/*!40000 ALTER TABLE `tbprodutor` DISABLE KEYS */;
INSERT INTO `tbprodutor` VALUES (1,'Junior arantes neto','mario.arantes@gmail.com','A','2016-11-26','$2a$08$NTU1MjYwNDE0NTgzY2ZmM.xncir0bFaN2vHDKRa7HTBgN5Vg.zvo6','N','14','332344','32342-43','19911300','rua jacaranda','casa 3','PR','BRA','123','centro','curitiba',NULL,NULL,'J','M'),(2,'eric ferraz','andre_tmi@hotmail.com','A','2016-11-29','$2a$08$MzQwNTI2MzM3NTgzZGFlMuIlxsM8AaZLWQ3XMh.fP2lAIVWxnWxMq','N','14','332332','43322','111111','blanck','casa','SP','BRA','422','centro','ourinhos',NULL,NULL,'F','M');
/*!40000 ALTER TABLE `tbprodutor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbtelefone`
--

DROP TABLE IF EXISTS `tbtelefone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbtelefone` (
  `id_telefone` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_usuario` bigint(20) NOT NULL,
  `tx_telefone` varchar(45) NOT NULL,
  `tx_tipo` varchar(10) NOT NULL,
  PRIMARY KEY (`id_telefone`),
  KEY `id_usuario` (`id_usuario`),
  CONSTRAINT `tbtelefone_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `tbusuario` (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbtelefone`
--

LOCK TABLES `tbtelefone` WRITE;
/*!40000 ALTER TABLE `tbtelefone` DISABLE KEYS */;
INSERT INTO `tbtelefone` VALUES (2,1,'(14)3244-3432','FIXO'),(3,1,'(14)98892-4242','CELULAR'),(4,2,'142332244','FIXO'),(5,3,'33244242','FIXO');
/*!40000 ALTER TABLE `tbtelefone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbtelefoneprodutor`
--

DROP TABLE IF EXISTS `tbtelefoneprodutor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbtelefoneprodutor` (
  `id_telefoneprodutor` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_produtor` bigint(20) NOT NULL,
  `tx_telefone` varchar(45) NOT NULL,
  `tx_tipo` varchar(10) NOT NULL,
  PRIMARY KEY (`id_telefoneprodutor`),
  KEY `id_produtor` (`id_produtor`),
  CONSTRAINT `tbtelefoneprodutor_ibfk_1` FOREIGN KEY (`id_produtor`) REFERENCES `tbprodutor` (`id_produtor`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbtelefoneprodutor`
--

LOCK TABLES `tbtelefoneprodutor` WRITE;
/*!40000 ALTER TABLE `tbtelefoneprodutor` DISABLE KEYS */;
INSERT INTO `tbtelefoneprodutor` VALUES (1,1,'9 8783-4322','CELULAR');
/*!40000 ALTER TABLE `tbtelefoneprodutor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbusuario`
--

DROP TABLE IF EXISTS `tbusuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbusuario` (
  `id_usuario` bigint(20) NOT NULL AUTO_INCREMENT,
  `tx_nome` varchar(60) NOT NULL,
  `tx_email` varchar(60) NOT NULL,
  `st_status` char(1) NOT NULL DEFAULT 'A',
  `dt_cadastro` date NOT NULL,
  `tx_senha` varchar(80) NOT NULL,
  `st_indsenhapadrao` char(1) NOT NULL DEFAULT 'S',
  `tx_ddd` varchar(2) DEFAULT NULL,
  `tx_numero` varchar(20) DEFAULT NULL,
  `tx_cpf` varchar(30) DEFAULT NULL,
  `tx_cep` varchar(8) DEFAULT NULL,
  `tx_rua` varchar(150) DEFAULT NULL,
  `tx_complemento` varchar(20) DEFAULT NULL,
  `tx_estado` varchar(2) DEFAULT NULL,
  `tx_pais` varchar(3) NOT NULL DEFAULT 'BRA',
  `tx_numerorua` varchar(10) DEFAULT NULL,
  `tx_bairro` varchar(40) DEFAULT NULL,
  `tx_cidade` varchar(40) DEFAULT NULL,
  `tx_ibge` varchar(40) DEFAULT NULL,
  `tx_gia` varchar(40) DEFAULT NULL,
  `tx_tipopessoa` char(1) NOT NULL DEFAULT 'F',
  `tx_sexo` char(1) NOT NULL DEFAULT 'M',
  PRIMARY KEY (`id_usuario`),
  UNIQUE KEY `tx_email_UNIQUE` (`tx_email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbusuario`
--

LOCK TABLES `tbusuario` WRITE;
/*!40000 ALTER TABLE `tbusuario` DISABLE KEYS */;
INSERT INTO `tbusuario` VALUES (1,'MARIO FERRAZ','mario.ferraz@gmail.com','A','2016-11-26','$2a$08$NTMzMTk1NDA0NTgzOWMyZeonvXpV39xZzR7dl8h71Ns76dYmLFguW','N','14','332443242','061002202-32','1991232','carambada ','casa 5','','BRA','42','centro','ourinhos',NULL,NULL,'J','M'),(2,'eric ferraz','ciencias_exatas@hotmail.com.br','A','2016-11-28','123456','S','14','32242','0623422','1998823','rua de teste','casa','SP','BRA','422','centro','ourinhos',NULL,NULL,'F','M'),(3,'sss','teste@gmail.com','I','2016-11-28','$2a$08$MjEyNzUzNzg5NzU4M2NmZOY/shdwysaQ5Yz9AGBEXi26fSI1Wgn.a','N','ss','xss','ssss','ss','ss','ass','SP','BRA','sss','ss','aaa',NULL,NULL,'F','M');
/*!40000 ALTER TABLE `tbusuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-29 16:38:44
